Sammoa readme
=============

Requirements
------------

- Sun jre 1.8


Installation
------------

Just unzip archive.


Launch
------

Launch sammoa with following scripts:

  sammoa.sh (linux 32 bits)
  sammoa.bat (windows 32 bits)
  sammoa64.sh (linux 64 bits)
  sammoa64.bat (windows 64 bits)

or command lines :

  java -Djava.library.path=x86 -jar sammoa.jar (32bits)
  java -Djava.library.path=x64 -jar sammoa.jar (64bits)
