package fr.ulr.sammoa.ui.swing.region;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.RegionImpl;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Created: 23/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class RegionUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_REGION_REFERENTIAL = "regionReferential";

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_CODE = "code";

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_UPDATE = "update";

    private static final long serialVersionUID = 1L;

    protected List<Region> regionReferential;

    protected String id;

    protected String code;

    protected String name;

    protected boolean update;

    /**
     * Prepare create using {@code regionReferential} to check unicity
     *
     * @param regionReferential List of Region as referential
     */
    public void prepareCreate(List<Region> regionReferential) {
        setId(null);
        setCode(null);
        setName(null);
        setRegionReferential(regionReferential);
    }

    /**
     * Prepare update base on {@code region} bean values
     *
     * @param region Bean to initialize the model
     */
    public void prepareUpdate(Region region) {
        setId(region.getTopiaId());
        setCode(region.getCode());
        setName(region.getName());
        setRegionReferential(null);
    }

    /**
     * Instanciate a new {@link Region} bean with values from this model.
     * Note that the {@code code} property is mandatory (check in validation)
     * otherwise an exception will occurs.
     *
     * @return a new {@link Region} bean
     */
    public Region newBean() {
        Region result = new RegionImpl(getCode());
        result.setTopiaId(getId());
        result.setName(getName());
        return result;
    }

    public List<Region> getRegionReferential() {
        if (regionReferential == null) {
            regionReferential = Lists.newArrayList();
        }
        return regionReferential;
    }

    public void setRegionReferential(List<Region> regionReferential) {
        List<Region> oldValue = Lists.newArrayList(getRegionReferential());
        this.regionReferential = regionReferential;
        firePropertyChange(PROPERTY_REGION_REFERENTIAL, oldValue, regionReferential);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);

        setUpdate(id != null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        String oldValue = getCode();
        this.code = code;
        firePropertyChange(PROPERTY_CODE, oldValue, code);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        boolean oldValue = isUpdate();
        this.update = update;
        firePropertyChange(PROPERTY_UPDATE, oldValue, update);
    }
}
