/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.bar.onBoard;

import fr.ulr.sammoa.application.device.gps.GpsLocationEvent;
import fr.ulr.sammoa.application.device.gps.GpsLocationListener;
import fr.ulr.sammoa.persistence.GeoPoint;
import org.jdesktop.beans.AbstractSerializableBean;

import java.awt.Color;

import static org.nuiton.i18n.I18n.t;

/** @author sletellier <letellier@codelutin.com> */
public class OnBoardBarModel extends AbstractSerializableBean implements GpsLocationListener {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_LATITUDE = "latitude";

    public static final String PROPERTY_LONGITUDE = "longitude";

    public static final String PROPERTY_ALT = "alt";

    public static final String PROPERTY_SPEED = "speed";

    public static final String PROPERTY_EFFORT_PANEL_COLOR = "effortPanelColor";

    protected float latitude;

    protected float longitude;

    protected float alt;

    protected float speed;

    protected Color effortPanelColor;

    public String getLatitude() {
        return t("sammoa.statusbar.latitude", latitude);
    }

    public void setLatitude(float latitude) {
        float oldValue = this.latitude;
        this.latitude = latitude;
        firePropertyChange(PROPERTY_LATITUDE, oldValue, latitude);
    }

    public String getLongitude() {
        return t("sammoa.statusbar.longitude", longitude);
    }

    public void setLongitude(float longitude) {
        float oldValue = this.longitude;
        this.longitude = longitude;
        firePropertyChange(PROPERTY_LONGITUDE, oldValue, longitude);
    }

    public String getAlt() {
        return t("sammoa.statusbar.alt", alt);
    }

    public void setAlt(float alt) {
        float oldValue = this.alt;
        this.alt = alt;
        firePropertyChange(PROPERTY_ALT, oldValue, alt);
    }

    public String getSpeed() {
        return t("sammoa.statusbar.speed", speed);
    }

    public void setSpeed(float speed) {
        float oldValue = this.speed;
        this.speed = speed;
        firePropertyChange(PROPERTY_SPEED, oldValue, speed);
    }

    public Color getEffortPanelColor() {
        return effortPanelColor;
    }

    public void setEffortPanelColor(Color effortPanelColor) {
        Color oldColor = this.effortPanelColor;
        this.effortPanelColor = effortPanelColor;
        firePropertyChange(PROPERTY_EFFORT_PANEL_COLOR, oldColor, this.effortPanelColor);
    }

    @Override
    public void locationChanged(GpsLocationEvent event) {
        GeoPoint newLocation = event.getNewValue();
        if (newLocation != null) {
            setLatitude((float) newLocation.getLatitude());
            setLongitude((float) newLocation.getLongitude());
            setSpeed((float) newLocation.getSpeed());
            setAlt((float) newLocation.getAltitude());
        }
    }
}
