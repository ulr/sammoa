/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.action;

import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.nuiton.util.Resource;

import javax.swing.Action;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 05/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class StopAction extends BaseFlightAction {

    private static final long serialVersionUID = 1L;

    public StopAction(JAXXContext context) {
        super(Resource.getIcon("/icons/playback_stop.png"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.stop.tip"));
        bindModelProperty(FlightUIModel.PROPERTY_FLIGHT_STATE, false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (SammoaUtil.askQuestion((FlightUI) context, t("sammoa.confirmDialog.stopFlight.message"))) {
            getFlightController().stop();
        }
    }

    @Override
    protected boolean checkEnabled() {
        return getModel().getFlight().getEndDate() == null
               && getModel().getFlightState() == FlightState.OFF_EFFORT;
    }
}
