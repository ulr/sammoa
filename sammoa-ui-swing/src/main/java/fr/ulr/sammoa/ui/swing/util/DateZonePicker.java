package fr.ulr.sammoa.ui.swing.util;

/*-
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.swing.JAXXDatePicker;

import java.util.Date;
import java.util.TimeZone;

public class DateZonePicker extends JAXXDatePicker {

    private static final long serialVersionUID = -1835397006047448618L;

    protected TimeZone timeZoneOffset;

    public TimeZone getTimeZoneOffset() {
        return timeZoneOffset;
    }

    protected Date dateReference;

    public void setTimeZoneOffset(TimeZone timeZoneOffset) {
        Date dateZone = getDateZone();
        this.timeZoneOffset = timeZoneOffset;
        setDateZone(dateZone);
    }

    @Override
    public void setDate(Date date) {
        Date old = getDateZone();
        super.setDate(date);
        firePropertyChange("dateZone", old, getDateZone());
    }

    public Date getDateZone() {
        Date date = updateDate(getDate(), true);
        return date;
    }

    public void setDateZone(Date dateZone) {
        this.dateReference = dateZone;
        Date date = updateDate(dateZone, false);
        setDate(date);
    }

    protected Date updateDate(Date oldDate, boolean inverse) {
        Date date = oldDate;
        if (oldDate != null && timeZoneOffset != null) {
            long time = this.dateReference.getTime();
            int offsetTimeZone = timeZoneOffset.getOffset(time);
            int offsetSystem = TimeZone.getDefault().getOffset(time);
            int offset = offsetTimeZone - offsetSystem;
            if (inverse) {
                offset = offset * -1;
            }
            date = new Date(oldDate.getTime() + offset);
        }
        return date;
    }
}
