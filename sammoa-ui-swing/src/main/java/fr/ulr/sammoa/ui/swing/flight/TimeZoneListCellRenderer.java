package fr.ulr.sammoa.ui.swing.flight;

/*-
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;
import java.util.TimeZone;

public class TimeZoneListCellRenderer extends DefaultListCellRenderer {
    private static final long serialVersionUID = -8568760710239904227L;

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        String label = null;
        if (value != null) {
            TimeZone timeZone = TimeZone.class.cast(value);
            label = timeZone.getID();
        }
        return super.getListCellRendererComponent(list, label, index, isSelected, cellHasFocus);
    }
}
