package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created: 17/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TextCellEditor extends DefaultCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    public TextCellEditor() {
        this(new JTextField());
    }

    public TextCellEditor(JTextField textField) {
        super(textField);
        setClickCountToStart(1);
        getComponent().addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent event) {
                autoSelectText();
            }

            @Override
            public void focusLost(FocusEvent event) {
                // nothing to do
            }
        });
        getComponent().addAncestorListener(new AncestorListener() {

            @Override
            public void ancestorAdded(AncestorEvent event) {
                autoSelectText();
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                // nothing to do
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
                // nothing to do
            }
        });
    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    protected void autoSelectText() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                getComponent().requestFocus();
                getComponent().selectAll();
            }
        });
    }
}
