package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.Component;
import java.util.Objects;

/**
 * This class is useful to avoid calling isHighlighted for each cell. This will
 * call {@link #isHighlighted(B)} once for each row.
 * <p/>
 * Created: 16/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public abstract class AbstractRowHighlightPredicate<B> implements HighlightPredicate {

    protected B lastObject;

    protected boolean lastValue;

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {

        int rowIndex = adapter.convertRowIndexToModel(adapter.row);
        B object = getValueAt(rowIndex);

        if (!Objects.equals(lastObject, object)) {
            lastObject = object;
            lastValue = isHighlighted(object);
        }
        return lastValue;
    }

    protected abstract boolean isHighlighted(B object);

    protected abstract B getValueAt(int rowIndex);
}
