/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing;

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.application.SammoaService;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import org.nuiton.widget.SwingSession;

import java.io.Closeable;
import java.io.IOException;

/** @author sletellier <letellier@codelutin.com> */
public class SammoaUIContext implements Closeable {

    private static SammoaUIContext uiContext;

    protected SammoaContext context;

    protected MainUIHandler mainUIHandler;

    protected SwingSession swingSession;

    protected String surveyId;

    protected String flightId;

    public static SammoaUIContext newUIContext(SammoaContext context) {
        Preconditions.checkNotNull(context);
        uiContext = new SammoaUIContext(context);
        return uiContext;
    }

    public static SammoaUIContext getUIContext() {
        Preconditions.checkNotNull(uiContext,
                                   "You must create the context with the method " +
                                   "#newUIContext(SammoaContext) before retrieving it");
        return uiContext;
    }

    protected SammoaUIContext(SammoaContext context) {
        this.context = context;
    }

    public SammoaContext getAppContext() {
        return context;
    }

    public <S extends SammoaService> S getService(Class<S> serviceType) {
        return context.getService(serviceType);
    }

    public <S extends SammoaService> void closeService(S service) throws IOException {
        context.closeService(service);
    }

    public SammoaConfig getConfig() {
        return context.getConfig();
    }

    public MainUIHandler getMainUIHandler() {
        return mainUIHandler;
    }

    public void setMainUIHandler(MainUIHandler mainUIHandler) {
        this.mainUIHandler = mainUIHandler;
    }

    public SwingSession getSwingSession() {
        return swingSession;
    }

    public void setSwingSession(SwingSession swingSession) {
        this.swingSession = swingSession;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void changeScreen(SammoaScreen newScreen) {
        changeScreen(newScreen, null, null);
    }

    public void changeScreen(SammoaScreen newScreen,
                             Survey survey,
                             Flight flight) {

        Preconditions.checkNotNull(mainUIHandler);

        if (survey != null) {
            this.surveyId = survey.getTopiaId();
        }
        if (flight != null) {
            this.flightId = flight.getTopiaId();
            this.surveyId = flight.getSurvey().getTopiaId();
        }
        mainUIHandler.setScreen(newScreen);
    }

    public void setStatusMessage(String message) {

        Preconditions.checkNotNull(mainUIHandler);

        mainUIHandler.getUI().getStatus().setStatus(message);
    }

    public boolean isValidationMode() {
        return mainUIHandler.getUI().getScreen() == SammoaScreen.VALIDATION;
    }

    public boolean isFlightMode() {
        return mainUIHandler.getUI().getScreen() == SammoaScreen.FLIGHT;
    }

    public void open() {
        context.open();
    }

    @Override
    public void close() throws IOException {

        // Clear data references
        mainUIHandler = null;
        surveyId = null;
        flightId = null;

        if (context != null) {
            context.close();
        };
    }
}
