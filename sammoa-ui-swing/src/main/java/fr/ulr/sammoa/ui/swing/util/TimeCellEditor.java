package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.util.Calendar;
import java.util.Date;

/**
 * Created: 17/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TimeCellEditor extends TextCellEditor implements TableCellEditor {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TimeCellEditor.class);

    private static final long serialVersionUID = 1L;

    protected DateZonePicker datePicker;

    protected Calendar referenceCalendar;

    public TimeCellEditor(DateZonePicker datePicker) {
        super(datePicker.getEditor());
        this.datePicker = datePicker;
        this.referenceCalendar = Calendar.getInstance();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        Date date = (Date) value;
        referenceCalendar.setTime(date);

        if (log.isDebugEnabled()) {
            log.debug(String.format("Set editor date: %1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS", date));
        }

        datePicker.setDateZone(date);
        return datePicker;
    }

    @Override
    public Object getCellEditorValue() {

        Calendar editCalendar = Calendar.getInstance();
        editCalendar.setTime(datePicker.getDateZone());
        editCalendar.set(Calendar.DATE, referenceCalendar.get(Calendar.DATE));
        editCalendar.set(Calendar.MONTH, referenceCalendar.get(Calendar.MONTH));
        editCalendar.set(Calendar.YEAR, referenceCalendar.get(Calendar.YEAR));

        if (log.isDebugEnabled()) {
            log.debug(String.format("Get editor date: %1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS", editCalendar.getTime()));
        }

        return editCalendar.getTime();
    }
}
