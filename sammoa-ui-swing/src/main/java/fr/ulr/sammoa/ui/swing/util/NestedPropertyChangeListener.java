package fr.ulr.sammoa.ui.swing.util;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.event.ListenableBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;

/**
 * This listener is used to manage nested properties. (Ex: route.deleted will
 * listen on property route and on the property deleted). After instanciation,
 * use {@link #init(Object)}  method to bind the listener with your own {@code model}.
 * Only model {@link TopiaEntity} or {@link AbstractBean} are supported.
 * <p/>
 * Created: 07/09/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class NestedPropertyChangeListener implements PropertyChangeListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(NestedPropertyChangeListener.class);

    protected PropertyChangeListener delegate;

    protected String property;

    protected NestedPropertyChangeListener embeddedListener;

    public NestedPropertyChangeListener(PropertyChangeListener delegate,
                                        String property,
                                        boolean all) {

        this.delegate = delegate;

        if (log.isDebugEnabled()) {
            log.debug("New NestedPropertyChangeListener for property '" + property + "'");
        }

        // Init embeddedListener for nestedProperty
        if (property != null && property.contains(".")) {
            Iterator<String> it =
                    Splitter.on(".").limit(2).split(property).iterator();

            // First element is the property
            this.property = it.next();

            // Next property is the nested one
            String nestedProperty = it.next();
            this.embeddedListener = new NestedPropertyChangeListener(delegate, nestedProperty, all);

        } else if (all) {
            this.property = property;
            this.embeddedListener = new NestedPropertyChangeListener(delegate, null, false);

        } else {
            this.property = property;
        }
    }

    public void init(Object model) {

        // Add this NestedPropertyChangeListener on model
        addNestedPropertyChangeListener(model, this);

        if (embeddedListener != null) {

            // Init the embedded NestedPropertyChangeListener with
            // the propertyValue as model
            Object propertyValue =
                    SammoaUtil.getPropertyValue(model, property);

            embeddedListener.init(propertyValue);
        }
    }

    public String getProperty() {
        return property;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (embeddedListener != null) {
            if (evt.getOldValue() != null) {
                removeNestedPropertyChangeListener(evt.getOldValue(), embeddedListener);
            }
            if (evt.getNewValue() != null) {
                addNestedPropertyChangeListener(evt.getNewValue(), embeddedListener);
            }
        }
        delegate.propertyChange(evt);
    }

    protected void addNestedPropertyChangeListener(Object object, NestedPropertyChangeListener listener) {
        String property = listener.getProperty();
        if (object instanceof ListenableBean) {
            ListenableBean entity = (ListenableBean) object;
            if (property == null) {
                entity.addPropertyChangeListener(listener);
            } else {
                entity.addPropertyChangeListener(property, listener);
            }

        } else if (object instanceof AbstractBean) {
            AbstractBean entity = (AbstractBean) object;
            if (property == null) {
                entity.addPropertyChangeListener(listener);
            } else {
                entity.addPropertyChangeListener(property, listener);
            }
        }
    }

    protected void removeNestedPropertyChangeListener(Object object, NestedPropertyChangeListener listener) {
        String property = listener.getProperty();
        if (object instanceof ListenableBean) {
            ListenableBean entity = (ListenableBean) object;
            if (property == null) {
                entity.removePropertyChangeListener(listener);
            } else {
                entity.removePropertyChangeListener(property, listener);
            }

        } else if (object instanceof AbstractBean) {
            AbstractBean entity = (AbstractBean) object;
            if (property == null) {
                entity.removePropertyChangeListener(listener);
            } else {
                entity.removePropertyChangeListener(property, listener);
            }
        }
    }
}
