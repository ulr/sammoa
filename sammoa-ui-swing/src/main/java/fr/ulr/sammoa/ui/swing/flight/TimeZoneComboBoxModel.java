package fr.ulr.sammoa.ui.swing.flight;

/*-
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.DefaultComboBoxModel;
import java.util.Arrays;
import java.util.TimeZone;

public class TimeZoneComboBoxModel extends DefaultComboBoxModel {

    private static final long serialVersionUID = -4440059584802817645L;

    public TimeZoneComboBoxModel() {
        super(Arrays.stream(TimeZone.getAvailableIDs()).map(TimeZone::getTimeZone).toArray());
    }
}
