package fr.ulr.sammoa.ui.swing.home;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Created: 19/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class HomeUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SURVEY = "survey";

    public static final String PROPERTY_FLIGHTS = "flights";

    public static final String PROPERTY_SURVEY_FOUND = "surveyFound";

    public static final String PROPERTY_SYSTEM_ID = "systemId";

    public static final String PROPERTY_FLIGHT = "flight";

    public static final String PROPERTY_FLIGHT_ENDED = "flightEnded";

    protected Survey survey;

    protected List<Flight> flights;

    protected String systemId;

    protected Flight flight;

    protected boolean flightEnded;

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        Survey oldValue = getSurvey();
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, oldValue, survey);
        firePropertyChange(PROPERTY_SURVEY_FOUND, oldValue != null, survey != null);
    }

    public List<Flight> getFlights() {
        if (flights == null) {
            flights = Lists.newArrayList();
        }
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        // Note: Can't use ImmutableList for copy, the list contains the null flight
        List<Flight> oldValue = Lists.newArrayList(getFlights());
        this.flights = flights;
        firePropertyChange(PROPERTY_FLIGHTS, oldValue, flights);
    }

    public boolean isSurveyFound() {
        return survey != null;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        String oldValue = getSystemId();
        this.systemId = systemId;
        firePropertyChange(PROPERTY_SYSTEM_ID, oldValue, systemId);
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        Flight oldValue = getFlight();
        this.flight = flight;
        firePropertyChange(PROPERTY_FLIGHT, oldValue, flight);

        setFlightEnded(flight != null && flight.getEndDate() != null);
    }

    public boolean isFlightEnded() {
        return flightEnded;
    }

    public void setFlightEnded(boolean flightEnded) {
        boolean oldValue = isFlightEnded();
        this.flightEnded = flightEnded;
        firePropertyChange(PROPERTY_FLIGHT_ENDED, oldValue, flightEnded);
    }
}
