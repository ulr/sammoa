package fr.ulr.sammoa.ui.swing.io.input;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.input.map.ShpImporter;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import org.nuiton.csv.ImportRuntimeException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class UIImporter {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(UIImporter.class);

    protected final Component ui;

    public UIImporter(Component ui) {
        this.ui = ui;
    }

    public boolean importShape(ShpImporter<?> importer,
                               SurveyStorage storage,
                               File file) {
        boolean result = false;

        try {

            SammoaUtil.updateBusyState(ui, true);

            String successMessage = importer.importShape(file, storage);

            SammoaUtil.showSuccessMessage(ui, successMessage);

            result = true;

        } catch (ImportRuntimeException | IOException e) {
            showError(file, e);
        } finally {
            SammoaUtil.updateBusyState(ui, false);
        }
        return result;
    }

    public boolean importCsv(CsvImporter importer, File file) {
        boolean result = false;

        try {

            SammoaUtil.updateBusyState(ui, true);

            String successMessage = importer.importCsvFile(file);

            SammoaUtil.showSuccessMessage(ui, successMessage);

            result = true;

        } catch (ImportRuntimeException | IOException e) {
            showError(file, e);
        } finally {
            SammoaUtil.updateBusyState(ui, false);
        }
        return result;
    }

    protected void showError(File file, Exception e) {

        if (log.isInfoEnabled()) {
            log.info("Import error from file '" + file.getAbsolutePath() + "'", e);
        }
        SammoaUtil.showErrorMessage(ui, e.getMessage());
    }

}
