/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import java.util.List;

/** @author sletellier <letellier@codelutin.com> */
public class TransectTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected FlightUIModel reference;

    public TransectTableModel(FlightUIModel reference) {
        this.reference = reference;
    }

    public Flight getFlight() {
        return reference.getFlight();
    }

    public TransectFlight getCurrentTransect() {
        Route route = reference.getCurrentRoute();
        return route != null ? route.getTransectFlight() : null;
    }

    public TransectFlight getNextTransect() {
        return reference.getNextTransect();
    }

    public List<TransectFlightModel> getTransectFlights() {
        return reference.getTransectFlights();
    }

    public TransectFlightModel getRow(int index) {
        return getTransectFlights().get(index);
    }

    public int getRowIndex(final TransectFlight transectFlight) {
        int result = TransectFlightModel.indexOfTransectFlight(
                getTransectFlights(), transectFlight);
        return result;
    }

    public void fireTableRowUpdated(TransectFlight transectFlight) {
        if (transectFlight != null) {
            int index = getRowIndex(transectFlight);
            fireTableRowsUpdated(index, index);
        }
    }

    @Override
    public String getColumnName(int column) {
        return TransectColumn.valueOf(column).getColumnName();
    }

    @Override
    public int getRowCount() {
        return getTransectFlights().size();
    }

    @Override
    public int getColumnCount() {
        return TransectColumn.values().length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        TransectColumn column = TransectColumn.valueOf(columnIndex);
        TransectFlightModel bean = getTransectFlights().get(rowIndex);
        return column.isEditable(bean);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        TransectColumn column = TransectColumn.valueOf(columnIndex);
        return column.getType();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TransectFlightModel bean = getTransectFlights().get(rowIndex);
        TransectColumn column = TransectColumn.valueOf(columnIndex);
        return column.getValue(rowIndex, bean);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        TransectFlightModel bean = getTransectFlights().get(rowIndex);
        TransectColumn column = TransectColumn.valueOf(columnIndex);
        column.setValue(bean, aValue, this);

        SwingUtilities.invokeLater(new UpdateRowRunnable(this, rowIndex));
    }

    static class UpdateRowRunnable implements Runnable {
        protected int rowIndex;

        protected TransectTableModel model;

        UpdateRowRunnable(TransectTableModel model, int rowIndex) {
            this.rowIndex = rowIndex;
            this.model = model;
        }

        @Override
        public void run() {
            model.fireTableRowsUpdated(rowIndex, rowIndex);
        }
    }

    protected enum TransectColumn {

        INDEX(true, Integer.class, TransectFlightModel.PROPERTY_INDEX),
        NAME(false, String.class,
                TransectFlightModel.PROPERTY_TRANSECT, TransectModel.PROPERTY_SOURCE, Transect.PROPERTY_NAME
        ),
        POSITION_NAVIGATOR(true, Observer.class, Position.NAVIGATOR.name()) {
            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
                bean.setObserverByPosition(Position.NAVIGATOR, (Observer) value);
            }

            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                ObserverPosition position =
                        bean.getObserverPositionByPosition(Position.NAVIGATOR);
                return position.getObserver();
            }
        },
        POSITION_LEFT(true, Observer.class, Position.FRONT_LEFT.name()) {
            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
                bean.setObserverByPosition(Position.FRONT_LEFT, (Observer) value);
            }

            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                ObserverPosition position =
                        bean.getObserverPositionByPosition(Position.FRONT_LEFT);
                return position.getObserver();
            }
        },
        POSITION_RIGHT(true, Observer.class, Position.FRONT_RIGHT.name()) {
            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
                bean.setObserverByPosition(Position.FRONT_RIGHT, (Observer) value);
            }

            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                ObserverPosition position =
                        bean.getObserverPositionByPosition(Position.FRONT_RIGHT);
                return position.getObserver();
            }
        },
        POSITION_CO_NAVIGATOR(true, Observer.class, Position.CO_NAVIGATOR.name()) {
            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
                bean.setObserverByPosition(Position.CO_NAVIGATOR, (Observer) value);
            }

            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                ObserverPosition position =
                        bean.getObserverPositionByPosition(Position.CO_NAVIGATOR);
                return position.getObserver();
            }
        },
        CROSSING_NUMBER(true, int.class, TransectFlightModel.PROPERTY_CROSSING_NUMBER),
        DELETED(true, TransectFlightModel.class, "deleted") {
            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                return bean;
            }

            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
            }
        },
        ACTION(true, TransectFlightModel.class, "action") {
            @Override
            public Object getValue(int index, TransectFlightModel bean) {
                return bean;
            }

            @Override
            public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
            }
        };

        private boolean editable;

        private String[] beanProperties;

        private Class<?> type;

        private final String columnName;

        TransectColumn(

                boolean editable,
                Class<?> type,
                String... beanProperties) {
            this.editable = editable;
            this.type = type;
            this.beanProperties = beanProperties;
            this.columnName = beanProperties[0];
        }

        public Class<?> getType() {
            return type;
        }

        public Object getValue(int index, TransectFlightModel bean) {
            return SammoaUtil.getPropertyValue(bean, beanProperties);
        }

        public void setValue(TransectFlightModel bean, Object value, TransectTableModel model) {
            SammoaUtil.setPropertyValue(bean, value, beanProperties);
        }

        public boolean isEditable(TransectFlightModel bean) {
            return !bean.isValid() && editable;
        }

        public static TransectColumn valueOf(int ordinal) {
            for (TransectColumn value : values()) {
                if (ordinal == value.ordinal()) {
                    return value;
                }
            }
            throw new EnumConstantNotPresentException(TransectColumn.class,
                                                      "ordinal=" + ordinal);
        }

        public String getColumnName() {
            return columnName;
        }
    }
}
