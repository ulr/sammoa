package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ValidationService;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.FlightUIHandler;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.flight.ValidModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import fr.ulr.sammoa.ui.swing.util.TableDataChangeListener;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public abstract class ValidAction extends BaseFlightAction {

    private static final Log log = LogFactory.getLog(ValidAction.class);

//    private Map<Class<?>, BeanListValidator<?>> validators;

    public ValidAction(String name, JAXXContext context) {
        super(name, context);
        getValidModel().addValidModelListener(new ValidModel.ValidModelListener() {

            @Override
            public void onReset(ValidModel source) {
                setEnabled(checkEnabled());
            }
        });
    }
//
//    protected Map<Class<?>, BeanListValidator<?>> getValidators() {
//        if (validators == null) {
//            validators = Maps.newHashMap();
//        }
//        return validators;
//    }

    protected ValidModel getValidModel() {
        return context.getContextValue(FlightUIHandler.class).getValidModel();
    }

    protected ValidationService getValidationService() {
        return getSammoaUIContext().getService(ValidationService.class);
    }

    protected FlightService getFlightService() {
        return getSammoaUIContext().getService(FlightService.class);
    }
//
//    /**
//     * Retrieve a validator for the {@code beanClass}. A cache
//     * is used to keep the validator for this property. A listener is also
//     * used to call checkEnabled() on each validation change.
//     *
//     * @param beanClass Class of the bean
//     * @param <E> generic type of the bean
//     * @return the {@link BeanListValidator} found
//     * @throws NullPointerException if no validator exists in context for the
//     *         given {@code modelPropertyName}
//     */
//    private <E extends Validable> BeanListValidator<E> getValidator(Class<E> beanClass) {
//        BeanListValidator<E> validator = (BeanListValidator<E>) getValidators().get(beanClass);
//        if (validator == null) {
//            String prefix = beanClass.getSimpleName().toLowerCase();
//            String contextValueName = prefix + "Validator";
//            validator = context.getContextValue(BeanListValidator.class,
//                                                contextValueName);
//            Preconditions.checkNotNull(validator,
//                                       "No validator exists for bean type : "
//                                       + beanClass.getName()
//                                       + " (property from context = "
//                                       + contextValueName + ")"
//            );
//            validator.addBeanListValidatorListener(new BeanListValidatorListener() {
//
//                @Override
//                public void onFieldChanged(BeanListValidatorEvent event) {
//                    setEnabled(checkEnabled());
//                }
//            });
//            getValidators().put(beanClass, validator);
//        }
//        return validator;
//    }
//
//    protected BeanListValidator<Route> getRouteValidator() {
//        return getValidator(Route.class);
//    }
//
//    protected BeanListValidator<Observation> getObservationValidator() {
//        return getValidator(Observation.class);
//    }

    protected boolean askConfirmDelete(String label) {
        return SammoaUtil.askQuestion(
                (FlightUI) context,
                t("sammoa.confirmDialog.validation.delete.message", label)
        );
    }

    protected boolean askConfirmDeleteByCascade(String label,
                                                String cascadeLabel) {
        return SammoaUtil.askQuestion(
                (FlightUI) context,
                t("sammoa.confirmDialog.validation.deleteByCascade.message", label, cascadeLabel)
        );
    }

    protected List<TableDataChangeListener> getTableDataChangeListeners(String property) {
        return Stream.of(getModel().getPropertyChangeListeners(property))
                .filter(TableDataChangeListener.class::isInstance)
                .map(TableDataChangeListener.class::cast)
                .collect(Collectors.toList());
    }

    protected void reloadObservations() {
        long startTime = System.currentTimeMillis();

        List<TableDataChangeListener> tableDataChangeListeners = getTableDataChangeListeners(FlightUIModel.PROPERTY_OBSERVATIONS);
        tableDataChangeListeners.forEach(l -> l.setEnabled(false));


        getModel().setObservationEditBean(null);

        // Reload all observations
        getModel().clearObservation();
        List<Observation> observations =
                getFlightService().getObservations(getModel().getFlight());
        getModel().addAllObservation(observations);

        getValidModel().cleanUpObservationValid();

        tableDataChangeListeners.forEach(l -> l.setEnabled(true));

        ((FlightUI) context).getEffortPanel().getObservationTableModel().fireTableDataChanged();

        if (log.isInfoEnabled()) {
            log.info("reload Observations durring " + (System.currentTimeMillis() - startTime) + " ms");
        }

    }

    protected void reloadRoutes() {
        long startTime = System.currentTimeMillis();

        List<TableDataChangeListener> tableDataChangeListeners = getTableDataChangeListeners(FlightUIModel.PROPERTY_ROUTES);
        tableDataChangeListeners.forEach(l -> l.setEnabled(false));

        Route routeEditBean = getModel().getRouteEditBean();
        getModel().setRouteEditBean(null);

        // Reload all routes
        getModel().clearRoute();
        List<Route> routes = getFlightService().getRoutes(getModel().getFlight());
        getModel().addAllRoute(routes);

        // Keep the route selected
        if (routeEditBean != null && !routeEditBean.isDeleted()) {
            getModel().setRouteEditBean(routeEditBean);
        }

        getValidModel().cleanUpRouteValid();

        tableDataChangeListeners.forEach(l -> l.setEnabled(true));

        ((FlightUI) context).getEffortPanel().getRouteTableModel().fireTableDataChanged();

        if (log.isInfoEnabled()) {
            log.info("reload Routes durring " + (System.currentTimeMillis() - startTime) + " ms");
        }
    }
}
