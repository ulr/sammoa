/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.survey;

import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.input.csv.ImportCsvService;
import fr.ulr.sammoa.application.io.input.map.ImportMapService;
import fr.ulr.sammoa.application.io.input.map.ShpImporter;
import fr.ulr.sammoa.application.io.input.map.StrateImportModel;
import fr.ulr.sammoa.application.io.input.map.TransectImportModel;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.ui.swing.SammoaScreen;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.io.input.CsvImporter;
import fr.ulr.sammoa.ui.swing.io.input.UIImporter;
import fr.ulr.sammoa.ui.swing.region.RegionUI;
import fr.ulr.sammoa.ui.swing.region.RegionUIModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableRenderer;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 04/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @since 0.2
 */
public class SurveyUIHandler implements SammoaUIHandler {

    private static final Log log =
            LogFactory.getLog(SurveyUIHandler.class);

    protected SammoaUIContext context;

    protected SurveyUI ui;

    protected RegionUI regionUi;

    protected final UIImporter uiImporter;

    protected final ImportMapService importMapService;

    protected final ImportCsvService importCsvService;

    protected final ReferentialService referentialService;

    protected final FlightService flightService;

    public SurveyUIHandler(SammoaUIContext context, SurveyUI ui) {
        this.context = context;
        this.ui = ui;
        this.uiImporter = new UIImporter(this.ui);

        this.importMapService = context.getService(ImportMapService.class);
        this.importCsvService = context.getService(ImportCsvService.class);
        this.flightService = context.getService(FlightService.class);
        this.referentialService = context.getService(ReferentialService.class);
    }

    protected SurveyUIModel getModel() {
        return ui.getModel();
    }

    @Override
    public void beforeInitUI() {

        SurveyUIModel model = new SurveyUIModel();

        List<Region> regions = referentialService.getRegions();
        model.setRegionReferential(regions);

        String surveyId = context.getSurveyId();

        if (surveyId == null) {
            List<Survey> surveys = referentialService.getSurveys();
            model.prepareCreate(surveys);

        } else {
            Survey survey = referentialService.getSurvey(surveyId);
            model.prepareUpdate(survey);
        }

        ui.setContextValue(model);
    }

    @Override
    public void afterInitUI() {

        regionUi = new RegionUI(context);

        regionUi.addComponentListener(regionUIListener);

        ui.getRegionComboBox().setRenderer(
                context.getService(UIDecoratorService.class).newListCellRender(Region.class));

        SwingValidatorUtil.installUI(ui.getErrorTable(),
                                     new SwingValidatorMessageTableRenderer());
    }

    @Override
    public void onCloseUI() {
        regionUi.removeComponentListener(regionUIListener);
    }

    public void showNewRegion() {

        if (log.isInfoEnabled()) {
            log.info("Prepare the RegionUI for new region");
        }

        // Prepare model
        RegionUIModel regionModel = regionUi.getModel();
        regionModel.prepareCreate(getModel().getRegionReferential());

        regionUi.getHandler().openUI();
    }

    public void showEditRegion() {

        Region selectedRegion = getModel().getRegion();

        if (log.isInfoEnabled()) {
            log.info("Prepare the RegionUI with id = " +
                        selectedRegion.getTopiaId());
        }

        // Prepare model
        RegionUIModel regionModel = regionUi.getModel();
        regionModel.prepareUpdate(selectedRegion);

        regionUi.getHandler().openUI();
    }

    public void saveSurvey() {

        try {
            boolean success = true;

            Survey survey = getModel().newBean();

            SammoaUtil.updateBusyState(ui, true);

            String surveyId = referentialService.saveSurvey(survey);
            getModel().setId(surveyId);

            SurveyStorage surveyStorage =
                    referentialService.getSurveyStorage(surveyId);

            File strateFile = ui.getStrateFileEditor().getSelectedFile();
            if (strateFile != null && strateFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Import strates file " +
                                strateFile.getAbsolutePath());
                }

                success &= uiImporter.importShape(strateImporter, surveyStorage, strateFile);
            }

            File transectFile = ui.getTransectFileEditor().getSelectedFile();
            if (transectFile != null && transectFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Import transects file " +
                                transectFile.getAbsolutePath());
                }

                success &= uiImporter.importShape(transectImporter,
                                                  surveyStorage, transectFile
                );
            }

            final File observerFile = ui.getObserverFileEditor().getSelectedFile();
            if (observerFile != null && observerFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Import observers file " +
                                observerFile.getAbsolutePath());
                }

                success &= uiImporter.importCsv(observerImporter, observerFile);
            }

            if (success) {
                context.setSurveyId(surveyId);
                close();
            }

        } catch (Exception e) {
            ErrorDialogUI.showError(e);
        } finally {
            SammoaUtil.updateBusyState(ui, false);
        }
    }

    public void close() {
        context.changeScreen(SammoaScreen.HOME);
    }

    protected ComponentListener regionUIListener = new ComponentListener() {

        @Override
        public void componentResized(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentShown(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentHidden(ComponentEvent e) {

            // On hide : retrieve the region saved and reset the region combo

            String regionId = regionUi.getModel().getId();

            if (regionId == null) {
                // nothing to do, no creation is done

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Retrieve from RegionUI the id = " +  regionId);
                }

                List<Region> regions = getModel().getRegionReferential();

                Region regionSaved = regionUi.getModel().newBean();

                if (regions.contains(regionSaved)) {
                    // nothing to do, the element exist in the list

                } else {
                    regions.add(regionSaved);
                    ui.getRegionListModel().addElement(regionSaved);
                    getModel().setRegion(regionSaved);
                }
            }
        }
    };

    protected CsvImporter observerImporter = new CsvImporter() {

        @Override
        public String importCsvFile(File file) throws IOException {
            int nbImported = importCsvService.importObservers(getModel().getId(), file);
            return t("sammoa.messageDialog.observers.import.success", nbImported);
        }
    };

    protected ShpImporter<Strate> strateImporter = new ShpImporter<Strate>(new StrateImportModel(), "strates") {

        protected String onDbfLoaded(Iterable<Strate> elements, SurveyStorage storage) {
            int nbImported = importMapService.importStrates(storage.getId(), elements);
            return t("sammoa.messageDialog.strates.import.success", nbImported);
        }

    };

    protected ShpImporter<Transect> transectImporter = new ShpImporter<Transect>(new TransectImportModel(), "transects") {

        @Override
        protected String onDbfLoaded(Iterable<Transect> elements, SurveyStorage storage) {
            int nbImported = importMapService.importTransects(storage.getId(), elements);
            return t("sammoa.messageDialog.transects.import.success", nbImported);
        }

    };

}
