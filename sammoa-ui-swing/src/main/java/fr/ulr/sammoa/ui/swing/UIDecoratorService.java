/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing;

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.DecoratorService;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.ui.swing.flight.StrateModel;
import fr.ulr.sammoa.ui.swing.flight.TransectFlightModel;
import fr.ulr.sammoa.ui.swing.flight.TransectModel;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import jaxx.runtime.swing.renderer.DecoratorTableCellRenderer;
import org.nuiton.decorator.Decorator;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 29/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 */
public class UIDecoratorService extends DecoratorService {

    @Override
    public void setSammoaContext(SammoaContext context) {
        super.setSammoaContext(context);

        // add extra ui decorators
        decoratorProvider.registerJXPathDecorator(TransectModel.class, "${source/name}$s");
        decoratorProvider.registerJXPathDecorator(TransectFlightModel.class, "${transect/source/name}$s (${crossingNumber}$d)");
        decoratorProvider.registerDecorator(new Decorator<StrateModel>(StrateModel.class) {

            private static final long serialVersionUID = 1L;

            @Override
            public String toString(Object bean) {
                StrateModel model = (StrateModel) bean;
                Strate strate = model.getSource();

                String result;
                if (strate == null) {
                    result = t("sammoa.strate.decorator.all");

                } else {
                    result = t("sammoa.strate.decorator.strate", strate.getCode());
                }

                return result;
            }
        });
    }

    public <O> ListCellRenderer newListCellRender(Class<O> type) {

        Preconditions.checkNotNull(type);

        Decorator<O> decorator = getDecoratorByType(type);
        Preconditions.checkNotNull(decorator);

        DecoratorListCellRenderer result = new DecoratorListCellRenderer(decorator);
        return result;
    }

    public <O> TableCellRenderer newTableCellRender(Class<O> type) {

        Preconditions.checkNotNull(type);

        Decorator<O> decorator = getDecoratorByType(type);
        Preconditions.checkNotNull(decorator);

        DecoratorTableCellRenderer result = new DecoratorTableCellRenderer(decorator);
        return result;
    }
}
