package fr.ulr.sammoa.ui.swing.util;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPoints;
import fr.ulr.sammoa.ui.swing.flight.effort.GeoPointTableModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * @author Sylvain Bavencoff <bavencoff@codelutin.com>
 */
public class ValidationHelper {

    protected static ValidationHelper INSTANCE = new ValidationHelper();

    public static int MAX_DELAY = 5;

    public static ValidationHelper getInstance() {
        return INSTANCE;
    }

    private ValidationHelper() {}

    public void setGeoPointTableModel(GeoPointTableModel geoPointTableModel) {
        this.geoPointTableModel = geoPointTableModel;
    }

    protected GeoPointTableModel geoPointTableModel;

    public boolean validateHasNoGeoPointLost(Date date) {

        GeoPointImpl point = new GeoPointImpl();
        point.setRecordTime(date);

        int search = Collections.binarySearch(geoPointTableModel.getBean(), point, Comparator.comparing(GeoPoint::getRecordTime));
        if (search < 0) {
            search = -2 - search;
        }

        boolean emptyOrOutOfDate = search < 0 || GeoPoints.isEmptyOrOutOfDate(geoPointTableModel.getBean().get(search), MAX_DELAY);

        return !emptyOrOutOfDate;
    }

}
