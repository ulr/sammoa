/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import com.bbn.openmap.InformationDelegator;
import com.bbn.openmap.LayerHandler;
import com.bbn.openmap.MapBean;
import com.bbn.openmap.MapHandler;
import com.bbn.openmap.MouseDelegator;
import com.bbn.openmap.event.OMMouseMode;
import com.bbn.openmap.gui.EmbeddedNavPanel;
import com.bbn.openmap.gui.EmbeddedScaleDisplayPanel;
import com.bbn.openmap.gui.OverlayMapPanel;
import com.bbn.openmap.gui.ToolPanel;
import com.bbn.openmap.layer.shape.BufferedShapeLayer;
import com.bbn.openmap.layer.shape.ShapeLayer;
import com.bbn.openmap.omGraphics.DrawingAttributes;
import com.bbn.openmap.plugin.esri.EsriPlugIn;
import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.util.PropUtils;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.device.gps.GpsLocationEvent;
import fr.ulr.sammoa.application.device.gps.GpsLocationListener;
import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.application.flightController.FlightControllerListener;
import fr.ulr.sammoa.application.flightController.FlightControllerOnBoard;
import fr.ulr.sammoa.application.flightController.FlightControllerValidation;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.application.flightController.ObservationEvent;
import fr.ulr.sammoa.application.flightController.RouteEvent;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPoints;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectFlights;
import fr.ulr.sammoa.ui.swing.SammoaColors;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.flight.action.AddAction;
import fr.ulr.sammoa.ui.swing.flight.action.BeginAction;
import fr.ulr.sammoa.ui.swing.flight.action.CenterObservationAction;
import fr.ulr.sammoa.ui.swing.flight.action.ChangeTimeZoneAction;
import fr.ulr.sammoa.ui.swing.flight.action.CircleBackAction;
import fr.ulr.sammoa.ui.swing.flight.action.DeleteTransectAction;
import fr.ulr.sammoa.ui.swing.flight.action.EndAction;
import fr.ulr.sammoa.ui.swing.flight.action.LeftObservationAction;
import fr.ulr.sammoa.ui.swing.flight.action.NextAction;
import fr.ulr.sammoa.ui.swing.flight.action.NextTransectAction;
import fr.ulr.sammoa.ui.swing.flight.action.RejoinAction;
import fr.ulr.sammoa.ui.swing.flight.action.RightObservationAction;
import fr.ulr.sammoa.ui.swing.flight.action.StartAction;
import fr.ulr.sammoa.ui.swing.flight.action.StartAudioAction;
import fr.ulr.sammoa.ui.swing.flight.action.StopAction;
import fr.ulr.sammoa.ui.swing.flight.action.StopAudioAction;
import fr.ulr.sammoa.ui.swing.flight.action.ValidFlightAction;
import fr.ulr.sammoa.ui.swing.flight.action.ValidObservationAction;
import fr.ulr.sammoa.ui.swing.flight.action.ValidRouteAction;
import fr.ulr.sammoa.ui.swing.flight.action.ValidTransectAction;
import fr.ulr.sammoa.ui.swing.flight.bar.onBoard.OnBoardBar;
import fr.ulr.sammoa.ui.swing.flight.bar.validation.ValidationBar;
import fr.ulr.sammoa.ui.swing.flight.effort.EffortPanelHandler;
import fr.ulr.sammoa.ui.swing.flight.layer.BaseGeoPointLayer;
import fr.ulr.sammoa.ui.swing.flight.layer.LineGeoPointLayer;
import fr.ulr.sammoa.ui.swing.flight.layer.SimpleGeoPointLayer;
import fr.ulr.sammoa.ui.swing.flight.layer.TransectLayer;
import fr.ulr.sammoa.ui.swing.transect.TransectUI;
import fr.ulr.sammoa.ui.swing.util.ButtonActionTableCellEditorRenderer;
import fr.ulr.sammoa.ui.swing.util.CheckBoxActionTableCellEditorRenderer;
import fr.ulr.sammoa.ui.swing.util.ColorTableCellHeaderRenderer;
import fr.ulr.sammoa.ui.swing.util.DeletedRowHighlightPredicate;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import fr.ulr.sammoa.ui.swing.util.SelectionModelAdapter;
import fr.ulr.sammoa.ui.swing.util.TableColumnModelStorage;
import fr.ulr.sammoa.ui.swing.util.TableDataChangeListener;
import fr.ulr.sammoa.ui.swing.util.ValidRowHighlightPredicate;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.util.TimeLog;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created: 03/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class FlightUIHandler implements SammoaUIHandler, FlightControllerListener {

    private static final Log log = LogFactory.getLog(FlightUIHandler.class);

    private static final TimeLog timeLog = new TimeLog(FlightUIHandler.class);

    protected SammoaUIContext context;

    protected FlightUI ui;

    protected long uiStartTime;

    protected TransectUI transectUi;

    protected BaseGeoPointLayer observationLayer;

    protected BaseGeoPointLayer flightLayer;

    protected final FlightService flightService;

    protected final ReferentialService referentialService;

    protected final UIDecoratorService decoratorService;

    protected FlightController flightController;

    protected boolean transectFlightSelectionIsAdjusting;

    protected boolean routeSelectionIsAdjusting;

    protected ValidModel validModel;

    protected TableColumnModelStorage transectsColumnModelStorage;

    protected JDialog questionDialog;

    public FlightUIHandler(SammoaUIContext context, FlightUI ui) {
        this.context = context;
        this.ui = ui;
        ui.setContextValue(context);

        this.flightService = context.getService(FlightService.class);
        this.referentialService = context.getService(ReferentialService.class);
        this.decoratorService = context.getService(UIDecoratorService.class);
    }

    public FlightController getFlightController() {
        return flightController;
    }

    public FlightUIModel getModel() {
        return ui.getModel();
    }

    public ValidModel getValidModel() {
        if (validModel == null) {
            BeanListValidator<Observation> observationValidator =
                    ui.getEffortPanel().getObservationValidator();
            BeanListValidator<Route> routeValidator =
                    ui.getEffortPanel().getRouteValidator();
            validModel = new ValidModel(observationValidator, routeValidator);
        }
        return validModel;
    }

    @Override
    public void beforeInitUI() {

        long startTime = TimeLog.getTime();

        String flightId = context.getFlightId();

        // TODO-fdesbois-2012-07-19 : load all this data in the same transaction
        // TODO-fdesbois-2012-07-19 : to avoid separated instances of same entities
        // TODO-fdesbois-2012-07-19 : like ObserverPosition or use proper Model like TransectModel
        Flight flight = flightService.getFlight(flightId);
        Preconditions.checkNotNull(flight);
        Survey survey = flight.getSurvey();
        List<Observer> referentialObservers =
                referentialService.getAllObservers(survey);
        List<Observer> observers = flightService.getFlightObserverForPositions(flight);
        List<Observation> observations = flightService.getObservations(flight);
        List<Route> routes = flightService.getRoutes(flight);
        List<GeoPoint> geoPoints = flightService.getFlightGeoPoints(flight);

        if (log.isTraceEnabled()) {
            for (GeoPoint geoPoint : geoPoints) {
                log.trace(String.format("GeoPoint={%1$tH:%1$tM:%1$tS,%2$f,%3$f}",
                                           geoPoint.getRecordTime(),
                                           geoPoint.getLatitude(),
                                           geoPoint.getLongitude())
                );
            }
        }

        List<Transect> transects = referentialService.getAllTransects(survey);

        if (log.isInfoEnabled()) {
            log.info(String.format("Init view with flight number '%d' [%s]",
                                      flight.getFlightNumber(),
                                      flight.getTopiaId())
            );
        }

        startTime = timeLog.log(startTime, "beforeInitUI", "entities are loaded");

        boolean validationMode = context.isValidationMode();

        if (validationMode) {
            flightController = context.getService(FlightControllerValidation.class);
            flightController.addFlightControllerListener(this);

            if (log.isDebugEnabled()) {
                log.debug("Open devices for VALIDATION");
            }

            openDevice(AudioReader.class);

        } else {
            flightController = context.getService(FlightControllerOnBoard.class);
            flightController.addFlightControllerListener(this);

            if (log.isDebugEnabled()) {
                log.debug("Open devices for ON_BOARD");
            }

            GpsHandler gpsHandler = openDevice(GpsHandler.class);
            AudioRecorder audioRecorder = openDevice(AudioRecorder.class);

            gpsHandler.addGpsLocationListener(gpsLocationListener);
            gpsHandler.addDeviceStateListener(deviceStateListener);
            gpsHandler.start();
            audioRecorder.addDeviceStateListener(deviceStateListener);
        }

        FlightUIModel model = new FlightUIModel();

        flightController.init(flight);

        model.setValidationMode(validationMode);
        model.setObservers(referentialObservers);
        model.setFlight(flight);
        model.setFlightObserverForPositions(observers);
        model.setObservations(observations);
        model.setRoutes(routes);
        model.setGeoPoints(geoPoints);

        model.setStrates(prepareStrates(transects));

        model.setTransectFlights(prepareTransectFlights(model, flight.getTransectFlight()));

        model.setFlightState(flightController.getState());
        model.setCurrentRoute(flightController.getCurrentRoute());
        model.setNextTransect(flightController.getNextTransect());

        model.setCircleBackEnabled(context.getConfig().isCircleBackEnabled());
        model.setCircleBackTimerEnabled(context.getConfig().isCircleBackTimerEnabled());

        ui.setContextValue(model);

        timeLog.log(startTime, "beforeInitUI", "model is created");

        uiStartTime = TimeLog.getTime();
    }

    protected List<StrateModel> prepareStrates(List<Transect> transects) {
        long startTime = TimeLog.getTime();

        // The default strateModel has a null strate and contains all the transects
        StrateModel strateAll = new StrateModel(null);

        Map<Strate, StrateModel> strateMap = Maps.newHashMap();
        for (Transect transect : transects) {

            TransectModel transectModel = new TransectModel(transect);

            Strate strate = transect.getStrate();
            StrateModel strateModel = strateMap.get(strate);
            if (strateModel == null) {

                strateModel = new StrateModel(strate);
                strateMap.put(strate, strateModel);
            }
            strateModel.getTransects().add(transectModel);
            strateAll.getTransects().add(transectModel);
        }

        List<StrateModel> result =
                Ordering.natural().sortedCopy(strateMap.values());

        result.add(0, strateAll);

        timeLog.log(startTime, "prepareStrates");
        return result;
    }

    protected List<TransectFlightModel> prepareTransectFlights(FlightUIModel model,
                                                               List<TransectFlight> transectFlights) {
        long startTime = TimeLog.getTime();

        Map<Transect, TransectModel> transectMap =
                Maps.uniqueIndex(model.getTransects(),
                                 TransectModel.toTransect());

        List<TransectFlightModel> result = Lists.newArrayList();
        for (TransectFlight transectFlight : transectFlights) {

            Transect transect = transectFlight.getTransect();
            TransectModel transectModel = transectMap.get(transect);

            transectModel.setInFlight(!transectFlight.isDeleted());

            TransectFlightModel instance = new TransectFlightModel(model, transectFlight, transectModel);

            instance.addPropertyChangeListener(transectFlightListener);

            result.add(instance);
        }
        startTime = timeLog.log(startTime, "prepareTransectFlights", "model prepared");

        // Retrieve realNbTimes only for transects in the flight

        List<Transect> transects =
                transectFlights.stream().map(TransectFlights.toTransect()).collect(Collectors.toList());

        Map<Transect, Long> transectRealNbTimes =
                flightService.getTransectRealNbTimes(transects);

        for (Map.Entry<Transect, Long> entry : transectRealNbTimes.entrySet()) {

            int realNbTimes = MoreObjects.firstNonNull(entry.getValue(), 0).intValue();

            TransectModel transectModel = transectMap.get(entry.getKey());
            if (transectModel != null) {
                transectModel.setRealNbTimes(realNbTimes);
            }
        }
        timeLog.log(startTime, "prepareTransectFlights", "realNbTimes set");
        return result;
    }

    @Override
    public void afterInitUI() {

        timeLog.log(uiStartTime, "initUI");

        long startTime = TimeLog.getTime();

        // Strate ComboBox
        {
            JComboBox comboBox = ui.getStrateCombobox();

            comboBox.setRenderer(decoratorService.newListCellRender(StrateModel.class));

            StrateModel strateAll = getModel().getStrateAll();
            SwingUtil.fillComboBox(comboBox, getModel().getStrates(), strateAll);

            selectStrate(strateAll);
        }

        ListCellRenderer observerListCellRenderer =
                decoratorService.newListCellRender(Observer.class);

        // Observer List
        {
            ui.getObserverSelector().setRenderer(
                    new ObserverListCellRenderer(observerListCellRenderer)
            );
        }

        // Transect List
        {
            ui.getTransectList().setCellRenderer(
                    new TransectListCellRenderer(decoratorService.newListCellRender((TransectModel.class))));
        }

        // Transect Table
        {
            JXTable table = ui.getTransectTable();

            SwingUtil.setI18nTableHeaderRenderer(
                    table,
                    n("sammoa.flightPanel.table.column.index"),
                    n("sammoa.flightPanel.table.column.index.tip"),
                    n("sammoa.flightPanel.table.column.name"),
                    n("sammoa.flightPanel.table.column.name.tip"),
                    n("sammoa.flightPanel.table.column.position.navigator"),
                    n("sammoa.flightPanel.table.column.position.navigator.tip"),
                    n("sammoa.flightPanel.table.column.position.left"),
                    n("sammoa.flightPanel.table.column.position.left.tip"),
                    n("sammoa.flightPanel.table.column.position.right"),
                    n("sammoa.flightPanel.table.column.position.right.tip"),
                    n("sammoa.flightPanel.table.column.position.co-navigator"),
                    n("sammoa.flightPanel.table.column.position.co-navigator.tip"),
                    n("sammoa.flightPanel.table.column.crossingNumber"),
                    n("sammoa.flightPanel.table.column.crossingNumber.tip"),
                    n("sammoa.flightPanel.table.column.deleted"),
                    n("sammoa.flightPanel.table.column.deleted.tip"),
                    n("sammoa.flightPanel.table.column.action"),
                    n("sammoa.flightPanel.table.column.action.tip")
            );

            TransectTableModel tableModel = ui.getTransectTableModel();

            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_TRANSECT_FLIGHTS,
                    new TableDataChangeListener(table, TableDataChangeListener.NO_COLUMN_INDEX_TO_EDIT_ON_INSERT));

            if (getModel().isValidationMode()) {

                table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

                SammoaUtil.addTableSelectionListener(
                        table, new SelectionModelAdapter<TransectFlightModel>() {

                    @Override
                    public List<TransectFlightModel> getElements() {
                        return getModel().getTransectFlights();
                    }

                    @Override
                    public void setSelectedElement(TransectFlightModel element) {
                        getModel().setTransectFlightEditBean(element);
                    }
                });

                getModel().addPropertyChangeListener(
                        FlightUIModel.PROPERTY_OBSERVATION_EDIT_BEAN, evt -> onObservationChangedForValidation(
                                (Observation) evt.getOldValue(),
                                (Observation) evt.getNewValue()));

                getModel().addPropertyChangeListener(
                        FlightUIModel.PROPERTY_ROUTE_EDIT_BEAN, evt -> onRouteChangedForValidation(
                                (Route) evt.getOldValue(),
                                (Route) evt.getNewValue()));

                getModel().addPropertyChangeListener(
                        FlightUIModel.PROPERTY_TRANSECT_FLIGHT_EDIT_BEAN, evt -> onTransectFlightChangedForValidation(
                                (TransectFlightModel) evt.getOldValue(),
                                (TransectFlightModel) evt.getNewValue()));
            }

            NumberCellEditor<Integer> numberCellEditor = JAXXWidgetUtil.newNumberTableCellEditor(Integer.class, false);
            table.setDefaultEditor(Integer.class, numberCellEditor);
            table.setDefaultEditor(int.class, numberCellEditor);

            TableCellRenderer stringRenderer = table.getDefaultRenderer(String.class);

            // OBSERVER columns (Left, Right, Nav, Co-Nav positions)
            {
                final JComboBox comboBox = new JComboBox();
                comboBox.setRenderer(observerListCellRenderer);
                SwingUtil.fillComboBox(comboBox, getModel().getFlightObserverForPositions(), null);

                getModel().addPropertyChangeListener(
                        FlightUIModel.PROPERTY_FLIGHT_OBSERVER_FOR_POSITIONS,
                        evt -> SwingUtil.fillComboBox(comboBox, (List<Observer>) evt.getNewValue(), null));

                table.setDefaultEditor(
                        Observer.class, new DefaultCellEditor(comboBox));
                table.setDefaultRenderer(
                        Observer.class, decoratorService.newTableCellRender(Observer.class));
            }

            // CROSSING_NUMBER column
            {
                TableColumn column = table.getColumn(TransectTableModel.TransectColumn.CROSSING_NUMBER.ordinal());

                TransectCrossingNumberCellRenderer cellRenderer =
                        new TransectCrossingNumberCellRenderer(stringRenderer, tableModel);

                column.setCellRenderer(cellRenderer);
            }

            // LEFT position column
            {
                TableColumn column = table.getColumn(TransectTableModel.TransectColumn.POSITION_LEFT.ordinal());
                column.setHeaderRenderer(new ColorTableCellHeaderRenderer(SammoaColors.POSITION_LEFT_COLOR,
                        t("sammoa.flightPanel.table.column.position.left"),
                        t("sammoa.flightPanel.table.column.position.left.tip")));
            }

            // RIGHT position column
            {
                TableColumn column = table.getColumn(TransectTableModel.TransectColumn.POSITION_RIGHT.ordinal());
                column.setHeaderRenderer(new ColorTableCellHeaderRenderer(SammoaColors.POSITION_RIGHT_COLOR,
                        t("sammoa.flightPanel.table.column.position.right"),
                        t("sammoa.flightPanel.table.column.position.right.tip")));
            }

            // ACTION deleted column
            {
                int columnIndex = TransectTableModel.TransectColumn.DELETED.ordinal();
                TableColumn column = table.getColumnModel().getColumn(columnIndex);
                CheckBoxActionTableCellEditorRenderer editorRenderer = new CheckBoxActionTableCellEditorRenderer(
                        DeleteTransectAction.CLIENT_PROPERTY_TRANSECT_FLIGHT,
                        () -> getActionMap().get("deleteTransect"),
                        input -> {
                            TransectFlight transectFlight = ((TransectFlightModel) input).getSource();

                            // Enabled only if no route match the transectFlight
                            // Note : the deleted route case is too complex,
                            // the user must delete routes before transect
                            boolean result = Routes.isAnyMatchTransectFlight(
                                    getModel().getRoutes(), transectFlight);
                            return !result;
                        },
                        TransectFlightModel.PROPERTY_DELETED);

                column.setCellEditor(editorRenderer);
                column.setCellRenderer(editorRenderer);
            }

            // ACTION nextTransect column
            {
                int columnIndex = TransectTableModel.TransectColumn.ACTION.ordinal();
                TableColumn column = table.getColumnModel().getColumn(columnIndex);
                ButtonActionTableCellEditorRenderer editorRenderer = new ButtonActionTableCellEditorRenderer(
                        NextTransectAction.CLIENT_PROPERTY_TRANSECT_FLIGHT,
                        () -> getActionMap().get("nextTransect"),
                        input -> {
                            TransectFlight transectFlight = ((TransectFlightModel) input).getSource();

                            // Enabled only if no route match the transectFlight
                            // Note : the deleted route case is too complex,
                            // the user must create a new transectFlight to
                            // use it as next one
                            boolean result = Routes.isAnyMatchTransectFlight(
                                    getModel().getRoutes(), transectFlight);
                            return !transectFlight.isDeleted() && !result;
                        });

                column.setCellEditor(editorRenderer);
                column.setCellRenderer(editorRenderer);
            }

            table.addHighlighter(SammoaUtil.newBackgroundColorHighlighter(
                    new CurrentTransectHighlightPredicate(tableModel),
                    SammoaColors.CURRENT_TRANSECT_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newBackgroundColorHighlighter(
                    new NextTransectHighlightPredicate(tableModel),
                    SammoaColors.NEXT_TRANSECT_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new DeletedRowHighlightPredicate(getModel().getTransectFlights()),
                    SammoaColors.DELETED_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new ValidRowHighlightPredicate(getModel().getTransectFlights()),
                    SammoaColors.VALID_ROW_COLOR)
            );

            transectsColumnModelStorage = new TableColumnModelStorage(table, context.getConfig(), SammoaConfig.Table.transects);
            transectsColumnModelStorage.init();
        }

        startTime = timeLog.log(startTime, "afterInitUI", "decoration done (editor/renderer)");

        initActions();

        startTime = timeLog.log(startTime, "afterInitUI", "actions are created");

        getModel().getFlight().addPropertyChangeListener(
                Flight.PROPERTY_OBSERVER, flightObserversListener);

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_CURRENT_ROUTE, currentRouteListener);

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_NEXT_TRANSECT, nextTransectListener);

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_TRANSECT_FLIGHTS, removeTransectListener);

        transectUi = new TransectUI(context);

        transectUi.addComponentListener(transectUIListener);

        startTime = timeLog.log(startTime, "afterInitUI", "other simple stuff");

        initMap();

        timeLog.log(startTime, "afterInitUI", "map is initialized");

        if (ui.getFlightBar() instanceof OnBoardBar) {
            ((OnBoardBar)ui.getFlightBar()).getAudioMeter().start();
        }

        if (getModel().isValidationMode()) {
            SwingUtilities.invokeLater(this::deleteUnUsedTransectFlight);
        }
    }

    private void deleteUnUsedTransectFlight() {
        List<TransectFlightModel> deleteTransectFlights = getModel().getTransectFlights()
                .stream()
                .filter(TransectFlightModel::isDeleted)
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(deleteTransectFlights)) {
            String message = deleteTransectFlights
                    .stream()
                    .map(tf -> t(
                            "sammoa.confirmDialog.deleteUnusedTransect.validation.item.message",
                            tf.getIndex(),
                            tf.getSource().getTransect().getName()))
                    .reduce(t("sammoa.confirmDialog.deleteUnusedTransect.validation.message"),
                            (m1, m2) -> m1 + "\n        " + m2);


            questionDialog = SammoaUtil.askQuestionNoModal(ui, message, delete -> {
                if (delete) {

                    FlightService service = context.getService(FlightService.class);

                    for (TransectFlightModel transectFlight : deleteTransectFlights) {
                        service.deleteTransectFlight(getModel().getFlight(), transectFlight.getSource());

                        if (log.isDebugEnabled()) {
                            log.debug("Delete transect " + transectFlight.getSource().getTransect().getName());
                        }

                        // Update model
                        int index = getModel().indexOfTransectFlights(transectFlight);
                        getModel().removeTransectFlight(index);
                    }
                }
            });
        }
    }

    @Override
    public void onCloseUI() {
        transectUi.removeComponentListener(transectUIListener);
        flightController.close();

        if (ui.getFlightBar() instanceof OnBoardBar) {
            ((OnBoardBar)ui.getFlightBar()).getAudioMeter().stop();
        }

        if (questionDialog != null && questionDialog.isVisible()) {
            questionDialog.setVisible(false);
            questionDialog.dispose();
        }

        try {
            context.closeService(flightController);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close flightController", e);
            }
        }

    }

    public JComponent newFlightBar() {
        JComponent result;
        if (getModel().isValidationMode()) {
            result = new ValidationBar(ui);
        } else {
            result = new OnBoardBar(ui);
        }
        return result;
    }

    public ActionMap getActionMap() {
        return ui.getActionMap();
    }

    public InputMap getInputMap() {
        // Use WHEN_IN_FOCUSED_WINDOW to don't have focus check for binding keys
        return ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    @Override
    public void onCurrentRouteChanged(RouteEvent event) {
        Route route = event.getRoute();
        getModel().setCurrentRoute(route);

        if (event.isNew()) {

            Route previousRoute = event.getPreviousRoute();
            if (previousRoute != null) {
                int previousIndex = getModel().indexOfRoutes(previousRoute);
                getModel().addRoute(previousIndex + 1, route);

            } else {
                getModel().addRoute(route);
            }
        }
    }

    @Override
    public void onNextTransectChanged(TransectFlight nextTransect) {
        getModel().setNextTransect(nextTransect);
    }

    @Override
    public void onStateChanged(FlightState state) {
        getModel().setFlightState(state);
    }

    @Override
    public void onObservationAdded(ObservationEvent event) {
        Observation observation = event.getObservation();
        getModel().addObservation(observation);

        setObservationGeoPointInMap(observation, event.getGeoPoint());
    }

    @Override
    public void onTimerChanged(String timer, int value) {
        switch (timer) {
            case "circleBack" :
                getModel().setCircleBackTimer(value);
                break;
            case "rejoin" :
                getModel().setRejoinTimer(value);
                break;
            case "deadtime" :
                getModel().setDeadtimeTimer(value);
                break;
        }
    }

    public void setObservationGeoPointInMap(Observation observation, GeoPoint geoPoint) {
        observationLayer.putGeoPoint(observation, geoPoint, Observations.toLabel(observation));
    }

    public void setObservationLabelInMap(Observation observation) {
        observationLayer.setLabel(observation, Observations.toLabel(observation));
    }

    public void selectStrate(StrateModel strate) {

        if (log.isDebugEnabled()) {
            log.debug(String.format("Select strate %s",
                                       strate.getSource() == null
                                       ? "all" : strate.getSource().getCode())
            );
        }

        StrateModel oldSelection = getModel().getCurrentStrate();

        if (oldSelection != null) {
            for (TransectModel transect : oldSelection.getTransects()) {
                transect.setInCurrentStrate(false);
            }
        }

        for (TransectModel transect : strate.getTransects()) {
            transect.setInCurrentStrate(true);
        }

        getModel().setCurrentStrate(strate);

        ui.getTransectListModel().setStrateModel(strate);
    }

    public void showNewTransect() {

        // The first strate contains all transect and doesn't have any strate,
        // we remove it because we don't need it in TransectUI
        StrateModel allStrate = getModel().getStrates().get(0);

        List<StrateModel> referential = Lists.newArrayList(getModel().getStrates());
        referential.remove(allStrate);

        StrateModel currentStrate = getModel().getCurrentStrate();
        if (allStrate.equals(currentStrate)) {
            currentStrate = null;
        }

        transectUi.getModel().prepareCreate(referential, currentStrate);

        transectUi.getHandler().openUI();
    }

    /**
     * Add selected transects to the flight
     *
     * @see FlightService#addTransects(Flight, int, Iterable)
     */
    public void addTransects() {

        TransectListModel listModel = ui.getTransectListModel();

        Iterable<TransectModel> transects = listModel.getSelectedTransects();

        // Add elements to the flight
        if (!Iterables.isEmpty(transects)) {

            // Retrieve the last selected row from the table
            JTable table = ui.getTransectTable();
            int fromIndex = SammoaUtil.getLastSelectedTableRow(table);
            if (fromIndex == -1) {
                // Add after the last row
                fromIndex = table.getRowCount();

            } else {
                // Add after the selection
                fromIndex++;
            }

            List<Transect> entities = StreamSupport.stream(transects.spliterator(), false)
                                                   .map(TransectModel.toTransect())
                                                   .collect(Collectors.toList());

            // Execute add transects to create the new transectFlights
            List<TransectFlight> transectFlights =
                    flightService.addTransects(getModel().getFlight(), fromIndex, entities);

            List<TransectFlightModel> newTransectFlights =
                    prepareTransectFlights(getModel(), transectFlights);

            getModel().addAllTransectFlights(fromIndex, newTransectFlights);
        }
    }

    public void selectTransects(ListSelectionEvent event) {
        ListSelectionModel selectionModel = (ListSelectionModel) event.getSource();

        StrateModel strateModel = getModel().getCurrentStrate();

        if (log.isDebugEnabled()) {
            log.debug(String.format("Select transects from strate %s",
                                       strateModel.getSource() == null
                                       ? "all" : strateModel.getSource().getCode())
            );
        }

        for (int i = event.getFirstIndex(); i <= event.getLastIndex() && i < strateModel.getTransects().size(); i++) {
            TransectModel model = strateModel.getTransects().get(i);
            boolean selected = selectionModel.isSelectedIndex(i);
            model.setSelectedInCurrentStrate(selected);
        }
        getModel().setTransectSelectionExists(!selectionModel.isSelectionEmpty());
    }

    public void selectTransectFlights(ListSelectionEvent event) {
        ListSelectionModel selectionModel = (ListSelectionModel) event.getSource();

        if (log.isDebugEnabled()) {
            log.debug("Select transectFlights " +  event.getFirstIndex());
        }
        if (event.getFirstIndex() != -1) {

            for (int i = event.getFirstIndex(),
                         transectFlightsize = getModel().getTransectFlights().size();
                 i <= event.getLastIndex() && i < transectFlightsize; i++) {

                TransectFlightModel model = getModel().getTransectFlights().get(i);

                boolean selected = selectionModel.isSelectedIndex(i);
                model.getTransect().setSelectedInFlight(selected);
            }
        }
    }

    public void initActions() {

        putAction("start", new StartAction(ui));
        putAction("stop", new StopAction(ui));
        putAction("begin", new BeginAction(ui));
        putAction("end", new EndAction(ui));
        putAction("next", new NextAction(ui));
        putAction("rejoin", new RejoinAction(ui));
        putAction("nextTransect", new NextTransectAction(ui));
        putAction("add", new AddAction(ui));
        putAction("leftObservation", new LeftObservationAction(ui));
        putAction("centerObservation", new CenterObservationAction(ui));
        putAction("rightObservation", new RightObservationAction(ui));
        putAction("circleBack", new CircleBackAction(ui));
        putAction("deleteTransect", new DeleteTransectAction(ui));
        if (getModel().isValidationMode()) {
            putAction("validObservation", new ValidObservationAction(ui));
            putAction("validRoute", new ValidRouteAction(ui));
            putAction("validTransect", new ValidTransectAction(ui));
            putAction("validFlight", new ValidFlightAction(ui));
            putAction("startAudio", new StartAudioAction(ui));
            putAction("stopAudio", new StopAudioAction(ui));
            putAction("changeTimeZone", new ChangeTimeZoneAction(ui));
        }

        getModel().setActionMap(getActionMap());

        // Init actions from MainUI for menu
        initActions(context.getMainUIHandler().getUI());

        // Init actions from ui
        initActions(ui);
    }

    protected void putAction(String actionName, Action action) {
        getActionMap().put(actionName, action);
        KeyStroke keyStroke = context.getConfig().getShortCut(actionName);
        if (keyStroke == null) {
            if (log.isWarnEnabled()) {
                log.warn("Shortcut not found for action " + actionName);
            }
        }
        getInputMap().put(keyStroke, actionName);
    }

    /**
     * Récupère tous les composants dans l'appli et regarde s'il ont besoin d'une action.
     * Si oui, on attache l'action récupérée dans l'actionMap global.
     */
    protected void initActions(JAXXObject jaxxObject) {
        for (Object object : jaxxObject.get$objectMap().values()) {
            if (jaxxObject != object && object instanceof JAXXObject) {
                initActions((JAXXObject) object);
            }
            if (object instanceof AbstractButton) {
                AbstractButton abstractButton = (AbstractButton) object;
                String actionName = (String) abstractButton.getClientProperty("actionName");
                if (!Strings.isNullOrEmpty(actionName)) {

                    if (log.isDebugEnabled()) {
                        log.debug("attach action '" + actionName + "' to button " + abstractButton);
                    }

                    Action action = getActionMap().get(actionName);

                    if (action == null) {
                        abstractButton.setEnabled(false);

                        if (log.isDebugEnabled()) {
                            log.debug("action '" + actionName + "' is not declared, the " +
                                         "button is disabled. Declared actions: " +
                                         Arrays.toString(getActionMap().keys()));
                        }

                    } else {

                        Icon icon = abstractButton.getIcon();
                        String text = abstractButton.getText();

                        abstractButton.setAction(action);

                        // keep button decoration for text and icon
                        Boolean actionKeepButtonDecoration =
                                (Boolean) abstractButton.getClientProperty("actionKeepButtonDecoration");
                        if (BooleanUtils.isTrue(actionKeepButtonDecoration)) {

                            if (log.isDebugEnabled()) {
                                log.debug("Keep button definition. Icon=" + icon + ", Text=" + text);
                            }

                            abstractButton.setIcon(icon);
                            abstractButton.setText(text);
                        }
                    }
                }
            }
        }
    }

    protected void initMap() {

        OverlayMapPanel overlayMapPanel = ui.getMapPanel();

        MapBean mapBean = overlayMapPanel.getMapBean();

        // Set background color to blue
//        mapBean.setBackgroundColor(new Color(0x99b3cc));

        // Set the map's center
        // TODO-fdesbois-2012-06-27 : will depends on the region coordinates
        // Be careful with the computing of negative coordinates : ask Benjamin, he knows
        mapBean.setCenter(new LatLonPoint.Double(46.555, 2.191));

        // Set the map's scale
        mapBean.setScale(20000000f);

        MapHandler mapHandler = overlayMapPanel.getMapHandler();

        // Boilerplate code pour avoir, sur la carte, la barre d'outil
        // la barre de zoom, le déplacement par la souris...
        mapHandler.add(new LayerHandler());
        mapHandler.add(new EmbeddedScaleDisplayPanel());
        mapHandler.add(new MouseDelegator());
        mapHandler.add(new OMMouseMode());
        mapHandler.add(new ToolPanel());
        EmbeddedNavPanel embeddedNavPanel = mapHandler.get(EmbeddedNavPanel.class);
        embeddedNavPanel.setSemiTransparency(1);
        embeddedNavPanel.setMinimumTransparency(1);

        // BackgroundLayer
        SammoaConfig config = context.getConfig();
        {
            File backgroundShapeFile = config.getBackgroundShapeFile();
            if (backgroundShapeFile != null && backgroundShapeFile.exists()) {

                Properties properties = new Properties();
                properties.put("prettyName", "Political Solid");
                properties.put(DrawingAttributes.linePaintProperty, "ff000000");
                properties.put(DrawingAttributes.fillPaintProperty, "ffbdde83");
                String path = backgroundShapeFile.getAbsolutePath();
                properties.put(ShapeLayer.shapeFileProperty, path);

                ShapeLayer shapeLayer = new BufferedShapeLayer();
                shapeLayer.setProperties(properties);

                mapHandler.add(shapeLayer);
            }
        }

        String surveyId = getModel().getFlight().getSurvey().getTopiaId();
        SurveyStorage surveyStorage =
                referentialService.getSurveyStorage(surveyId);

        // StrateLayer
        {
            File file = surveyStorage.getMapFile("strates.shp");
            if (file.exists()) {

                Properties properties = new Properties();
                properties.put("prettyName", "Strates");
                properties.put(ShapeLayer.shapeFileProperty, file.toURI().getPath());
//                properties.put(EsriPlugIn.PARAM_SHP, file.toURI().getPath());

                ShapeLayer strateLayer = new BufferedShapeLayer();
                strateLayer.setProperties(properties);

//                EsriPlugIn esriPlugIn = new EsriPlugIn();
//                esriPlugIn.setProperties(properties);
//
//                PlugInLayer strateLayer = new PlugInLayer();
//                strateLayer.setPlugIn(esriPlugIn);

                mapHandler.add(strateLayer);

            } else {
                log.warn("No strate file is defined, the file must be " +
                            "defined here: " +  file.getAbsolutePath());
            }
        }

        // TransectLayer
        {
            File file = surveyStorage.getMapFile("transects.shp");
            if (file.exists()) {

                Properties properties = new Properties();
                properties.put("prettyName", "Transects");
                properties.put(EsriPlugIn.PARAM_SHP, file.toURI().getPath());
                String lineColor = PropUtils.getProperty(SammoaColors.TRANSECT_LINE_COLOR);
                properties.put(DrawingAttributes.linePaintProperty, lineColor);
                String selectColor = PropUtils.getProperty(SammoaColors.TRANSECT_SELECT_LINE_COLOR);
                properties.put(DrawingAttributes.selectPaintProperty, selectColor);

                EsriPlugIn esriPlugIn = new EsriPlugIn();
                esriPlugIn.setProperties(properties);

                TransectLayer transectLayer = new TransectLayer();
                transectLayer.setPlugIn(esriPlugIn);
                transectLayer.init(getModel());

                mapHandler.add(transectLayer);

            } else {
                log.warn("No transect file is defined, the file must be " +
                            "defined here: " +  file.getAbsolutePath());
            }
        }

        // FlightLayer (used also for GPS tracking)
        {
            Properties properties = new Properties();
            String lineColor = PropUtils.getProperty(SammoaColors.FLIGHT_TRACKING_LINE_COLOR);
            properties.put(DrawingAttributes.linePaintProperty, lineColor);

            flightLayer = new LineGeoPointLayer();
            flightLayer.setProperties(properties);
            flightLayer.setGeoPoints(getModel().getGeoPoints());

            mapHandler.add(flightLayer);
        }

        // ObservationLayer
        {
            Properties properties = new Properties();
            properties.put(DrawingAttributes.PointOvalProperty, "true");

            observationLayer = new SimpleGeoPointLayer();
            observationLayer.setProperties(properties);
            observationLayer.setGeoPoints(
                    getModel().getObservations(),
                    Observations.toGeoPoint(getModel().getGeoPoints()),
                    Observations.toLabel()
            );

            mapHandler.add(observationLayer);
        }

        // InformationDelegator with JCheckBox for mapFollow property
        {
            InformationDelegator informationDelegator = new InformationDelegator();
            informationDelegator.setShowLights(false);

            // add combo box to informationDelegator
            JCheckBox followMapCheckBox = new JCheckBox(t("sammoa.map.mapFollow"));
            followMapCheckBox.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    getModel().setMapFollow(e.getStateChange() == ItemEvent.SELECTED);
                }
            });
            informationDelegator.add(followMapCheckBox);

            mapHandler.add(informationDelegator);
        }
    }

    protected <T extends DeviceManager> T openDevice(Class<T> deviceManagerClass) {

        try {
            flightController.openDeviceManager(deviceManagerClass);

        } catch (DeviceTechnicalException ex) {
            if (log.isErrorEnabled()) {
                log.error("Error on " + deviceManagerClass.getSimpleName() + " init", ex);
            }
            SammoaUtil.showErrorMessage(ui, ex.getMessageWithCause());
            SammoaUtil.updateBusyState(context.getMainUIHandler().getUI(), true);
        }
        return flightController.getDeviceManager(deviceManagerClass);
    }

    // ---------- VALIDATION MODE METHODS -------- //

    protected void fireRoutesUpdated(TransectFlightModel transectFlight, boolean scrollToFirst) {

        Iterable<Route> routes = Iterables.filter(
                getModel().getRoutes(), Routes.withTransectFlight(transectFlight.getSource()));

        SammoaUtil.fireTableRowsUpdated(ui.getEffortPanel().getRouteTable(),
                                        getModel().getRoutes(),
                                        routes,
                                        scrollToFirst);
    }

    /**
     * Select the {@code route} that matches the given {@code observation}.
     * An adjusting flag is used to avoid loop when the route try to
     * ensure the current observation with {@link #selectObservationFromRoute(Route)}
     *
     * @param observation Observation
     * @see Routes#findWithObservation(Iterable, Observation, boolean)
     */
    protected void selectRouteByObservation(Observation observation) {

        if (!routeSelectionIsAdjusting) {

            routeSelectionIsAdjusting = true;

            Route route = Routes.findWithObservation(
                    getModel().getRoutes(), observation, true);

            int routeIndex = getModel().indexOfRoutes(route);
            SammoaUtil.selectTableRow(ui.getEffortPanel().getRouteTable(), routeIndex);

            routeSelectionIsAdjusting = false;
        }
    }

    /**
     * Unselect the current observation if not in given {@code route}.
     * An adjusting flag is used to avoid loop when the observation try to
     * select the matching route with {@link #selectRouteByObservation(Observation)}
     *
     * @param route Route could be null
     * @see Observations#inRoute(Observation, Route, Iterable, boolean)
     */
    protected void selectObservationFromRoute(Route route) {

        if (!routeSelectionIsAdjusting) {

            routeSelectionIsAdjusting = true;

            if (route == null) {
                SammoaUtil.unselectAll(ui.getEffortPanel().getObservationTable());

            } else {

                // Remove observationEditBean if not in route
                Observation previousObservation = getModel().getObservationEditBean();
                if (previousObservation != null
                    && !Observations.inRoute(previousObservation, route, getModel().getRoutes(), true)) {

                    SammoaUtil.unselectAll(ui.getEffortPanel().getObservationTable());
                }
            }

            routeSelectionIsAdjusting = false;
        }
    }

    /**
     * Select the {@code transectFlight} that matches the given {@code route}.
     * An adjusting flag is used to avoid loop when the transectFlight try to
     * select its first route with {@link #selectRouteFromTransectFlight(TransectFlightModel)}
     *
     * @param route Route that contains the {@code transectFlight}
     */
    protected void selectTransectFlightByRoute(Route route) {

        if (!transectFlightSelectionIsAdjusting) {

            transectFlightSelectionIsAdjusting = true;

            TransectFlight transectFlight = route.getTransectFlight();

            // for TRANSIT/CIRCLE_BACK route, we unselect all the table
            if (transectFlight == null) {
                SammoaUtil.unselectAll(ui.getTransectTable());

            } else {

                int index = TransectFlightModel.indexOfTransectFlight(
                        getModel().getTransectFlights(), transectFlight);
                SammoaUtil.selectTableRow(ui.getTransectTable(), index);
            }

            transectFlightSelectionIsAdjusting = false;
        }
    }

    /**
     * Select the first route of the given {@code transectFlight}. If the
     * {@code transectFlight} doesn't have any route, all the routeTable is
     * unselected.
     * An adjusting flag is used to avoid loop when the route try to
     * select the matching transectFlight with {@link #selectTransectFlightByRoute(Route)}
     *
     * @param transectFlight TransectFlightModel
     * @see Routes#withTransectFlight(Route, TransectFlight)
     */
    protected void selectRouteFromTransectFlight(TransectFlightModel transectFlight) {

        if (!transectFlightSelectionIsAdjusting) {

            transectFlightSelectionIsAdjusting = true;

            Route route = getModel().getRoutes().stream().filter(Routes.withTransectFlight(transectFlight.getSource())).findFirst().orElse(null);

            if (route == null) {
                SammoaUtil.unselectAll(ui.getEffortPanel().getRouteTable());

            } else {

                int routeIndex = getModel().indexOfRoutes(route);
                SammoaUtil.selectTableRow(ui.getEffortPanel().getRouteTable(), routeIndex);
            }

            transectFlightSelectionIsAdjusting = false;
        }
    }

    protected void selectGeoPointFromTime(Date time) {

        Optional<GeoPoint> geoPointOptional = getModel().getGeoPoints().stream().filter(GeoPoints.afterDate(time)).findFirst();

        if (geoPointOptional.isPresent()) {

            int geoPointIndex = getModel().indexOfGeoPoint(geoPointOptional.get());
            SammoaUtil.selectTableRow(ui.getEffortPanel().getGeoPointTable(), geoPointIndex);

        } else {
            SammoaUtil.unselectAll(ui.getEffortPanel().getGeoPointTable());
        }
    }

    protected GeoPoint ensureGeoPoint(Date date) {

        List<GeoPoint> modelPoints = getModel().getGeoPoints();
        GeoPoint result = modelPoints.stream().filter(GeoPoints.withDate(date)::apply).findFirst().orElse(null);

        if (result == null) {
            FlightService service = context.getService(FlightService.class);
            result = service.getGeoPoint(getModel().getFlight(), date);
            modelPoints.add(result);
        }
        return result;
    }

    protected void setAudioReaderPositionDate(Date date) {

        if (getModel().isValidationMode() && getFlightController() instanceof FlightControllerValidation) {

            ((FlightControllerValidation) getFlightController()).setAudioReaderPositionDate(date);

        }

    }

    private void onTransectFlightChangedForValidation(TransectFlightModel oldValue,
                                                      TransectFlightModel newValue) {

        if (oldValue != null) {
            oldValue.removePropertyChangeListener(transectFlightChangeListener);

            // Fire routes table (matching routes)
            fireRoutesUpdated(oldValue, false);
        }

        if (newValue != null) {
            newValue.addPropertyChangeListener(transectFlightChangeListener);

            // Fire routes table (matching routes)
            fireRoutesUpdated(newValue, true);

            // Select the first route
            selectRouteFromTransectFlight(newValue);

            // Reset the ValidModel
            getValidModel().resetTransectFlightValid(newValue.getSource());
        }
    }

    private void onRouteChangedForValidation(Route oldValue,
                                             Route newValue) {

        getFlightController().setCurrentRoute(newValue);

        if (oldValue != null) {
            oldValue.removePropertyChangeListener(routeChangeListener);
        }

        // unselect the observation if not matching the route
        selectObservationFromRoute(newValue);

        if (newValue != null) {
            newValue.addPropertyChangeListener(routeChangeListener);

            // Set audio position
            setAudioReaderPositionDate(newValue.getBeginTime());

            // Select the matching TransectFlight
            selectTransectFlightByRoute(newValue);

            // Reset the ValidModel
            getValidModel().resetRouteValid(newValue);

            // set geo point
            selectGeoPointFromTime(newValue.getBeginTime());
        }
    }

    private void onObservationChangedForValidation(Observation oldValue,
                                                   Observation newValue) {

        if (oldValue != null) {
            oldValue.removePropertyChangeListener(observationChangeListener);
        }

        if (newValue != null) {
            newValue.addPropertyChangeListener(observationChangeListener);

            // Select the matching route
            selectRouteByObservation(newValue);

            // Set audio position
            setAudioReaderPositionDate(newValue.getObservationTime());

            // Reset the ValidModel
            getValidModel().resetObservationValid(newValue);

            // set geo point
            selectGeoPointFromTime(newValue.getObservationTime());
        }
    }

    private PropertyChangeListener observationChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            Observation observation = (Observation) evt.getSource();

            // Only on observationTime changed
            if (evt.getPropertyName().equals(Observation.PROPERTY_OBSERVATION_TIME)) {

                GeoPoint geoPoint = ensureGeoPoint((Date) evt.getNewValue());

                setObservationGeoPointInMap(observation, geoPoint);

                selectRouteByObservation(observation);
            }

            // Reset ValidModel
            getValidModel().resetObservationValid(observation);
        }
    };

    private PropertyChangeListener routeChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            Route route = (Route) evt.getSource();

            // Only on beginTime changed
            if (evt.getPropertyName().equals(Route.PROPERTY_BEGIN_TIME)) {
                ensureGeoPoint((Date) evt.getNewValue());
            }

            // Reset ValidModel
            getValidModel().resetRouteValid(route);
        }
    };

    private PropertyChangeListener transectFlightChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            TransectFlightModel transectFlight = (TransectFlightModel) evt.getSource();

            // Reset ValidModel
            getValidModel().resetTransectFlightValid(transectFlight.getSource());
        }
    };

    // ---------- COMMON LISTENERS -------- //

    protected PropertyChangeListener flightObserversListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            // Add binding for Flight observers to update comboboxes references
            // based on flightObserverForPositions
            Flight flight = (Flight) evt.getSource();
            List<Observer> observers = flightService.getFlightObserverForPositions(flight);
            getModel().setFlightObserverForPositions(observers);

            // Refresh all the table if some observer is removed (index = old collection size)
            if (evt instanceof  IndexedPropertyChangeEvent) {
                int oldSize = ((IndexedPropertyChangeEvent) evt).getIndex();
                int currentSize = getModel().getFlight().sizeObserver();

                if (currentSize < oldSize) {
                    ui.getTransectTableModel().fireTableDataChanged();
                }
            }
        }
    };

    protected PropertyChangeListener currentRouteListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            Route oldValue = (Route) evt.getOldValue();
            if (oldValue != null) {
                ui.getTransectTableModel().fireTableRowUpdated(oldValue.getTransectFlight());
            }
            Route newValue = (Route) evt.getNewValue();
            if (newValue != null) {
                ui.getTransectTableModel().fireTableRowUpdated(newValue.getTransectFlight());
            }
        }
    };

    protected PropertyChangeListener nextTransectListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            ui.getTransectTableModel().fireTableRowUpdated((TransectFlight) evt.getOldValue());
            ui.getTransectTableModel().fireTableRowUpdated((TransectFlight) evt.getNewValue());
        }
    };

    protected PropertyChangeListener removeTransectListener = new PropertyChangeListener() {

        // Update transectRealNbTimes on transectFlight removal

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            if (evt instanceof IndexedPropertyChangeEvent) {
                int index = ((IndexedPropertyChangeEvent) evt).getIndex();

                // REMOVE case
                if (SammoaUtil.isCollectionRemoveEvent(evt)) {

                    List<TransectFlightModel> oldList =
                            (List<TransectFlightModel>) evt.getOldValue();

                    TransectFlightModel model = oldList.get(index);

                    TransectModel transectModel = model.getTransect();
                    FlightService service = context.getService(FlightService.class);
                    Long value = service.getTransectRealNbTimes(transectModel.getSource());

                    int realNbTimes = MoreObjects.firstNonNull(value, 0).intValue();
                    transectModel.setRealNbTimes(realNbTimes);
                }
            }
        }
    };

    protected PropertyChangeListener transectFlightListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            String propertyName = evt.getPropertyName();

            // Listen index to fire the table
            if (TransectFlightModel.PROPERTY_INDEX.equals(propertyName)) {

                int min = Math.min((Integer) evt.getOldValue(), (Integer) evt.getNewValue());
                int max = Math.max((Integer) evt.getOldValue(), (Integer) evt.getNewValue());

                ui.getTransectTableModel().fireTableRowsUpdated(min, max);

                // Listen deleted to update transectModel inFlight property
            } else if (TransectFlightModel.PROPERTY_DELETED.equals(propertyName)) {

                boolean deleted = (Boolean) evt.getNewValue();
                TransectFlightModel source = (TransectFlightModel) evt.getSource();
                source.getTransect().setInFlight(!deleted);
            }
        }
    };

    protected DeviceStateListener deviceStateListener = new DeviceStateListener() {

        @Override
        public void stateChanged(DeviceStateEvent event) {
            DeviceTechnicalException error = event.getError();
            if (event.getOldValue() != DeviceState.ERROR &&
                event.getNewValue() == DeviceState.ERROR &&
                    error != null) {

                if (log.isErrorEnabled()) {
                    log.error(error.getMessageWithCause(), error);
                }
                context.setStatusMessage(error.getMessageWithCause());
            }
        }
    };

    protected GpsLocationListener gpsLocationListener = new GpsLocationListener() {

        @Override
        public void locationChanged(GpsLocationEvent event) {

            GeoPoint location = event.getNewValue();

            if (!GeoPoints.isCoordinatesEmpty(location)) {

                // Add the GeoPoint to the layer
                if (flightLayer != null) {
                    flightLayer.addGeoPoint(location);
                }

                // Center the map on the location if mapFollow is activated
                if (getModel() != null && getModel().isMapFollow()) {
                    MapBean mapBean = ui.getMapPanel().getMapBean();
                    LatLonPoint center = new LatLonPoint.Double(
                            location.getLatitude(), location.getLongitude());
                    mapBean.setCenter(center);
                }
            }
        }
    };

    protected ComponentListener transectUIListener = new ComponentListener() {

        @Override
        public void componentResized(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentShown(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentHidden(ComponentEvent e) {

            // On hide : retrieve the region saved and reset the region combo

            String transectId = transectUi.getModel().getId();

            if (transectId == null) {
                // nothing to do, no creation is done

            } else {

                if (log.isInfoEnabled()) {
                    log.info(String.format("Retrieve from TransectUI the id = %s", transectId));
                }

                Transect transectBean = referentialService.getTransect(transectId);

                StrateModel strate = Iterables.find(
                        getModel().getStrates(), StrateModel.withStrate(transectBean.getStrate()));

                TransectModel transect = new TransectModel(transectBean);

                strate.getTransects().add(transect);
                getModel().getStrateAll().getTransects().add(transect);

                ui.getStrateCombobox().setSelectedItem(strate);

                ui.getTransectList().ensureIndexIsVisible(strate.getTransects().size() - 1);
            }
        }
    };

    public void reloadColumnTableOrder() {
        transectsColumnModelStorage.loadColumnsOrder();

        EffortPanelHandler effortPanelHandler = ui.getEffortPanel().getHandler();
        effortPanelHandler.reloadColumnTableOrder();
    }

    private static class TransectCrossingNumberCellRenderer implements TableCellRenderer {

        protected TableCellRenderer delegate;

        protected TransectTableModel model;

        public TransectCrossingNumberCellRenderer(TableCellRenderer delegate,
                                                  TransectTableModel model) {
            this.delegate = delegate;
            this.model = model;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            TransectFlightModel transectFlight = model.getRow(table.convertRowIndexToModel(row));
            TransectModel transect = transectFlight.getTransect();

            int crossingNumber = transectFlight.getCrossingNumber();
            int realNbTimes = transect.getRealNbTimes();

            String newValue = crossingNumber + " (" + realNbTimes + ")";

            return delegate.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
        }
    }
}
