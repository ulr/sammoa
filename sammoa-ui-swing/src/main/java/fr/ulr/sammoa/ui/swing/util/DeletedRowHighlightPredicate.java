package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Validable;

import java.util.List;

/**
 * Created: 22/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DeletedRowHighlightPredicate extends AbstractRowHighlightPredicate<Validable> {

    protected List<? extends Validable> model;

    public DeletedRowHighlightPredicate(List<? extends Validable> model) {
        this.model = model;
    }

    @Override
    protected boolean isHighlighted(Validable validable) {
        return validable.isDeleted();
    }

    @Override
    protected Validable getValueAt(int rowIndex) {
        return model.get(rowIndex);
    }
}
