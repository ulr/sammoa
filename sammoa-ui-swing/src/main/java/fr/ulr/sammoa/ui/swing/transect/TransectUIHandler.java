/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.transect;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.flight.StrateModel;
import fr.ulr.sammoa.ui.swing.flight.TransectModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableRenderer;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFrame;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 04/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @since 0.2
 */
public class TransectUIHandler implements SammoaUIHandler {

    private static final Log log =
            LogFactory.getLog(TransectUIHandler.class);

    protected final SammoaUIContext context;

    protected final TransectUI ui;

    private final ReferentialService referentialService;

    public TransectUIHandler(SammoaUIContext context,
                             TransectUI ui) {
        this.context = context;
        this.ui = ui;
        this.referentialService = context.getService(ReferentialService.class);
    }

    protected TransectUIModel getModel() {
        return ui.getModel();
    }

    @Override
    public void beforeInitUI() {

        TransectUIModel model = new TransectUIModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInitUI() {

        UIDecoratorService decoratorService =
                context.getService(UIDecoratorService.class);

        ui.getTransectStrateComboBox().setRenderer(
                decoratorService.newListCellRender(StrateModel.class));
        SwingValidatorUtil.installUI(ui.getErrorTable(),
                                     new SwingValidatorMessageTableRenderer());
    }

    @Override
    public void onCloseUI() {
        ui.setVisible(false);
//        context.getSwingSession().save();
    }

    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("Open TransectUI ");
        }

        JFrame parent = ui.getParentContainer(JFrame.class);

        SammoaUtil.openInDialog(ui, parent,
                                t("sammoa.dialog.title.transect"),
                                ui.getCloseAction());

        StrateModel defaultStrate = getModel().getStrate();

        SwingUtil.fillComboBox(ui.getTransectStrateComboBox(),
                               getModel().getStrateReferential(),
                               defaultStrate);

        if (defaultStrate != null) {
            selectStrate(defaultStrate);
        }

        ui.setVisible(true);
//        context.getSwingSession().add(ui);
    }

    public void selectStrate(StrateModel strate) {

        if (strate == null) {
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("Select the strate " +  strate.getSource().getCode());
        }

        getModel().setStrate(strate);

        // Set the transect referential for validation
        List<Transect> transects = Lists.transform(strate.getTransects(),
                                                   TransectModel.toTransect());
        getModel().setTransectReferential(transects);

        // Prepare the iteration for the new element
        // The new element name will have : {strateCode}/{lastNumber + 1}, ex: C1/36
        Transect lastTransect = Iterables.getLast(transects);

        String[] split = lastTransect.getName().split("/");

        int lastNumber = Integer.parseInt(split[1]);
        int newNumber = lastNumber + 1;

        String strateCode = strate.getSource().getCode();

        String name = strateCode + "/" + newNumber;

        if (log.isInfoEnabled()) {
            log.info("Use name " + name + " for the new transect");
        }

        getModel().setName(name);
    }

    public void createTransect() {

        try {
            boolean success = true;

            // Prepare new instance
            Transect transect = getModel().newBean();

            // Save the entity
            String transectId = referentialService.createTransect(transect);
            getModel().setId(transectId);

            // Close on success
            if (success) {
                ui.getCloseButton().doClick();
            }

        } catch (IllegalArgumentException e) {
            ErrorDialogUI.showError(e);
        }
    }
}
