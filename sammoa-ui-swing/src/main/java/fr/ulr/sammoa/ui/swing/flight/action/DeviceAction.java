/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.action;

import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import jaxx.runtime.JAXXContext;

import javax.swing.Icon;

/**
 * Created: 03/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public abstract class DeviceAction<T extends DeviceManager> extends BaseFlightAction {

    private static final long serialVersionUID = 1L;

    protected T device;

    protected DeviceAction(Icon icon, JAXXContext context) {
        super(icon, context);
    }

    protected DeviceAction(String name, JAXXContext context) {
        super(name, context);
    }

    protected T getDevice() {
        if (device == null) {
            device = getFlightController().getDeviceManager(getDeviceClass());
            device.addDeviceStateListener(new DeviceStateListener() {

                @Override
                public void stateChanged(DeviceStateEvent event) {
                    setEnabled(checkEnabled());
                }
            });
        }
        return device;
    }

    protected boolean hasDevice() {
        return device != null || getFlightController().getDeviceManager(getDeviceClass()) != null;
    }

    protected abstract Class<T> getDeviceClass();
}
