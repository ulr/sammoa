/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.util;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.appender.rolling.DefaultRolloverStrategy;
import org.apache.logging.log4j.core.appender.rolling.TimeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.File;

/**
 * Created: 31/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class LogFileInitializer {

    private static final String LOG_PATTERN = "%date %level [%thread] %logger [:%line] : %msg%n";

    private String fileName;

    public LogFileInitializer(File file) {
        Preconditions.checkNotNull(file);
        this.fileName = file.getAbsolutePath();
    }

    public void addFileAppender() {

        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final Configuration config = ctx.getConfiguration();
        PatternLayout layout = PatternLayout.newBuilder()
                .withPattern(LOG_PATTERN)
                .withConfiguration(config)
                .build();

        TimeBasedTriggeringPolicy policy = TimeBasedTriggeringPolicy.createPolicy("1", null);
  
        DefaultRolloverStrategy strategy = DefaultRolloverStrategy.createStrategy(
                "10",
                null,
                null,
                null,
                null,
                true,
                config);

        Appender appender = RollingFileAppender.newBuilder()
                .withName("File")
                .withFileName(fileName)
                .withFilePattern(getFilePattern())
                .withPolicy(policy)
                .withStrategy(strategy)
                .withLayout(layout)
                .setConfiguration(config)
                .build();

        appender.start();
        config.addAppender(appender);
        LoggerConfig loggerConfig = config.getLoggerConfig("fr.ulr.sammoa");
        loggerConfig.addAppender(appender, null, null);
        ctx.updateLoggers();
    }

    protected String getFilePattern() {

        String ext = Files.getFileExtension(fileName);
        String name = fileName;
        if (ext.isEmpty()) {
            ext = "log";

        } else {
            name = name.replace("." + ext, "");
        }


        return name + "-%d{yyyy-MM-dd}." + ext;
    }
}
