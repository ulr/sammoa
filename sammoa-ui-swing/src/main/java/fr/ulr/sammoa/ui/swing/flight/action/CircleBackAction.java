/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.action;

import com.google.common.collect.Iterables;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import jaxx.runtime.JAXXContext;
import org.nuiton.util.Resource;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 03/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class CircleBackAction extends BaseFlightAction {

    private static final long serialVersionUID = 1L;

    public static final String CLIENT_PROPERTY_OBSERVATION = "observation";

    protected static final ImageIcon CIRCLE_BACK_ICON = Resource.getIcon("/icons/undo.png");

    public CircleBackAction(JAXXContext context) {
        super(CIRCLE_BACK_ICON, context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.circleBack.tip"));
        bindModelProperty(FlightUIModel.PROPERTY_CURRENT_ROUTE, false);
        bindModelProperty(FlightUIModel.PROPERTY_CIRCLE_BACK_ENABLED, false);
        bindModelProperty(FlightUIModel.PROPERTY_CIRCLE_BACK_TIMER, false);
        bindModelProperty(FlightUIModel.PROPERTY_DEADTIME_TIMER, false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Observation observation = null;
        Object source = e.getSource();
        if (source instanceof JComponent) {
            JComponent comp = (JComponent) source;
            observation = (Observation) comp.getClientProperty(CLIENT_PROPERTY_OBSERVATION);
        }

        if (observation == null) {
            List<Observation> obsList = getModel().getObservations();
            observation = Iterables.getLast(obsList);
        }

        getFlightController().circleBack(observation);
    }

    @Override
    protected boolean checkEnabled() {
        boolean result = getModel().isCircleBackEnabled();
        if (result) {
            Route route = getModel().getCurrentRoute();
            if (route != null) {
                result = route.getRouteType() != RouteType.TRANSIT;
            }

            //Check we are in the circleBack time frame
            result = result && getModel().getCircleBackTimer() != 0 && getModel().getDeadtimeTimer() == 0;
        }
        return result;
    }
}
