package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Maps;

import javax.swing.AbstractButton;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.Map;

/**
 * Created: 20/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ButtonActionTableCellEditorRenderer extends AbstractCellEditor
        implements TableCellRenderer, TableCellEditor {

    protected String actionPropertyName;

    protected Supplier<Action> actionSupplier;

    protected Predicate<Object> enabledPredicate;

    protected Map<Object, AbstractButton> cache;

    public ButtonActionTableCellEditorRenderer(String actionPropertyName,
                                               Supplier<Action> actionSupplier,
                                               Predicate<Object> enabledPredicate) {

        this.actionPropertyName = actionPropertyName;
        this.actionSupplier = actionSupplier;
        this.enabledPredicate = enabledPredicate;
        this.cache = Maps.newHashMap();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        Component result = getButton(value);
        if (enabledPredicate != null) {
            result.setEnabled(enabledPredicate.apply(value));
        }
        return result;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component result = getButton(value);
        if (enabledPredicate != null) {
            result.setEnabled(enabledPredicate.apply(value));
        }
        return result;
    }

    protected AbstractButton getButton(Object bean) {
        AbstractButton result = cache.get(bean);
        if (result == null) {

            result = new JButton();
            result.putClientProperty(actionPropertyName, bean);
            result.setAction(actionSupplier.get());
            cache.put(bean, result);
        }
        return result;
    }
}
