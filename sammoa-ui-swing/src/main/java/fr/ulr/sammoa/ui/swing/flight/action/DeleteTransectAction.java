package fr.ulr.sammoa.ui.swing.flight.action;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.TransectFlightModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 03/09/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DeleteTransectAction extends BaseFlightAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DeleteTransectAction.class);

    public static final String CLIENT_PROPERTY_TRANSECT_FLIGHT = "transectFlight";

    public DeleteTransectAction(JAXXContext context) {
        super((String) null, context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.deleteTransect.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source instanceof JComponent) {
            JComponent comp = (JComponent) source;

            final TransectFlightModel transectFlight =
                    (TransectFlightModel) comp.getClientProperty(CLIENT_PROPERTY_TRANSECT_FLIGHT);

            if (log.isDebugEnabled()) {
                log.debug("Change transect " + transectFlight.getSource().getTransect().getName() + " deleted flag to " + !transectFlight.isDeleted());
            }

            transectFlight.setDeleted(!transectFlight.isDeleted());

            if (transectFlight.isDeleted()) {
                boolean askDeleteTransect = false;
                String message = null;

                // #1372: If flight isn't started, we propose to delete definitely the transect
                if (FlightState.WAITING == getModel().getFlightState()) {

                    askDeleteTransect = true;
                    message = t("sammoa.confirmDialog.deleteTransect.waitingFlight.message");

                } else if (getModel().isValidationMode()) {

                    boolean transectFlightNotUsed = getModel().getRoutes()
                            .stream()
                            .noneMatch(r -> transectFlight.equals(r.getTransectFlight()));
                    if (transectFlightNotUsed) {
                        askDeleteTransect = true;
                        message = t("sammoa.confirmDialog.deleteTransect.validation.message");
                    }
                }

                if (askDeleteTransect && SammoaUtil.askQuestion((FlightUI) context, message)) {

                    FlightService service =
                            getSammoaUIContext().getService(FlightService.class);
                    service.deleteTransectFlight(getModel().getFlight(), transectFlight.getSource());

                    if (log.isDebugEnabled()) {
                        log.debug("Delete transect " + transectFlight.getSource().getTransect().getName());
                    }

                    // Update model
                    int index = getModel().indexOfTransectFlights(transectFlight);
                    getModel().removeTransectFlight(index);
                }
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        return true;
    }
}
