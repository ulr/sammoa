package fr.ulr.sammoa.ui.swing.flight.effort;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/**
 * @author Sylvain Bavencoff <bavencoff@codelutin.com>
 */
public class GeoPointTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected FlightUIModel flightUIModel;

    public GeoPointTableModel(FlightUIModel flightUIModel) {
        this.flightUIModel = flightUIModel;
    }

    @Override
    public int getRowCount() {
        return getBean().size();
    }

    @Override
    public int getColumnCount() {
        return GeoPointColumn.values().length;
    }

    @Override
    public String getColumnName(int column) {
        GeoPointColumn geoPointColumn = GeoPointColumn.valueOf(column);
        return geoPointColumn.getColumnName();
    }

    @Override
    public Class<?> getColumnClass(int column) {
        GeoPointColumn geoPointColumn = GeoPointColumn.valueOf(column);
        return geoPointColumn.getType();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        GeoPoint geoPoint = getBean(row);
        GeoPointColumn geoPointColumn = GeoPointColumn.valueOf(column);
        boolean result = geoPointColumn.isEditable(geoPoint, flightUIModel.isValidationMode());
        return result;
    }

    @Override
    public Object getValueAt(int row, int column) {
        GeoPoint geoPoint = getBean(row);
        GeoPointColumn geoPointColumn = GeoPointColumn.valueOf(column);
        Object result = geoPointColumn.getValue(geoPoint);
        return result;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        GeoPoint geoPoint = getBean(row);
        GeoPointColumn geoPointColumn = GeoPointColumn.valueOf(column);
        geoPointColumn.setValue(geoPoint, aValue);
        fireTableRowsUpdated(row, row);
    }

    public int getBeanIndex(GeoPoint bean) {
        int row = getBean().indexOf(bean);
        return row;
    }

    public List<GeoPoint> getBean() {
        return flightUIModel.getGeoPoints();
    }

    public GeoPoint getBean(int row) {
        SwingUtil.ensureRowIndex(this, row);
        GeoPoint bean = getBean().get(row);
        return bean;
    }

    public Pair<Integer, Integer> getCell(GeoPoint bean, String fieldName) {

        int row = getBeanIndex(bean);
        int col = GeoPointColumn.getValueFromFieldName(fieldName).ordinal();

        Pair<Integer, Integer> cell = Pair.of(row, col);
        return cell;
    }

    public FlightUIModel getFlightUIModel() {
        return flightUIModel;
    }


    public enum GeoPointColumn {

        RECORD_TIME(false, Date.class, GeoPoint.PROPERTY_RECORD_TIME),
        LATITUDE(false, double.class, GeoPoint.PROPERTY_LATITUDE),
        LONGITUDE(false, double.class, GeoPoint.PROPERTY_LONGITUDE),
        ALTITUDE(false, double.class, GeoPoint.PROPERTY_ALTITUDE),
        SPEED(false, double.class, GeoPoint.PROPERTY_SPEED),
        CAPTURE_DELAY(false, int.class, GeoPoint.PROPERTY_CAPTURE_DELAY);

        private boolean editable;

        private String[] beanProperties;

        private Class<?> type;

        private final String columnName;

        GeoPointColumn(boolean editable,
                               Class<?> type,
                               String... beanProperties) {
            this.editable = editable;
            this.type = type;
            this.beanProperties = beanProperties;
            this.columnName = beanProperties[0];
        }

        public Class<?> getType() {
            return type;
        }

        public String getColumnName() {
            return columnName;
        }

        public int getColumnIndex() {
            return ordinal();
        }

        public Object getValue(GeoPoint bean) {
            Object result = SammoaUtil.getPropertyValue(bean, beanProperties);
            return result;
        }

        public void setValue(GeoPoint bean, Object value) {
            if (type.isPrimitive() && value == null) {
                // can not set a null value to a primitive field
            } else {
                SammoaUtil.setPropertyValue(bean, value, beanProperties);
            }
        }

        public boolean isEditable(GeoPoint bean, boolean validationMode) {
            boolean result = editable && validationMode;
            return result;
        }

        public static GeoPointColumn valueOf(int ordinal) {
            for (GeoPointColumn value : values()) {
                if (ordinal == value.ordinal()) {
                    return value;
                }
            }
            throw new EnumConstantNotPresentException(GeoPointColumn.class,
                    "ordinal=" + ordinal);
        }

        public static GeoPointColumn getValueFromFieldName(String fieldName) {
            GeoPointColumn result = null;
            for (GeoPointColumn value : values()) {
                if (fieldName.equals(value.columnName)) {
                    result = value;
                    break;
                }
            }
            return result;
        }


    }

}
