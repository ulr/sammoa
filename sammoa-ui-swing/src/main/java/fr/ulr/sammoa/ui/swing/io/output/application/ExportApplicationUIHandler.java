package fr.ulr.sammoa.ui.swing.io.output.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.io.BackupService;
import fr.ulr.sammoa.application.io.output.application.ExportApplicationModel;
import fr.ulr.sammoa.application.io.output.application.ExportApplicationService;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.ui.swing.SammoaScreen;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.nuiton.decorator.Decorator;
import org.nuiton.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of {@link ExportApplicationUI}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ExportApplicationUIHandler implements SammoaUIHandler {

    private static final Log log =
            LogFactory.getLog(ExportApplicationUIHandler.class);

    private final ExportApplicationUI ui;

    protected final SammoaUIContext context;

    protected final ExportApplicationService exportService;

    protected final ReferentialService referentialService;

    protected final BackupService backupService;

    protected final FlightService flightService;

    protected final UIDecoratorService decoratorService;

    protected final SammoaConfig config;

    public ExportApplicationUIHandler(SammoaUIContext context,
                                      ExportApplicationUI ui) {
        this.context = context;
        this.ui = ui;
        this.flightService = context.getService(FlightService.class);
        this.referentialService = context.getService(ReferentialService.class);
        this.exportService = context.getService(ExportApplicationService.class);
        this.decoratorService = context.getService(UIDecoratorService.class);
        this.backupService = context.getService(BackupService.class);
        this.config = context.getConfig();
    }

    @Override
    public void beforeInitUI() {

        //-- create model --//

        ExportApplicationUIModel model = new ExportApplicationUIModel();

        List<Survey> surveys = referentialService.getSurveys();
        //TODO-tchemit-2012-08-03 add an null survey (for test purpose, should be removed...)
        surveys.add(0, null);
        model.setSurveys(surveys);

        String surveyId = context.getSurveyId();

        if (surveyId != null) {

            Survey survey = referentialService.getSurvey(surveyId);
            model.setSelectedSurvey(survey);
        }

        String flightId = context.getFlightId();

        if (flightId != null) {

            Flight flight = flightService.getFlight(flightId);
            model.setSelectedFlight(flight);
        }

        //-- share model and handler in jaxx context --//
        ui.setContextValue(this);
        ui.setContextValue(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                String propertyName = evt.getPropertyName();

                if (ExportApplicationUIModel.PROPERTY_SELECTED_SURVEY.equals(propertyName)) {
                    Survey newValue = (Survey) evt.getNewValue();
                    onSelectedSurveyChanged(newValue);
                } else if (ExportApplicationUIModel.PROPERTY_FLIGHTS.equals(propertyName)) {
                    Collection<Flight> newValue = (Collection<Flight>) evt.getNewValue();
                    onFlightsChanged(newValue);
                } if (ExportApplicationUIModel.PROPERTY_SELECTED_FLIGHT.equals(propertyName)) {
                    onSelectedFlightChanged();
                } else if (ExportApplicationUIModel.PROPERTY_CALLBACK.equals(propertyName)) {
                    ExportApplicationCallbackMode newValue =
                            (ExportApplicationCallbackMode) evt.getNewValue();
                    onCallbackChanged(newValue);
                }

                if (!ExportApplicationUIModel.PROPERTY_VALID.equals(propertyName)) {

                    // validate model
                    validateModel();
                }
            }
        });
    }

    @Override
    public void afterInitUI() {

        SwingUtil.setLayerUI(ui.getBody(), ui.getBusyBlockLayerUI());

        ui.getSurveyCombobox().setRenderer(new DecoratorListCellRenderer(
                decoratorService.getDecoratorByType(Survey.class)));
        ui.getFlightCombobox().setRenderer(new DecoratorListCellRenderer(
                decoratorService.getDecoratorByType(Flight.class)));

        ExportApplicationUIModel model = getModel();

        File currentDirectory = FileUtil.getCurrentDirectory();
        String absolutePath = currentDirectory.getAbsolutePath();
        if (absolutePath.endsWith(File.separator + ".")) {
            currentDirectory = new File(
                    absolutePath.substring(0, absolutePath.length() - 2));
        }
        if (log.isInfoEnabled()) {
            log.info("Current directory to use: " + currentDirectory);
        }
        model.setExportDirectory(currentDirectory);

        String backupFilename = String.format(
                "backup-%1$td-%1$tm-%1$ty-%1$tH_%1$tM_%1$tS.zip", new Date());
        model.setBackupFilename(backupFilename);

        SwingUtil.fillComboBox(ui.getSurveyCombobox(),
                               model.getSurveys(),
                               model.getSelectedSurvey());

        onSelectedSurveyChanged(model.getSelectedSurvey());
    }

    @Override
    public void onCloseUI() {
    }

    public void exportApplication() {

        JFrame frame = ui.getParentContainer(JFrame.class);

        ExportApplicationUIModel model = getModel();

        ExportApplicationModel dataModel = model.toModel();

        boolean doExport = checkFileToExport(dataModel);

        if (!doExport) {

            // nothing to do, quit now
            return;
        }

        if (model.isBackup()) {

            String backupFilename = model.getBackupFilename();

            try {
                SammoaUtil.updateBusyState(frame.getRootPane(), true);
                ui.getBusyBlockLayerUI().setBlock(true);

                // backup sammoa data
                backupService.backupApplication(backupFilename, s-> {});
                SammoaUtil.showSuccessMessage(
                        frame,
                        t("sammoa.messageDialog.sammoa.backup.success",
                          getBackupFile(backupFilename)));
            } catch (Exception e) {
                ErrorDialogUI.showError(e);
            } finally {
                ui.getBusyBlockLayerUI().setBlock(false);
                SammoaUtil.updateBusyState(frame.getRootPane(), false);
            }
        }
        try {

            ui.getBusyBlockLayerUI().setBlock(true);
            SammoaUtil.updateBusyState(frame.getRootPane(), true);

            exportService.exportApplication(dataModel);

            SammoaUtil.showSuccessMessage(
                    frame,
                    t("sammoa.messageDialog.sammoa.export.success",
                      dataModel.getExportFile()));

            boolean removeFlightId = false;
            boolean removeCampaingId = false;

            switch (model.getCallback()) {

                case NOTHING:
                    // nothing to do
                    break;
                case REMOVE_FLIGHTS:
                    removeFlights(dataModel);
                    removeFlightId = true;
                    break;
                case REMOVE_SURVEY:
                    removeSurvey(dataModel);
                    removeCampaingId = true;
                    break;
                case REMOVE_ALL:
                    removeAllData();
                    removeCampaingId = true;
                    break;

            }

            if (removeCampaingId) {
                context.setSurveyId(null);
                removeFlightId = true;
            }

            if (removeFlightId) {
                context.setFlightId(null);
            }
            close();

        } catch (Exception e) {
            ErrorDialogUI.showError(e);
        } finally {
            ui.getBusyBlockLayerUI().setBlock(false);
            SammoaUtil.updateBusyState(frame.getRootPane(), false);
        }
    }

    public String getBackupInfo(String filename) {
        File backupFile = getBackupFile(filename == null ? "" : filename);
        String result = t("sammoa.label.exportApplication.backupInfo", backupFile);
        return result;
    }

    protected void removeFlights(ExportApplicationModel model) {
        String surveyId = model.getSurveyId();
        Iterable<String> flightIds = model.getFlightIds();
        if (log.isInfoEnabled()) {
            log.info("Will remove selected flights for survey " + surveyId + " : " + flightIds);
        }

        for (String flightId : flightIds) {
            flightService.deleteFlight(surveyId, flightId);
        }
    }

    protected void removeSurvey(ExportApplicationModel model) {
        if (log.isInfoEnabled()) {
            log.info("Will remove survey data " + model.getSurveyId());
        }

        referentialService.deleteSurvey(model.getSurveyId());
    }

    protected void removeAllData() {
        if (log.isInfoEnabled()) {
            log.info("Will remove all data from current db (create a new db...)");
        }

        referentialService.deleteAllData();
    }

    protected void validateModel() {
        boolean valid = true;

        ExportApplicationUIModel model = getModel();

        if (model.getSelectedSurvey() == null) {

            // no survey selected
            valid = false;
        } else {

            if (model.getExportDirectory() == null ||
                !model.getExportDirectory().exists()) {

                // export directory does not exists
                valid = false;
            }
        }

        if (model.isBackup()) {

            // validate that backup filename not empty and does not exist
            // in backup directory
            String backupFilename = model.getBackupFilename();
            if (Strings.isNullOrEmpty(backupFilename)) {
                valid = false;
            } else {
                File backupFile = getBackupFile(backupFilename);
                if (backupFile.exists()) {
                    valid = false;
                }
            }
        }

        model.setValid(valid);
    }

    protected File getBackupFile(String backupFilename) {
        File result = SammoaDirectories.getBackupFile(
                config.getDataDirectory(), backupFilename);
        return result;
    }

    protected boolean checkFileToExport(ExportApplicationModel dataModel) {
        boolean doExport = true;

        if (dataModel.getExportFile().exists()) {
            doExport = SammoaUtil.askQuestion(
                    ui,
                    t("sammoa.messageDialog.sammoa.exportFile.alreadyExists",
                      dataModel.getExportFile()));
        }
        return doExport;
    }

    public void close() {
        context.changeScreen(SammoaScreen.HOME);
    }

    public void chooseExportDirectory() {
        File f = SammoaUtil.chooseDirectory(
                ui,
                t("sammoa.title.choose.export.directory"),
                t("sammoa.action.choose.export.directory"),
                ui.getModel().getExportDirectory()
        );
        ui.getModel().setExportDirectory(f);
    }


    protected ExportApplicationUIModel getModel() {
        return ui.getModel();
    }

    protected void onSelectedSurveyChanged(Survey newSurvey) {

        // reset flights,...
        if (log.isInfoEnabled()) {
            Decorator<Survey> decorator =
                    decoratorService.getDecoratorByType(Survey.class);
            log.info("New selected campain: " + decorator.toString(newSurvey));
        }

        ExportApplicationUIModel model = getModel();

        List<Flight> flights;
        if (newSurvey == null) {
            flights = Collections.emptyList();
        } else {
            flights = flightService.getFlights(newSurvey);
            // add a null value to unselect flights
            flights.add(0, null);
        }
        if (log.isDebugEnabled()) {
            log.debug("New flights: " +  flights.size());
        }
        model.setFlights(flights);
        updateFileName();
    }

    protected void onFlightsChanged(Collection<Flight> flights) {

        ExportApplicationUIModel model = getModel();
        Flight selectedFlight = model.getSelectedFlight();
        if (selectedFlight != null && !flights.contains(selectedFlight)) {
            model.setSelectedFlight(null);
        }

        SwingUtil.fillComboBox(ui.getFlightCombobox(),
                               flights,
                               selectedFlight);
    }

    protected void onSelectedFlightChanged() {
        updateFileName();
    }

    protected void updateFileName() {
        String filename = "";

        ExportApplicationUIModel model = getModel();

        Survey survey = model.getSelectedSurvey();
        Flight flight = model.getSelectedFlight();

        if (survey != null) {

            if (flight != null) {

                filename = String.format("%1$s_%4$s_%2$tY_%2$tm_%2$td_FLIGHT%3$d.sammoa",
                        survey.getCode(), new Date(), flight.getFlightNumber(), flight.getSystemId());
            } else {

                filename = String.format("%1$s_%2$tY_%2$tm_%2$td.sammoa",
                        survey.getCode(), new Date());
            }

        }


        if (log.isDebugEnabled()) {
            log.debug("New filename: " +  filename);
        }

        model.setExportFilename(filename);
    }

    protected void onCallbackChanged(ExportApplicationCallbackMode callback) {

        // always force to backup if data could be lost
        getModel().setBackup(callback != ExportApplicationCallbackMode.NOTHING);
    }


}
