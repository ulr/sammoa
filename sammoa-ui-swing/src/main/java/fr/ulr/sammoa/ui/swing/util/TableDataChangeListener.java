package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.JXTable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.table.AbstractTableModel;
import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

/**
 * Created: 17/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TableDataChangeListener implements PropertyChangeListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TableDataChangeListener.class);

    public static final int NO_COLUMN_INDEX_TO_EDIT_ON_INSERT = -1;

    protected boolean enabled;

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected JXTable table;

    protected AbstractTableModel tableModel;

    protected int firstEditableColumn;

    public TableDataChangeListener(JXTable table,
                                   int firstEditableColumn) {
        this.enabled = true;
        this.table = table;
        this.tableModel = (AbstractTableModel) table.getModel();
        this.firstEditableColumn = firstEditableColumn;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        if (enabled) {
            // All data is fired
            if (evt.getOldValue() == null) {
                tableModel.fireTableDataChanged();

                // UPDATE : we use null newValue to fire update because no
                // event is fired if the size is the same and all elements equals
            } else if (evt.getNewValue() == null) {

                int oldSize = ((Collection) evt.getOldValue()).size();

                int rowIndex;
                if (evt instanceof IndexedPropertyChangeEvent) {
                    rowIndex = ((IndexedPropertyChangeEvent) evt).getIndex();
                } else {
                    rowIndex = -1;
                }

                if (rowIndex == -1) {
                    tableModel.fireTableRowsUpdated(0, oldSize - 1);

                } else {
                    tableModel.fireTableRowsUpdated(rowIndex, rowIndex);
                }

            } else {

                int oldSize = ((Collection) evt.getOldValue()).size();
                int newSize = ((Collection) evt.getNewValue()).size();

                int rowIndex;
                if (evt instanceof IndexedPropertyChangeEvent) {
                    rowIndex = ((IndexedPropertyChangeEvent) evt).getIndex();
                } else {
                    rowIndex = -1;
                }

                if (log.isDebugEnabled()) {
                    log.debug("Fire table change oldSize:" + oldSize + ", newSize:" + newSize + ", index:" + rowIndex);
                }

                // INSERT
                if (oldSize < newSize) {

                    if (rowIndex == -1) {
                        tableModel.fireTableRowsInserted(oldSize, newSize - 1);
                        if (oldSize > 0) {
                            tableModel.fireTableRowsUpdated(0, oldSize - 1);
                        }

                    } else {
                        // use diff between size for multiple insert (addAll)
                        int diff = newSize - oldSize;
                        tableModel.fireTableRowsInserted(rowIndex, rowIndex + diff - 1);

                        if (firstEditableColumn != NO_COLUMN_INDEX_TO_EDIT_ON_INSERT) {
                            int viewRowIndex = table.convertRowIndexToView(rowIndex);
                            int viewColIndex = table.convertColumnIndexToView(firstEditableColumn);
                            table.changeSelection(viewRowIndex, viewColIndex, false, false);
                            table.editCellAt(viewRowIndex, viewColIndex);
                        }
                    }

                    // DELETE
                } else if (oldSize > newSize) {

                    if (rowIndex == -1) {
                        tableModel.fireTableRowsDeleted(newSize, oldSize - 1);
                        if (newSize > 0) {
                            tableModel.fireTableRowsUpdated(0, newSize - 1);
                        }

                    } else {
                        // use diff between size for multiple delete (removeAll)
                        int diff = oldSize - newSize;
                        tableModel.fireTableRowsDeleted(rowIndex, rowIndex + diff - 1);
                    }

                    // UPDATE
                } else {

                    if (rowIndex == -1) {
                        tableModel.fireTableRowsUpdated(0, newSize - 1);

                    } else {
                        tableModel.fireTableRowsUpdated(rowIndex, rowIndex);
                    }
                }
            }
        }
    }
}
