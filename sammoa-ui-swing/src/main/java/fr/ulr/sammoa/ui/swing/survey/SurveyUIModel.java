package fr.ulr.sammoa.ui.swing.survey;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyImpl;
import fr.ulr.sammoa.persistence.Region;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Date;
import java.util.List;

/**
 * Created: 23/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SurveyUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_SURVEY_REFERENTIAL = "surveyReferential";

    public static final String PROPERTY_REGION_REFERENTIAL = "regionReferential";

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_CODE = "code";

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_REGION = "region";

    public static final String PROPERTY_BEGIN_DATE = "beginDate";

    public static final String PROPERTY_END_DATE = "endDate";

    public static final String PROPERTY_UPDATE = "update";

    private static final long serialVersionUID = 1L;

    protected List<Survey> surveyReferential;

    protected List<Region> regionReferential;

    protected String id;

    protected String code;

    protected String name;

    protected Region region;

    protected Date beginDate;

    protected Date endDate;

    protected boolean update;

    /**
     * Prepare create using {@code surveyReferential} to check unicity
     *
     * @param surveyReferential List of Survey as referential
     */
    public void prepareCreate(List<Survey> surveyReferential) {
        setId(null);
        setCode(null);
        setName(null);
        setRegion(null);
        setBeginDate(null);
        setEndDate(null);
        setSurveyReferential(surveyReferential);
    }

    /**
     * Prepare update base on {@code survey} bean values
     *
     * @param survey Bean to initialize the model
     */
    public void prepareUpdate(Survey survey) {
        setId(survey.getTopiaId());
        setCode(survey.getCode());
        setName(survey.getName());
        setRegion(survey.getRegion());
        setBeginDate(survey.getBeginDate());
        setEndDate(survey.getEndDate());
        setSurveyReferential(null);
    }

    /**
     * Instanciate a new {@link Survey} bean with values from this model.
     * Note that the {@code code} and {@code region} properties are mandatory
     * (check in validation) otherwise an exception will occurs.
     *
     * @return a new {@link Survey} bean
     */
    public Survey newBean() {
        Survey result = new SurveyImpl(getCode(), getRegion());
        result.setTopiaId(getId());
        result.setName(getName());
        result.setBeginDate(getBeginDate());
        result.setEndDate(getEndDate());
        return result;
    }

    public List<Survey> getSurveyReferential() {
        if (surveyReferential == null) {
            surveyReferential = Lists.newArrayList();
        }
        return surveyReferential;
    }

    public void setSurveyReferential(List<Survey> surveyReferential) {
        List<Survey> oldValue = Lists.newArrayList(getSurveyReferential());
        this.surveyReferential = surveyReferential;
        firePropertyChange(PROPERTY_SURVEY_REFERENTIAL, oldValue, surveyReferential);
    }

    public List<Region> getRegionReferential() {
        if (regionReferential == null) {
            regionReferential = Lists.newArrayList();
        }
        return regionReferential;
    }

    public void setRegionReferential(List<Region> regionReferential) {
        List<Region> oldValue = Lists.newArrayList(getRegionReferential());
        this.regionReferential = regionReferential;
        firePropertyChange(PROPERTY_REGION_REFERENTIAL, oldValue, regionReferential);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
        setUpdate(id != null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        String oldValue = getCode();
        this.code = code;
        firePropertyChange(PROPERTY_CODE, oldValue, code);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        Region oldValue = getRegion();
        this.region = region;
        firePropertyChange(PROPERTY_REGION, oldValue, region);
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        Date oldValue = getBeginDate();
        this.beginDate = beginDate;
        firePropertyChange(PROPERTY_BEGIN_DATE, oldValue, beginDate);
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        Date oldValue = getEndDate();
        this.endDate = endDate;
        firePropertyChange(PROPERTY_END_DATE, oldValue, endDate);
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        boolean oldValue = isUpdate();
        this.update = update;
        firePropertyChange(PROPERTY_UPDATE, oldValue, update);
    }
}
