/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.util;

import com.ezware.oxbow.swingbits.util.Preconditions;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.ui.swing.MainUI;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.nuiton.util.FileUtil;

import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;

import static javax.swing.JOptionPane.getRootFrame;

/**
 * Created: 14/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class SammoaUtil {

    private static final Log log = LogFactory.getLog(SammoaUtil.class);

    private SammoaUtil() {
        // never instanciate util class
    }

    public static <T> void fireTableRowsUpdated(JXTable table,
                                                Iterable<T> references,
                                                Iterable<T> updated,
                                                boolean scrollToFirst) {

        if (Iterables.isEmpty(updated)) {
            // nothing to do

        } else {

            int firstIndex = -1;
            int lastIndex = -1;
            for (T element : updated) {
                int index = Iterables.indexOf(references, Predicates.equalTo(element));
                if (firstIndex == -1 || firstIndex > index) {
                    firstIndex = index;
                }
                if (lastIndex == -1 || lastIndex < index) {
                    lastIndex = index;
                }
            }
            ((AbstractTableModel) table.getModel()).fireTableRowsUpdated(firstIndex, lastIndex);
            if (scrollToFirst) {
                table.scrollRowToVisible(table.convertRowIndexToView(firstIndex));
            }
        }
    }

    /**
     * Select the row with the model {@code index} in given {@code table}.
     *
     * @param table Jtable to select the row
     * @param index Model index to select
     */
    public static void selectTableRow(JTable table, int index) {
        if (index >= 0) {
            int viewIndex = table.convertRowIndexToView(index);
            table.setRowSelectionInterval(viewIndex, viewIndex);
        }
    }

    public static void unselectAll(JTable table) {
        int size = table.getRowCount();
        if (size > 0) {
            table.removeRowSelectionInterval(0, size - 1);
        }
    }

    public static int getLastSelectedTableRow(JTable table) {
        int result;
        int nbRows = table.getSelectedRows().length;
        if (nbRows > 0) {
            int lastRow = table.getSelectedRows()[nbRows - 1];
            result = table.convertRowIndexToModel(lastRow);

        } else {
            // No line selected
            result = -1;
        }
        return result;
    }

    public static boolean isCollectionRemoveEvent(PropertyChangeEvent event) {
        boolean result = false;
        if (event.getOldValue() != null && event.getNewValue() != null) {
            Preconditions.checkArgument(event.getOldValue() instanceof Collection);
            Preconditions.checkArgument(event.getNewValue() instanceof Collection);

            int oldSize = ((Collection) event.getOldValue()).size();
            int newSize = ((Collection) event.getNewValue()).size();

            result = oldSize > newSize;
        }
        return result;
    }

    public static <T> TableSelectionListener<T> addTableSelectionListener(JTable table,
                                                                          SelectionModelAdapter<T> selectionModelAdapter) {
        TableSelectionListener<T> result =
                new TableSelectionListener<T>(table, selectionModelAdapter);
        table.getSelectionModel().addListSelectionListener(result);
        return result;
    }

    public static Highlighter newBackgroundColorHighlighter(HighlightPredicate predicate, Color color) {
        return new SammoaColorHighlighter(predicate, color, false);
    }

    public static Highlighter newForegroundColorHighlighter(HighlightPredicate predicate, Color color) {
        return new SammoaColorHighlighter(predicate, color, true);
    }

    public static Object getPropertyValue(Object bean, String... properties) {
        Object result;
        try {
            String path = Joiner.on(".").join(properties);
            result = PropertyUtils.getNestedProperty(bean, path);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new SammoaTechnicalException(e);
        } catch (NestedNullException e) {
            result = null;
        }
        return result;
    }

    public static void setPropertyValue(Object bean, Object value, String... properties) {
        try {
            String path = Joiner.on(".").join(properties);
            PropertyUtils.setNestedProperty(bean, path, value);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new SammoaTechnicalException(e);
        }
    }

    public static JDialog openInDialog(JComponent ui,
                                       JFrame frame,
                                       String title,
                                       Action closeAction) {
        return openInDialog(ui, frame, title, closeAction, null);
    }

    public static JDialog openInDialog(JComponent ui,
                                       JFrame frame,
                                       String title,
                                       Action closeAction,
                                       Dimension dim) {

        JDialog result = new JDialog(frame, false);
        result.setTitle(title);
        result.add(ui);
        result.setResizable(true);
        if (dim == null) {
            result.setSize(550, 450);
        } else {
            result.setSize(dim);
        }
        if (closeAction != null) {
            JRootPane rootPane = result.getRootPane();
            rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "close");
            rootPane.getActionMap().put("close", closeAction);
        }
        result.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                Component ui = (Component) e.getSource();
                if (log.isInfoEnabled()) {
                    log.info("Destroy ui " + ui);
                }
                JAXXUtil.destroy(ui);
            }
        });
        SwingUtil.center(frame, result);
        result.setVisible(true);
        return result;
    }

    /**
     * Choisir un fichier via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre du dialogue de sélection
     * @param buttonLabel le label du boutton d'acceptation
     * @param incoming    le fichier de base à utilier
     * @param filters     les filtres + descriptions sur le sélecteur de
     *                    fichiers
     * @return le fichier choisi ou le fichier incoming si l'opération a été
     *         annulée
     */
    public static File chooseFile(Component parent,
                                  String title,
                                  String buttonLabel,
                                  File incoming,
                                  String... filters) {
        File oldBasedir = FileUtil.getCurrentDirectory();
        if (incoming != null) {
            File basedir;
            if (incoming.isFile()) {
                basedir = incoming.getParentFile();
            } else {
                basedir = incoming;
            }
            if (basedir.exists()) {
                FileUtil.setCurrentDirectory(basedir);
            }
        }
        File file = FileUtil.getFile(title, buttonLabel, parent, filters);
        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        FileUtil.setCurrentDirectory(oldBasedir);
        File result = file == null ? incoming : file;
        return result;
    }

    /**
     * Choisir un répertoire via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre de la boite de dialogue de sléection
     * @param buttonLabel le label de l'action d'acceptation
     * @param incoming    le fichier de base à utiliser
     * @return le répertoire choisi ou le répertoire incoming si l'opération a
     *         été annulée
     */
    public static File chooseDirectory(Component parent,
                                       String title,
                                       String buttonLabel,
                                       File incoming) {
        File oldBasedir = FileUtil.getCurrentDirectory();
        if (incoming != null) {
            File basedir;
            if (incoming.isFile()) {
                basedir = incoming.getParentFile();
            } else {
                basedir = incoming;
            }
            if (basedir.exists()) {
                FileUtil.setCurrentDirectory(basedir);
            }
        }
        String file = FileUtil.getDirectory(parent, title, buttonLabel);
        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        FileUtil.setCurrentDirectory(oldBasedir);
        return file == null ? incoming : new File(file);
    }

    public static void updateBusyState(Component ui, boolean busy) {
        if (JAXXObject.class.isInstance(ui)) {
            MainUI mainUI = JAXXObject.class.cast(ui).getParentContainer(MainUI.class);
            if (mainUI != null) {
                mainUI.getStatus().setBusy(busy);
            }
        }
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }

    public static void showSuccessMessage(Component ui, String message) {
        JOptionPane.showMessageDialog(
                ui,
                message,
                "Success",
                JOptionPane.INFORMATION_MESSAGE,
                UIManager.getIcon("info")
        );
    }

    public static void showErrorMessage(Component ui, String message) {
        JOptionPane.showMessageDialog(
                ui,
                message,
                "Error",
                JOptionPane.ERROR_MESSAGE,
                UIManager.getIcon("error")
        );
    }

    public static boolean askQuestion(Component ui, String message) {
        int i = JOptionPane.showConfirmDialog(
                ui,
                message,
                "Question?",
                JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }

    public static <V> V askValueForce(Component ui, String title, String question, V[] values, V defaultValue) {
        JPanel panel = new JPanel();

        panel.setMaximumSize(new Dimension(400, panel.getMaximumSize().height));
        panel.setSize(new Dimension(400, 80));
        panel.setPreferredSize(new Dimension(400, 80));

        JLabel label = new JLabel();
        label.setText("<html>" + question + "</html>");
        label.setSize(new Dimension(400, 40));
        label.setPreferredSize(new Dimension(400, 40));
        panel.add(label);

        JComboBox<V> valuesComboBox = new JComboBox(values);
        valuesComboBox.setSelectedItem(defaultValue);
        panel.add(valuesComboBox);
        JOptionPane.showMessageDialog(ui, panel, title, JOptionPane.QUESTION_MESSAGE);

        return (V) valuesComboBox.getSelectedItem();
    }

    public static <V> Optional<V> askValue(Component ui, String title, String question, V[] values, V defaultValue) {
        JPanel panel = new JPanel();

        panel.setSize(new Dimension(400, 60));
        panel.setPreferredSize(new Dimension(400, 60));

        JLabel label = new JLabel();
        label.setText("<html>" + question + "</html>");
        label.setSize(new Dimension(400, 20));
        label.setPreferredSize(new Dimension(400, 20));
        panel.add(label);

        JComboBox<V> valuesComboBox = new JComboBox(values);
        valuesComboBox.setSelectedItem(defaultValue);
        panel.add(valuesComboBox);
        int option = JOptionPane.showConfirmDialog(ui, panel, title, JOptionPane.OK_CANCEL_OPTION);
        Optional<V> result;
        if (option == 0) {
            result = Optional.of( (V) valuesComboBox.getSelectedItem());
        } else {
            result = Optional.empty();
        }

        return result;
    }

    public static JDialog askQuestionNoModal(Component parentComponent, String message, Consumer<Boolean> action) {
        String title = "Question?";
        int optionType = JOptionPane.YES_NO_OPTION;
        int messageType = JOptionPane.QUESTION_MESSAGE;
        JOptionPane pane = new JOptionPane(message, messageType,
                optionType, null,
                null, null);

        pane.setInitialValue(null);
        pane.setComponentOrientation(((parentComponent == null) ?
                getRootFrame() : parentComponent).getComponentOrientation());

        JDialog dialog = pane.createDialog(title);

        pane.selectInitialValue();
        dialog.setModal(false);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
        pane.addPropertyChangeListener(evt -> {
            if (JOptionPane.VALUE_PROPERTY.equals(evt.getPropertyName())) {
                dialog.dispose();

                Object selectedValue = evt.getNewValue();
                boolean value = Integer.class.isInstance(selectedValue) && JOptionPane.YES_OPTION == Integer.class.cast(selectedValue);
                action.accept(value);
            }
        });
        return dialog;
    }
}
