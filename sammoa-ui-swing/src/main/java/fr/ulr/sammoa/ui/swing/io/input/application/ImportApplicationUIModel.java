package fr.ulr.sammoa.ui.swing.io.input.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.io.input.application.ImportApplicationModel;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.ui.swing.io.AbstractApplicationModelWithBackup;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Model for sammoa import.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportApplicationUIModel extends AbstractApplicationModelWithBackup {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SURVEY_EXIST = "surveyExist";

    public static final String PROPERTY_IMPORT_FILE_EXIST = "importFileExist";

    public static final String PROPERTY_IMPORT_FILE_LOADED = "importFileLoaded";

    protected List<FlightImportEntry> flightEntries = Lists.newArrayList();

    public boolean isImportFileLoaded() {
        return CollectionUtils.isNotEmpty(flightEntries);
    }

    public List<FlightImportEntry> getFlightEntries() {
        return flightEntries;
    }

    public ImportApplicationModel toModel() {

        ImportApplicationModel result = new ImportApplicationModel();

        for (FlightImportEntry entry : getFlightEntries()) {
            if (entry.isTreat()) {

                //flight to import
                result.addFlightToImport(entry.getStorage(), entry.getFlightStorage().getId());

                Flight existingFlight = entry.getExistingFlight();
                if (existingFlight != null) {
                    result.addFlightToRemove(entry.getOldStorage(), existingFlight.getTopiaId());
                }
            }
        }
        return result;
    }


    public void addFlightImportEntries(List<FlightImportEntry> flightImportEntries) {
        flightEntries.removeIf(flightImportEntries::contains);
        flightEntries.addAll(flightImportEntries);
        firePropertyChange(PROPERTY_IMPORT_FILE_EXIST, false, isImportFileLoaded());
        firePropertyChange(PROPERTY_IMPORT_FILE_LOADED, false, isImportFileLoaded());
    }
}
