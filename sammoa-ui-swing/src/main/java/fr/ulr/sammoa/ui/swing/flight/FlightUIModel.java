/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.jdesktop.beans.AbstractSerializableBean;

import javax.swing.ActionMap;
import java.util.Collection;
import java.util.List;

/**
 * Created: 11/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class FlightUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_FLIGHT = "flight";

    public static final String PROPERTY_OBSERVERS = "observers";

    public static final String PROPERTY_CURRENT_ROUTE = "currentRoute";

    public static final String PROPERTY_NEXT_TRANSECT = "nextTransect";

    public static final String PROPERTY_FLIGHT_STATE = "flightState";

    public static final String PROPERTY_ACTION_MAP = "actionMap";

    public static final String PROPERTY_OBSERVATIONS = "observations";

    public static final String PROPERTY_ROUTES = "routes";

//    public static final String PROPERTY_VALIDATOR_TABLE_MODEL = "validatorTableModel";

    public static final String PROPERTY_OBSERVATION_EDIT_BEAN = "observationEditBean";

    public static final String PROPERTY_ROUTE_EDIT_BEAN = "routeEditBean";

    public static final String PROPERTY_TRANSECT_FLIGHT_EDIT_BEAN = "transectFlightEditBean";

    public static final String PROPERTY_FLIGHT_OBSERVER_FOR_POSITIONS = "flightObserverForPositions";

    public static final String PROPERTY_MAP_FOLLOW = "mapFollow";

    public static final String PROPERTY_CURRENT_STRATE = "currentStrate";

    public static final String PROPERTY_STRATES = "strates";

    public static final String PROPERTY_TRANSECT_FLIGHTS = "transectFlights";

    public static final String PROPERTY_TRANSECT_SELECTION_EXISTS = "transectSelectionExists";

    public static final String PROPERTY_GEO_POINTS = "geoPoints";

    public static final String PROPERTY_VALIDATION_MODE = "validationMode";

    public static final String PROPERTY_CIRCLE_BACK_ENABLED = "circleBackEnabled";

    public static final String PROPERTY_CIRCLE_BACK_TIMER_ENABLED = "circleBackTimerEnabled";

    public static final String PROPERTY_CIRCLE_BACK_TIMER = "circleBackTimer";

    public static final String PROPERTY_REJOIN_TIMER = "rejoinTimer";

    public static final String PROPERTY_DEADTIME_TIMER = "deadtimeTimer";

    protected Flight flight;

    protected List<Observer> observers;

    protected Route currentRoute;

    protected TransectFlight nextTransect;

    protected FlightState flightState;

    protected ActionMap actionMap;

    protected List<Observation> observations;

    /**
     * La liste des routes
     *
     * @since 0.3
     */
    protected List<Route> routes;

    /**
     * L'observation en cours d'édition.
     *
     * @since 0.2
     */
    protected Observation observationEditBean;

    protected boolean observationValueIsAdjusting;

    /**
     * La route en cours d'édition.
     *
     * @since 0.2
     */
    protected Route routeEditBean;

    protected TransectFlightModel transectFlightEditBean;

    /**
     * La liste des observateurs du vol, sans les pilotes avec l'observateur null
     *
     * @since 0.3
     */
    protected List<Observer> flightObserverForPositions;

//    protected List<Transect> transects;

    protected List<StrateModel> strates;

    protected boolean transectSelectionExists;

    protected StrateModel currentStrate;

    protected boolean mapFollow;

    protected List<TransectFlightModel> transectFlights;

    protected List<GeoPoint> geoPoints;

    protected boolean validationMode;

    protected boolean circleBackEnabled;

    protected boolean circleBackTimerEnabled;

    protected int circleBackTimer;

    protected int rejoinTimer;

    protected int deadtimeTimer;
//
//    protected SwingValidatorMessageTableModel validatorTableModel
//            = new SwingValidatorMessageTableModel();

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        Flight oldValue = getFlight();
        this.flight = flight;
        firePropertyChange(PROPERTY_FLIGHT, oldValue, flight);
    }

    public List<Observer> getObservers() {
        if (observers == null) {
            observers = Lists.newArrayList();
        }
        return observers;
    }

    public void setObservers(List<Observer> observers) {
        List<Observer> oldValue = ImmutableList.copyOf(getObservers());
        this.observers = observers;
        firePropertyChange(PROPERTY_OBSERVERS, oldValue, observers);
    }

    public Route getCurrentRoute() {
        return currentRoute;
    }

    public void setCurrentRoute(Route currentRoute) {
        Route oldValue = getCurrentRoute();
        this.currentRoute = currentRoute;
        firePropertyChange(PROPERTY_CURRENT_ROUTE, oldValue, currentRoute);
    }

    public TransectFlight getNextTransect() {
        return nextTransect;
    }

    public void setNextTransect(TransectFlight nextTransect) {
        TransectFlight oldValue = getNextTransect();
        this.nextTransect = nextTransect;
        firePropertyChange(PROPERTY_NEXT_TRANSECT, oldValue, nextTransect);
    }

    public FlightState getFlightState() {
        return flightState;
    }

    public void setFlightState(FlightState flightState) {
        FlightState oldValue = getFlightState();
        this.flightState = flightState;
        firePropertyChange(PROPERTY_FLIGHT_STATE, oldValue, flightState);
    }

    public ActionMap getActionMap() {
        return actionMap;
    }

    public void setActionMap(ActionMap actionMap) {
        ActionMap oldValue = getActionMap();
        this.actionMap = actionMap;
        firePropertyChange(PROPERTY_ACTION_MAP, oldValue, actionMap);
    }

    public List<Observation> getObservations() {
        if (observations == null) {
            observations = Lists.newArrayList();
        }
        return observations;
    }

    public int indexOfObservations(Observation observation) {
        return getObservations().indexOf(observation);
    }

    public int sizeObservations() {
        return getObservations().size();
    }

    public void setObservations(List<Observation> observations) {
        this.observations = observations;
        firePropertyChange(PROPERTY_OBSERVATIONS, null, observations);
    }

    public void addObservation(Observation observation) {
        List<Observation> oldValue = ImmutableList.copyOf(getObservations());
        int index = sizeObservations();
        getObservations().add(observation);
        fireIndexedPropertyChange(PROPERTY_OBSERVATIONS, index, oldValue, getObservations());
    }

    public void removeObservation(Observation observation) {
        List<Observation> oldValue = ImmutableList.copyOf(getObservations());
        int index = indexOfObservations(observation);
        getObservations().remove(index);
        fireIndexedPropertyChange(PROPERTY_OBSERVATIONS, index, oldValue, getObservations());
    }

    public void updateObservation(Observation observation) {
        int index = indexOfObservations(observation);
        getObservations().set(index, observation);
        fireIndexedPropertyChange(PROPERTY_OBSERVATIONS, index, getObservations(), null);
    }

    public void clearObservation() {
        List<Observation> oldValue = ImmutableList.copyOf(getObservations());
        getObservations().clear();
        firePropertyChange(PROPERTY_OBSERVATIONS, oldValue, getObservations());
    }

    public void addAllObservation(Collection<Observation> observations) {
        List<Observation> oldValue = ImmutableList.copyOf(getObservations());
        getObservations().addAll(observations);
        firePropertyChange(PROPERTY_OBSERVATIONS, oldValue, getObservations());
    }

    public List<Route> getRoutes() {
        if (routes == null) {
            routes = Lists.newArrayList();
        }
        return routes;
    }

    public int sizeRoutes() {
        return getRoutes().size();
    }

    public int indexOfRoutes(Route route) {
        return getRoutes().indexOf(route);
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
        firePropertyChange(PROPERTY_ROUTES, null, routes);
    }

    public void addRoute(Route route) {
        int index = sizeRoutes();
        addRoute(index, route);
    }

    public void addRoute(int index, Route route) {
        List<Route> oldValue = ImmutableList.copyOf(getRoutes());
        getRoutes().add(index, route);
        fireIndexedPropertyChange(PROPERTY_ROUTES, index, oldValue, getRoutes());
    }

    public void removeRoute(Route route) {
        List<Route> oldValue = ImmutableList.copyOf(getRoutes());
        int index = getRoutes().indexOf(route);
        getRoutes().remove(index);
        fireIndexedPropertyChange(PROPERTY_ROUTES, index, oldValue, getRoutes());
    }

    public void updateRoute(Route route) {
        int index = getRoutes().indexOf(route);
        getRoutes().set(index, route);
        fireIndexedPropertyChange(PROPERTY_ROUTES, index, getRoutes(), null);
    }

    public void clearRoute() {
        List<Route> oldValue = ImmutableList.copyOf(getRoutes());
        getRoutes().clear();
        firePropertyChange(PROPERTY_ROUTES, oldValue, getRoutes());
    }

    public void addAllRoute(Collection<Route> routes) {
        List<Route> oldValue = ImmutableList.copyOf(getRoutes());
        getRoutes().addAll(routes);
        firePropertyChange(PROPERTY_ROUTES, oldValue, getRoutes());
    }
//
//    public SwingValidatorMessageTableModel getValidatorTableModel() {
//        return validatorTableModel;
//    }
//
//    public void setValidatorTableModel(SwingValidatorMessageTableModel validatorTableModel) {
//        SwingValidatorMessageTableModel oldValidatorTableModel = validatorTableModel;
//        this.validatorTableModel = validatorTableModel;
//        firePropertyChange(PROPERTY_VALIDATOR_TABLE_MODEL, oldValidatorTableModel, validatorTableModel);
//    }

    public Observation getObservationEditBean() {
        return observationEditBean;
    }

    public void setObservationEditBean(Observation observationEditBean) {
        try {
            observationValueIsAdjusting = true;
            Observation oldObservationEditBean = this.observationEditBean;
            this.observationEditBean = observationEditBean;
            firePropertyChange(PROPERTY_OBSERVATION_EDIT_BEAN, oldObservationEditBean, observationEditBean);
        } finally {
            observationValueIsAdjusting = false;
        }
    }

    public boolean isObservationValueIsAdjusting() {
        return observationValueIsAdjusting;
    }

    public Route getRouteEditBean() {
        return routeEditBean;
    }

    public void setRouteEditBean(Route routeEditBean) {
        Route oldRouteEditBean = this.routeEditBean;
        this.routeEditBean = routeEditBean;
        firePropertyChange(PROPERTY_ROUTE_EDIT_BEAN, oldRouteEditBean, routeEditBean);
    }

    public TransectFlightModel getTransectFlightEditBean() {
        return transectFlightEditBean;
    }

    public void setTransectFlightEditBean(TransectFlightModel transectFlightEditBean) {
        TransectFlightModel oldValue = this.transectFlightEditBean;
        this.transectFlightEditBean = transectFlightEditBean;
        firePropertyChange(PROPERTY_TRANSECT_FLIGHT_EDIT_BEAN, oldValue, transectFlightEditBean);
    }

    public List<Observer> getFlightObserverForPositions() {
        if (flightObserverForPositions == null) {
            flightObserverForPositions = Lists.newArrayList();
        }
        return flightObserverForPositions;
    }

    public void setFlightObserverForPositions(List<Observer> flightObserverForPositions) {
        // Note: Can't use ImmutableList for copy, the list contains the null observer
        List<Observer> oldValue = Lists.newArrayList(getFlightObserverForPositions());
        this.flightObserverForPositions = flightObserverForPositions;
        firePropertyChange(PROPERTY_FLIGHT_OBSERVER_FOR_POSITIONS, oldValue, flightObserverForPositions);
    }

    public boolean isMapFollow() {
        return mapFollow;
    }

    public void setMapFollow(boolean mapFollow) {
        boolean oldValue = this.mapFollow;
        this.mapFollow = mapFollow;
        firePropertyChange(PROPERTY_MAP_FOLLOW, oldValue, this.mapFollow);
    }

    public StrateModel getCurrentStrate() {
        return currentStrate;
    }

    public void setCurrentStrate(StrateModel currentStrate) {
        StrateModel oldValue = getCurrentStrate();
        this.currentStrate = currentStrate;
        firePropertyChange(PROPERTY_CURRENT_STRATE, oldValue, currentStrate);
    }

    public StrateModel getStrateAll() {
        return !getStrates().isEmpty() ? getStrates().get(0) : null;
    }

    public List<StrateModel> getStrates() {
        if (strates == null) {
            strates = Lists.newArrayList();
        }
        return strates;
    }

    public void setStrates(List<StrateModel> strates) {
        List<StrateModel> oldValue = ImmutableList.copyOf(getStrates());
        this.strates = strates;
        firePropertyChange(PROPERTY_STRATES, oldValue, strates);
    }

    public List<TransectModel> getTransects() {
        return getStrates().isEmpty()
               ? Lists.<TransectModel>newArrayList()
               : getStrates().get(0).getTransects();
    }

    public boolean isTransectSelectionExists() {
        return transectSelectionExists;
    }

    public void setTransectSelectionExists(boolean transectSelectionExists) {
        boolean oldValue = isTransectSelectionExists();
        this.transectSelectionExists = transectSelectionExists;
        firePropertyChange(PROPERTY_TRANSECT_SELECTION_EXISTS, oldValue, transectSelectionExists);
    }

    public List<TransectFlightModel> getTransectFlights() {
        if (transectFlights == null) {
            transectFlights = Lists.newArrayList();
        }
        return transectFlights;
    }

    public int indexOfTransectFlights(TransectFlightModel transectFlight) {
        return getTransectFlights().indexOf(transectFlight);
    }

    public void setTransectFlights(List<TransectFlightModel> transectFlights) {
        List<TransectFlightModel> oldValue = ImmutableList.copyOf(getTransectFlights());
        this.transectFlights = transectFlights;
        firePropertyChange(PROPERTY_TRANSECT_FLIGHTS, oldValue, transectFlights);
    }

    public void addAllTransectFlights(int index, Collection<TransectFlightModel> transectFlights) {
        List<TransectFlightModel> oldValue = ImmutableList.copyOf(getTransectFlights());
        getTransectFlights().addAll(index, transectFlights);
        fireIndexedPropertyChange(PROPERTY_TRANSECT_FLIGHTS, index, oldValue, getTransectFlights());
    }

    public void removeTransectFlight(int index) {
        List<TransectFlightModel> oldValue = ImmutableList.copyOf(getTransectFlights());
        getTransectFlights().remove(index);
        fireIndexedPropertyChange(PROPERTY_TRANSECT_FLIGHTS, index, oldValue, getTransectFlights());
    }

    public void updateTransectFlight(TransectFlight transectFlight) {
        int index = TransectFlightModel.indexOfTransectFlight(
                getTransectFlights(), transectFlight);
        getTransectFlights().get(index).setSource(transectFlight);
        fireIndexedPropertyChange(PROPERTY_TRANSECT_FLIGHTS, index, getTransectFlights(), null);

        // Update instance in flight list
        int flightIndex = getFlight().getTransectFlight().indexOf(transectFlight);
        flight.getTransectFlight().set(flightIndex, transectFlight);
    }

    public List<GeoPoint> getGeoPoints() {
        if (geoPoints == null) {
            geoPoints = Lists.newArrayList();
        }
        return geoPoints;
    }

    public void setGeoPoints(List<GeoPoint> geoPoints) {
        List<GeoPoint> oldValue = ImmutableList.copyOf(getGeoPoints());
        this.geoPoints = geoPoints;
        firePropertyChange(PROPERTY_GEO_POINTS, oldValue, geoPoints);
    }

    public boolean isValidationMode() {
        return validationMode;
    }

    public void setValidationMode(boolean validationMode) {
        boolean oldValue = isValidationMode();
        this.validationMode = validationMode;
        firePropertyChange(PROPERTY_VALIDATION_MODE, oldValue, validationMode);
    }

    public boolean isCircleBackEnabled() {
        return circleBackEnabled;
    }

    public void setCircleBackEnabled(boolean circleBackEnabled) {
        boolean oldValue = isCircleBackEnabled();
        this.circleBackEnabled = circleBackEnabled;
        firePropertyChange(PROPERTY_CIRCLE_BACK_ENABLED, oldValue, circleBackEnabled);
    }

    public boolean isCircleBackTimerEnabled() {
        return circleBackTimerEnabled;
    }

    public void setCircleBackTimerEnabled(boolean circleBackTimerEnabled) {
        boolean oldValue = isCircleBackTimerEnabled();
        this.circleBackTimerEnabled = circleBackTimerEnabled;
        firePropertyChange(PROPERTY_CIRCLE_BACK_TIMER_ENABLED, oldValue, circleBackTimerEnabled);
    }

    public int indexOfGeoPoint(GeoPoint geoPoint) {
        return getGeoPoints().indexOf(geoPoint);
    }

    public int getCircleBackTimer() {
        return circleBackTimer;
    }

    public void setCircleBackTimer(int circleBackTimer) {
        int oldValue = getCircleBackTimer();
        this.circleBackTimer = circleBackTimer;
        firePropertyChange(PROPERTY_CIRCLE_BACK_TIMER, oldValue, circleBackTimer);
    }

    public int getRejoinTimer() {
        return rejoinTimer;
    }

    public void setRejoinTimer(int rejoinTimer) {
        int oldValue = getRejoinTimer();
        this.rejoinTimer = rejoinTimer;
        firePropertyChange(PROPERTY_REJOIN_TIMER, oldValue, rejoinTimer);
    }

    public int getDeadtimeTimer() {
        return deadtimeTimer;
    }

    public void setDeadtimeTimer(int deadtimeTimer) {
        int oldValue = getDeadtimeTimer();
        this.deadtimeTimer = deadtimeTimer;
        firePropertyChange(PROPERTY_DEADTIME_TIMER, oldValue, deadtimeTimer);
    }
}
