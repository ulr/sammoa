package fr.ulr.sammoa.ui.swing.flight.bar.validation;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.audio.AudioPositionListener;
import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.application.device.audio.SammoaAudioReader;
import org.apache.commons.lang3.time.FastDateFormat;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

/**
 * Composant qui permet de charge un fichier audio et de le jouer. On peut se
 * placer ou l'on veut dans le fichier audio.
 *
 * usage:
 * <li> SoundPlayer sp = new SoundPlayer();
 * <li> panel.add(sp);
 * <li> sp.load(myFileWav);
 *
 * Created: 03 septembre 2012
 *
 * @author Benjamin POUSSIN <poussin@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SoundPlayer extends JComponent implements DeviceStateListener, AudioPositionListener {
    protected AudioReader reader;

    // le fichier charge acutellement au niveau de l'ui (config du slider
//    protected File currentFile;

    // The following fields are for the GUI
    protected JButton play;             // The Play button
    protected JButton stop;             // The Stop button
    protected JSlider progress;         // Shows and sets current position in sound
    protected JLabel time;              // Displays audioPosition as a number

    protected FastDateFormat dateFormat;
    protected FastDateFormat longDateFormat;

    // Create a SoundPlayer component for the specified file.
    public SoundPlayer() {
        this(null);
    }

    public SoundPlayer(AudioReader reader) {
        initUI();
        setReader(reader);
        setTimeZone(null);
    }

    public void setTimeZone(TimeZone timeZone) {
        dateFormat = FastDateFormat.getInstance("HH:mm", timeZone);
        longDateFormat = FastDateFormat.getInstance("HH:mm:ss", timeZone);

        if (this.reader != null) {
            updateTime();
            changeSliderInfo(true);
        }
    }

    protected void initUI() {
        // Now create the basic GUI
        play = new JButton("Play");
        play.setEnabled(false);
        stop = new JButton("Stop");
        stop.setEnabled(false);
        
        progress = new JSlider(0, 0, 0);
        progress.setMajorTickSpacing(60000); // toutes les 60s
        progress.setMinorTickSpacing(10000); // toutes les 10s
        progress.setPaintTicks(true);
        progress.setPaintLabels(true);

        time = new JLabel("00:00:00");                    // Shows position as a #

        // put those controls in a row
        Box row = Box.createHorizontalBox();

        Box buttons = Box.createVerticalBox();
        buttons.add(play);
        buttons.add(stop);
        
        row.add(buttons);
        row.add(progress);

        Box timeBox = Box.createVerticalBox();
        timeBox.add(time);
        timeBox.add(Box.createVerticalGlue());

        row.add(timeBox);

        // And add them to this component.
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(row);

        // ADD LISTENER

        // When clicked, start or stop playing the sound
        play.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                play();
            }
        });

        // When clicked, start or stop playing the sound
        stop.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });

        progress.addMouseListener(new MouseAdapter() {
            boolean inPlay = false;
            
            @Override
            public void mousePressed(MouseEvent e) {
                inPlay = getAudioReader().getState() == DeviceState.RUNNING;
                if (inPlay) {
                    reader.stop();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                int value = progress.getValue( );
                // Update the time label
//                time.setText(value/1000 + "." + (value%1000)/100);
                // If we're not already there, skip there.
                if (value >= 0 && value <= reader.getLength()) {
                    reader.setPosition(value);
                }
                
                if (inPlay) {
                    reader.start();
                }
            }
        });

        // Whenever the slider value changes, update the time label.
        progress.addChangeListener(e -> updateTime());
    }

    @Override
    public void stateChanged(DeviceStateEvent event) {
        DeviceState state = event.getNewValue();
        switch (state) {
            case READY: // on vient de charger un fichier, ou arrete la lecture du fichier
                play.setEnabled(true);
                stop.setEnabled(false);
                progress.setEnabled(true);
                changeSliderInfo(false);
                break;
            case RUNNING: // un fichier est en cours de lecture
                play.setEnabled(false);
                stop.setEnabled(true);
                progress.setEnabled(true);
                break;
            case UNAVAILABLE: // aucun fichier charge
                play.setEnabled(false);
                stop.setEnabled(false);
                progress.setEnabled(false);
                changeSliderInfo(false);
                break;
            case ERROR: // erreur sur le lecture audio (erreur I/O)
                play.setEnabled(false);
                stop.setEnabled(false);
                progress.setEnabled(false);
                changeSliderInfo(false);
                break;
        }
    }

    protected void updateTime() {
        // changement du text de position
        Date date = getAudioReader().getStartDate();
        long start = 0;
        if (date != null) {
            start = date.getTime();
        }
        int value = progress.getValue();
        String timeString = longDateFormat.format(start + value);
        time.setText(timeString);
    }

    /**
     * On met a la seconde dans le slider
     */
    protected void changeSliderInfo(boolean force) {
        Date date = getAudioReader().getStartDate();
        long start = date == null ? 0 : date.getTime();

        long audioLength = getAudioReader().getLength();
        int max = (int)audioLength;
        double width = progress.getSize().getWidth();

        // si la longueur du morceau change, on change l'info affichee
        if ((progress.getMaximum() != max || force) && width > 0) {
            progress.setValue((int)getAudioReader().getPosition());
            progress.setMaximum(max);

            // calcul de l'espace entre chaque affichage de l'heure en fonction de la longueur du slider
            long step = audioLength * 100 / (long)width; // on minimum tous les 100px
            step = Math.max(step, 60000); // on prend pas en dessous de la minute

            //Create the label table
            Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
            labelTable.put( 0, new JLabel(dateFormat.format(start)) );
            for (int i = (int)step; i+step<max; i+=step) { // on ne met pas l'avant dernier vu qu'on met max ensuite
                labelTable.put( i, new JLabel(dateFormat.format(start + i)) );
            }
            labelTable.put( max, new JLabel(dateFormat.format(start + audioLength)) );
            progress.setLabelTable( labelTable );
        }
    }

    public JButton getPlayButton() {
        return play;
    }

    public JButton getStopButton() {
        return stop;
    }

    @Override
    public void positionChanged(AudioReader source, long audioPosition) {
        // changement de la position du slider
        int pos = (int)audioPosition;
        progress.setValue(pos);
    }

    public void setReader(AudioReader reader) {
        if (this.reader != null) {
            this.reader.removeAudioPositionListener(this);
            this.reader.removeDeviceStateListener(this);
        }
        this.reader = reader;
        if (this.reader != null) {
            reader.addDeviceStateListener(this);
            reader.addAudioPositionListener(this);
        }

    }

    /**
     * retourne le lecture utilise par ce composant
     * @return
     */
    public AudioReader getAudioReader() {
        return reader;
    }

//    /**
//     * Charge un nouveau fichier
//     * @param f le fichier a charger
//     */
//    public void loadFile(File f) {
//        getAudioReader().load(f);
//    }
//
//    /**
//     * Donne la position actuelle de la lecture en milliseconde
//     * @return
//     */
//    public long getPosition() {
//        return getAudioReader().getPosition();
//    }
//
//    /**
//     * Donne la longueur total du fichier en milliseconde
//     * @return
//     */
//    public long getLength() {
//        return getAudioReader().getLength();
//    }

    /**
     * Start playing the sound at the current position
     */
    public void play() {
        getAudioReader().start();
    }

    /**
     * Stop playing the sound, but retain the current position
     */
    public void stop() {
        getAudioReader().stop();
    }

    // The main method just creates a SoundPlayer in a Frame and displays it
    public static void main(String[] args) {
        SoundPlayer player;

        File file;
        if (args.length == 0) {
            file = new File("/tmp/junkk.wav");
        } else {
            file = new File(args[0]);
        }

        // Create a SoundPlayer object to play the sound.
        player = new SoundPlayer();
        AudioReader reader = new SammoaAudioReader();
        player.setReader(reader);
        reader.load(file, new Date());

        // Put it in a window and play it
        JFrame f = new JFrame("SoundPlayer");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(player, "Center");
        f.pack();
        f.setVisible(true);
    }
}
