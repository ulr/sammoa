package fr.ulr.sammoa.ui.swing.flight.layer;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicConstants;
import com.bbn.openmap.omGraphics.OMLine;
import com.bbn.openmap.omGraphics.OMPoint;
import com.google.common.collect.Iterables;
import fr.ulr.sammoa.persistence.GeoPoint;

/**
 * Created: 07/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class LineGeoPointLayer extends BaseGeoPointLayer {

    private static final long serialVersionUID = 1L;

    @Override
    protected OMGraphic newGraphic(GeoPoint geoPoint) {

        OMGraphic result;
        if (!getList().isEmpty()) {

            OMGraphic lastGraphic = Iterables.getLast(getList());

            double lat, lon;
            if (lastGraphic instanceof  OMPoint) {
                lat = ((OMPoint) lastGraphic).getLat();
                lon = ((OMPoint) lastGraphic).getLon();
            } else {
                lat = ((OMLine) lastGraphic).getLL()[2];
                lon = ((OMLine) lastGraphic).getLL()[3];
            }

//            GeoPoint previousPoint = (GeoPoint) lastGraphic.getAppObject();

            result = new OMLine(
                    lat,
                    lon,
                    geoPoint.getLatitude(),
                    geoPoint.getLongitude(),
                    OMGraphicConstants.LINETYPE_GREATCIRCLE
            );

        } else {
            result = new OMPoint(geoPoint.getLatitude(), geoPoint.getLongitude());
        }
        return result;
    }
}
