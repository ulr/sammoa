package fr.ulr.sammoa.ui.swing.flight;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.Validable;
import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created: 27/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectFlightModel extends AbstractSerializableBean implements Validable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SOURCE = "source";

    public static final String PROPERTY_TRANSECT = "transect";

    public static final String PROPERTY_DELETED = "deleted";

    public static final String PROPERTY_INDEX = "index";

    public static final String PROPERTY_CROSSING_NUMBER = "crossingNumber";

    protected FlightUIModel flightModel;

    protected TransectFlight source;

    protected TransectModel transect;

    public TransectFlightModel(FlightUIModel flightModel,
                               TransectFlight source,
                               TransectModel transect) {

        Preconditions.checkNotNull(flightModel);
        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(transect);
        this.flightModel = flightModel;
        this.source = source;
        this.transect = transect;
    }

    public TransectFlight getSource() {
        return source;
    }

    public void setSource(TransectFlight source) {
        TransectFlight oldValue = getSource();
        this.source = source;
        firePropertyChange(PROPERTY_SOURCE, oldValue, source);
    }

    public TransectModel getTransect() {
        return transect;
    }

    public void setTransect(TransectModel transect) {
        TransectModel oldValue = getTransect();
        this.transect = transect;
        firePropertyChange(PROPERTY_TRANSECT, oldValue, transect);
    }

    @Override
    public boolean isDeleted() {
        return getSource().isDeleted();
    }

    public void setDeleted(boolean deleted) {
        boolean oldValue = isDeleted();
        firePropertyChange(PROPERTY_DELETED, oldValue, deleted);
        getSource().setDeleted(deleted);
    }

    @Override
    public boolean isValid() {
        return getSource().isValid();
    }

    public int getIndex() {
        return Iterables.indexOf(getFlight().getTransectFlight(), Predicates.equalTo(getSource()));
    }

    public void setIndex(int index) {

        int oldValue = getIndex();

        // Manage maxBound to avoid IndexOutOfBoundsException
        if (index >= getFlight().getTransectFlight().size()) {
            index = getFlight().getTransectFlight().size() - 1;
        }

        getFlight().getTransectFlight().remove(getSource());

        // Use add on Flight to fire change
        getFlight().addTransectFlight(index, getSource());

        flightModel.getTransectFlights().remove(this);

        flightModel.getTransectFlights().add(index, this);

        firePropertyChange(PROPERTY_INDEX, oldValue, index);
    }

    public int getCrossingNumber() {
        return getSource().getCrossingNumber();
    }

    public void setCrossingNumber(int crossingNumber) {
        int oldValue = getCrossingNumber();
        getSource().setCrossingNumber(crossingNumber);
        firePropertyChange(PROPERTY_CROSSING_NUMBER, oldValue, crossingNumber);
    }

    public ObserverPosition getObserverPositionByPosition(Position position) {
        return getSource().getObserverPositionByPosition(position);
    }

//    public ObserverPosition getObserverPositionByObserver(Observer observer) {
//        return getSource().getObserverPositionByObserver(observer);
//    }

    public void setObserverByPosition(Position position, Observer observer) {
        getSource().setObserverByPosition(position, observer);
    }

    protected Flight getFlight() {
        return flightModel.getFlight();
    }

    public static TransectFlightModel findTransectFlightModel(Iterable<TransectFlightModel> transectFlightModels,
                                                              TransectFlight transectFlight) {
        return Iterables.find(
                transectFlightModels, new WithTransectFlightPredicate(transectFlight));
    }

    public static int indexOfTransectFlight(Iterable<TransectFlightModel> transectFlightModels,
                                            TransectFlight transectFlight) {
        return Iterables.indexOf(
                transectFlightModels, new WithTransectFlightPredicate(transectFlight));
    }

    private static class WithTransectFlightPredicate implements Predicate<TransectFlightModel> {

        protected TransectFlight transectFlight;

        public WithTransectFlightPredicate(TransectFlight transectFlight) {
            this.transectFlight = transectFlight;
        }

        @Override
        public boolean apply(TransectFlightModel input) {
            return transectFlight.equals(input.getSource());
        }
    }
}
