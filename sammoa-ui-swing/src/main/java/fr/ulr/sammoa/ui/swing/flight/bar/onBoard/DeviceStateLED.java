/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.bar.onBoard;

import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import org.nuiton.util.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Panel qui écoute l'état du peripherique pour afficher à l'utilsateur un voyant
 * lunineux dépendant de l'état du peripherique.
 * <p/>
 * Indicateurs pour l'état du peripherique
 * <ul>
 * <li>voyant gris : pas de peripherique
 * <li>voyant bleu : peripherique prêt
 * <li>voyant vert : enregistrement en cours
 * <li>voyant rouge clignotant : enregistrement mais pas de données
 * </ul>
 *
 * @author chatellier
 */
public class DeviceStateLED extends JLabel implements DeviceStateListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(DeviceStateLED.class);

    protected DeviceState state;

    protected static final ImageIcon NO_DEVICE_ICON = Resource.getIcon("/icons/device/nodevice.png");

    protected static final ImageIcon READY_ICON = Resource.getIcon("/icons/device/ready.png");

    protected static final ImageIcon RECORDING_ICON = Resource.getIcon("/icons/device/recording.png");

    protected static final ImageIcon NO_DATA_ICON = Resource.getIcon("/icons/device/nodata.gif");

    @Override
    public void stateChanged(DeviceStateEvent event) {
        setState(event.getNewValue());
    }

    public void setState(DeviceState state) {
        DeviceState oldValue = getState();
        this.state = state;

        if (oldValue != state) {

            if (log.isDebugEnabled()) {
                log.debug("[" + getClass().getSimpleName() + "] Update LED status: " + state);
            }

            switch (state) {
                case UNAVAILABLE:
                    setIcon(NO_DEVICE_ICON);
                    break;
                case READY:
                    setIcon(READY_ICON);
                    break;
                case RUNNING:
                    setIcon(RECORDING_ICON);
                    break;
                case ERROR:
                    setIcon(NO_DATA_ICON);
                    break;
            }
        }
    }

    public DeviceState getState() {
        return state;
    }
}
