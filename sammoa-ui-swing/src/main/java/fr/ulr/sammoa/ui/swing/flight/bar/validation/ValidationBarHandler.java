/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.bar.validation;

import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.JAXXUtil;

import javax.swing.JButton;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.TimeZone;

/** @author fdesbois <fdesbois@codelutin.com> */
public class ValidationBarHandler {

    protected final ValidationBar ui;

    public ValidationBarHandler(ValidationBar ui) {
        this.ui = ui;
    }

    public FlightUI getParentUI() {
        return ui.getContextValue(FlightUI.class, JAXXUtil.PARENT);
    }

    protected FlightUIModel getModel() {
        return ui.getModel();
    }

    public void afterInitUI() {

        FlightController flightController =
                getParentUI().getHandler().getFlightController();

        AudioReader audioReader = flightController.getDeviceManager(AudioReader.class);
        ui.getSoundPlayer().setReader(audioReader);

        ui.getDateFormat().setTimeZone(getModel().getFlight().getTimeZone());
        ui.getDataBinding(ValidationBar.BINDING_AUDIO_TIME_TEXT).processDataBinding();

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_TRANSECT_FLIGHT_EDIT_BEAN,
                new ValidableChangeListener(ui, ui.getValidTransectButton()));

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_ROUTE_EDIT_BEAN,
                new ValidableChangeListener(ui, ui.getValidRouteButton()));

        getModel().addPropertyChangeListener(
                FlightUIModel.PROPERTY_OBSERVATION_EDIT_BEAN,
                new ValidableChangeListener(ui, ui.getValidObservationButton()));

        getModel().getFlight().addPropertyChangeListener(Flight.PROPERTY_TIME_ZONE,
                evt -> {
                    TimeZone timeZone = TimeZone.class.cast(evt.getNewValue());
                    ui.getDateFormat().setTimeZone(timeZone);
                    ui.getDataBinding(ValidationBar.BINDING_AUDIO_TIME_TEXT).processDataBinding();
                });
    }

    private static class ValidableChangeListener implements PropertyChangeListener {

        protected SammoaUIContext context;

        protected JButton button;

        private ValidableChangeListener(JAXXObject jaxxObject,
                                        JButton button) {
            this.context = jaxxObject.getContextValue(SammoaUIContext.class);
            this.button = button;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object newValue = evt.getNewValue();

            String text;
            if (newValue != null) {
                UIDecoratorService service = context.getService(UIDecoratorService.class);
                text = service.getDecorator(newValue).toString(newValue);

            } else {
                text = null;
            }
            button.setText(text);
        }
    }
}
