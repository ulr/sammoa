package fr.ulr.sammoa.ui.swing;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.audio.AudioLineProvider;
import fr.ulr.sammoa.application.device.audio.SammoaAudioReader;
import fr.ulr.sammoa.application.device.audio.SammoaAudioRecorder;
import fr.ulr.sammoa.ui.swing.flight.bar.validation.SoundPlayer;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import fr.ulr.sammoa.ui.swing.util.SoundMeter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Date;

//import javax.media.CannotRealizeException;
//import javax.media.NoPlayerException;

/**
 * Petite application de test de l'audio. Elle permet de choisir differente
 * configuration audio. De faire des enregistrements et de relire le fichier
 * enregistre.
 * <p>
 * La configuration choisi est inscrite et permet de la copier dans le fichier
 * de configuration
 * <p>
 * Si la machine ne supporte pas deux enregistrement simultane, delay est mis
 * a 0.
 *
 * Created: 27 aout 2012
 *
 * @author Benjamin POUSSIN <poussin@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class AudioCheck extends JFrame {

    private static final Log log = LogFactory.getLog(AudioCheck.class);

    protected SammoaAudioRecorder recorder;
    /** delay par defaut de chevauchement d'enregistrement */
    protected int delay = 0;

    protected SoundMeter audioMeter = new SoundMeter();

    protected JTextField filenameField = new JTextField("Test.wav");
    protected JTextArea informationArea = new JTextArea();
    
    protected JButton captureBtn = new JButton("Capture");
    protected JButton stopBtn = new JButton("Stop");
    protected JButton loadBtn = new JButton("Load");
    protected SoundPlayer playerPanel = new SoundPlayer(new SammoaAudioReader());

    protected JLabel compressionLabel = new JLabel("Compression");
    protected JTextField compression = new JTextField("ULAW");

    protected JPanel btnPanelSampleRate = new JPanel();
    protected ButtonGroup btnGroupSampleRate = new ButtonGroup();
    JRadioButton[] btnSampleRate = new JRadioButton[]{
        new JRadioButton("8000",true),
        new JRadioButton("11025"),
        new JRadioButton("16000"),
        new JRadioButton("22050"),
        new JRadioButton("44100")
    };

    protected JPanel btnPanelSampleSizeInBits = new JPanel();
    protected ButtonGroup btnGroupSampleSizeInBits = new ButtonGroup();
    JRadioButton[] btnSampleSizeInBits = new JRadioButton[]{
        new JRadioButton("8"),
        new JRadioButton("16",true)
    };

    public AudioCheck(){//constructor
        initUI();
        checkMultiRecord();
    }

    protected void initUI () {
        Box buttons = Box.createHorizontalBox();
        buttons.add(captureBtn);
        buttons.add(stopBtn);
        buttons.add(audioMeter);

        audioMeter.start();

        Box box = Box.createVerticalBox();
        box.add(filenameField);
        box.add(buttons);

        Box compressionBox = Box.createHorizontalBox();
        compressionBox.add(compressionLabel);
        compressionBox.add(compression);
        box.add(compressionBox);

        for (JRadioButton b : btnSampleRate) {
            //Include the radio buttons in a group
            btnGroupSampleRate.add(b);
            //Add the radio buttons to the JPanel
            btnPanelSampleRate.add(b);
            b.addActionListener(
                new ActionListener(){
                    public void actionPerformed(
                            ActionEvent e){
                        updateConfigInfo();
                    }
                });
        }

        //Put the JPanel in the JFrame
        box.add(btnPanelSampleRate);

        for (JRadioButton b : btnSampleSizeInBits) {
            //Include the radio buttons in a group
            btnGroupSampleSizeInBits.add(b);
            //Add the radio buttons to the JPanel
            btnPanelSampleSizeInBits.add(b);
            b.addActionListener(
                new ActionListener(){
                    public void actionPerformed(
                            ActionEvent e){
                        updateConfigInfo();
                    }
                });
        }

        //Put the JPanel in the JFrame
        box.add(btnPanelSampleSizeInBits);

        informationArea.setBorder(BorderFactory.createRaisedBevelBorder());
        informationArea.setLineWrap(true);
        box.add(informationArea);
        box.add(loadBtn);
        box.add(playerPanel);

        //Finish the GUI and make visible
        setTitle("Sound Test");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        getContentPane().add(box, "Center");
        setBounds(0, 0, 400, 400);

        captureBtn.setEnabled(true);
        stopBtn.setEnabled(false);

        recorder = new SammoaAudioRecorder(getSampleRate(), getSampleSizeInBits(), getCompression(), 0, 1);
        recorder.open();

        // ADD LISTENER

        compression.addKeyListener(
                new KeyAdapter(){
                    @Override
                    public void keyReleased(KeyEvent e) {
                        updateConfigInfo();
                    }
                });

        //Register anonymous listeners
        captureBtn.addActionListener(
                new ActionListener(){
                    public void actionPerformed(
                            ActionEvent e){
                        boolean audioRecording = captureAudio();
                        if (audioRecording) {
                            captureBtn.setEnabled(false);
                            stopBtn.setEnabled(true);
                        }
                    }
                });

        stopBtn.addActionListener(
                new ActionListener(){
                    public void actionPerformed(
                            ActionEvent e){
                        captureBtn.setEnabled(true);
                        stopBtn.setEnabled(false);
                        recorder.stop();
                    }
                });

        loadBtn.addActionListener(
                new ActionListener(){
                    public void actionPerformed(
                            ActionEvent e){
                        File file = new File(filenameField.getText());
                        playerPanel.getAudioReader().load(file, new Date());
                    }
                });

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                if (recorder != null) {
                    recorder.close();
                }
                audioMeter.stop();
            }
        });

        recorder.addDeviceStateListener(evt -> {
            if (DeviceState.READY.equals(evt.getNewValue())) {
                captureBtn.setEnabled(true);
                stopBtn.setEnabled(false);
            }
        });
    }

    protected boolean captureAudio(){
        File file = new File(filenameField.getText());
        try {
            if (recorder != null) {
                recorder.close();
            }
            recorder.setSample(getSampleRate(), getSampleSizeInBits(), getCompression());
            recorder.record(file, true);
            return true;

        } catch (Exception eee) {
            // Close only on error, otherwise no recording will be launch
            recorder.close();

            if (log.isErrorEnabled()) {
                log.error("Can't capture audio", eee);
            }
            SammoaUtil.showErrorMessage(this, "Can't capture audio : " + eee.getMessage());
            return false;
        }
    }

    protected String getCompression() {
        return compression.getText();
    }

    protected float getSampleRate() {
        float result = 44100;
        for (JRadioButton b : btnSampleRate) {
            if (b.isSelected()) {
                result = Float.parseFloat(b.getText());
            }
        }
        return result;
    }

    protected int getSampleSizeInBits() {
        int result = 44100;
        for (JRadioButton b : btnSampleSizeInBits) {
            if (b.isSelected()) {
                result = Integer.parseInt(b.getText());
            }
        }
        return result;
    }

    protected void updateConfigInfo() {
        informationArea.setText(String.format(
                "sammoa.audio.recordDelayInSeconds=%s\n"
                + "sammoa.audio.sampleRate=%s\n"
                + "sammoa.audio.sampleSizeInBits=%s\n"
                + "sammoa.audio.compression=%s\n",
                delay, getSampleRate(), getSampleSizeInBits(), getCompression()));
    }

    protected void checkMultiRecord() {
        if (AudioLineProvider.getInstance().getMaxLines() == 1) {

            delay = 5;

            if (log.isInfoEnabled()) {
                log.info("The recording delay is set to 5 seconds between " +
                        "each recording, you can update this value from" +
                        " configuration interface");
            }

        } else {

            delay = 0;

            if (log.isInfoEnabled()) {
                log.info("Can't record multiple file at the same time, 0 " +
                        "second delay between recording must be used");
            }

        }

        updateConfigInfo();
    }


    public static void main( String args[]) {
        AudioCheck frame = new AudioCheck();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
