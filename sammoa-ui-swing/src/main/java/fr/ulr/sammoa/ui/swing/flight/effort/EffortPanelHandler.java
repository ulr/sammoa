package fr.ulr.sammoa.ui.swing.flight.effort;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationStatus;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.SpeciesImpl;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.ui.swing.SammoaColors;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.FlightUIHandler;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.flight.TransectFlightModel;
import fr.ulr.sammoa.ui.swing.flight.action.CircleBackAction;
import fr.ulr.sammoa.ui.swing.flight.effort.action.MoveToNextEditableCellAction;
import fr.ulr.sammoa.ui.swing.flight.effort.action.MoveToNextRowEditableAction;
import fr.ulr.sammoa.ui.swing.flight.effort.action.MoveToPreviousEditableCellAction;
import fr.ulr.sammoa.ui.swing.flight.effort.action.MoveToPreviousRowEditableAction;
import fr.ulr.sammoa.ui.swing.util.AbstractRowHighlightPredicate;
import fr.ulr.sammoa.ui.swing.util.ButtonActionTableCellEditorRenderer;
import fr.ulr.sammoa.ui.swing.util.DateFlightRender;
import fr.ulr.sammoa.ui.swing.util.DateZonePicker;
import fr.ulr.sammoa.ui.swing.util.DeletedRowHighlightPredicate;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import fr.ulr.sammoa.ui.swing.util.SelectionModelAdapter;
import fr.ulr.sammoa.ui.swing.util.TableColumnModelStorage;
import fr.ulr.sammoa.ui.swing.util.TableDataChangeListener;
import fr.ulr.sammoa.ui.swing.util.TextCellEditor;
import fr.ulr.sammoa.ui.swing.util.TimeCellEditor;
import fr.ulr.sammoa.ui.swing.util.ValidRowHighlightPredicate;
import fr.ulr.sammoa.ui.swing.util.ValidationHelper;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import jaxx.runtime.validator.swing.SwingListValidatorDataLocator;
import jaxx.runtime.validator.swing.SwingListValidatorHighlightPredicate;
import jaxx.runtime.validator.swing.SwingListValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingListValidatorMessageTableRenderer;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Handler of {@link EffortPanel} ui.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.4
 */
public class EffortPanelHandler implements DeviceStateListener {

    private static final Log log =
            LogFactory.getLog(EffortPanelHandler.class);

    protected static final Color DEVICE_ERROR_BACKGROUND_COLOR = Color.RED;

    public static final String ROUTE_VALIDATOR_CONTEXT_VALUE =
            "routeValidator";

    public static final String OBSERVATION_VALIDATOR_CONTEXT_VALUE =
            "observationValidator";

    public static final String  GEO_POINT_VALIDATOR_CONTEXT_VALUE =
            "geoPointValidator";

    protected boolean comboIsAdjusting;

    private final SammoaUIContext context;

    private final EffortPanel ui;

    protected ReferentialService referentialService;

    protected TableColumnModelStorage routesColumnModelStorage;

    protected TableColumnModelStorage observationsColumnModelStorage;

    protected TableColumnModelStorage geoPointsColumnModelStorage;

    public EffortPanelHandler(EffortPanel ui) {
        this.ui = ui;
        context = ui.getContextValue(SammoaUIContext.class);

        this.referentialService = context.getService(ReferentialService.class);
    }

    public FlightUI getParentUI() {
        return ui.getContextValue(FlightUI.class, JAXXUtil.PARENT);
    }

    public FlightUIModel getModel() {
        return ui.getModel();
    }

    public void init() {

        final UIDecoratorService decoratorService =
                context.getService(UIDecoratorService.class);

        ListCellRenderer observeCellRenderer =
                decoratorService.newListCellRender(Observer.class);

        ui.getNavComboBox().setRenderer(observeCellRenderer);
        ui.getLeftComboBox().setRenderer(observeCellRenderer);
        ui.getRightComboBox().setRenderer(observeCellRenderer);
        ui.getConavComboBox().setRenderer(observeCellRenderer);

        JTable errorTable = ui.getErrorTable();

        // prepare error table ui
        SwingListValidatorMessageTableRenderer render = new SwingListValidatorMessageTableRenderer() {

            private static final long serialVersionUID = 1L;

            @Override
            protected String decorateBean(Object bean) {
                return decoratorService.getDecorator(bean).toString(bean);
            }

        };
        SwingValidatorUtil.installUI(
                errorTable,
                render
        );
        //FIXME: this should be done in JAXX (http://nuiton.org/issues/2303)
        //errorTable.getColumnModel().getColumn(1).setCellRenderer(render);

        ValidationHelper.getInstance().setGeoPointTableModel(ui.getGeoPointTableModel());

        initRouteTable();

        initObservationTable();

        initGeoPointTable();
    }


    /**
     * Add hightlighters on the editor of beans.
     *
     * @param validator   the validator where to find bean states
     * @param editor      the editor of beans
     * @param dataLocator the data locator
     * @param isToValidate the predicate if bean is to validate (may be null)
     * @param scopes      scopes to hightlight
     * @param <O>         type of bean to validate
     * @since 2.5.3
     */
    public static <O> void addHightLighterOnEditor(BeanListValidator<O> validator,
                                                   JXTable editor,
                                                   SwingListValidatorDataLocator<O> dataLocator,
                                                   HighlightPredicate isToValidate,
                                                   NuitonValidatorScope... scopes) {

        for (NuitonValidatorScope scope : scopes) {

            HighlightPredicate predicate = SwingListValidatorHighlightPredicate.newPredicate(
                    scope,
                    validator, dataLocator
            );

            if (isToValidate != null) {
                predicate = new HighlightPredicate.AndHighlightPredicate(isToValidate, predicate);
            }

            Color color = SwingValidatorUtil.getColor(scope);
            if (NuitonValidatorScope.INFO.equals(scope)) {
                color = new Color(0x40BAFF);
            }

            Highlighter highlighter = SammoaUtil.newBackgroundColorHighlighter(
                    predicate, color);
            editor.addHighlighter(highlighter);
        }
    }

    public Observer getObserverByPosition(Route route, Position position) {
        Observer result;
        if (route != null) {
            ObserverPosition observerPosition =
                    route.getObserverPositionByPosition(position);
            result = observerPosition.getObserver();

        } else {
            result = null;
        }
        return result;
    }

    public void setObserverByPosition(Observer newValue, Position position) {
        if (!comboIsAdjusting) {

            Route route = getModel().getRouteEditBean();
            if (route != null) {

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Set observer %s for position %s", newValue, position));
                }

                FlightService flightService = context.getService(FlightService.class);
                flightService.setRouteObserverByPosition(route, newValue, position);

                // the method setObserverByPosition could update other positions
                // so we need to refresh all comboboxes
                comboIsAdjusting = true;

                try {
                    if (Position.NAVIGATOR != position) {
                        ObserverPosition navPosition = route.getObserverPositionByPosition(Position.NAVIGATOR);
                        ui.getNavComboBox().setSelectedItem(navPosition.getObserver());
                    }

                    if (Position.FRONT_LEFT != position) {
                        ObserverPosition leftPosition = route.getObserverPositionByPosition(Position.FRONT_LEFT);
                        ui.getLeftComboBox().setSelectedItem(leftPosition.getObserver());
                    }

                    if (Position.FRONT_RIGHT != position) {
                        ObserverPosition rightPosition = route.getObserverPositionByPosition(Position.FRONT_RIGHT);
                        ui.getRightComboBox().setSelectedItem(rightPosition.getObserver());
                    }

                    if (Position.CO_NAVIGATOR != position) {
                        ObserverPosition conavPosition = route.getObserverPositionByPosition(Position.CO_NAVIGATOR);
                        ui.getConavComboBox().setSelectedItem(conavPosition.getObserver());
                    }
                } finally {

                    comboIsAdjusting = false;
                }

                // Fire on TableModel to update the row (highlighting)
                int index = getModel().indexOfRoutes(route);
                SwingUtil.ensureRowIndex(ui.getRouteTableModel(), index);
                ui.getRouteTableModel().fireTableRowsUpdated(index, index);

                // Update observation table to ensure observer's initials
                fireObservationsUpdated(route, false);
            }
        }
    }

    @Override
    public void stateChanged(DeviceStateEvent event) {
        DeviceState state = event.getNewValue();
        if (DeviceState.ERROR == state) {
            ui.getObservationTable().setBackground(DEVICE_ERROR_BACKGROUND_COLOR);

        } else {
            ui.getObservationTable().setBackground(Color.WHITE);
        }
    }

    public void initRouteTable() {

        JXTable table = ui.getRouteTable();
        RouteTableModel tableModel = ui.getRouteTableModel();

        SwingUtil.setI18nTableHeaderRenderer(
                table,
                n("sammoa.observations.routeTable.column.effortNumber"),
                n("sammoa.observations.routeTable.column.effortNumber.tip"),
                n("sammoa.observations.routeTable.column.beginTime"),
                n("sammoa.observations.routeTable.column.beginTime.tip"),
                n("sammoa.observations.routeTable.column.routeType"),
                n("sammoa.observations.routeTable.column.routeType.tip"),
                n("sammoa.observations.routeTable.column.transect"),
                n("sammoa.observations.routeTable.column.transect.tip"),
                n("sammoa.observations.routeTable.column.seaState"),
                n("sammoa.observations.routeTable.column.seaState.tip"),
                n("sammoa.observations.routeTable.column.swell"),
                n("sammoa.observations.routeTable.column.swell.tip"),
                n("sammoa.observations.routeTable.column.turbidity"),
                n("sammoa.observations.routeTable.column.turbidity.tip"),
                n("sammoa.observations.routeTable.column.skyGlint"),
                n("sammoa.observations.routeTable.column.skyGlint.tip"),
                n("sammoa.observations.routeTable.column.glareFrom"),
                n("sammoa.observations.routeTable.column.glareFrom.tip"),
                n("sammoa.observations.routeTable.column.glareTo"),
                n("sammoa.observations.routeTable.column.glareTo.tip"),
                n("sammoa.observations.routeTable.column.glareSeverity"),
                n("sammoa.observations.routeTable.column.glareSeverity.tip"),
                n("sammoa.observations.routeTable.column.glareUnder"),
                n("sammoa.observations.routeTable.column.glareUnder.tip"),
                n("sammoa.observations.routeTable.column.cloudCover"),
                n("sammoa.observations.routeTable.column.cloudCover.tip"),
                n("sammoa.observations.routeTable.column.subjectiveConditions"),
                n("sammoa.observations.routeTable.column.subjectiveConditions.tip"),
                n("sammoa.observations.routeTable.column.unexpectedLeft"),
                n("sammoa.observations.routeTable.column.unexpectedLeft.tip"),
                n("sammoa.observations.routeTable.column.unexpectedRight"),
                n("sammoa.observations.routeTable.column.unexpectedRight.tip"),
                n("sammoa.observations.routeTable.column.comment"),
                n("sammoa.observations.routeTable.column.comment.tip"),
                n("sammoa.observations.routeTable.column.deleted"),
                n("sammoa.observations.routeTable.column.deleted.tip")
        );

        init(table, new SelectionModelAdapter<Route>() {

            @Override
            public List<Route> getElements() {
                return getModel().getRoutes();
            }

            @Override
            public void setSelectedElement(Route element) {
                getModel().setRouteEditBean(element);
            }
        });

        // Listeners
        {
            int columnToEditOnInsert = RouteTableModel.RouteColumn.SEA_STATE.ordinal();
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_ROUTES,
                    new TableDataChangeListener(table, columnToEditOnInsert)
            );
        }

        BeanListValidator<Route> validator = ui.getRouteValidator();
        RouteValidatorDataLocator dataLocator = new RouteValidatorDataLocator();

        // Validation
        {
            JTable errorTable = ui.getErrorTable();
            SwingListValidatorMessageTableModel errorTableModel = ui.getErrorTableModel();

            validator.setContext(getValidatorContext());
            getParentUI().setContextValue(validator, ROUTE_VALIDATOR_CONTEXT_VALUE);

            SwingValidatorUtil.registerListValidator(
                    validator,
                    errorTableModel,
                    table,
                    errorTable,
                    dataLocator
            );

            for (Route route : tableModel.getBean()) {
                validator.addBean(route);
            }
        }

        // Renderers/Editors/Highlighters
        {
            UIDecoratorService decoratorService =
                    context.getService(UIDecoratorService.class);

            // TransectFlight
            {
                TableCellRenderer tableRenderer =
                        decoratorService.newTableCellRender(TransectFlightModel.class);
                TableCellRenderer renderer = new TransectFlightCellRenderer(
                        tableRenderer, getModel());

                JComboBox comboBox = new JComboBox();
                ListCellRenderer listRenderer =
                        decoratorService.newListCellRender(TransectFlightModel.class);
                comboBox.setRenderer(listRenderer);
                TableCellEditor editor = new TransectFlightCellEditor(
                        comboBox, getModel());

                table.setDefaultRenderer(TransectFlight.class, renderer);
                table.setDefaultEditor(TransectFlight.class, editor);
            }

            table.addHighlighter(SammoaUtil.newBackgroundColorHighlighter(
                    new RouteForSelectedTransectFlightHighlightPredicate(getModel()),
                    SammoaColors.ROUTE_FOR_TRANSECT_ROW_COLOR)
            );

            addHightLighterOnEditor(
                    validator,
                    table,
                    dataLocator,
                    null,
                    NuitonValidatorScope.ERROR,
                    NuitonValidatorScope.WARNING,
                    NuitonValidatorScope.INFO
            );

            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new RouteNoModificationHighlightPredicate(tableModel),
                    SammoaColors.ROUTE_NO_MODIFICATION_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new DeletedRowHighlightPredicate(getModel().getRoutes()),
                    SammoaColors.DELETED_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new ValidRowHighlightPredicate(getModel().getRoutes()),
                    SammoaColors.VALID_ROW_COLOR)
            );
        }

        routesColumnModelStorage = new TableColumnModelStorage(table, context.getConfig(), SammoaConfig.Table.routes);
        routesColumnModelStorage.init();
    }

    public void initObservationTable() {

        JXTable table = ui.getObservationTable();
        ObservationTableModel tableModel = ui.getObservationTableModel();

        List<String> headers = new ArrayList<>();


        headers.add(n("sammoa.observations.observationTable.column.observationNumber"));
        headers.add(n("sammoa.observations.observationTable.column.observationNumber.tip"));
        headers.add(n("sammoa.observations.observationTable.column.observationTime"));
        headers.add(n("sammoa.observations.observationTable.column.observationTime.tip"));
        headers.add(n("sammoa.observations.observationTable.column.position"));
        headers.add(n("sammoa.observations.observationTable.column.position.tip"));
        headers.add(n("sammoa.observations.observationTable.column.podSize"));
        headers.add(n("sammoa.observations.observationTable.column.podSize.tip"));
        headers.add(n("sammoa.observations.observationTable.column.species"));
        headers.add(n("sammoa.observations.observationTable.column.species.tip"));
        headers.add(n("sammoa.observations.observationTable.column.age"));
        headers.add(n("sammoa.observations.observationTable.column.age.tip"));
        headers.add(n("sammoa.observations.observationTable.column.decAngle"));
        headers.add(n("sammoa.observations.observationTable.column.decAngle.tip"));
        headers.add(n("sammoa.observations.observationTable.column.cue"));
        headers.add(n("sammoa.observations.observationTable.column.cue.tip"));
        headers.add(n("sammoa.observations.observationTable.column.behaviour"));
        headers.add(n("sammoa.observations.observationTable.column.behaviour.tip"));
        headers.add(n("sammoa.observations.observationTable.column.swimDir"));
        headers.add(n("sammoa.observations.observationTable.column.swimDir.tip"));

        if (getConfig().isDiveEnabled()) {
            headers.add(n("sammoa.observations.observationTable.column.dive"));
            headers.add(n("sammoa.observations.observationTable.column.dive.tip"));
        }

        if (getConfig().isReactionEnabled()) {
            headers.add(n("sammoa.observations.observationTable.column.reaction"));
            headers.add(n("sammoa.observations.observationTable.column.reaction.tip"));
        }

        headers.add(n("sammoa.observations.observationTable.column.calves"));
        headers.add(n("sammoa.observations.observationTable.column.calves.tip"));
        headers.add(n("sammoa.observations.observationTable.column.photo"));
        headers.add(n("sammoa.observations.observationTable.column.photo.tip"));
        headers.add(n("sammoa.observations.observationTable.column.comment"));
        headers.add(n("sammoa.observations.observationTable.column.comment.tip"));
        headers.add(n("sammoa.observations.observationTable.column.observationStatus"));
        headers.add(n("sammoa.observations.observationTable.column.observationStatus.tip"));
        headers.add(n("sammoa.observations.observationTable.column.deleted"));
        headers.add(n("sammoa.observations.observationTable.column.deleted.tip"));
        headers.add(n("sammoa.observations.observationTable.column.circleback"));
        headers.add(n("sammoa.observations.observationTable.column.circleback.tip"));

        SwingUtil.setI18nTableHeaderRenderer(table, headers.toArray(new String[0]));

        init(table, new SelectionModelAdapter<Observation>() {

            @Override
            public List<Observation> getElements() {
                return getModel().getObservations();
            }

            @Override
            public void setSelectedElement(Observation element) {
                getModel().setObservationEditBean(element);
            }
        });

        // Listeners
        {
            FlightController flightController =
                    getParentUI().getHandler().getFlightController();

            int columnToEditOnInsert = tableModel.getColumnIndex(ObservationTableModel.ObservationColumn.POD_SIZE);
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_OBSERVATIONS,
                    new TableDataChangeListener(table, columnToEditOnInsert)
            );

            // For CircleBack buttons
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_FLIGHT_STATE,
                    new PropertyChangeListener() {

                        @Override
                        public void propertyChange(PropertyChangeEvent evt) {

                            FlightUIModel model = (FlightUIModel) evt.getSource();

                            if (model.sizeObservations() > 0) {

                                ui.getObservationTableModel().fireTableRowsUpdated(
                                        0, model.sizeObservations() - 1);
                            }
                        }
                    });

            // Refresh matching observations from selected route
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_ROUTE_EDIT_BEAN, new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    onRouteChanged((Route) evt.getOldValue(),
                                   (Route) evt.getNewValue());
                }
            });

            // Used to update map for each Species change, Note that this could be
            // a heavy coast for onBoard screen
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_OBSERVATION_EDIT_BEAN, new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    onObservationChanged((Observation) evt.getOldValue(),
                                         (Observation) evt.getNewValue());
                }
            });

            if (!getModel().isValidationMode()) {
                flightController.getDeviceManager(GpsHandler.class).addDeviceStateListener(this);
                flightController.getDeviceManager(AudioRecorder.class).addDeviceStateListener(this);
            }

            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_CIRCLE_BACK_ENABLED,
                    evt -> onCircleBackEnabledChanged());

        }

        BeanListValidator<Observation> validator = ui.getObservationValidator();
        ObservationValidatorDataLocator dataLocator =
                new ObservationValidatorDataLocator();

        // Validation
        {
            JTable errorTable = ui.getErrorTable();
            SwingListValidatorMessageTableModel errorTableModel = ui.getErrorTableModel();

            validator.setContext(getValidatorContext());
            getParentUI().setContextValue(validator, OBSERVATION_VALIDATOR_CONTEXT_VALUE);

            SwingValidatorUtil.registerListValidator(
                    validator,
                    errorTableModel,
                    table,
                    errorTable,
                    dataLocator
            );

            for (Observation observation : tableModel.getBean()) {
                validator.addBean(observation);
            }
        }

        // Renderers/Editors/Highlighters
        {
            UIDecoratorService decoratorService =
                    context.getService(UIDecoratorService.class);

            TableCellRenderer stringRenderer = table.getDefaultRenderer(String.class);

            // ObservationStatus
            {
                JComboBox comboBox = new JComboBox(ObservationStatus.values());
                table.setDefaultEditor(
                        ObservationStatus.class, new DefaultCellEditor(comboBox));
            }

            // Position
            {
                TableCellRenderer renderer = new ObservationPositionCellRenderer(
                        stringRenderer, decoratorService, ui.getObservationTableModel());

                JComboBox comboBox = new JComboBox();
                comboBox.setRenderer(decoratorService.newListCellRender(Position.class));
                SwingUtil.fillComboBox(comboBox, ImmutableList.of(
                        Position.FRONT_LEFT, Position.NAVIGATOR, Position.FRONT_RIGHT
                ), null);

                table.setDefaultRenderer(Position.class, renderer);
                table.setDefaultEditor(
                        Position.class, new DefaultCellEditor(comboBox));
            }

            // CircleBack action
            {
                int circleBack = tableModel.getColumnIndex(ObservationTableModel.ObservationColumn.CIRCLE_BACK);
                ButtonActionTableCellEditorRenderer editorRenderer = new ButtonActionTableCellEditorRenderer(
                        CircleBackAction.CLIENT_PROPERTY_OBSERVATION,
                        () -> {
                            FlightUIHandler flightUIHandler = getParentUI().getHandler();
                            return flightUIHandler.getActionMap().get("circleBack");
                        },
                        observation -> {
                            boolean result = getModel().isCircleBackEnabled();
                            if (result) {
                                Route route = getModel().getCurrentRoute();
                                if (route != null) {
                                    result = route.getRouteType() != RouteType.TRANSIT;
                                }

                                //Check we are in the circleBack time frame
                                Date now = new Date();
                                boolean circleBackDateAllowed = (now.getTime() - ((Observation)observation).getObservationTime().getTime()) < context.getConfig().getCountdownCircleBackConfirmation() * 1000L;
                                result = result && ((circleBackDateAllowed && getModel().getCircleBackTimer() != 0 && getModel().getDeadtimeTimer() == 0) || !context.getConfig().isCircleBackTimerEnabled());
                            }
                            return result;
                        }
                );
                TableColumnExt column = table.getColumnExt(circleBack);

                column.setSortable(false);
                column.setCellEditor(editorRenderer);
                column.setCellRenderer(editorRenderer);
            }


            table.addHighlighter(SammoaUtil.newBackgroundColorHighlighter(
                    new ObservationForSelectedRouteHighlightPredicate(getModel()),
                    SammoaColors.OBSERVATION_FOR_ROUTE_ROW_COLOR)
            );

            addHightLighterOnEditor(
                    validator,
                    table,
                    dataLocator,
                    null,
                    NuitonValidatorScope.ERROR,
                    NuitonValidatorScope.WARNING
            );

            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new DeletedRowHighlightPredicate(getModel().getObservations()),
                    SammoaColors.DELETED_ROW_COLOR)
            );
            table.addHighlighter(SammoaUtil.newForegroundColorHighlighter(
                    new ValidRowHighlightPredicate(getModel().getObservations()),
                    SammoaColors.VALID_ROW_COLOR)
            );
        }

        observationsColumnModelStorage = new TableColumnModelStorage(table, context.getConfig(), SammoaConfig.Table.observations);
        observationsColumnModelStorage.init();
    }

    public SammoaConfig getConfig() {
        return context.getConfig();
    }

    public Species getSpecies(String code, Region region) {
        Species species = referentialService.getSpecies(code, region);
        if (species == null) {
            species = new SpeciesImpl(code, region);
            species.setLocalCreation(true);
        }
        return species;
    }

    public void initGeoPointTable() {
        JXTable table = ui.getGeoPointTable();

        if (getModel().isValidationMode()) {

            SwingUtil.setI18nTableHeaderRenderer(
                    table,
                    n("sammoa.geoPoints.geoPointTable.column.recordTime"),
                    n("sammoa.geoPoints.geoPointTable.column.recordTime.tip"),
                    n("sammoa.geoPoints.geoPointTable.column.latitude"),
                    n("sammoa.geoPoints.geoPointTable.column.latitude.tip"),
                    n("sammoa.geoPoints.geoPointTable.column.longitude"),
                    n("sammoa.geoPoints.geoPointTable.column.longitude.tip"),
                    n("sammoa.geoPoints.geoPointTable.column.altitude"),
                    n("sammoa.geoPoints.geoPointTable.column.altitude.tip"),
                    n("sammoa.geoPoints.geoPointTable.column.speed"),
                    n("sammoa.geoPoints.geoPointTable.column.speed.tip"),
                    n("sammoa.geoPoints.geoPointTable.column.captureDelay"),
                    n("sammoa.geoPoints.geoPointTable.column.captureDelay.tip")
            );

            init(table, new SelectionModelAdapter<GeoPoint>() {

                @Override
                public List<GeoPoint> getElements() {
                    return getModel().getGeoPoints();
                }

                @Override
                public void setSelectedElement(GeoPoint element) {

                }
            });

            // listener

            // Refresh matching observations from selected route
            getModel().addPropertyChangeListener(
                    FlightUIModel.PROPERTY_OBSERVATION_EDIT_BEAN, new PropertyChangeListener() {

                        @Override
                        public void propertyChange(PropertyChangeEvent evt) {
                            onObservationChanged((Observation) evt.getOldValue(),
                                    (Observation) evt.getNewValue());
                        }
                    }
            );

            table.addHighlighter(SammoaUtil.newBackgroundColorHighlighter(
                            new GoePointForSelectedObservationHighlightPredicate(getModel()),
                            SammoaColors.OBSERVATION_FOR_ROUTE_ROW_COLOR)
            );

            geoPointsColumnModelStorage = new TableColumnModelStorage(table, context.getConfig(), SammoaConfig.Table.geoPoints);
            geoPointsColumnModelStorage.init();

        } else {

            ui.getGeoPointTableScroll().setVisible(false);

        }
    }

    public <T> void init(final JXTable table,
                         SelectionModelAdapter<T> selectionModelAdapter) {

        final Action moveToNextEditableCell = new MoveToNextEditableCellAction(table);
        final Action moveToPreviousEditableCell = new MoveToPreviousEditableCellAction(table);
        final Action moveToPreviousRowEditableCell = new MoveToPreviousRowEditableAction(table);
        final Action moveToNextRowEditableCell = new MoveToNextRowEditableAction(table);

        // redéfini les comportements par defaut de la table par les notres
        table.getActionMap().put("selectNextRowCell", moveToNextEditableCell);
        table.getActionMap().put("selectNextColumnCell", moveToNextEditableCell);
        table.getActionMap().put("selectNextColumn", moveToNextEditableCell);
        table.getActionMap().put("selectPreviousColumn", moveToPreviousEditableCell);

        // Key adapter à ajouter sur les éditeurs où l'on souhaite gérer les
        // touches "entrer", "gauche", "doite" de facon personnalisée.
        KeyListener goNextCellAdapter = new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                    e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    e.consume();
                    moveToNextEditableCell.actionPerformed(null);
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    e.consume();
                    moveToPreviousEditableCell.actionPerformed(null);
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    e.consume();
                    moveToPreviousRowEditableCell.actionPerformed(null);
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    e.consume();
                    moveToNextRowEditableCell.actionPerformed(null);
                }
            }
        };

        // Text
        {
            TextCellEditor editor = new TextCellEditor();
            JTextField textField = editor.getComponent();
            textField.addKeyListener(goNextCellAdapter);
            textField.setBorder(new LineBorder(Color.GRAY, 2));
//            textField.setBorder(BasicBorders.getTextFieldBorder());
            table.setDefaultEditor(String.class, editor);
        }

        // Number
        {
            NumberCellEditor<Integer> editor =
                    JAXXWidgetUtil.newNumberTableCellEditor(Integer.class, false);
            editor.getNumberEditor().setSelectAllTextOnError(true);
            JTextField textField = editor.getNumberEditor().getTextField();
            textField.addKeyListener(goNextCellAdapter);
            textField.setBorder(new LineBorder(Color.GRAY, 2));
//            textField.setBorder(BasicBorders.getTextFieldBorder());
            table.setDefaultEditor(int.class, editor);
            table.setDefaultEditor(Integer.class, editor);

            NumberCellEditor<Double> editorDouble =
                    JAXXWidgetUtil.newNumberTableCellEditor(Double.class, true);
            editorDouble.getNumberEditor().setSelectAllTextOnError(true);
            JTextField textFieldDouble = editorDouble.getNumberEditor().getTextField();
            textFieldDouble.addKeyListener(goNextCellAdapter);
            textFieldDouble.setBorder(new LineBorder(Color.GRAY, 2));
//            textField.setBorder(BasicBorders.getTextFieldBorder());
            table.setDefaultEditor(double.class, editorDouble);
            table.setDefaultEditor(Double.class, editorDouble);

        }

        // Boolean
        {
            TableCellRenderer renderer = table.getDefaultRenderer(Boolean.class);
            table.setDefaultRenderer(boolean.class, renderer);

            TableCellEditor editor = table.getDefaultEditor(Boolean.class);
            table.setDefaultEditor(boolean.class, editor);
        }

        // Date
        {
            TimeZone timeZone = getModel().getFlight().getTimeZone();
            TableCellRenderer defaultDateCellRenderer = table.getDefaultRenderer(Date.class);
            DateFlightRender renderer = new DateFlightRender(
                    defaultDateCellRenderer, t("sammoa.timePattern"), timeZone);
            table.setDefaultRenderer(Date.class, renderer);

            DateZonePicker datePicker = new DateZonePicker();
            datePicker.setShowPopupButton(false);
            datePicker.setPatternLayout(t("sammoa.timePattern"));
            datePicker.setTimeZoneOffset(timeZone);


            getModel().getFlight().addPropertyChangeListener(
                    Flight.PROPERTY_TIME_ZONE,
                    evt -> {
                        TimeZone tz = TimeZone.class.cast(evt.getNewValue());
                        renderer.setTimeZone(tz);
                        datePicker.setTimeZoneOffset(tz);
                        table.repaint();
                    }
            );

            JTextField textField = datePicker.getEditor();
//            textField.addKeyListener(goNextCellAdapter);
            textField.setBorder(new LineBorder(Color.GRAY, 2));
            table.setDefaultEditor(Date.class, new TimeCellEditor(datePicker));
        }

        if (getModel().isValidationMode()) {
            TableRowFilterSupport.forTable(table).searchable(true).useTableRenderers(true).apply();
        }

        SammoaUtil.addTableSelectionListener(table, selectionModelAdapter);

        SwingUtil.scrollToTableSelection(table);
    }

    protected String getValidatorContext() {
        return getModel().isValidationMode() ? "validation" : "onBoard";
    }

    protected void fireObservationsUpdated(Route route, boolean scrollToFirst) {

        Iterable<Observation> observations = Observations.filterInRoute(
                getModel().getObservations(), route, getModel().getRoutes(), true);

        SammoaUtil.fireTableRowsUpdated(ui.getObservationTable(),
                                        getModel().getObservations(),
                                        observations,
                                        scrollToFirst);
    }

    private void onRouteChanged(Route oldValue, Route newValue) {

        if (oldValue != null) {
            fireObservationsUpdated(oldValue, false);
        }
        if (newValue != null) {
            fireObservationsUpdated(newValue, !getModel().isObservationValueIsAdjusting());
        }
    }

    private void onObservationChanged(Observation oldValue,
                                      Observation newValue) {

        if (oldValue != null) {
            oldValue.removePropertyChangeListener(
                    Observation.PROPERTY_SPECIES, observationSpeciesChangeListener);
        }

        if (newValue != null) {
            newValue.addPropertyChangeListener(
                    Observation.PROPERTY_SPECIES, observationSpeciesChangeListener);
        }
    }

    private void onCircleBackEnabledChanged() {

        ObservationTableModel tableModel = ui.getObservationTableModel();

        int rowCount = tableModel.getRowCount();
        tableModel.fireTableRowsUpdated(0, rowCount - 1);
    }

    private PropertyChangeListener observationSpeciesChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            Observation observation = (Observation) evt.getSource();

            getParentUI().getHandler().setObservationLabelInMap(observation);
        }
    };

    public void reloadColumnTableOrder() {
        routesColumnModelStorage.loadColumnsOrder();
        observationsColumnModelStorage.loadColumnsOrder();
        geoPointsColumnModelStorage.loadColumnsOrder();
    }

    /**
     * Editor for type {@link TransectFlight}. This will use the
     * {@link DefaultCellEditor} with {@link JComboBox} as editor component.
     * <p/>
     * The model used is the list of {@link TransectFlightModel} from
     * {@link FlightUIModel}. So we need to convert {@link TransectFlightModel}
     * to {@link TransectFlight} and listen the model change on property
     * {@link FlightUIModel#PROPERTY_TRANSECT_FLIGHTS}.
     * <p/>
     * We also want to ensure the change by asking a user question to confirm it.
     */
    private static class TransectFlightCellEditor extends DefaultCellEditor
            implements PropertyChangeListener {

        protected FlightUIModel model;

        protected TransectFlight initValue;

        public TransectFlightCellEditor(JComboBox comboBox,
                                        FlightUIModel model) {
            super(comboBox);
            this.model = model;
            SwingUtil.fillComboBox(comboBox, model.getTransectFlights(), null);
            model.addPropertyChangeListener(FlightUIModel.PROPERTY_TRANSECT_FLIGHTS, this);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {

            initValue = (TransectFlight) value;
            TransectFlightModel modelValue = TransectFlightModel.findTransectFlightModel(
                    model.getTransectFlights(), initValue);

            return super.getTableCellEditorComponent(table, modelValue, isSelected, row, column);
        }

        @Override
        public TransectFlight getCellEditorValue() {
            TransectFlightModel modelValue = (TransectFlightModel) super.getCellEditorValue();
            return modelValue == null ? null : modelValue.getSource();
        }

        @Override
        public boolean stopCellEditing() {
            boolean result;

            TransectFlight value = getCellEditorValue();
            if (value == null
                || Objects.equal(value, initValue)
                || SammoaUtil.askQuestion(editorComponent, t(
                    "sammoa.confirmDialog.changeRouteTransect.message",
                    value.getTransect().getName()))) {

                result = super.stopCellEditing();

            } else {
                cancelCellEditing();
                result = true;
            }
            return result;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            List<TransectFlightModel> newValue =
                    (List<TransectFlightModel>) evt.getNewValue();

            if (newValue != null) {
                SwingUtil.fillComboBox((JComboBox) editorComponent, newValue, null);
            }
        }
    }

    private static class TransectFlightCellRenderer implements TableCellRenderer {

        protected TableCellRenderer delegate;

        protected FlightUIModel model;

        public TransectFlightCellRenderer(TableCellRenderer delegate,
                                          FlightUIModel model) {
            this.delegate = delegate;
            this.model = model;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            Object newValue = value;

            if (value != null) {
                TransectFlight transectFlight = (TransectFlight) value;

                newValue = TransectFlightModel.findTransectFlightModel(
                        model.getTransectFlights(), transectFlight);
            }
            return delegate.getTableCellRendererComponent(
                    table, newValue, isSelected, hasFocus, row, column);
        }
    }

    private static class ObservationPositionCellRenderer implements TableCellRenderer {

        protected TableCellRenderer delegate;

        protected Decorator<Position> decorator;

        protected ObservationTableModel model;

        public ObservationPositionCellRenderer(TableCellRenderer delegate,
                                               UIDecoratorService decoratorService,
                                               ObservationTableModel model) {
            this.delegate = delegate;
            this.decorator = decoratorService.getDecoratorByType(Position.class);
            this.model = model;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            Observation observation = model.getBean(table.convertRowIndexToModel(row));

            String newValue = decorator.toString(observation.getPosition());

            Route route = Routes.findWithObservation(model.getFlightUIModel().getRoutes(), observation, true);

            if (route != null) {
                ObserverPosition observerPosition = route.getObserverPositionByPosition(observation.getPosition());

                if (observerPosition != null && observerPosition.getObserver() != null) {
                    newValue += " (" + observerPosition.getObserver().getInitials() + ")";
                }

            }

            return delegate.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
        }
    }

    private static class RouteValidatorDataLocator implements SwingListValidatorDataLocator<Route> {

        @Override
        public boolean acceptType(Class<?> beanType) {
            return Route.class.isAssignableFrom(beanType);
        }

        @Override
        public Pair<Integer, Integer> locateDataCell(TableModel tableModel,
                                                     Route bean,
                                                     String fieldName) {
            RouteTableModel model = (RouteTableModel) tableModel;

            Pair<Integer, Integer> cell =
                    model.getCell(bean, fieldName);
            return cell;
        }

        @Override
        public int locateBeanRowIndex(TableModel tableModel, Route bean) {
            RouteTableModel model = (RouteTableModel) tableModel;
            return model.getBeanIndex(bean);
        }

        @Override
        public Route locateBean(TableModel tableModel, int rowIndex) {
            RouteTableModel model =
                    (RouteTableModel) tableModel;
            return model.getBean(rowIndex);
        }
    }

    private static class ObservationValidatorDataLocator implements SwingListValidatorDataLocator<Observation> {

        @Override
        public boolean acceptType(Class<?> beanType) {
            return Observation.class.isAssignableFrom(beanType);
        }

        @Override
        public Pair<Integer, Integer> locateDataCell(TableModel tableModel,
                                                     Observation bean,
                                                     String fieldName) {
            ObservationTableModel model = (ObservationTableModel) tableModel;

            Pair<Integer, Integer> cell =
                    model.getCell(bean, fieldName);
            return cell;
        }

        @Override
        public int locateBeanRowIndex(TableModel tableModel, Observation bean) {
            ObservationTableModel model = (ObservationTableModel) tableModel;
            return model.getBeanIndex(bean);
        }

        @Override
        public Observation locateBean(TableModel tableModel, int rowIndex) {
            ObservationTableModel model =
                    (ObservationTableModel) tableModel;
            return model.getBean(rowIndex);
        }
    }

    private static class GeoPointValidatorDataLocator implements SwingListValidatorDataLocator<GeoPoint> {

        @Override
        public boolean acceptType(Class<?> beanType) {
            return GeoPoint.class.isAssignableFrom(beanType);
        }

        @Override
        public Pair<Integer, Integer> locateDataCell(TableModel tableModel,
                                                     GeoPoint bean,
                                                     String fieldName) {
            GeoPointTableModel model = (GeoPointTableModel) tableModel;

            Pair<Integer, Integer> cell =
                    model.getCell(bean, fieldName);
            return cell;
        }

        @Override
        public int locateBeanRowIndex(TableModel tableModel, GeoPoint bean) {
            GeoPointTableModel model = (GeoPointTableModel) tableModel;
            return model.getBeanIndex(bean);
        }

        @Override
        public GeoPoint locateBean(TableModel tableModel, int rowIndex) {
            GeoPointTableModel model =
                    (GeoPointTableModel) tableModel;
            return model.getBean(rowIndex);
        }
    }

    public static class RouteForSelectedTransectFlightHighlightPredicate extends AbstractRowHighlightPredicate<Route> {

        protected FlightUIModel UIModel;

        public RouteForSelectedTransectFlightHighlightPredicate(FlightUIModel UIModel) {
            this.UIModel = UIModel;
        }

        @Override
        protected boolean isHighlighted(Route route) {

            TransectFlightModel selectedTransectFlight = UIModel.getTransectFlightEditBean();

            boolean result;
            if (selectedTransectFlight != null) {

                result = selectedTransectFlight.getSource().equals(route.getTransectFlight());

            } else {
                result = false;
            }
            return result;
        }

        @Override
        protected Route getValueAt(int rowIndex) {
            return UIModel.getRoutes().get(rowIndex);
        }
    }

    public static class ObservationForSelectedRouteHighlightPredicate extends AbstractRowHighlightPredicate<Observation> {

        protected FlightUIModel UIModel;

        public ObservationForSelectedRouteHighlightPredicate(FlightUIModel UIModel) {
            this.UIModel = UIModel;
        }

        @Override
        protected boolean isHighlighted(Observation observation) {

            Route selectedRoute = UIModel.getRouteEditBean();

            boolean result;
            if (selectedRoute != null) {

                result = Observations.inRoute(observation, selectedRoute, UIModel.getRoutes(), true);

            } else {
                result = false;
            }
            return result;
        }

        @Override
        protected Observation getValueAt(int rowIndex) {
            return UIModel.getObservations().get(rowIndex);
        }
    }

    public class RouteNoModificationHighlightPredicate extends AbstractRowHighlightPredicate<Route> {

        protected RouteTableModel model;

        public RouteNoModificationHighlightPredicate(RouteTableModel model) {
            this.model = model;
        }

        @Override
        protected boolean isHighlighted(Route route) {

            boolean result = false;

            if (route.isDeleted()) {
                // do nothing

            } else {

                int previousRouteIndex = model.getBeanIndex(route) - 1;
                if (previousRouteIndex >= 0) {

                    Route previousRoute = getValueAt(previousRouteIndex);

                    result = Routes.equal(route, previousRoute);
                }
            }
            return result;
        }

        @Override
        protected Route getValueAt(int rowIndex) {
            return model.getRow(rowIndex);
        }
    }

    public static class GoePointForSelectedObservationHighlightPredicate extends AbstractRowHighlightPredicate<GeoPoint> {

        protected FlightUIModel UIModel;

        public GoePointForSelectedObservationHighlightPredicate(FlightUIModel UIModel) {
            this.UIModel = UIModel;
        }

        @Override
        protected boolean isHighlighted(GeoPoint geoPoint) {

            Observation observation = UIModel.getObservationEditBean();

            boolean result;
            if (observation != null) {

                result = observation.getObservationTime().equals(geoPoint.getRecordTime());

            } else {
                result = false;
            }
            return result;
        }

        @Override
        protected GeoPoint getValueAt(int rowIndex) {
            return UIModel.getGeoPoints().get(rowIndex);
        }
    }
}
