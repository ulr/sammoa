/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import com.google.common.collect.Iterables;

import javax.swing.AbstractListModel;

/**
 * Created: 12/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectListModel extends AbstractListModel {

    private static final long serialVersionUID = 1L;

    protected StrateModel strateModel;

//    protected ListSelectionModel selectionModel;

    public void setStrateModel(StrateModel strateModel) {
        this.strateModel = strateModel;
        fireContentsChanged(this, 0, strateModel.getTransects().size());
    }

    public StrateModel getStrateModel() {
        return strateModel;
    }

//    public ListSelectionModel getSelectionModel() {
//        if (selectionModel == null) {
//            selectionModel = new DefaultListSelectionModel();
//            selectionModel.addListSelectionListener(this);
//            selectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
//        }
//        return selectionModel;
//    }

    public Iterable<TransectModel> getSelectedTransects() {
        return Iterables.filter(getStrateModel().getTransects(), TransectModel.selectedInCurrentStrate());
    }

    @Override
    public int getSize() {
        return strateModel != null ? strateModel.getTransects().size() : 0;
    }

    @Override
    public TransectModel getElementAt(int index) {
        return strateModel != null ? strateModel.getTransects().get(index) : null;
    }
}
