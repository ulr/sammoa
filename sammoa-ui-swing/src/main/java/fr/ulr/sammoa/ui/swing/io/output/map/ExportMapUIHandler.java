package fr.ulr.sammoa.ui.swing.io.output.map;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.io.output.map.ExportMapModel;
import fr.ulr.sammoa.application.io.output.map.ExportMapService;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.Species2;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StringRef;
import fr.ulr.sammoa.persistence.StringRefs;
import fr.ulr.sammoa.ui.swing.SammoaScreen;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.swing.OneClicListSelectionModel;
import jaxx.runtime.swing.editor.bean.BeanListHeader;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of {@link ExportMapUI}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class ExportMapUIHandler implements SammoaUIHandler {

    private static final Log log =
            LogFactory.getLog(ExportMapUIHandler.class);

    private final ExportMapUI ui;

    protected SammoaUIContext context;

    protected final ExportMapService exportService;

    protected final ReferentialService referentialService;

    protected final UIDecoratorService decoratorService;

    public ExportMapUIHandler(SammoaUIContext context,
                              ExportMapUI ui) {
        this.context = context;
        this.ui = ui;
        this.referentialService = context.getService(ReferentialService.class);
        this.exportService = context.getService(ExportMapService.class);
        this.decoratorService = context.getService(UIDecoratorService.class);
    }

    @Override
    public void beforeInitUI() {

        //-- create model --//

        ExportMapUIModel model = new ExportMapUIModel();

        List<Survey> surveys = referentialService.getSurveys();
        //TODO-tchemit-2012-08-03 add an null survey (for test purpose, should be removed...)
        surveys.add(0, null);
        model.setSurveys(surveys);
        model.setRouteTypes(Lists.newArrayList(RouteType.values()));

        String surveyId = context.getSurveyId();

        if (surveyId != null) {

            Survey survey = referentialService.getSurvey(surveyId);

            // selected cam
            model.setSelectedSurvey(survey);
        }

        //-- share model and handler in jaxx context --//
        ui.setContextValue(this);
        ui.setContextValue(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                if (ExportMapUIModel.PROPERTY_SELECTED_SURVEY.equals(evt.getPropertyName())) {
                    Survey newValue = (Survey) evt.getNewValue();
                    onSelectedSurveyChanged(newValue);
                } else if (ExportMapUIModel.PROPERTY_STRATES.equals(evt.getPropertyName())) {
                    List<Strate> newValue = (List<Strate>) evt.getNewValue();
                    onStratesChanged(newValue);
                } else if (ExportMapUIModel.PROPERTY_SPECIES.equals(evt.getPropertyName())) {
                    List<Species> newValue = (List<Species>) evt.getNewValue();
                    onSpeciesChanged(newValue);
                } else if (ExportMapUIModel.PROPERTY_SPECIES_TYPES.equals(evt.getPropertyName())) {
                    List<StringRef> newValue = (List<StringRef>) evt.getNewValue();
                    onSpeciesTypesChanged(newValue);
                }
            }
        });
    }

    @Override
    public void afterInitUI() {

        ui.getSurveyCombobox().setRenderer(
                decoratorService.newListCellRender(Survey.class));

        ExportMapUIModel model = getModel();

        String suffix = String.format("_%1$tY_%1$tm_%1$td", new Date());
        model.setExportEffortsFilename("Efforts" + suffix);
        model.setExportObservationsFilename("Observations" + suffix);
        model.setExportGeoPointsFilename("GeoPoints" + suffix);

        File currentDirectory = FileUtil.getCurrentDirectory();
        String absolutePath = currentDirectory.getAbsolutePath();
        if (absolutePath.endsWith(File.separator + ".")) {
            currentDirectory = new File(absolutePath.substring(0, absolutePath.length() - 2));
        }
        currentDirectory = new File(currentDirectory, "export");
        if (log.isInfoEnabled()) {
            log.info("Current directory to use: " +  currentDirectory);
        }
        model.setExportDirectory(currentDirectory);

        SwingUtil.fillComboBox(ui.getSurveyCombobox(),
                               model.getSurveys(),
                               model.getSelectedSurvey());

        prepareList(ui.getStrateFilterListHeader(), model.getStrates());
        prepareList(ui.getRouteTypeFilterListHeader(), model.getRouteTypes());
        prepareList(ui.getSpeciesTypeFilterListHeader(), model.getSpeciesTypes());
        prepareList(ui.getSpeciesFilterListHeader(), model.getSpecies());

        onSelectedSurveyChanged(model.getSelectedSurvey());
    }

    protected void onStratesChanged(List<Strate> newValue) {
        if (log.isInfoEnabled()) {
            log.info("New strates to use: " +  newValue.size());
        }
        ui.getStrateFilterListHeader().setData(newValue);
    }

    protected void onSpeciesChanged(List<Species> newValue) {
        if (log.isInfoEnabled()) {
            log.info("New species to use: " +  newValue.size());
        }
        ui.getSpeciesFilterListHeader().setData(newValue);
    }

    protected void onSpeciesTypesChanged(List<StringRef> newValue) {
        if (log.isInfoEnabled()) {
            log.info("New species types to use: " +  newValue.size());
        }
        ui.getSpeciesTypeFilterListHeader().setData(newValue);
    }

    protected void onSelectedSurveyChanged(Survey newSurvey) {

        // reset strates,...
        if (log.isInfoEnabled()) {
            Decorator<Survey> decorator =
                    decoratorService.getDecoratorByType(Survey.class);
            log.info("New selected campain: " +
                        decorator.toString(newSurvey));
        }

        ExportMapUIModel model = getModel();

        model.setSelectedStrates(Collections.<Strate>emptyList());
        model.setSelectedRouteTypes(Collections.<RouteType>emptyList());
        model.setSelectedSpecies(Collections.<Species>emptyList());
        model.setSelectedSpeciesTypes(Collections.<StringRef>emptyList());

        List<Strate> strates;
        if (newSurvey == null) {
            strates = Collections.emptyList();
        } else {
            strates = referentialService.getAllStrates(newSurvey);
        }
        if (log.isDebugEnabled()) {
            log.debug("New strates: " +  strates.size());
        }
        model.setStrates(strates);

        List<Species> species;
        List<StringRef> speciesTypes;

        if (newSurvey == null) {
            species = Collections.emptyList();
            speciesTypes = Collections.emptyList();

        } else {
            species = referentialService.getAllSpecies(newSurvey);
            speciesTypes = StringRefs.toRefs(Species2.toSpeciesTypes(species));
        }
        model.setSpecies(species);
        model.setSpeciesTypes(speciesTypes);
    }

    public void close() {
        context.changeScreen(SammoaScreen.HOME);
    }

    @Override
    public void onCloseUI() {
    }

    public void chooseEffortDirectory() {
        File f = SammoaUtil.chooseDirectory(
                ui,
                t("sammoa.title.choose.export.directory"),
                t("sammoa.action.choose.export.directory"),
                ui.getModel().getExportDirectory()
        );
        ui.getModel().setExportDirectory(f);
    }

    public void exportEfforts() {

        Action startAction = new AbstractStartAction(
                t("sammoa.action.startExport"),
                ui) {

            private static final long serialVersionUID = 1L;

            @Override
            protected String getSuccessMessage(int nbRow, ExportMapModel dataModel) {
                return t("sammoa.messageDialog.map.export.success",
                        nbRow, dataModel.getExportDirectory());
            }

            @Override
            protected int doImport(ExportMapModel dataModel) {
                return exportService.exportEffortsMap(dataModel);
            }

            @Override
            protected ExportMapModel prepareModel(ExportMapUIModel model) {
                // persist the file name
                model.setExportEffortsFilename(model.getExportFilename());

                // create export service model
                ExportMapModel dataModel =
                        model.toModel(model.getExportEffortsFilename());
                return dataModel;
            }
        };

        ExportMapUIModel model = getModel();
        model.setSelectedStrates(
                getFilterValues(Strate.class, ui.getStrateFilter()));
        model.setSelectedRouteTypes(
                getFilterValues(RouteType.class, ui.getRouteTypeFilter()));

        displayExportLauncher(t("sammoa.title.export.exportEfforts"),
                              model.getExportEffortsFilename(),
                              model,
                              startAction);
    }


    public void exportObservations() {

        Action startAction = new AbstractStartAction(
                t("sammoa.action.startExport"),
                ui) {

            private static final long serialVersionUID = 1L;

            @Override
            protected String getSuccessMessage(int nbRow, ExportMapModel dataModel) {
                return t("sammoa.messageDialog.map.export.success",
                        nbRow, dataModel.getExportDirectory());
            }

            @Override
            protected int doImport(ExportMapModel dataModel) {
                return exportService.exportObservationsMap(dataModel);
            }

            @Override
            protected ExportMapModel prepareModel(ExportMapUIModel model) {
                // persist the file name
                model.setExportObservationsFilename(model.getExportFilename());

                // create export service model
                ExportMapModel dataModel =
                        model.toModel(model.getExportObservationsFilename());
                return dataModel;
            }
        };

        ExportMapUIModel model = getModel();
        model.setSelectedStrates(
                getFilterValues(Strate.class, ui.getStrateFilter()));
        model.setSelectedRouteTypes(
                getFilterValues(RouteType.class, ui.getRouteTypeFilter()));

        model.setSelectedSpecies(
                getFilterValues(Species.class, ui.getSpeciesFilter()));
        model.setSelectedSpeciesTypes(
                getFilterValues(StringRef.class, ui.getSpeciesTypeFilter()));

        displayExportLauncher(t("sammoa.title.export.exportObservations"),
                              model.getExportObservationsFilename(),
                              model,
                              startAction);
    }

    public void exportGeoPoints() {

        Action startAction = new AbstractStartAction(
                t("sammoa.action.startExport"),
                ui) {

            private static final long serialVersionUID = 1L;

            @Override
            protected String getSuccessMessage(int nbRow, ExportMapModel dataModel) {
                return t("sammoa.messageDialog.map.export.success",
                        nbRow, dataModel.getExportDirectory());
            }

            @Override
            protected int doImport(ExportMapModel dataModel) {
                return exportService.exportGeoPointsMap(dataModel);
            }

            @Override
            protected ExportMapModel prepareModel(ExportMapUIModel model) {
                // persist the file name
                model.setExportGeoPointsFilename(model.getExportFilename());

                // create export service model
                ExportMapModel dataModel =
                        model.toModel(model.getExportGeoPointsFilename());
                return dataModel;
            }
        };

        ExportMapUIModel model = getModel();
        displayExportLauncher(t("sammoa.title.export.exportGeoPoints"),
                              model.getExportGeoPointsFilename(),
                              model,
                              startAction);
    }

    public ExportMapUIModel getModel() {
        return ui.getModel();
    }

    protected <E> List<E> getFilterValues(Class<E> clazz, JList list) {
        List<E> result = Lists.newArrayList();
        for (Object object : list.getSelectedValuesList()) {
            result.add(clazz.cast(object));
        }
        return result;
    }

    protected void displayExportLauncher(String effortTitle,
                                         String defaultFilename,
                                         ExportMapUIModel model,
                                         Action startAction) {

        model.setExportFilename(defaultFilename);
        final ExportMapLauncherUI exportUI = new ExportMapLauncherUI(this);

        SwingUtil.setLayerUI(exportUI.getBody(),
                             exportUI.getBusyBlockLayerUI());

        JFrame frame = ui.getParentContainer(JFrame.class);
        Action closeAction = new AbstractAction(
                t("sammoa.action.cancelExport"),
                SwingUtil.createActionIcon("cancel")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                exportUI.getParentContainer(JDialog.class).setVisible(false);
            }
        };

        exportUI.getStart().setAction(startAction);
        exportUI.getCancel().setAction(closeAction);

        SammoaUtil.openInDialog(exportUI,
                                frame,
                                effortTitle,
                                closeAction,
                                new Dimension(550, 150));
    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list         le component graphique à initialiser
     * @param incomingData les données à mettre dans la liste
     * @since 0.5
     */
    protected <E> void prepareList(BeanListHeader<E> list, List<E> incomingData) {

        Class<E> beanType = list.getBeanType();

        JList jList = list.getList();

        //FIXME-tchemit 2012-08-01 Fix selection model which bugs when deselect
        OneClicListSelectionModel.installModel(jList);

        // init list
        Decorator<E> decorator = decoratorService.getDecoratorByType(beanType);
        Preconditions.checkNotNull(decorator,
                                   "No decorator found for type " + beanType);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        list.putClientProperty("decorator", decorator);
        List<E> data = Lists.newArrayList(incomingData);

        // sort data from first decorator context
        DecoratorUtil.sort((JXPathDecorator<E>) decorator, data, 0);

        // set datas to list and init renderer
        list.init((JXPathDecorator<E>) decorator, data);

        list.putClientProperty("data", data);
    }

    protected static abstract class AbstractStartAction extends AbstractAction {

        private static final long serialVersionUID = 1L;

        private final ExportMapUI ui;

        protected AbstractStartAction(String name, ExportMapUI ui) {
            super(name, SwingUtil.createActionIcon("accept"));
            this.ui = ui;
        }

        @Override
        public final void actionPerformed(ActionEvent e) {

            JButton source = (JButton) e.getSource();
            try {

                ExportMapUIModel model = ui.getModel();

                ExportMapModel dataModel = prepareModel(model);

                boolean doExport = checkFileToExport(dataModel);

                if (doExport) {

                    ExportMapLauncherUI fileChooserUI = SwingUtil.getParentContainer(
                            source, ExportMapLauncherUI.class);

                    fileChooserUI.getBusyBlockLayerUI().setBlock(true);
                    SammoaUtil.updateBusyState(fileChooserUI, true);

                    int nbRow = doImport(dataModel);

                    String successMessage = getSuccessMessage(nbRow, dataModel);

                    SammoaUtil.showSuccessMessage(fileChooserUI, successMessage);
                }

            } catch (Exception eee) {
                ErrorDialogUI.showError(eee);
            } finally {

                ExportMapLauncherUI fileChooserUI = SwingUtil.getParentContainer(
                        source, ExportMapLauncherUI.class);

                fileChooserUI.getBusyBlockLayerUI().setBlock(false);

                SwingUtil.getParentContainer(
                        source, JDialog.class).setVisible(false);

                SammoaUtil.updateBusyState(fileChooserUI, false);
            }
        }

        protected abstract String getSuccessMessage(int nbRow, ExportMapModel dataModel);

        protected abstract int doImport(ExportMapModel dataModel);

        protected abstract ExportMapModel prepareModel(ExportMapUIModel model);

        private boolean checkFileToExport(ExportMapModel dataModel) {
            boolean doExport = true;
            File dbf = dataModel.getExportFileWithoutExtension(".dbf");
            File shp = dataModel.getExportFileWithoutExtension(".shp");
            if (dbf.exists() ||
                shp.exists()) {
                doExport = SammoaUtil.askQuestion(
                        ui,
                        t("sammoa.messageDialog.mapOrDbf.exportFile.alreadyExists",
                                dbf, shp));
            }
            return doExport;
        }

    }

}
