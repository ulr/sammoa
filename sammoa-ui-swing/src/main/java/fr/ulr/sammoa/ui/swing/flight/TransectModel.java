package fr.ulr.sammoa.ui.swing.flight;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import fr.ulr.sammoa.persistence.Transect;
import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created: 26/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SOURCE = "source";

    public static final String PROPERTY_IN_CURRENT_STRATE = "inCurrentStrate";

    public static final String PROPERTY_SELECTED_IN_CURRENT_STRATE = "selectedInCurrentStrate";

    public static final String PROPERTY_IN_FLIGHT = "inFlight";

    public static final String PROPERTY_SELECTED_IN_FLIGHT = "selectedInFlight";

    public static final String PROPERTY_REAL_NB_TIMES = "realNbTimes";

    protected Transect source;

    protected boolean inCurrentStrate;

    protected boolean selectedInCurrentStrate;

    protected boolean inFlight;

    protected boolean selectedInFlight;

    protected int realNbTimes;

    public TransectModel(Transect source) {
        Preconditions.checkNotNull(source);
        this.source = source;
    }

    public Transect getSource() {
        return source;
    }

    public void setSource(Transect source) {
        Transect oldValue = getSource();
        this.source = source;
        firePropertyChange(PROPERTY_SOURCE, oldValue, source);
    }

    public boolean isInCurrentStrate() {
        return inCurrentStrate;
    }

    public void setInCurrentStrate(boolean inCurrentStrate) {
        boolean oldValue = isInCurrentStrate();
        this.inCurrentStrate = inCurrentStrate;
        firePropertyChange(PROPERTY_IN_CURRENT_STRATE, oldValue, inCurrentStrate);
        setSelectedInCurrentStrate(false);
    }

    public boolean isSelectedInCurrentStrate() {
        return selectedInCurrentStrate;
    }

    public void setSelectedInCurrentStrate(boolean selectedInCurrentStrate) {
        boolean oldValue = isSelectedInCurrentStrate();
        this.selectedInCurrentStrate = selectedInCurrentStrate;
        firePropertyChange(PROPERTY_SELECTED_IN_CURRENT_STRATE, oldValue, selectedInCurrentStrate);
    }

    public boolean isInFlight() {
        return inFlight;
    }

    public void setInFlight(boolean inFlight) {
        boolean oldValue = isInFlight();
        this.inFlight = inFlight;
        firePropertyChange(PROPERTY_IN_FLIGHT, oldValue, inFlight);
        if (!inFlight) {
            setSelectedInFlight(false);
        }
    }

    public boolean isSelectedInFlight() {
        return selectedInFlight;
    }

    public void setSelectedInFlight(boolean selectedInFlight) {
        boolean oldValue = isSelectedInFlight();
        this.selectedInFlight = selectedInFlight;
        firePropertyChange(PROPERTY_SELECTED_IN_FLIGHT, oldValue, selectedInFlight);
    }

    public int getRealNbTimes() {
        return realNbTimes;
    }

    public void setRealNbTimes(int realNbTimes) {
        int oldValue = getRealNbTimes();
        this.realNbTimes = realNbTimes;
        firePropertyChange(PROPERTY_REAL_NB_TIMES, oldValue, realNbTimes);
    }

    public static Predicate<TransectModel> selectedInCurrentStrate() {
        return new Predicate<TransectModel>() {

            @Override
            public boolean apply(TransectModel input) {
                return input.isSelectedInCurrentStrate();
            }
        };
    }

    public static Function<TransectModel, Transect> toTransect() {
        return new Function<TransectModel, Transect>() {

            @Override
            public Transect apply(TransectModel input) {
                return input.getSource();
            }
        };
    }
}
