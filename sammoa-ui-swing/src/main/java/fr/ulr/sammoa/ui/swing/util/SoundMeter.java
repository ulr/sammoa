package fr.ulr.sammoa.ui.swing.util;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.audio.AudioLineProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

/**
 * @author Sylvain Bavencoff <bavencoff@codelutin.com>
 */
public class SoundMeter extends JPanel {

    private static final Log log = LogFactory.getLog(SoundMeter.class);

    protected static final Color LEVEL_COLOR = new Color(0, 228, 0);

    protected static final Color WARNING_COLOR = new Color(253, 253, 0);

    protected static final Color SATURATE_COLOR = new Color(228, 0, 0);

    protected static final float WARNING_LEVEL = 0.85f;

    protected static final float SATURATE_LEVEL = 0.92f;

    protected static final int REFRESH_TIME = 50;

    protected AudioLineProvider audioLineProvider;

    protected float level;

    protected Timer timer;

    public SoundMeter() {

        try {
            audioLineProvider = AudioLineProvider.getInstance();

            timer = new Timer(REFRESH_TIME, e -> {
                level = audioLineProvider.getLevel();
                repaint();
            });
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to open sound meter", e);
            }
        }

        setBorder(BorderFactory.createLoweredBevelBorder());
    }

    public void start() {
        if (timer != null) {
            timer.start();
        }
    }

    public void stop() {
        if (timer != null) {
            audioLineProvider.closeLevelLine();
            timer.stop();
        }

        level = 0;
        repaint();
    }

    public void paint (Graphics g) {
        super.paint(g);

        if (isEnabled()) {
            Dimension d = getSize();

            int borderThickness = 2;

            int height = d.height - (borderThickness * 2);
            int width = d.width - (borderThickness * 2);

            if (level > 0) {
                if (level >= SATURATE_LEVEL) {
                    g.setColor(SATURATE_COLOR);
                } else if (level >= WARNING_LEVEL) {
                    g.setColor(WARNING_COLOR);
                } else {
                    g.setColor(LEVEL_COLOR);
                }

                int widthLevel = (int) (level * (float) width);
                g.fillRect(borderThickness, borderThickness, widthLevel, height);
            }

            // echele
            g.setColor(Color.black);
            for (int step = 1; step < 10; step++) {
                int x = borderThickness + step * width / 10;
                g.drawLine(x, borderThickness + height * 3 / 4, x, borderThickness + height);
            }

            g.setColor(WARNING_COLOR);
            int x = borderThickness + (int) (WARNING_LEVEL * width);
            g.drawLine(x, borderThickness, x, borderThickness + height);

            g.setColor(SATURATE_COLOR);
            x = borderThickness + (int) (SATURATE_LEVEL * width);
            g.drawLine(x, borderThickness, x, borderThickness + height);
        }
    }

}
