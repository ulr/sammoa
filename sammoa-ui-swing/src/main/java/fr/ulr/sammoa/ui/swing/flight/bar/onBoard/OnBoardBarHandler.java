/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.bar.onBoard;

import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.ui.swing.GPSCheck;
import fr.ulr.sammoa.ui.swing.SammoaColors;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/** @author sletellier <letellier@codelutin.com> */
public class OnBoardBarHandler implements PropertyChangeListener {

    private static final Log log =
            LogFactory.getLog(OnBoardBarHandler.class);

    protected final OnBoardBar ui;

    public OnBoardBarHandler(OnBoardBar ui) {
        this.ui = ui;
    }

    public FlightUI getParentUI() {
        return ui.getContextValue(FlightUI.class, JAXXUtil.PARENT);
    }

    protected OnBoardBarModel getModel() {
        return ui.getModel();
    }

    public void afterInitUI() {

        FlightUIModel flightUIModel = ui.getFlightUIModel();
        flightUIModel.addPropertyChangeListener(this);

        FlightController flightController =
                getParentUI().getHandler().getFlightController();

        AudioRecorder audioRecorder = flightController.getDeviceManager(AudioRecorder.class);
        ui.getAudioLED().setState(audioRecorder.getState());
        audioRecorder.addDeviceStateListener(ui.getAudioLED());

        GpsHandler gpsHandler = flightController.getDeviceManager(GpsHandler.class);
        ui.getGpsLED().setState(gpsHandler.getState());
        gpsHandler.addDeviceStateListener(ui.getGpsLED());
        gpsHandler.addGpsLocationListener(getModel());

        if (FlightState.OFF_EFFORT.equals(ui.getFlightUIModel().getFlightState())) {
            getModel().setEffortPanelColor(SammoaColors.ON_EFFORT_BACKGROUND_COLOR);
        }
    }
    
    protected void showGpsCheck() {
        FlightController flightController =
                getParentUI().getHandler().getFlightController();
        GpsHandler gpsHandler = flightController.getDeviceManager(GpsHandler.class);
        GPSCheck frame = new GPSCheck();
        frame.setGpsHandler(gpsHandler);
        frame.initConfig(SammoaUIContext.getUIContext().getConfig().getGpsConfig());
        frame.setVisible(true);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();

        if (FlightUIModel.PROPERTY_FLIGHT_STATE.equals(propertyName)) {

            if (log.isDebugEnabled()) {
                log.debug("Modify flight bar background color");
            }
            // si l'etat du vol passe en off effort
            // affiche à l'utilisateur
            FlightState newState = (FlightState) evt.getNewValue();
            if (FlightState.OFF_EFFORT.equals(newState)) {
                getModel().setEffortPanelColor(SammoaColors.ON_EFFORT_BACKGROUND_COLOR);

            } else {
                getModel().setEffortPanelColor(null);
            }

        } else if (FlightUIModel.PROPERTY_CURRENT_ROUTE.equals(propertyName)) {

            Route route = (Route) evt.getNewValue();

            if (log.isDebugEnabled()) {
                log.debug("New value received for current route " + route);
            }

            if (route == null || route.getTransectFlight() == null) {
                // set null will hide label
                ui.getLblTransect().setText(null);
            }

        }
    }
}
