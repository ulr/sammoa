package fr.ulr.sammoa.ui.swing;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.gps.GpsConfig;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.device.gps.GpsHandlerGpsylon;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.ui.swing.flight.bar.onBoard.DeviceStateLED;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;

/**
 * Petite application de test du GPS. Elle permet de choisir differentes
 * configuration gps et de reconnecter le gps si besoin.
 * <p>
 * La configuration choisie est inscrite et permet de la copier dans le fichier
 * de configuration (elle n'est pas enregistrée par l'application).
 * <p>
 *
 * Created: 10 decembre 2018
 *
 * @author Jean COUTEAU <couteau@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class GPSCheck extends JFrame {

    private static final Log log = LogFactory.getLog(GPSCheck.class);

    protected GpsHandler gpsHandler;
    protected GpsConfig config = new GpsConfig(new ApplicationConfig());

    protected JLabel latLabel = new JLabel("Lat : ");
    protected JLabel lat = new JLabel("");

    protected JLabel lonLabel = new JLabel("Lon : ");
    protected JLabel lon = new JLabel("");

    protected JLabel altLabel = new JLabel("Alt : ");
    protected JLabel alt = new JLabel("");

    protected JLabel spdLabel = new JLabel("Speed : ");
    protected JLabel spd = new JLabel("");

    protected JButton connectBtn = new JButton("Reconnect");

    protected JLabel deviceLabel = new JLabel("Device");
    protected JTextField device = new JTextField(config.getDevice());

    protected JLabel speedLabel = new JLabel("Speed");
    protected JTextField speed = new JTextField(Integer.toString(config.getSpeed()));

    protected JTextArea informationArea = new JTextArea();

    protected JButton closeBtn = new JButton("Close");

    protected DeviceStateLED gpsLED = new DeviceStateLED();


    public GPSCheck(){
        initUI();
        updateConfigInfo();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        updatePositionInfo();
    }

    protected void initUI () {
        Box box = Box.createVerticalBox();

        Box latBox = Box.createHorizontalBox();
        latBox.add(latLabel);
        latBox.add(lat);
        box.add(latBox);


        Box lonBox = Box.createHorizontalBox();
        lonBox.add(lonLabel);
        lonBox.add(lon);
        box.add(lonBox);


        Box altBox = Box.createHorizontalBox();
        altBox.add(altLabel);
        altBox.add(alt);
        box.add(altBox);


        Box spdBox = Box.createHorizontalBox();
        spdBox.add(spdLabel);
        spdBox.add(spd);
        box.add(spdBox);

        box.add(gpsLED);

        box.add(connectBtn);

        Box deviceBox = Box.createHorizontalBox();
        deviceBox.add(deviceLabel);
        deviceBox.add(device);
        try {
            device.setEnabled(!SammoaUIContext.getUIContext().isFlightMode());
        } catch (NullPointerException npe) {
            //launched in standalone mode
            device.setEnabled(true);
        }
        box.add(deviceBox);

        Box speedBox = Box.createHorizontalBox();
        speedBox.add(speedLabel);
        speedBox.add(speed);
        try {
            speed.setEnabled(!SammoaUIContext.getUIContext().isFlightMode());
        }  catch (NullPointerException npe) {
            //launched in standalone mode
            speed.setEnabled(true);
        }
        box.add(speedBox);

        informationArea.setBorder(BorderFactory.createRaisedBevelBorder());
        informationArea.setLineWrap(true);
        try {
            informationArea.setEnabled(!SammoaUIContext.getUIContext().isFlightMode());
        }  catch (NullPointerException npe) {
            //launched in standalone mode
            informationArea.setEnabled(true);
        }
        box.add(informationArea);

        box.add(closeBtn);

        //Finish the GUI and make visible
        setTitle("GPS Test");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        getContentPane().add(box, "Center");
        setBounds(0, 0, 400, 400);

        connectBtn.setEnabled(true);
        closeBtn.setEnabled(true);

        // ADD LISTENER
        device.addKeyListener(
                new KeyAdapter(){
                    @Override
                    public void keyReleased(KeyEvent e) {
                        if (config != null) {
                            config.setDevice(getDevice());
                        }
                        if (gpsHandler != null) {
                            gpsHandler.setConfig(config);
                        }
                        updateConfigInfo();
                    }
                });

        speed.addKeyListener(
                new KeyAdapter(){
                    @Override
                    public void keyReleased(KeyEvent e) {
                        if (config != null) {
                            config.setSpeed(getSpeed());
                        }
                        if (gpsHandler != null) {
                            gpsHandler.setConfig(config);
                        }
                        updateConfigInfo();
                    }
                });

        //Register anonymous listeners
        connectBtn.addActionListener(evt -> {

            if ( gpsHandler== null){
                setGpsHandler(new GpsHandlerGpsylon(config));
            }
            switch (gpsHandler.getState()) {
                case UNAVAILABLE:
                    gpsHandler.close();
                    gpsHandler.open();
                    gpsHandler.start();
                    break;
                case ERROR:
                    gpsHandler.close();
                    gpsHandler.open();
                    gpsHandler.start();
                    break;
                case READY:
                    gpsHandler.stop();
                    gpsHandler.start();
                    break;
                case RUNNING:
                    gpsHandler.stop();
                    gpsHandler.start();
                    break;
                default:
                    gpsHandler.close();
                    gpsHandler.open();
                    gpsHandler.start();
            }
        });

        closeBtn.addActionListener(evt ->
                dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
    }

    protected String getDevice() {
        return device.getText();
    }

    protected String getSpeed() {
        return speed.getText();
    }

    protected void updateConfigInfo() {
        informationArea.setText(String.format(
                "sammoa.gps.device=%s\n"
                + "sammoa.gps.speed=%s\n",
                getDevice(), getSpeed()));
    }

    protected void setLatitude(float latitude){
        lat.setText(Float.toString(latitude));
    }

    protected void setLongitude(float longitude){
        lon.setText(Float.toString(longitude));
    }

    protected void setAlt(float latitude){
        alt.setText(Float.toString(latitude));
    }

    protected void setSpd(float speed){
        spd.setText(Float.toString(speed));
    }

    protected void setDevice(String device){
        this.device.setText(device);
    }

    protected void setSpeed(int speed){
        this.speed.setText(Integer.toString(speed));
    }

    public void initConfig(GpsConfig config){
        this.config = config;
        setDevice(config.getDevice());
        setSpeed(config.getSpeed());
        updateConfigInfo();
    }

    public void setGpsHandler(GpsHandler handler){
        if (gpsHandler !=null) {
            gpsHandler.removeDeviceStateListener(gpsLED);
        }
        gpsHandler = handler;

        gpsLED.setState(gpsHandler.getState());
        gpsHandler.addDeviceStateListener(gpsLED);

        gpsHandler.addGpsLocationListener(evt -> {
            setPosition(evt.getNewValue());
        });
    }

    public static void main( String args[]) {
        GPSCheck frame = new GPSCheck();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void updatePositionInfo(){
        if (gpsHandler !=null) {
            setPosition(gpsHandler.getCurrentLocation());
        }
    }

    public void setPosition(GeoPoint position){
        if (position != null) {
            setLatitude((float) position.getLatitude());
            setLongitude((float) position.getLongitude());
            setSpd((float) position.getSpeed());
            setAlt((float) position.getAltitude());
        }
    }
}
