package fr.ulr.sammoa.ui.swing.flight;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.ui.swing.SammoaColors;
import org.jdesktop.swingx.util.PaintUtils;

import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectListCellRenderer implements ListCellRenderer {

    protected ListCellRenderer delegate;

    public TransectListCellRenderer(ListCellRenderer delegate) {
        this.delegate = delegate;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Preconditions.checkArgument(value instanceof TransectModel, "This renderer is for TransectModel only");

        Component result = delegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        TransectModel transect = (TransectModel) value;
        if (transect.getSource().getGraphicIndex() == null) {
            Color color = SammoaColors.TRANSECT_WITHOUT_GRAPHIC_BACKGROUND_COLOR;
            result.setBackground(color);
            if (isSelected) {
                result.setForeground(PaintUtils.computeForeground(color));
            }
        }
        return result;
    }
}
