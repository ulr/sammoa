package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ValidRouteAction extends ValidAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidRouteAction.class);

    private static final long serialVersionUID = 1L;

    protected boolean validatorIsAdjusting;

    public ValidRouteAction(JAXXContext context) {
        super(t("sammoa.action.validRoute"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.validRoute.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Route route = getModel().getRouteEditBean();

        if (route != null) {

            if (log.isDebugEnabled()) {
                log.debug("Validation for route " + route.getRouteType() + " : " + route.getEffortNumber());
            }

            if (route.isDeleted()) {

                if (askConfirmDelete(t("sammoa.validable.route"))) {
                    try {
                        SammoaUtil.updateBusyState((FlightUI) context, true);
                        Route routeChanged = getValidationService().validateRoute(route);
                        getModel().removeRoute(routeChanged);
                    } finally {
                        SammoaUtil.updateBusyState((FlightUI) context, false);
                    }
                }

            } else {

                boolean hasDeletedObservation = Observations.isAnyDeletedFromRoute(
                        getModel().getObservations(), route, getModel().getRoutes());

                if (!hasDeletedObservation
                    || askConfirmDeleteByCascade(t("sammoa.validable.route"),
                                                 t("sammoa.validable.observation"))) {

                    try {
                        SammoaUtil.updateBusyState((FlightUI) context, true);
                        Route routeChanged = getValidationService().validateRoute(route);
                        getModel().updateRoute(routeChanged);

                        validatorIsAdjusting = true;

                        getModel().setRouteEditBean(routeChanged);

                        reloadObservations();

                        validatorIsAdjusting = false;

                    } finally {
                        SammoaUtil.updateBusyState((FlightUI) context, false);
                    }
                }
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        boolean result = isEnabled();
        if (!validatorIsAdjusting) {
            Route bean = getModel().getRouteEditBean();
            result = bean != null
                     && getValidModel().isRouteValid(bean);
        }
        return result;
    }
}
