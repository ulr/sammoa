package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Observation;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ValidObservationAction extends ValidAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidObservationAction.class);

    private static final long serialVersionUID = 1L;

    public ValidObservationAction(JAXXContext context) {
        super(t("sammoa.action.validObservation"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.validObservation.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Observation observation = getModel().getObservationEditBean();

        if (observation != null) {

            if (log.isDebugEnabled()) {
                log.debug("Validation for observation " +  observation.getObservationNumber());
            }

            if (observation.isDeleted()) {

                if (askConfirmDelete(t("sammoa.validable.observation"))) {
                    Observation observationChanged = getValidationService().validateObservation(observation);
                    getModel().removeObservation(observationChanged);
                }

            } else {
                Observation observationChanged = getValidationService().validateObservation(observation);
                getModel().updateObservation(observationChanged);
                getModel().setObservationEditBean(observationChanged);
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        Observation observation = getModel().getObservationEditBean();
        if (observation != null && log.isDebugEnabled()) {
            log.debug("Check valid observation : " +  getValidModel().isObservationValid(observation));
        }
        return observation != null
               && getValidModel().isObservationValid(observation);
    }
}
