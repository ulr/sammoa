/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.action;

import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.flight.FlightUIHandler;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.util.NestedPropertyChangeListener;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created: 03/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public abstract class BaseFlightAction extends AbstractAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BaseFlightAction.class);

    protected JAXXContext context;

    public BaseFlightAction(String name, Icon icon, JAXXContext context) {
        super(name, icon);
        init(context);
    }

    public BaseFlightAction(String name, JAXXContext context) {
        super(name);
        init(context);
    }

    public BaseFlightAction(Icon icon, JAXXContext context) {
        this("", icon, context);
    }

    protected void init(JAXXContext context) {
        this.context = context;
        setEnabled(checkEnabled());
    }

    protected FlightController getFlightController() {
        return context.getContextValue(FlightUIHandler.class).getFlightController();
    }

    protected FlightUIModel getModel() {
        return context.getContextValue(FlightUIHandler.class).getModel();
    }

    protected SammoaUIContext getSammoaUIContext() {
        return context.getContextValue(SammoaUIContext.class);
    }

    protected void bindModelProperty(String property, boolean all) {
        NestedPropertyChangeListener listener =
                new NestedPropertyChangeListener(enabledListener, property, all);
        listener.init(getModel());
    }

    protected PropertyChangeListener enabledListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            setEnabled(checkEnabled());
        }
    };

    protected abstract boolean checkEnabled();
}
