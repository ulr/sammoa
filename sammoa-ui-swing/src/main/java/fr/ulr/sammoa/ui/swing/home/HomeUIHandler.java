package fr.ulr.sammoa.ui.swing.home;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Flights;
import fr.ulr.sammoa.persistence.HasTopiaIdPredicate;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.ui.swing.SammoaScreen;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 19/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class HomeUIHandler implements SammoaUIHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(HomeUIHandler.class);

    // XXX-fdesbois-2012-07-23 : used to avoid set unexpected set null during 
    // selections, this is a strange behavior of setElements that clear with 
    // fire remove previous elements and put selection to null
    protected boolean comboIsAdjusting;

    protected final SammoaUIContext context;

    protected final HomeUI ui;

    protected final ReferentialService referentialService;

    protected final FlightService flightService;

    public HomeUIHandler(SammoaUIContext context, HomeUI ui) {
        this.context = context;
        this.ui = ui;
        this.flightService = context.getService(FlightService.class);
        this.referentialService = context.getService(ReferentialService.class);
    }

    protected HomeUIModel getModel() {
        return ui.getModel();
    }

    protected SammoaConfig getConfig() {
        return context.getConfig();
    }

    @Override
    public void beforeInitUI() {
    }

    @Override
    public void afterInitUI() {

        UIDecoratorService decoratorService =
                context.getService(UIDecoratorService.class);

        ui.getSurveyCombobox().setRenderer(
                decoratorService.newListCellRender(Survey.class));
        ui.getFlightCombobox().setRenderer(
                decoratorService.newListCellRender(Flight.class));

        List<Survey> surveys = referentialService.getSurveys();
        ui.getSurveyListModel().setElements(surveys);

        if (surveys.isEmpty()) {
            // do nothing

        } else {

            Survey selectedSurvey = null;

            // Get survey from context, then from config
            String surveyId = context.getSurveyId();
            if (surveyId == null) {
                surveyId = getConfig().getSurveyId();
            }
            if (surveyId != null) {
                selectedSurvey =
                        Iterables.find(surveys, new HasTopiaIdPredicate(surveyId), null);
            }

            // Otherwise select the first one
            if (selectedSurvey == null) {
                selectedSurvey = surveys.get(0);
            }

            ui.getSurveyListModel().setSelectedItem(selectedSurvey);
            selectSurvey();
        }
    }

    @Override
    public void onCloseUI() {
        // nothing to do
    }

    public void selectSurvey() {

        if (!comboIsAdjusting) {

            comboIsAdjusting = true;

            Survey survey = ui.getSurveyListModel().getSelectedItem();

            if (log.isDebugEnabled()) {
                log.debug("Select the survey " +  survey.getCode());
            }

            getModel().setSurvey(survey);

            if (log.isDebugEnabled()) {
                log.debug("Get flights from survey " +  survey.getCode());
            }

            List<Flight> flights = flightService.getFlights(survey);
            ui.getModel().setFlights(flights);

            Set<String> systemIds =
                    Sets.newTreeSet(Iterables.transform(flights, Flights.toSystemId()));
            systemIds.add(getConfig().getSystemId());

            flights.add(0, null);

            if (log.isDebugEnabled()) {

                String systemsIdString = "[" + Joiner.on(";").join(
                        systemIds
                ) + "]";

                log.debug("Init systemId combobox = " +  systemsIdString);
            }

            ui.getSystemIdListModel().setElements(systemIds);

            Flight firstFlight = getFirstFlight(flights);

            String selectedSystemId = firstFlight == null
                                      ? getConfig().getSystemId()
                                      : firstFlight.getSystemId();

            ui.getSystemIdListModel().setSelectedItem(selectedSystemId);

            comboIsAdjusting = false;

            selectSystemId();
        }
    }

    public void selectSystemId() {

        if (!comboIsAdjusting) {

            comboIsAdjusting = true;

            String systemId = ui.getSystemIdListModel().getSelectedItem();

            if (log.isDebugEnabled()) {
                log.debug("Select the systemId " +  systemId);
            }

            getModel().setSystemId(systemId);

            Collection<Flight> flights = ui.getModel().getFlights();

            List<Flight> filteredFlights =
                    Lists.newArrayList(Iterables.filter(flights, Flights.withSystemId(systemId)));

            if (log.isDebugEnabled()) {

                String flightString = "[" + Joiner.on(";").join(
                        Iterables.transform(filteredFlights, Flights.toFlightNumber())
                ) + "]";

                log.debug("Init flights combobox filtered by " + systemId + " = " + flightString);
            }

            ui.getFlightListModel().setElements(filteredFlights);

            Flight selectedFlight = getFirstFlight(filteredFlights);

            ui.getFlightListModel().setSelectedItem(selectedFlight);

            comboIsAdjusting = false;

            selectFlight();
        }
    }

    public void selectFlight() {
        if (!comboIsAdjusting) {

            Flight flight = ui.getFlightListModel().getSelectedItem();

            if (log.isDebugEnabled()) {
                log.debug("Select the flight " + (flight != null ? "" + flight.getFlightNumber() : "null"));
            }

            getModel().setFlight(flight);
        }
    }

    public void showSelectedSurvey() {

        Survey survey = getModel().getSurvey();
        context.changeScreen(SammoaScreen.SURVEY, survey, null);
    }

    public void showNewSurvey() {
        context.setSurveyId(null);
        context.changeScreen(SammoaScreen.SURVEY);
    }

    public void showImportApplication() {
        context.changeScreen(SammoaScreen.IMPORT_APPLICATION);
    }

    public void showExportMap() {
        Survey survey = getModel().getSurvey();
        context.changeScreen(SammoaScreen.EXPORT_MAP, survey, null);
    }

    public void showExportApplication() {
        Survey survey = getModel().getSurvey();
        Flight flight = getModel().getFlight();
        context.changeScreen(SammoaScreen.EXPORT_APPLICATION, survey, flight);
    }

    public void showOnBoard() {

        // Save the surveyId in the config for next loading
        Survey survey = getModel().getSurvey();
        Preconditions.checkNotNull(survey);

        // check before that the region used has some species
        // see http://forge.codelutin.com/issues/1588
        List<Species> allSpecies =
                referentialService.getAllSpecies(survey);
        if (CollectionUtils.isEmpty(allSpecies)) {

            // can not authorize to go to this screen
            SammoaUtil.showErrorMessage(
                    ui, t("sammoa.validator.no.species.in.region"));
            return;
        }

        // ok can continue

        getConfig().setSurveyId(survey.getTopiaId());
        getConfig().save();

        Flight flight = getModel().getFlight();

        if (flight == null) {

            int nextFlightNumber = flightService.getNextFlightNumber();

            String response = JOptionPane.showInputDialog(
                    ui, t("sammoa.inputDialog.flightNumber.message"), nextFlightNumber);

            if (response == null) {
                // nothing to do

            } else {
                try {
                    int flightNumber = Integer.parseInt(response);

                    if (flightNumber < nextFlightNumber) {
                        SammoaUtil.showErrorMessage(
                                ui, t("sammoa.inputDialog.flightNumber.error.lessThanNextFlightNumber", nextFlightNumber));

                    } else {

                        // Create the flight
                        flight = flightService.createFlight(survey, flightNumber);

                        context.changeScreen(SammoaScreen.FLIGHT, survey, flight);
                    }

                } catch (NumberFormatException ex) {
                    SammoaUtil.showErrorMessage(
                            ui, t("sammoa.inputDialog.flightNumber.error.notANumber"));
                }
            }

        } else {

            context.changeScreen(SammoaScreen.FLIGHT, survey, flight);
        }
    }

    public void showValidation() {
        Survey survey = getModel().getSurvey();
        Flight flight = getModel().getFlight();
        context.changeScreen(SammoaScreen.VALIDATION, survey, flight);
    }

    protected Flight getFirstFlight(List<Flight> flights) {
        return flights.size() > 1 ? flights.get(1) : null;
    }

}
