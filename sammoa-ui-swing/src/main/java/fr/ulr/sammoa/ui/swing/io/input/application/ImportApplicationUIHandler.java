package fr.ulr.sammoa.ui.swing.io.input.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.io.BackupService;
import fr.ulr.sammoa.application.io.FlightStorage;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.input.application.ImportApplicationModel;
import fr.ulr.sammoa.application.io.input.application.ImportApplicationService;
import fr.ulr.sammoa.application.migration.MigrationRequester;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.ui.swing.MainUI;
import fr.ulr.sammoa.ui.swing.SammoaScreen;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.UIDecoratorService;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * UI Handler fo {@link ImportApplicationUI}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportApplicationUIHandler implements SammoaUIHandler, MigrationRequester {

    private static final Log log =
            LogFactory.getLog(ImportApplicationUIHandler.class);

    private final ImportApplicationUI ui;

    protected SammoaUIContext context;

    protected final ImportApplicationService importService;

    protected final ReferentialService referentialService;

    protected final FlightService flightService;

    protected final UIDecoratorService decoratorService;

    protected final BackupService backupService;

    public ImportApplicationUIHandler(SammoaUIContext context,
                                      ImportApplicationUI ui) {
        this.context = context;
        this.ui = ui;
        this.importService = context.getService(ImportApplicationService.class);
        this.flightService = context.getService(FlightService.class);
        this.referentialService = context.getService(ReferentialService.class);
        this.decoratorService = context.getService(UIDecoratorService.class);
        this.backupService = context.getService(BackupService.class);
    }

    @Override
    public void beforeInitUI() {

        //-- create model --//

        ImportApplicationUIModel model = new ImportApplicationUIModel();

        //-- share model and handler in jaxx context --//
        ui.setContextValue(this);
        ui.setContextValue(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                String propertyName = evt.getPropertyName();
                if (ImportApplicationUIModel.PROPERTY_IMPORT_FILE_LOADED.equals(propertyName)) {
                    boolean newValue = (Boolean) evt.getNewValue();
                    onImportFileLoaded(newValue);
                } else if (ImportApplicationUIModel.PROPERTY_SURVEY_EXIST.equals(propertyName)) {
                    boolean newValue = (Boolean) evt.getNewValue();
                    onSurveyExistChanged(newValue);
                }

                if (!ImportApplicationUIModel.PROPERTY_VALID.equals(propertyName)) {

                    // validate model
                    validateModel();
                }
            }
        });
    }

    @Override
    public void afterInitUI() {

        String backupFilename = String.format(
                "backup-%1$td-%1$tm-%1$ty-%1$tH_%1$tM_%1$tS.zip", new Date());
        getModel().setBackupFilename(backupFilename);

        // Boolean editor/renderer on table
        {
            JTable table = ui.getLoadImportFileResultFlightsTable();
            TableCellEditor editor = table.getDefaultEditor(Boolean.class);
            table.setDefaultEditor(boolean.class, editor);
            TableCellRenderer renderer = table.getDefaultRenderer(Boolean.class);
            table.setDefaultRenderer(boolean.class, renderer);
        }

        SwingUtil.setLayerUI(ui.getLoadImportFileResultPanel(),
                             ui.getBusyBlockLayerUI());

        onImportFileLoaded(false);
    }

    @Override
    public void onCloseUI() {
    }

    public void importFiles() {

        String absolutePath = FileUtil.getCurrentDirectory().getAbsolutePath();
        if (absolutePath.endsWith(File.separator + ".")) {
            absolutePath = absolutePath.substring(0, absolutePath.length() - 2);
        }

        JFileChooser chooser = new JFileChooser(absolutePath);
        chooser.setDialogTitle(t("sammoa.dialog.title.importSammoaFile"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Sammoa files", "sammoa"));
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(true);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.showOpenDialog(ui);
        File[] files = chooser.getSelectedFiles();

        JFrame frame = ui.getParentContainer(JFrame.class);
        List<FlightImportEntry> entriesAlreadyLoad;
        List<FlightImportEntry> flightImportEntries;

        try {
            SammoaUtil.updateBusyState(frame.getRootPane(), true);

            flightImportEntries = Stream.of(files)
                    .map(file -> importService.loadSurveyStorage(file, this))
                    .map(this::addSurveyStorage)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            entriesAlreadyLoad = flightImportEntries
                    .stream()
                    .filter(flight1 -> getModel().getFlightEntries()
                            .stream()
                            .anyMatch(flight1::equals))
                    .collect(Collectors.toList());
        } finally {
            SammoaUtil.updateBusyState(frame.getRootPane(), false);
        }

        if (CollectionUtils.isNotEmpty(entriesAlreadyLoad)) {
            String message = entriesAlreadyLoad
                    .stream()
                    .map(flight -> t(
                            "sammoa.confirmDialog.replaceAlReadyLoadFlight.item.message",
                            flight.getStorage().getName(),
                            flight.getFlightStorage().getName()))
                    .reduce(t("sammoa.confirmDialog.replaceAlReadyLoadFlight.message"),
                            (m1, m2) -> m1 + "\n        " + m2);


            if (!SammoaUtil.askQuestion(ui, message)) {
                flightImportEntries = flightImportEntries
                        .stream()
                        .filter(flight1 -> getModel().getFlightEntries()
                                .stream()
                                .noneMatch(flight1::equals))
                        .collect(Collectors.toList());

            }
        }

        getModel().addFlightImportEntries(flightImportEntries);

        onImportFileLoaded(true);
    }

    public void importApplication() {

        SwingWorker task = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                ImportApplicationUIModel model = getModel();

                if (model.isValid()) {

                    if (model.isBackup()) {

                        String backupFilename = model.getBackupFilename();

                        try {
                            SammoaUtil.updateBusyState(ui, true);
                            ui.getBusyBlockLayerUI().setBlock(true);

                            // backup sammoa data
                            backupService.backupApplication(backupFilename, s -> setStatus(s));
                            SammoaUtil.showSuccessMessage(
                                    ui,
                                    t("sammoa.messageDialog.sammoa.backup.success",
                                            getBackupFile(backupFilename)));
                        } catch (Exception e) {
                            ErrorDialogUI.showError(e);
                        } finally {
                            ui.getBusyBlockLayerUI().setBlock(false);
                            SammoaUtil.updateBusyState(ui, false);
                        }
                    }

                    // get service model

                    ImportApplicationModel dataModel = model.toModel();

                    SammoaUtil.updateBusyState(ui, true);
                    try {
                        importService.importApplication(dataModel, s -> setStatus(s));

                        SammoaUtil.updateBusyState(ui, false);

                        SammoaUtil.showSuccessMessage(
                                ui,
                                t("sammoa.messageDialog.aplication.import.success"));
                        setStatus("");

                    } catch (Exception e) {
                        ErrorDialogUI.showError(e);
                    } finally {
                        SammoaUtil.updateBusyState(ui, false);
                        close();
                    }


                }
                return null;
            }
        };
        task.execute();

    }

    protected void setStatus(String status) {
        MainUI mainUI = ui.getParentContainer(MainUI.class);
        mainUI.getStatus().setStatus(status);
    }

    public void close() {
        context.changeScreen(SammoaScreen.HOME);
    }

    public String getBackupInfo(String filename) {
        File backupFile = getBackupFile(filename == null ? "" : filename);
        String result = t("sammoa.label.exportApplication.backupInfo", backupFile);
        return result;
    }

    protected ImportApplicationUIModel getModel() {
        return ui.getModel();
    }

    protected List<FlightImportEntry> addSurveyStorage(SurveyStorage storage) {

        List<FlightImportEntry> flightImportEntryList;

        boolean existingSurvey;

        SurveyStorage oldStorage = null;
        Map<String, Object> naturalId = storage.getNaturalId();

        if (log.isInfoEnabled()) {
            log.info("Discover campaing " +  naturalId);
        }

        Survey survey = referentialService.getSurveyByNaturalId(naturalId);

        existingSurvey = survey != null;

        if (existingSurvey) {

            oldStorage = referentialService.getSurveyStorage(survey.getTopiaId());
        }

        if (log.isInfoEnabled()) {
            log.info("Is survey exist ? " +  existingSurvey);
        }

        flightImportEntryList = Lists.newArrayList();

        for (FlightStorage flightStorage : storage) {

            Map<String, Object> flightNaturalId = flightStorage.getNaturalId();

            if (log.isInfoEnabled()) {
                log.info("Discover flight " +  flightNaturalId);
            }
            Flight flight = null;

            if (existingSurvey) {

                // try to find existing fligth from his natural id
                flight = flightService.getFlightByNaturalId(flightNaturalId);

                if (log.isInfoEnabled()) {
                    log.info("Is flight exist ? " +  flight != null);
                }
            }

            FlightImportEntry flightImportEntry = new FlightImportEntry(storage, existingSurvey, flightStorage, oldStorage, flight);
            flightImportEntry.setTreat(true);
            flightImportEntryList.add(flightImportEntry);
        }

        return flightImportEntryList;
    }

    protected void onImportFileLoaded(boolean newValue) {

        JTable table = ui.getLoadImportFileResultFlightsTable();
        JLabel label = ui.getLoadImportFileResultInfo();

        String infoLabel;

        ImportApplicationUIModel model = getModel();
            // import file was loaded, let's add
        if (CollectionUtils.isNotEmpty(model.getFlightEntries())) {

            FlightTableModel flightTableModel = new FlightTableModel(model.getFlightEntries());
            flightTableModel.addTableModelListener(e -> this.validateModel());

            table.setModel(flightTableModel);

            infoLabel =
                    t("sammoa.info.importApplication.importfile.loaded");

            SwingUtil.setI18nTableHeaderRenderer(
                    table,
                    n("sammoa.importApplication.flightTable.column.surveyName"),
                    n("sammoa.importApplication.flightTable.column.surveyName.tip"),
                    n("sammoa.importApplication.flightTable.column.surveyExist"),
                    n("sammoa.importApplication.flightTable.column.surveyExist.tip"),
                    n("sammoa.importApplication.flightTable.column.flightName"),
                    n("sammoa.importApplication.flightTable.column.flightName.tip"),
                    n("sammoa.importApplication.flightTable.column.flightExist"),
                    n("sammoa.importApplication.flightTable.column.flightExist.tip"),
                    n("sammoa.importApplication.flightTable.column.toTreat"),
                    n("sammoa.importApplication.flightTable.column.toTreat.tip"));


        } else {

            // nothing is loaded, just remove table model and change label

            table.setModel(new DefaultTableModel());
            infoLabel = t("sammoa.info.importApplication.no.importfile.loaded");
        }

        label.setText(infoLabel);
    }

    protected void onSurveyExistChanged(boolean newValue) {

        // always force to backup if data could be lost
        if (newValue) {
            getModel().setBackup(true);
        }
    }

    protected void validateModel() {

        ImportApplicationUIModel model = getModel();

        boolean valid = model.getFlightEntries().stream().anyMatch(FlightImportEntry::isTreat);

        if (model.isBackup()) {

            // validate that backup filename not empty and does not exist
            // in backup directory
            String backupFilename = model.getBackupFilename();
            if (Strings.isNullOrEmpty(backupFilename)) {
                valid = false;
            } else {
                File backupFile = getBackupFile(backupFilename);
                if (backupFile.exists()) {
                    valid = false;
                }
            }
        }

        model.setValid(valid);
    }

    protected File getBackupFile(String backupFilename) {
        File result = SammoaDirectories.getBackupFile(
                context.getConfig().getDataDirectory(), backupFilename);
        return result;
    }

    @Override
    public <V> V ask(String title, String question, V[] values, V defaultValue) {
        return SammoaUtil.askValueForce(ui, title, question, values, defaultValue);
    }
}
