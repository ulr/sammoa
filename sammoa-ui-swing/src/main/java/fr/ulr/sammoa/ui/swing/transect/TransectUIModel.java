package fr.ulr.sammoa.ui.swing.transect;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectImpl;
import fr.ulr.sammoa.ui.swing.flight.StrateModel;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Created: 23/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_TRANSECT_REFERENTIAL = "transectReferential";

    public static final String PROPERTY_STRATE_REFERENTIAL = "strateReferential";

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_STRATE = "strate";

    public static final String PROPERTY_NAME = "name";

    protected List<Transect> transectReferential;

    protected List<StrateModel> strateReferential;

    protected String id;

    protected StrateModel strate;

    protected String name;

    /**
     * Prepare create using {@code transectReferential} to check unicity
     *
     * @param strateReferential List of Strate as referential
     * @param currentStrate     Set the current strate
     */
    public void prepareCreate(List<StrateModel> strateReferential,
                              StrateModel currentStrate) {
        setId(null);
        setStrate(currentStrate);
        setName(null);
        setTransectReferential(null);
        setStrateReferential(strateReferential);
    }

    /**
     * Instanciate a new {@link fr.ulr.sammoa.persistence.Transect} bean with values from this model.
     * Note that the {@code name} and {@code strate} properties are mandatory (check in validation)
     * otherwise an exception will occurs.
     *
     * @return a new {@link fr.ulr.sammoa.persistence.Transect} bean
     */
    public Transect newBean() {
        Transect result = new TransectImpl(getName(), getStrate().getSource());
        result.setTopiaId(getId());
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
    }

    public List<Transect> getTransectReferential() {
        if (transectReferential == null) {
            transectReferential = Lists.newArrayList();
        }
        return transectReferential;
    }

    public void setTransectReferential(List<Transect> transectReferential) {
        List<Transect> oldValue = Lists.newArrayList(getTransectReferential());
        this.transectReferential = transectReferential;
        firePropertyChange(PROPERTY_TRANSECT_REFERENTIAL, oldValue, transectReferential);
    }

    public List<StrateModel> getStrateReferential() {
        if (strateReferential == null) {
            strateReferential = Lists.newArrayList();
        }
        return strateReferential;
    }

    public void setStrateReferential(List<StrateModel> strateReferential) {
        List<StrateModel> oldValue = Lists.newArrayList(getStrateReferential());
        this.strateReferential = strateReferential;
        firePropertyChange(PROPERTY_STRATE_REFERENTIAL, oldValue, strateReferential);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    public StrateModel getStrate() {
        return strate;
    }

    public void setStrate(StrateModel strate) {
        StrateModel oldValue = getStrate();
        this.strate = strate;
        firePropertyChange(PROPERTY_STRATE, oldValue, strate);
    }
}
