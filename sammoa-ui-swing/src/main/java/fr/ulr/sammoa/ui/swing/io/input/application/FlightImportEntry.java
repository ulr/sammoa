package fr.ulr.sammoa.ui.swing.io.input.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.FlightStorage;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.persistence.Flight;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Objects;

/**
 * Define a flight storage entry found in storage to import with his optional
 * existing flight from db.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class FlightImportEntry extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    /** Loaded survey storage. */
    protected final SurveyStorage storage;

    /** Is survey exists ? */
    protected final boolean surveyExist;

    /** The storage of the flight loaded from the sammoa file. */
    protected final FlightStorage flightStorage;

    /** The optional existing flight loaded from db. */
    protected final SurveyStorage oldStorage;

    /** The optional existing flight loaded from db. */
    protected final Flight existingFlight;

    /** Flag to treat or not the incoming flight. */
    protected boolean treat;

    public FlightImportEntry(SurveyStorage storage, boolean surveyExist, FlightStorage flightStorage,
                             SurveyStorage oldStorage, Flight existingFlight) {
        this.storage = storage;
        this.surveyExist = surveyExist;
        this.flightStorage = flightStorage;
        this.oldStorage = oldStorage;
        this.existingFlight = existingFlight;
    }

    public SurveyStorage getStorage() {
        return storage;
    }

    public boolean isSurveyExist() {
        return surveyExist;
    }

    public FlightStorage getFlightStorage() {
        return flightStorage;
    }

    public SurveyStorage getOldStorage() {
        return oldStorage;
    }

    public Flight getExistingFlight() {
        return existingFlight;
    }

    public boolean isTreat() {
        return treat;
    }

    public void setTreat(boolean treat) {
        this.treat = treat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightImportEntry that = (FlightImportEntry) o;
        return Objects.equals(getStorage().getId(), that.getStorage().getId()) &&
                Objects.equals(getFlightStorage().getId(), that.getFlightStorage().getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getStorage().getId(), getFlightStorage().getId());
    }
}
