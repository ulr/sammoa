package fr.ulr.sammoa.ui.swing.io.output.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.io.output.application.ExportApplicationModel;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.ui.swing.io.AbstractApplicationModelWithBackup;
import org.nuiton.topia.persistence.TopiaEntities;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Model of export application UI.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ExportApplicationUIModel extends AbstractApplicationModelWithBackup {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SURVEYS = "surveys";

    public static final String PROPERTY_SELECTED_SURVEY = "selectedSurvey";

    public static final String PROPERTY_FLIGHTS = "flights";

    public static final String PROPERTY_SELECTED_FLIGHT = "selectedFlight";

    public static final String PROPERTY_EXPORT_DIRECTORY = "exportDirectory";

    public static final String PROPERTY_EXPORT_FILENAME = "exportFilename";

    public static final String PROPERTY_CALLBACK = "callback";

    protected List<Survey> surveys;

    protected Survey selectedSurvey;

    protected List<Flight> flights;

    protected Flight selectedFlight;

    protected File exportDirectory;

    protected String exportFilename;

    protected ExportApplicationCallbackMode callback;

    public File getExportDirectory() {
        return exportDirectory;
    }

    public void setExportDirectory(File exportDirectory) {
        File oldValue = this.exportDirectory;
        this.exportDirectory = exportDirectory;
        firePropertyChange(PROPERTY_EXPORT_DIRECTORY, oldValue, exportDirectory);
    }

    public String getExportFilename() {
        return exportFilename;
    }

    public void setExportFilename(String exportFilename) {
        String oldValue = this.exportFilename;
        this.exportFilename = exportFilename;
        firePropertyChange(PROPERTY_EXPORT_FILENAME, oldValue, exportFilename);
    }

    public List<Survey> getSurveys() {
        if (surveys == null) {
            surveys = Lists.newArrayList();
        }
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        List<Survey> oldValue = this.surveys;
        this.surveys = surveys;
        firePropertyChange(PROPERTY_SURVEYS, oldValue, surveys);
    }

    public Survey getSelectedSurvey() {
        return selectedSurvey;
    }

    public void setSelectedSurvey(Survey selectedSurvey) {
        Survey oldValue = this.selectedSurvey;
        this.selectedSurvey = selectedSurvey;
        firePropertyChange(PROPERTY_SELECTED_SURVEY, oldValue, selectedSurvey);
    }

    public List<Flight> getFlights() {
        if (flights == null) {
            flights = Lists.newArrayList();
        }
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        List<Flight> oldValue = this.flights;
        this.flights = flights;
        firePropertyChange(PROPERTY_FLIGHTS, oldValue, flights);
    }

    public Flight getSelectedFlight() {
        return selectedFlight;
    }

    public void setSelectedFlight(Flight selectedFlight) {
        Flight oldValue = this.selectedFlight;
        this.selectedFlight = selectedFlight;
        firePropertyChange(PROPERTY_SELECTED_FLIGHT, oldValue, selectedFlight);
    }

    public ExportApplicationCallbackMode getCallback() {
        return callback;
    }

    public void setCallback(ExportApplicationCallbackMode callback) {
        ExportApplicationCallbackMode oldValue = this.callback;
        this.callback = callback;
        firePropertyChange(PROPERTY_CALLBACK, oldValue, callback);
    }

    public ExportApplicationModel toModel() {

        Set<Flight> flightsToExport;

        if (getSelectedFlight() == null) {

            // all flights of this survey
            flightsToExport = Sets.newHashSet(getFlights());
            // remove the null value
            flightsToExport.remove(null);
        } else {

            // only one flight to export
            flightsToExport = Sets.newHashSet(getSelectedFlight());
        }
        ExportApplicationModel result = ExportApplicationModel.newModel(
                new File(getExportDirectory(), getExportFilename()),
                getSelectedSurvey().getTopiaId(),
                Iterables.transform(flightsToExport, TopiaEntities.getTopiaIdFunction())
        );
        return result;
    }
}
