/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.effort;

import com.google.common.base.Strings;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationStatus;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/** @author sletellier <letellier@codelutin.com> */
public class ObservationTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected FlightUIModel flightUIModel;

    protected EffortPanelHandler effortPanelHandler;

    protected int optionnableColumnToRemove = 2;

    protected int diveColumnIndex = 10;
    protected int reactionColumnIndex = 11;

    public ObservationTableModel(FlightUIModel flightUIModel, EffortPanelHandler effortPanelHandler) {
        this.flightUIModel = flightUIModel;
        this.effortPanelHandler = effortPanelHandler;

        SammoaConfig config = effortPanelHandler.getConfig();

        if (config.isDiveEnabled()) {
            optionnableColumnToRemove--;
        }

        if (config.isReactionEnabled()) {
            optionnableColumnToRemove--;
        }
    }

    @Override
    public int getRowCount() {
        return getBean().size();
    }

    @Override
    public int getColumnCount() {
        return ObservationColumn.values().length - optionnableColumnToRemove;
    }

    @Override
    public String getColumnName(int column) {
        ObservationColumn observationColumn = getColumnAt(column);
        return observationColumn.getColumnName();
    }

    @Override
    public Class<?> getColumnClass(int column) {
        ObservationColumn observationColumn = getColumnAt(column);
        return observationColumn.getType();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        Observation observation = getBean(row);
        ObservationColumn observationColumn = getColumnAt(column);
        boolean result = observationColumn.isEditable(observation, flightUIModel.isValidationMode());
        return result;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Observation observation = getBean(row);
        ObservationColumn observationColumn = getColumnAt(column);
        Object result = observationColumn.getValue(observation);
        return result;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        Observation observation = getBean(row);
        ObservationColumn observationColumn = getColumnAt(column);
        observationColumn.setValue(observation, aValue, effortPanelHandler);
        fireTableRowsUpdated(row, row);
    }

    protected ObservationColumn getColumnAt(int column) {
        ObservationColumn observationColumn;
        if (column < diveColumnIndex) {
            observationColumn = ObservationColumn.valueOf(column);
        } else if (column == diveColumnIndex) {
            if (effortPanelHandler.getConfig().isDiveEnabled()) {
                observationColumn = ObservationColumn.valueOf(column);
            } else {
                if (effortPanelHandler.getConfig().isReactionEnabled()) {
                    observationColumn = ObservationColumn.valueOf(column + 1);
                } else {
                    observationColumn = ObservationColumn.valueOf(column + 2);
                }
            }
        } else if (column == reactionColumnIndex) {
            if (effortPanelHandler.getConfig().isReactionEnabled()) {
                if (effortPanelHandler.getConfig().isDiveEnabled()) {
                    observationColumn = ObservationColumn.valueOf(column);
                } else {
                    observationColumn = ObservationColumn.valueOf(column + 1);
                }
            } else {
                if (effortPanelHandler.getConfig().isDiveEnabled()) {
                    observationColumn = ObservationColumn.valueOf(column + 1);
                } else {
                    observationColumn = ObservationColumn.valueOf(column + 2);
                }
            }
        } else {
            observationColumn = ObservationColumn.valueOf(column + optionnableColumnToRemove);
        }
        return observationColumn;
    }

    protected int getColumnIndex(ObservationColumn column) {
        for (int index = 0; index <= ObservationColumn.values().length; index++ ) {
            if (column.ordinal() == getColumnAt(index).ordinal()) {
                return index;
            }
        }
        return -1;
    }

    public int getBeanIndex(Observation bean) {
        int row = getBean().indexOf(bean);
        return row;
    }

    public List<Observation> getBean() {
        return flightUIModel.getObservations();
    }

    public Observation getBean(int row) {
        SwingUtil.ensureRowIndex(this, row);
        Observation bean = getBean().get(row);
        return bean;
    }

    public Pair<Integer, Integer> getCell(Observation bean, String fieldName) {

        int row = getBeanIndex(bean);
        int col = ObservationColumn.getValueFromFieldName(fieldName).ordinal();

        Pair<Integer, Integer> cell = Pair.of(row, col);
        return cell;
    }

    public FlightUIModel getFlightUIModel() {
        return flightUIModel;
    }

    public enum ObservationColumn {

        OBSERVATION_NUMBER(false, Integer.class, Observation.PROPERTY_OBSERVATION_NUMBER),
        OBSERVATION_TIME(true, Date.class, Observation.PROPERTY_OBSERVATION_TIME) {

            @Override
            public boolean isEditable(Observation bean, boolean validationMode) {
                return super.isEditable(bean, validationMode) && validationMode;
            }
        },
        POSITION(true, Position.class, Observation.PROPERTY_POSITION),
        POD_SIZE(true, int.class, Observation.PROPERTY_POD_SIZE),
        SPECIES(true, String.class, Observation.PROPERTY_SPECIES, Species.PROPERTY_CODE) {
            @Override
            public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
                String newValue = (String) value;

                if (!Strings.isNullOrEmpty(newValue)) {

                    String oldValue = bean.getSpecies() == null
                                      ? null : bean.getSpecies().getCode();

                    if (!newValue.equals(oldValue)) {

                        // Always use a new instance for different value to
                        // fire change on Observation and not on Species
                        Flight flight = bean.getFlight();
                        Survey survey = flight.getSurvey();
                        Region region = survey.getRegion();

                        bean.setSpecies(effortPanelHandler.getSpecies((String) value, region));
                    }

                } else {
                    bean.setSpecies(null);
                }
            }
        },
        AGE(true, String.class, Observation.PROPERTY_AGE),
        DEC_ANGLE(true, int.class, Observation.PROPERTY_DEC_ANGLE),
        CUE(true, String.class, Observation.PROPERTY_CUE),
        BEHAVIOUR(true, String.class, Observation.PROPERTY_BEHAVIOUR) {
            @Override
            public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
                String newValue = (String) value;
                if (Strings.isNullOrEmpty(newValue)) {
                    newValue = null;
                }
                bean.setBehaviour(newValue);
            }
        },
        SWIM_DIR(true, int.class, Observation.PROPERTY_SWIM_DIR),
        DIVE(true, String.class, Observation.PROPERTY_DIVE) {
            /**
             * Max 10 characters
             */
            @Override
            public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
                String newValue = (String) value;
                if (Strings.isNullOrEmpty(newValue)) {
                    newValue = null;
                } else if (newValue.length() > 10 ) {
                    newValue = newValue.substring(0, 10);
                }
                bean.setDive(newValue);
            }
        },
        REACTION(true, String.class, Observation.PROPERTY_REACTION) {
            /**
             * Max 10 characters
             */
            @Override
            public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
                String newValue = (String) value;
                if (Strings.isNullOrEmpty(newValue)) {
                    newValue = null;
                } else if (newValue.length() > 10 ) {
                    newValue = newValue.substring(0, 10);
                }
                bean.setReaction(newValue);
            }
        },
        CALVES(true, String.class, Observation.PROPERTY_CALVES),
        PHOTO(true, boolean.class, Observation.PROPERTY_PHOTO),
        COMMENT(true, String.class, Observation.PROPERTY_COMMENT),
        OBSERVATION_STATUS(true, ObservationStatus.class, Observation.PROPERTY_OBSERVATION_STATUS),
        DELETED(true, boolean.class, Observation.PROPERTY_DELETED),
        CIRCLE_BACK(true, Observation.class, "circleBack") {
            @Override
            public Object getValue(Observation bean) {
                return bean;
            }

            @Override
            public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
            }
        };

        private boolean editable;

        private String[] beanProperties;

        private Class<?> type;

        private final String columnName;

        ObservationColumn(boolean editable,
                                  Class<?> type,
                                  String... beanProperties) {
            this.editable = editable;
            this.type = type;
            this.beanProperties = beanProperties;
            this.columnName = beanProperties[0];
        }

        public Class<?> getType() {
            return type;
        }

        public String getColumnName() {
            return columnName;
        }

        public int getColumnIndex() {
            return ordinal();
        }

        public Object getValue(Observation bean) {
            Object result = SammoaUtil.getPropertyValue(bean, beanProperties);
            return result;
        }

        public void setValue(Observation bean, Object value, EffortPanelHandler effortPanelHandler) {
            if (type.isPrimitive() && value == null) {
                // can not set a null value to a primitive field
            } else {
                SammoaUtil.setPropertyValue(bean, value, beanProperties);
            }
        }

        public boolean isEditable(Observation bean, boolean validationMode) {
            boolean result = !bean.isValid() && editable;
            return result;
        }

        public static ObservationColumn valueOf(int ordinal) {
            for (ObservationColumn value : values()) {
                if (ordinal == value.ordinal()) {
                    return value;
                }
            }
            throw new EnumConstantNotPresentException(ObservationColumn.class,
                                                      "ordinal=" + ordinal);
        }

        public static ObservationColumn getValueFromFieldName(String fieldName) {
            ObservationColumn result = null;
            for (ObservationColumn value : values()) {
                if (fieldName.equals(value.columnName)) {
                    result = value;
                    break;
                }
            }
            return result;
        }
    }
}
