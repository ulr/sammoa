package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import org.jdesktop.beans.AbstractBean;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JCheckBox;

/**
 * Created: 14/03/18
 *
 * @author Sylvain Bavencoff <bavencoff@codelutin.com>
 */
public class CheckBoxActionTableCellEditorRenderer extends ButtonActionTableCellEditorRenderer {

    protected String propertyName;

    public CheckBoxActionTableCellEditorRenderer(String actionPropertyName,
                                                 Supplier<Action> actionSupplier,
                                                 Predicate<Object> enabledPredicate,
                                                 String propertyName) {
        super(actionPropertyName, actionSupplier, enabledPredicate);
        this.propertyName = propertyName;
    }

    protected AbstractButton getButton(Object bean) {
        AbstractButton result = cache.get(bean);
        if (result == null) {
            JCheckBox checkBox = new JCheckBox();
            checkBox.putClientProperty(actionPropertyName, bean);
            checkBox.setAction(actionSupplier.get());
            AbstractBean.class.cast(bean).addPropertyChangeListener(propertyName, evt -> {
                boolean check = (boolean) evt.getNewValue();
                checkBox.setSelected(check);
            });
            boolean check = (boolean) SammoaUtil.getPropertyValue(bean, propertyName);
            checkBox.setSelected(check);
            result = checkBox;
            cache.put(bean, result);
        }
        return result;
    }
}
