/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import fr.ulr.sammoa.persistence.Observer;
import jaxx.runtime.swing.model.GenericListModel;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/** @author sletellier <letellier@codelutin.com> */
public class ObserverTableCellEditor extends AbstractCellEditor
        implements TableCellEditor, FocusListener {

    private static final long serialVersionUID = 1L;

    protected ListCellRenderer cellRenderer;

    protected GenericListModel<Observer> model;

    public ObserverTableCellEditor(FlightUIModel flightUIModel,
                                   ListCellRenderer cellRenderer) {
        model = new GenericListModel<Observer>();
        model.setElements(flightUIModel.getFlightObserverForPositions());

        flightUIModel.addPropertyChangeListener(
                FlightUIModel.PROPERTY_FLIGHT_OBSERVER_FOR_POSITIONS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                model.setElements((List<Observer>) evt.getNewValue());
            }
        });
        this.cellRenderer = cellRenderer;
    }

    @Override
    public Observer getCellEditorValue() {
        return model.getSelectedValue();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        model.setSelectedItem(value);
        JComboBox result = new JComboBox(model);
        result.setRenderer(cellRenderer);
        result.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
        return result;
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        stopCellEditing();
    }
}
