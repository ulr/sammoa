package fr.ulr.sammoa.ui.swing.flight;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Flights;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectFlights;
import org.nuiton.validator.bean.list.BeanListValidator;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * This model is used to keep validation for all routes, transectFlights and
 * observations. This will avoid multiple check if not necessary.
 * <p/>
 * We use {@link CacheBuilder} that acts as {@link Function} for different
 * validation check.
 * <p/>
 * Created: 25/09/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 * @see Observations#isValid(Observation, BeanListValidator)
 * @see Routes#isValid(Route, BeanListValidator, Iterable, Function)
 * @see TransectFlights#isValid(TransectFlight, Iterable, Function)
 * @see Flights#isValid(Flight, Iterable, Function, Function)
 */
public class ValidModel implements Serializable {

    private static final long serialVersionUID = 1L;

    protected LoadingCache<Observation, Boolean> observationValid;

    protected LoadingCache<Route, Boolean> routeValid;

    protected LoadingCache<TransectFlight, Boolean> transectFlightValid;

    protected BeanListValidator<Observation> observationValidator;

    protected BeanListValidator<Route> routeValidator;

    protected Set<ValidModelListener> listeners;

    public ValidModel(BeanListValidator<Observation> oValidator,
                      BeanListValidator<Route> rValidator) {

        this.observationValidator = oValidator;
        this.routeValidator = rValidator;
        this.listeners = Sets.newHashSet();

        observationValid = CacheBuilder.newBuilder().build(new CacheLoader<Observation, Boolean>() {

            @Override
            public Boolean load(Observation key) throws Exception {
                boolean result = Observations.isValid(key, observationValidator);
                return result;
            }
        });

        routeValid = CacheBuilder.newBuilder().build(new CacheLoader<Route, Boolean>() {

            @Override
            public Boolean load(Route key) throws Exception {
                boolean result = Routes.isValid(key, routeValidator, observationValidator.getBeans(), observationValid);
                return result;
            }
        });

        transectFlightValid = CacheBuilder.newBuilder().build(new CacheLoader<TransectFlight, Boolean>() {

            @Override
            public Boolean load(TransectFlight key) throws Exception {
                boolean result = TransectFlights.isValid(key, routeValidator.getBeans(), routeValid);
                return result;
            }
        });
    }

    public void cleanUpObservationValid() {
        observationValid.cleanUp();
    }

    public void resetObservationValid(Observation observation) {
        observationValid.invalidate(observation);

        // Reset the route
        Route route = Routes.findWithObservation(routeValidator.getBeans(), observation, true);
        if (route != null) {
            resetRouteValid(route);
        }
    }

    public void cleanUpRouteValid() {
        routeValid.cleanUp();
    }

    public void resetRouteValid(Route route) {
        routeValid.invalidate(route);

        // Reset the transectFlight
        if (route.getTransectFlight() != null) {
            resetTransectFlightValid(route.getTransectFlight());

        } else {
            fireReset();
        }
    }

    public void cleanUpTransectFlightValid() {
        transectFlightValid.cleanUp();
    }

    public void resetTransectFlightValid(TransectFlight transectFlight) {
        transectFlightValid.invalidate(transectFlight);
        fireReset();
    }

    public boolean isObservationValid(Observation observation) {
        try {
            return observationValid.get(observation);
        } catch (ExecutionException ex) {
            throw new SammoaTechnicalException(ex);
        }
    }

    public boolean isRouteValid(Route route) {
        try {
            return routeValid.get(route);
        } catch (ExecutionException ex) {
            throw new SammoaTechnicalException(ex);
        }
    }

    public boolean isTransectFlightValid(TransectFlight transectFlight) {
        try {
            return transectFlightValid.get(transectFlight);
        } catch (ExecutionException ex) {
            throw new SammoaTechnicalException(ex);
        }
    }

    public boolean isFlightValid(Flight flight) {
        boolean result = Flights.isValid(flight, routeValidator.getBeans(), transectFlightValid, routeValid);
        return result;
    }

    public boolean getFlightValidFlag(Flight flight) {
        boolean result = Flights.withValidFlag(flight, routeValidator);
        return result;
    }

    public void addValidModelListener(ValidModelListener listener) {
        listeners.add(listener);
    }

    public void removeValidModelListener(ValidModelListener listener) {
        listeners.remove(listener);
    }

    protected void fireReset() {
        for (ValidModelListener listener : listeners) {
            listener.onReset(this);
        }
    }

    public static interface ValidModelListener {

        void onReset(ValidModel source);
    }
}
