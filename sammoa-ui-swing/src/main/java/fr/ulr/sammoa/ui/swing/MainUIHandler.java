/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing;

import com.opensymphony.xwork2.util.ValueStack;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.application.device.audio.AudioConfig;
import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsConfig;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.flightController.FlightController;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.home.HomeUI;
import fr.ulr.sammoa.ui.swing.io.input.application.ImportApplicationUI;
import fr.ulr.sammoa.ui.swing.io.output.application.ExportApplicationUI;
import fr.ulr.sammoa.ui.swing.io.output.map.ExportMapUI;
import fr.ulr.sammoa.ui.swing.survey.SurveyUI;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import fr.ulr.sammoa.ui.swing.util.ValidationHelper;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.AboutPanel;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.swing.config.ConfigUIHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.validator.xwork2.XWork2ValidatorUtil;
import org.nuiton.widget.SwingSession;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created: 03/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class MainUIHandler implements SammoaUIHandler {

    private static final Log log = LogFactory.getLog(MainUIHandler.class);

    protected SammoaUIContext context;

    protected MainUI ui;

    protected JComponent currentBody;

    protected MainUIHandler(SammoaUIContext context,
                            MainUI ui) {
        this.context = context;
        this.context.setMainUIHandler(this);
        this.ui = ui;
    }

    protected PropertyChangeListener titleListener;

    public MainUI getUI() {
        return ui;
    }

    public SammoaContext getAppContext() {
        return context.getAppContext();
    }

    public SammoaConfig getConfig() {
        return context.getConfig();
    }

    @Override
    public void beforeInitUI() {
        // init validator value stack
        ValueStack valueStack = XWork2ValidatorUtil.getSharedValueStack();

        valueStack.push(ValidationHelper.getInstance());
    }

    @Override
    public void afterInitUI() {

        // Sync to error dialog
        ErrorDialogUI.init(ui);

        // Init SwingSession
        SwingSession swingSession = new SwingSession(getConfig().getUIConfigFile(), false);
        swingSession.add(ui);
        swingSession.save();
        context.setSwingSession(swingSession);

        //SwingUtil.getLayer(ui.getBody()).setUI(new BetaLayerUI());

        UIManager.put("Table.focusCellHighlightBorder",
                new BorderUIResource.LineBorderUIResource(Color.BLACK));


        JProgressBar busyWidget = ui.getStatus().getBusyWidget();
        Dimension dimension = new Dimension(100, busyWidget.getHeight());
        busyWidget.setSize(dimension);
        busyWidget.setPreferredSize(dimension);
        busyWidget.setMaximumSize(dimension);

        setScreen(SammoaScreen.HOME);
    }

    @Override
    public void onCloseUI() {

        closeCurrentBody();

        try {
            context.close();
        } catch (IOException e) {
            throw new SammoaTechnicalException(e);
        }
    }

    public void setScreen(SammoaScreen screen) {

        try {

            // busy ui
            SammoaUtil.updateBusyState(ui, true);

            if (titleListener != null && FlightUI.class.isInstance(currentBody)) {
                FlightUI flightUI = FlightUI.class.cast(currentBody);
                Flight flight = flightUI.getBean();
                flight.removePropertyChangeListener(Flight.PROPERTY_TIME_ZONE, titleListener);
                titleListener = null;
            }

            // remove any screen
            ui.setScreen(null);

            closeCurrentBody();

            // set new screen (needed by validation before creating the ui)
            ui.setScreen(screen);

            switch (screen) {
                default:
                case HOME:
                    changeScreen(new HomeUI(context),
                                t("sammoa.title.home"));
                    break;
                case VALIDATION:
                case FLIGHT:
                    FlightUI flightUI = new FlightUI(context);
                    Flight flight = flightUI.getModel().getFlight();
                    Survey survey = flight.getSurvey();

                    changeScreen(flightUI,
                                t("sammoa.title.flight",
                                        flight.getSystemId(),
                                        survey.getCode(),
                                        survey.getRegion().getCode(),
                                        flight.getFlightNumber(),
                                        flight.getTimeZone().getID()));

                    // change title if timeZone change
                    titleListener = evt ->
                        ui.getBody().setTitle(t("sammoa.title.flight",
                                flight.getSystemId(),
                                survey.getCode(),
                                survey.getRegion().getCode(),
                                flight.getFlightNumber(),
                                flight.getTimeZone().getID()));


                    flight.addPropertyChangeListener(Flight.PROPERTY_TIME_ZONE, titleListener);

                    break;
                case SURVEY:
                    changeScreen(new SurveyUI(context),
                                t("sammoa.title.survey"));
                    break;
                case EXPORT_MAP:
                    changeScreen(new ExportMapUI(context),
                                t("sammoa.title.exportshape"));
                    break;
                case EXPORT_APPLICATION:
                    changeScreen(new ExportApplicationUI(context),
                                t("sammoa.title.exportApplication"));
                    break;
                case IMPORT_APPLICATION:
                    changeScreen(new ImportApplicationUI(context),
                                t("sammoa.title.importApplication"));
                    break;
            }

        } catch (Exception e) {

            ErrorDialogUI.showError(e);

            // go back to home
            setScreen(SammoaScreen.HOME);
        } finally {

            // free ui
            SammoaUtil.updateBusyState(ui, false);
        }
    }

    public void closeSammoa() {
        closeSammoa(true);
    }

    public void reloadSammoa() {

        // Close the application
        closeSammoa(false);

        // Re-open the context
        try {
            context.open();
        } catch (TopiaException ex) {
            ErrorDialogUI.showError(ex);
        }

        MainUI mainUI = new MainUI(context);
        mainUI.setVisible(true);
    }

    public void showConfig() {
        SammoaConfig config = context.getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config);

        helper.registerCallBack(
                "ui", n("sammoa.action.reload.ui"),
                SwingUtil.createActionIcon("reload-ui"),
                new Runnable() {

                    @Override
                    public void run() {
                        reloadSammoa();
                    }
                }
        ).registerCallBack(
                "home", n("sammoa.action.reload.home"),
                SwingUtil.createActionIcon("config"),
                new Runnable() {

                    @Override
                    public void run() {
                        if (SammoaScreen.HOME == ui.getScreen()) {
                            HomeUI homeUI = (HomeUI) currentBody;
                            homeUI.getHandler().selectSurvey();
                        }
                    }
                }
        ).registerCallBack(
                "actions", n("sammoa.action.reload.actions"),
                SwingUtil.createActionIcon("config"),
                new Runnable() {

                    @Override
                    public void run() {

                        if (SammoaScreen.FLIGHT == ui.getScreen()
                            || SammoaScreen.VALIDATION == ui.getScreen()) {

                            FlightUI flightUI = (FlightUI) currentBody;
                            flightUI.getHandler().initActions();
                        }
                    }
                }
        ).registerCallBack(
                "audio", n("sammoa.action.reload.audio"),
                SwingUtil.createActionIcon("config"),
                new Runnable() {

                    @Override
                    public void run() {

                        if (SammoaScreen.FLIGHT == ui.getScreen()) {

                            reloadDevice((FlightUI) currentBody, AudioRecorder.class);

                        } else if (SammoaScreen.VALIDATION == ui.getScreen()) {

                            reloadDevice((FlightUI) currentBody, AudioReader.class);
                        }
                    }
                }
        ).registerCallBack(
                "gps", n("sammoa.action.reload.gps"),
                SwingUtil.createActionIcon("config"),
                new Runnable() {

                    @Override
                    public void run() {

                        if (SammoaScreen.FLIGHT == ui.getScreen()) {

                            reloadDevice((FlightUI) currentBody, GpsHandler.class);
                        }
                    }
                }
        ).registerCallBack(
                "circleBack", n("sammoa.action.reload.circleBack"),
                SwingUtil.createActionIcon("config"),
                () -> {

                    if (SammoaScreen.FLIGHT == ui.getScreen()
                            || SammoaScreen.VALIDATION == ui.getScreen()) {
                        FlightUI flightUI = (FlightUI) currentBody;
                        flightUI.getModel().setCircleBackEnabled(getConfig().isCircleBackEnabled());
                    }
                }
        ).registerCallBack(
                "circleBackTimer", n("sammoa.action.reload.circleBackTimer"),
                SwingUtil.createActionIcon("config"),
                () -> {

                    if (SammoaScreen.FLIGHT == ui.getScreen()
                            || SammoaScreen.VALIDATION == ui.getScreen()) {
                        FlightUI flightUI = (FlightUI) currentBody;
                        flightUI.getModel().setCircleBackTimerEnabled(getConfig().isCircleBackTimerEnabled());
                    }
                }
        ).registerCallBack(
                "columnTableOrder", n("sammoa.action.reload.columnTableOrder"),
                SwingUtil.createActionIcon("config"),
                () -> {

                    if (SammoaScreen.FLIGHT == ui.getScreen()
                            || SammoaScreen.VALIDATION == ui.getScreen()) {
                        FlightUI flightUI = (FlightUI) currentBody;
                        flightUI.getHandler().reloadColumnTableOrder();
                    }
                }
        );

        // APPLICATION

        helper.addCategory(n("sammoa.config.category.applications"),
                           n("sammoa.config.category.applications.description"))
                .addOption(SammoaConfig.SammoaConfigOption.SYSTEM_ID)
                .setOptionCallBack("home")
                .addOption(SammoaConfig.SammoaConfigOption.DATA_DIRECTORY)
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.FLIGHT_NUMBER)
                .addOption(SammoaConfig.SammoaConfigOption.BACKGROUND_SHAPE_FILE)
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.AUTO_COMMIT_DELAY) // milliseconds
                .setOptionCallBack("ui");

        // UI
        helper.addCategory(n("sammoa.config.category.ui"),
                n("sammoa.config.category.ui.description"))
                .addOption(SammoaConfig.SammoaConfigOption.CIRCLE_BACK_ENABLE)
                .setOptionCallBack("circleBack")
                .addOption(SammoaConfig.SammoaConfigOption.CIRCLE_BACK_TIMER_ENABLE)
                .setOptionCallBack("circleBackTimer")
                .addOption(SammoaConfig.SammoaConfigOption.COUNTDOWN_CIRCLE_CONFIRMATION) // milliseconds
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.COUNTDOWN_REJOIN_TRACKLINE) // milliseconds
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.DEADTIME_AFTER_CIRCLE_BACK) // milliseconds
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.DIVE_ENABLE)
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.REACTION_ENABLE)
                .setOptionCallBack("ui")
                .addOption(SammoaConfig.SammoaConfigOption.TABLE_TRANSECTS_COLUMNS_ORDER_CUSTOM)
                .setOptionCallBack("columnTableOrder")
                .addOption(SammoaConfig.SammoaConfigOption.TABLE_ROUTES_COLUMNS_ORDER_CUSTOM)
                .setOptionCallBack("columnTableOrder")
                .addOption(SammoaConfig.SammoaConfigOption.TABLE_OBSERVATIONS_COLUMNS_ORDER_CUSTOM)
                .setOptionCallBack("columnTableOrder")
                .addOption(SammoaConfig.SammoaConfigOption.TABLE_GEO_POINTS_COLUMNS_ORDER_CUSTOM)
                .setOptionCallBack("columnTableOrder");

        // SHORTCUT

        helper.addCategory(n("sammoa.config.category.shortcuts"),
                           n("sammoa.config.category.shortcuts.description"), "actions")
                .addOption(SammoaConfig.SammoaConfigOption.KEY_START)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_STOP)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_BEGIN)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_END)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_ADD)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_LEFT_OBSERVATION)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_CENTER_OBSERVATION)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_RIGHT_OBSERVATION)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_CIRCLE_BACK)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_REJOIN)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_VALID_FLIGHT)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_VALID_TRANSECT)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_VALID_OBSERVATION)
                .addOption(SammoaConfig.SammoaConfigOption.KEY_VALID_ROUTE);

        // GPS

        helper.addCategory(n("sammoa.config.category.gps"),
                           n("sammoa.config.category.gps.description"), "gps")
                .addOption(GpsConfig.GpsConfigOption.GPS_HANDLER,
                           SammoaConfig.PROPERTY_GPS_CONFIG + "." + GpsConfig.PROPERTY_GPS_HANDLER_CLASS)
                .addOption(GpsConfig.GpsConfigOption.GPS_DEVICE)
                .addOption(GpsConfig.GpsConfigOption.GPS_SPEED)
                .addOption(GpsConfig.GpsConfigOption.GPS_CHECK_PERIOD)
                .addOption(GpsConfig.GpsConfigOption.GPS_TIMEOUT);

        // AUDIO

        helper.addCategory(n("sammoa.config.category.audio"),
                           n("sammoa.config.category.audio.description"), "audio")
                .addOption(AudioConfig.AudioConfigOption.SAMPLE_RATE)
                .addOption(AudioConfig.AudioConfigOption.SAMPLE_SIZE_IN_BITS)
                .addOption(AudioConfig.AudioConfigOption.RECORD_DELAY_IN_SECONDS)
                .addOption(AudioConfig.AudioConfigOption.COMPRESSION);


        // OTHER

        helper.addCategory(n("sammoa.config.category.other"),
                           n("sammoa.config.category.other.description"))
                .addOption(SammoaConfig.SammoaConfigOption.SITE_URL)
                .addOption(SammoaConfig.SammoaConfigOption.UI_CONFIG_FILE)
                .setOptionCallBack("ui");


        helper.buildUI(ui, "sammoa.config.category.applications");

        helper.displayUI(ui, false);
    }

    public void showAudioCheck() {
        AudioCheck frame = new AudioCheck();
        frame.setVisible(true);
    }

    protected void closeCurrentBody() {
        if (currentBody != null) {
            SammoaUI<?> body = (SammoaUI<?>) currentBody;
            body.getHandler().onCloseUI();

            context.getSwingSession().save();

            ui.getBody().remove(currentBody);

            currentBody = null;
        }
    }

    protected void changeScreen(JComponent newBody, String title) {
        currentBody = newBody;
        context.getSwingSession().add(currentBody);
        ui.getBody().setTitle(title);
        ui.getBody().add(currentBody, 1);
    }

    protected void closeSammoa(boolean exit) {

        if (checkCurrentFlight(
               t("sammoa.confirmDialog.flightInProgress.message.exit"))) {

            context.getSwingSession().save();

            ui.setVisible(false);
            ui.dispose();

            onCloseUI();

            if (exit) {
                System.exit(0);
            }
        }
    }

    /**
     * Reload the given device identified by its {@code deviceManagerClass} for
     * {@code ui}.
     *
     * @param ui                 FlightUI contains the controller that manage devices
     * @param deviceManagerClass Class of the device to reload
     * @param <T>                Type of {@link DeviceManager}
     * @see FlightController#openDeviceManager(Class)
     */
    protected <T extends DeviceManager> void reloadDevice(FlightUI ui,
                                                          Class<T> deviceManagerClass) {
        try {
            FlightController flightController = ui.getHandler().getFlightController();
            flightController.openDeviceManager(deviceManagerClass);
        } catch (DeviceTechnicalException ex) {
            if (log.isErrorEnabled()) {
                log.error("Error on new " + deviceManagerClass.getSimpleName(), ex);
            }
            SammoaUtil.showErrorMessage(ui, ex.getMessageWithCause());
        }
    }

    public void showHome() {
        if (checkCurrentFlight(
                t("sammoa.confirmDialog.flightInProgress.message.showHome"))) {
            setScreen(SammoaScreen.HOME);
        }
    }

    public void showAbout() {

        ApplicationConfig applicationConfig =
                context.getConfig().getApplicationConfig();

        String iconPath = applicationConfig.getOption("application.icon.path");
        String name = "sammoa-ui-swing";
        String licensePath = "META-INF/" + name + "-LICENSE.txt";
        String thirdPartyPath = "META-INF/" + name + "-THIRD-PARTY.txt";

        AboutPanel about = new AboutPanel();
        about.setTitle(t("sammoa.title.about"));
        about.setAboutText(t("sammoa.about.message"));

        SammoaConfig config = context.getConfig();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int inceptionYear = config.getInceptionYear();
        String years;
        if (currentYear != inceptionYear) {
            years = inceptionYear + "-" + currentYear;
        } else {
            years = inceptionYear + "";
        }

        about.setBottomText(t("sammoa.about.bottomText",
                config.getOrganizationName(),
                years,
                config.getVersion()));
        about.setIconPath(iconPath);
        about.setLicenseFile(licensePath);
        about.setThirdpartyFile(thirdPartyPath);
        about.buildTopPanel();
        about.init();
        about.showInDialog(ui, true);

        // register on swing session
        context.getSwingSession().add(about);
    }

    public void gotoSite() {
        SammoaConfig config = context.getConfig();

        URL siteURL = config.getSiteUrl();

        if (log.isInfoEnabled()) {
            log.info(t("sammoa.message.goto.site", siteURL));
        }

        if (log.isDebugEnabled()) {
            log.debug("goto " + siteURL);
        }
        if (Desktop.isDesktopSupported() &&
            Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(siteURL.toURI());
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Failed to open '" + siteURL + "' in browser", e);
                }
                ErrorDialogUI.showError(e);
            }
        }
    }

    public void showLogs() {
        if (Desktop.isDesktopSupported() &&
            Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            File logFile = context.getConfig().getLogFile();
            try {
                Desktop.getDesktop().open(logFile);
            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Failed to open '" + logFile.getName() + "' file", ex);
                }
                ErrorDialogUI.showError(ex);
            }
        }
    }

    /**
     * Used to check if the {@link FlightUI} is opened and a flight is in
     * progress. In this case, a confirm dialog is opened to display the
     * given {@code message} at warning level. If the user accept, the
     * response will be true.
     *
     * @param message String message to display to user when flight is in progress
     * @return true if the confirmation is Ok or no flight is in progress
     */
    protected boolean checkCurrentFlight(String message) {

        boolean result = true;

        if (SammoaScreen.FLIGHT == ui.getScreen()) {

            FlightUI ui = (FlightUI) currentBody;
            FlightState state = ui.getModel().getFlightState();

            if (state == FlightState.ON_EFFORT || state == FlightState.OFF_EFFORT) {

                int dialogResponse = JOptionPane.showConfirmDialog(
                        ui,
                        message,
                        t("sammoa.confirmDialog.flightInProgress.title"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        UIManager.getIcon("warning")
                );

                if (dialogResponse == JOptionPane.NO_OPTION) {
                    result = false;
                }
            }
        }
        return result;
    }
}
