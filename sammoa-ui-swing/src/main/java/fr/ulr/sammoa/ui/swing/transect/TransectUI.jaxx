<!--
  #%L
  SAMMOA :: UI Swing
  %%
  Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='transectUI'
        layout='{new BorderLayout()}'
        implements='fr.ulr.sammoa.ui.swing.SammoaUI&lt;TransectUIHandler&gt;'>

  <import>
    fr.ulr.sammoa.ui.swing.flight.StrateModel
    fr.ulr.sammoa.ui.swing.CloseAction
    fr.ulr.sammoa.ui.swing.SammoaUIContext

    jaxx.runtime.validator.swing.SwingValidatorUtil
    jaxx.runtime.validator.swing.SwingValidatorMessageTableModel

    static org.nuiton.i18n.I18n.t
  </import>

  <script><![CDATA[

    public TransectUI(SammoaUIContext context) {
        TransectUIHandler handler = new TransectUIHandler(context, this);
        setContextValue(handler);
        handler.beforeInitUI();
    }

    protected void $afterCompleteSetup() {
        getHandler().afterInitUI();
    }
  ]]></script>

  <SwingValidatorMessageTableModel id='errorTableModel'/>

  <TransectUIHandler id='handler'
                     initializer='getContextValue(TransectUIHandler.class)'/>

  <TransectUIModel id='model'
                   initializer='getContextValue(TransectUIModel.class)'/>

  <BeanValidator id='validator'
                 bean='model'
                 errorTableModel='errorTableModel'
                 uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='strate' component='transectStrateComboBox'/>
    <field name='name' component='transectNameField'/>
  </BeanValidator>

  <CloseAction id='closeAction' constructorParams='this'/>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell anchor='west'>
        <JLabel id='transectStrateLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1.0'>
        <JComboBox id='transectStrateComboBox'
                   onActionPerformed='getHandler().selectStrate((StrateModel) transectStrateComboBox.getSelectedItem())'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='transectNameLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1.0'>
        <JTextField id='transectNameField'
                    onKeyReleased='getModel().setName(transectNameField.getText())'/>
      </cell>
    </row>
    <row>
      <cell fill='both' columns='2'>
        <JPanel id='messagePanel' layout='{new GridLayout()}'>
          <JScrollPane columnHeaderView='{errorTable.getTableHeader()}'>
            <JTable id='errorTable' />
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell fill='both' columns='2'>
        <JPanel layout='{new GridLayout(1,2,0,0)}'>
          <JButton id='closeButton'/>
          <JButton id='createButton'
                   onActionPerformed='getHandler().createTransect()'/>
        </JPanel>
      </cell>
    </row>
  </Table>
</JPanel>
