package fr.ulr.sammoa.ui.swing.flight.layer;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.layer.OMGraphicHandlerLayer;
import com.bbn.openmap.omGraphics.DrawingAttributes;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicConstants;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.omGraphics.OMTextLabeler;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPoints;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Properties;

/**
 * Created: 06/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public abstract class BaseGeoPointLayer extends OMGraphicHandlerLayer {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(BaseGeoPointLayer.class);

    protected DrawingAttributes drawingAttributes = DrawingAttributes.getDefaultClone();

    public void setGeoPoints(Iterable<GeoPoint> geoPoints) {

        setList(new OMGraphicList());

        for (GeoPoint reference : geoPoints) {
            if (GeoPoints.isCoordinatesEmpty(reference)) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't add geoPoint, no coordinates available " +
                                "at " +  reference.getRecordTime());
                }
            } else {
                getList().add(createGraphic(reference, reference, null));
            }
        }
    }

    public <T> void setGeoPoints(Iterable<T> references,
                                 Function<T, GeoPoint> toGeoPoint,
                                 Function<T, String> toLabel) {

        setList(new OMGraphicList());

        for (T reference : references) {
            GeoPoint geoPoint = toGeoPoint.apply(reference);
            String label = toLabel.apply(reference);

            if (GeoPoints.isCoordinatesEmpty(geoPoint)) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't add geoPoint, no coordinates available " +
                                "at " + geoPoint.getRecordTime() + " (label=" + label + ")");
                }
            } else {
                getList().add(createGraphic(reference, geoPoint, label));
            }
        }
    }

    public void addGeoPoint(GeoPoint geoPoint) {
        putGeoPoint(geoPoint, geoPoint, null);
    }

    public void setLabel(Object reference, String label) {
        OMGraphic graphic = Iterables.find(getList(), withReference(reference), null);
        if (graphic != null) {
            setLabel(graphic, label);
            applyChange();
        }
    }

    public void putGeoPoint(Object reference, GeoPoint geoPoint, String label) {

        int index = Iterables.indexOf(getList(), withReference(reference));

        if (GeoPoints.isCoordinatesEmpty(geoPoint)) {
            if (log.isWarnEnabled()) {
                log.warn("Can't add geoPoint, no coordinates available " +
                            "(label=" + label + ")");
            }

            if (index != -1) {
                if (log.isDebugEnabled()) {
                    log.debug("Remove geoPoint at index " + index + " (label=" + label + ")");
                }
                getList().remove(index);
            }

        } else {

            OMGraphic graphic = createGraphic(reference, geoPoint, label);

            if (index != -1) {
                if (log.isDebugEnabled()) {
                    log.debug("Update geoPoint at index " + index + " (label=" + label + ")");
                }
                getList().set(index, graphic);

            } else {
                if (log.isDebugEnabled() && label != null) {
                    log.debug("Add geoPoint (label=" + label + ")");
                }
                getList().add(graphic);
            }
        }

        applyChange();
    }

    @Override
    public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);
        drawingAttributes.setProperties(prefix, props);
    }

    @Override
    public Properties getProperties(Properties props) {
        Properties result = super.getProperties(props);
        drawingAttributes.getProperties(result);
        return result;
    }

    @Override
    public Properties getPropertyInfo(Properties list) {
        Properties result = super.getPropertyInfo(list);
        drawingAttributes.getPropertyInfo(list);
        return result;
    }

    protected OMGraphic createGraphic(Object reference, GeoPoint geoPoint, String label) {
        OMGraphic result = newGraphic(geoPoint);
        if (label != null) {
            setLabel(result, label);
        }
        drawingAttributes.setTo(result);
        result.setAppObject(reference);
        return result;
    }

    protected void setLabel(OMGraphic graphic, String label) {
        graphic.putAttribute(OMGraphicConstants.LABEL,
                             new OMTextLabeler(label, OMText.JUSTIFY_CENTER));
    }

    protected void applyChange() {

        // Need to generate the projection to apply change
        getList().generate(getProjection());

        // Refresh the view
        repaint();
    }

    protected abstract OMGraphic newGraphic(GeoPoint geoPoint);

    public static Predicate<OMGraphic> withReference(Object reference) {
        return new WithReferencePredicate(reference);
    }

    private static class WithReferencePredicate implements Predicate<OMGraphic> {

        protected Object reference;

        private WithReferencePredicate(Object reference) {
            this.reference = reference;
        }

        @Override
        public boolean apply(OMGraphic input) {
            return Objects.equal(input.getAppObject(), reference);
        }
    }
}
