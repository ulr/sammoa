/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.region;

import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.io.input.csv.ImportCsvService;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.ui.swing.SammoaUIContext;
import fr.ulr.sammoa.ui.swing.SammoaUIHandler;
import fr.ulr.sammoa.ui.swing.io.input.CsvImporter;
import fr.ulr.sammoa.ui.swing.io.input.UIImporter;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableRenderer;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFrame;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 04/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @since 0.2
 */
public class RegionUIHandler implements SammoaUIHandler {

    private static final Log log =
            LogFactory.getLog(RegionUIHandler.class);

    protected final RegionUI ui;

    protected final UIImporter uiImporter;

    private final ImportCsvService importCsvService;

    private final ReferentialService referentialService;

    public RegionUIHandler(SammoaUIContext context,
                           RegionUI ui) {
        this.ui = ui;
        this.uiImporter = new UIImporter(ui);
        this.importCsvService = context.getService(ImportCsvService.class);
        this.referentialService = context.getService(ReferentialService.class);
    }

    public RegionUIModel getModel() {
        return ui.getModel();
    }

    @Override
    public void beforeInitUI() {

        RegionUIModel model = new RegionUIModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInitUI() {

        SwingValidatorUtil.installUI(ui.getErrorTable(),
                                     new SwingValidatorMessageTableRenderer());
    }

    @Override
    public void onCloseUI() {
        ui.setVisible(false);
    }

    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("Open RegionUI ");
        }

        JFrame parent = ui.getParentContainer(JFrame.class);

        SammoaUtil.openInDialog(ui, parent,
                                t("sammoa.dialog.title.region"),
                                ui.getCloseAction());

        ui.setVisible(true);
    }

    public void saveRegion() {

        try {
            boolean success = true;

            // Prepare new instance
            Region region = getModel().newBean();

            SammoaUtil.updateBusyState(ui, true);

            // Save the entity
            String regionId = referentialService.saveRegion(region);
            getModel().setId(regionId);

            // Import species if necessary
            File speciesFile = ui.getSpeciesFileEditor().getSelectedFile();
            if (speciesFile != null && speciesFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Import species file " +
                                speciesFile.getAbsolutePath());
                }

                success = uiImporter.importCsv(speciesImporter, speciesFile);
            }

            // Close on success
            if (success) {
                ui.getCloseButton().doClick();
            }

        } catch (Exception e) {
            ErrorDialogUI.showError(e);
        } finally {
            SammoaUtil.updateBusyState(ui, false);
        }
    }


    protected final CsvImporter speciesImporter = new CsvImporter() {

        @Override
        public String importCsvFile(File file) throws IOException {
            int nbImported = importCsvService.importSpecies(getModel().getId(), file);
            return t("sammoa.messageDialog.species.import.success", nbImported);
        }
    };
}
