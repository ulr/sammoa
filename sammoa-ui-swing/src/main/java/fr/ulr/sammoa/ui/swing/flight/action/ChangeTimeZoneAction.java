package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;
import java.util.Optional;
import java.util.TimeZone;

import static org.nuiton.i18n.I18n.t;

public class ChangeTimeZoneAction extends ValidAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeTimeZoneAction.class);

    private static final long serialVersionUID = 1L;

    public ChangeTimeZoneAction(JAXXContext context) {
        super(t("sammoa.action.changeTimeZone"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.changeTimeZone.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Object source = e.getSource();
        Flight flight = getModel().getFlight();


        if (JComponent.class.isInstance(source) && flight != null && getModel().isValidationMode()) {
            JComponent comp = (JComponent) source;

            String[] timeZoneIds = TimeZone.getAvailableIDs();

            Optional<String> timeZoneId = SammoaUtil.askValue(comp,
                    t("sammoa.action.changeTimeZone.title"),
                    t("sammoa.action.changeTimeZone.question", flight.getSurvey().getCode(), flight.getFlightNumber()),
                    timeZoneIds,
                    flight.getTimeZone().getID());

            if (timeZoneId.isPresent()) {
                TimeZone timeZone = TimeZone.getTimeZone(timeZoneId.get());
                flight.setTimeZone(timeZone);
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        return getModel().isValidationMode();

    }
}
