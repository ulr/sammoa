package fr.ulr.sammoa.ui.swing.flight.action;

/*-
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2022 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import jaxx.runtime.JAXXContext;

import javax.swing.*;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

public class RejoinAction  extends BaseFlightAction {

    private static final long serialVersionUID = 1L;

    public RejoinAction(JAXXContext context) {
        super(t("sammoa.action.rejoin"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.rejoin.tip"));
        bindModelProperty(FlightUIModel.PROPERTY_FLIGHT_STATE, false);
        bindModelProperty(FlightUIModel.PROPERTY_CURRENT_ROUTE, false);
        bindModelProperty(FlightUIModel.PROPERTY_REJOIN_TIMER, false);
        bindModelProperty(FlightUIModel.PROPERTY_NEXT_TRANSECT + ".deleted", false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        getFlightController().rejoin();
    }

    @Override
    protected boolean checkEnabled() {
        Route currentRoute = getModel().getCurrentRoute();
        boolean isCircleBack = currentRoute != null
                && RouteType.CIRCLE_BACK == currentRoute.getRouteType();

        TransectFlight nextTransect = getModel().getNextTransect();
        boolean isReadyForLeg = nextTransect != null
                && !nextTransect.isDeleted()
                && getModel().getFlightState() == FlightState.OFF_EFFORT;

        boolean hasRejoined = getModel().getRejoinTimer() == 0;

        return isCircleBack && isReadyForLeg && !hasRejoined;
    }
}
