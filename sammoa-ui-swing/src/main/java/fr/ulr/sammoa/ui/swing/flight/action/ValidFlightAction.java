package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.Validables;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import java.awt.event.ActionEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ValidFlightAction extends ValidAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidFlightAction.class);

    private static final long serialVersionUID = 1L;

    protected boolean validatorIsAdjusting;

    public ValidFlightAction(JAXXContext context) {
        super(t("sammoa.action.validFlight"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.validFlight.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Flight flight = getModel().getFlight();
        List<TransectFlight> transectFlights = flight.getTransectFlight();
        boolean hasDeletedTransect = Validables.isAnyDeleted(transectFlights);

        if (!hasDeletedTransect
            || askConfirmDeleteByCascade(t("sammoa.validable.flight"),
                                         t("sammoa.validable.transect"))) {

            List<Route> routes = getModel().getRoutes();

            boolean hasDeletedRoute = Validables.isAnyDeleted(routes);

            if (!hasDeletedRoute
                || askConfirmDeleteByCascade(t("sammoa.validable.flight"),
                                             t("sammoa.validable.route"))) {

                boolean hasDeletedObservation = Validables.isAnyDeleted(getModel().getObservations());

                if (!hasDeletedObservation
                    || askConfirmDeleteByCascade(t("sammoa.validable.flight"),
                                                 t("sammoa.validable.observation"))) {

                    long startTime = System.currentTimeMillis();
                    SammoaUtil.updateBusyState((FlightUI) context, true);
                    try {
                        // Toggle valid value
                        boolean valid = !getValidModel().getFlightValidFlag(flight);

                        getValidationService().validateFlight(flight, routes, valid);


                        validatorIsAdjusting = true;

                        // Update all transectFlights
                        for (TransectFlight transectFlight : transectFlights) {
                            getModel().updateTransectFlight(transectFlight);
                        }

                        getValidModel().cleanUpTransectFlightValid();

                        reloadRoutes();

                        reloadObservations();

                        validatorIsAdjusting = false;

                        if (log.isInfoEnabled()) {
                            log.info("Validation Flight  durring " + (System.currentTimeMillis() - startTime) + " ms");
                        }

                    } finally {
                        SammoaUtil.updateBusyState((FlightUI) context, false);
                    }
                }
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        boolean result = isEnabled();
        if (!validatorIsAdjusting) {

            Flight flight = getModel().getFlight();
            List<Route> routes = getModel().getRoutes();

            result = getValidModel().isFlightValid(flight);

            if (log.isDebugEnabled()) {
                log.debug("Flight is valid : " +  result);
            }
        }
        return result;
    }
}
