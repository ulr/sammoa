package fr.ulr.sammoa.ui.swing.io;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Abstract model which support backup configuration.
 *
 * <strong>Note:</strong> Do not remove explicit fqn of super-classe otherwise jaxx does not complie :(
 * @author tchemit <chemit@codelutin.com>
 * @since 0.7
 */
public abstract class AbstractApplicationModelWithBackup extends org.jdesktop.beans.AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VALID = "valid";

    public static final String PROPERTY_BACKUP = "backup";

    public static final String PROPERTY_BACKUP_FILENAME = "backupFilename";

    protected boolean backup;

    protected String backupFilename;

    protected boolean valid;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        boolean oldValue = this.valid;
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }

    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        boolean oldValue = this.backup;
        this.backup = backup;
        firePropertyChange(PROPERTY_BACKUP, oldValue, backup);
    }

    public String getBackupFilename() {
        return backupFilename;
    }

    public void setBackupFilename(String backupFilename) {
        String oldValue = this.backupFilename;
        this.backupFilename = backupFilename;
        firePropertyChange(PROPERTY_BACKUP_FILENAME, oldValue, backupFilename);
    }
}
