/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.layer;

import com.bbn.openmap.dataAccess.shape.EsriGraphicList;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.plugin.PlugInLayer;
import com.bbn.openmap.plugin.esri.EsriPlugIn;
import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.ui.swing.SammoaColors;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.flight.TransectModel;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created: 12/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectLayer extends PlugInLayer implements PropertyChangeListener {

    private static final long serialVersionUID = 1L;

    protected FlightUIModel model;

    public void init(FlightUIModel model) {

        this.model = model;

        for (TransectModel transect : getModel().getTransects()) {
            transect.addPropertyChangeListener(this);
        }
        getModel().addPropertyChangeListener(FlightUIModel.PROPERTY_FLIGHT_STATE, this);
        getModel().addPropertyChangeListener(FlightUIModel.PROPERTY_CURRENT_ROUTE, this);
        getModel().addPropertyChangeListener(FlightUIModel.PROPERTY_NEXT_TRANSECT, this);

        // Visibility
        ensureVisible();

        // Current transect color
        Route currentRoute = getModel().getCurrentRoute();
        if (currentRoute != null && currentRoute.getTransectFlight() != null) {
            updateLineColor(currentRoute.getTransectFlight().getTransect(), SammoaColors.CURRENT_TRANSECT_ROW_COLOR);
        }

        // Next transect color
        TransectFlight nextTransect = getModel().getNextTransect();
        if (nextTransect != null) {
            updateLineColor(nextTransect.getTransect(), SammoaColors.NEXT_TRANSECT_ROW_COLOR);
        }
    }

    protected EsriGraphicList getEsriGraphicList() {
        return getEsriPlugin().getEsriGraphicList();
    }

    protected EsriPlugIn getEsriPlugin() {
        return (EsriPlugIn) getPlugIn();
    }

    protected FlightUIModel getModel() {
        return model;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        String propertyName = evt.getPropertyName();

        if (TransectModel.PROPERTY_IN_CURRENT_STRATE.equals(propertyName)
            || TransectModel.PROPERTY_IN_FLIGHT.equals(propertyName)) {

            TransectModel transect = (TransectModel) evt.getSource();

            Integer graphicIndex = transect.getSource().getGraphicIndex();

            if (graphicIndex != null) {

                OMGraphic graphic = getEsriGraphicList().get(graphicIndex);

                checkVisible(transect, getModel().getFlightState(), graphic);

                graphic.regenerate(getProjection());
                repaint();
            }

        } else if (TransectModel.PROPERTY_SELECTED_IN_CURRENT_STRATE.equals(propertyName)) {

            TransectModel transect = (TransectModel) evt.getSource();

            Integer graphicIndex = transect.getSource().getGraphicIndex();

            if (graphicIndex != null) {

                OMGraphic graphic = getEsriGraphicList().get(graphicIndex);

                boolean visible = checkVisible(transect, getModel().getFlightState(), graphic);

                if (visible) {

                    // Selection
                    boolean value = (Boolean) evt.getNewValue();
                    if (value) {
                        graphic.select();

                    } else {
                        graphic.deselect();
                    }
                }

                graphic.regenerate(getProjection());
                repaint();
            }

        } else if (TransectModel.PROPERTY_SELECTED_IN_FLIGHT.equals(propertyName)) {

            TransectModel transect = (TransectModel) evt.getSource();

            Integer graphicIndex = transect.getSource().getGraphicIndex();

            if (graphicIndex != null) {

                OMGraphic graphic = getEsriGraphicList().get(graphicIndex);

                boolean value = (Boolean) evt.getNewValue();
                graphic.setMatted(value);

                graphic.regenerate(getProjection());
                repaint();
            }

        } else if (FlightUIModel.PROPERTY_FLIGHT_STATE.equals(propertyName)) {

            FlightState oldState = (FlightState) evt.getOldValue();
            FlightState newState = (FlightState) evt.getNewValue();

            if (oldState == FlightState.WAITING || newState == FlightState.WAITING) {

                ensureVisible();
            }

        } else if (FlightUIModel.PROPERTY_CURRENT_ROUTE.equals(propertyName)) {

            Route oldValue = (Route) evt.getOldValue();
            Route newValue = (Route) evt.getNewValue();

            if (oldValue != null && oldValue.getTransectFlight() != null) {
                updateLineColor(oldValue.getTransectFlight().getTransect(), null);
            }
            if (newValue != null && newValue.getTransectFlight() != null) {
                updateLineColor(newValue.getTransectFlight().getTransect(), SammoaColors.CURRENT_TRANSECT_ROW_COLOR);
            }
            repaint();

        } else if (FlightUIModel.PROPERTY_NEXT_TRANSECT.equals(propertyName)) {

            TransectFlight oldValue = (TransectFlight) evt.getOldValue();
            TransectFlight newValue = (TransectFlight) evt.getNewValue();

            if (oldValue != null) {
                updateLineColor(oldValue.getTransect(), null);
            }
            if (newValue != null) {
                updateLineColor(newValue.getTransect(), SammoaColors.NEXT_TRANSECT_ROW_COLOR);
            }
            repaint();
        }
    }

    protected boolean checkVisible(TransectModel transect, FlightState flightState, OMGraphic graphic) {

        boolean result = transect.isInFlight()
                         || transect.isSelectedInCurrentStrate()
                         || (transect.isInCurrentStrate() && flightState == FlightState.WAITING);

        graphic.setVisible(result);

        return result;
    }

    protected void updateLineColor(Transect transect, Color colorChanged) {

        Integer graphicIndex = transect.getGraphicIndex();

        if (graphicIndex != null) {

            OMGraphic graphic = getEsriGraphicList().get(graphicIndex);

            if (colorChanged != null) {
                graphic.setLinePaint(colorChanged);
            } else {
                graphic.setLinePaint(SammoaColors.TRANSECT_LINE_COLOR);
            }

            graphic.regenerate(getProjection());
        }
    }

    protected void ensureVisible() {

        for (TransectModel transect : getModel().getTransects()) {

            Integer graphicIndex = transect.getSource().getGraphicIndex();

            if (graphicIndex != null) {

                OMGraphic graphic = getEsriGraphicList().get(graphicIndex);

                checkVisible(transect, getModel().getFlightState(), graphic);
                graphic.regenerate(getProjection());
            }
        }
        repaint();
    }

}
