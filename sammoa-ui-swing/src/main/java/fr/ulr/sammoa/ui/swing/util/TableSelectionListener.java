package fr.ulr.sammoa.ui.swing.util;

/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;

/**
 * Created: 31/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class TableSelectionListener<T> implements ListSelectionListener {

    private static final Log log = LogFactory.getLog(TableSelectionListener.class);

    protected JTable table;

    protected SelectionModelAdapter<T> selectionModelAdapter;

    public TableSelectionListener(JTable table,
                                  SelectionModelAdapter<T> selectionModelAdapter) {
        this.table = table;
        this.selectionModelAdapter = selectionModelAdapter;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (!e.getValueIsAdjusting()) {

            ListSelectionModel listSelectionModel =
                    (ListSelectionModel) e.getSource();
            int firstIndex = e.getFirstIndex();
            int lastIndex = e.getLastIndex();
            Integer newSelectedRow = null;

            // Use the last selected row
            for (int i = firstIndex; i <= lastIndex; i++) {
                if (listSelectionModel.isSelectedIndex(i)) {
                    newSelectedRow = table.convertRowIndexToModel(i);
                }
            }

//            if (listSelectionModel.isSelectionEmpty()) {
//
//                // no selection
//            } else if (listSelectionModel.isSelectedIndex(firstIndex)) {
//
//                // use first index
//                newSelectedRow = table.convertRowIndexToModel(firstIndex);
//            } else if (listSelectionModel.isSelectedIndex(lastIndex)) {
//
//                // use last index
//                newSelectedRow = table.convertRowIndexToModel(lastIndex);
//            }
            List<T> elements = selectionModelAdapter.getElements();
            T element = null;

            if (newSelectedRow != null &&
                newSelectedRow < elements.size()) {
                element = elements.get(newSelectedRow);

                if (log.isInfoEnabled()) {
                    log.info("Select " + element.getClass().getSimpleName() + " at model index " + newSelectedRow);
                }

            } else {
                if (log.isInfoEnabled()) {
                    log.info("No element selected");
                }
            }

            // set new element in adapter
            selectionModelAdapter.setSelectedElement(element);
        }
    }
}
