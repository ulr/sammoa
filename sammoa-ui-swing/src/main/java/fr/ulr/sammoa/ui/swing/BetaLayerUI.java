/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ulr.sammoa.ui.swing;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;

import javax.swing.JComponent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 * Layer qui affiche "beta version".
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class BetaLayerUI extends AbstractLayerUI<JComponent> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 309245880711380974L;

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        super.paintLayer(g2, l);

        // position
        g2.translate(l.getBounds().getMaxX() - 140, l.getBounds().getMaxY()); // bottom right

        // yellow backgroung 
        g2.rotate(Math.PI * -42 / 180);
        g2.setColor(Color.GREEN);
        g2.fillRect(10, 10, 170, 30);

        // text
        g2.translate(35, 20);
        g2.setFont(new Font("Dialog", Font.BOLD, 16));
        g2.setColor(Color.BLACK);
        g2.drawString("RC version", 10, 10);
    }
}
