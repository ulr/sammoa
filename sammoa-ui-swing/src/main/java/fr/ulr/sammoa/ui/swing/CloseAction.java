package fr.ulr.sammoa.ui.swing;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import jaxx.runtime.JAXXObject;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 24/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class CloseAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    protected JComponent ui;

    public CloseAction(JComponent ui) {
        super(t("sammoa.action.close"));
        Preconditions.checkArgument(ui instanceof SammoaUI);
        this.ui = ui;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog container = ((JAXXObject) ui).getParentContainer(JDialog.class);
        if (container != null) {
            container.dispose();
        } else {
            ui.setVisible(false);
        }
        ((SammoaUI<?>) ui).getHandler().onCloseUI();
    }
}
