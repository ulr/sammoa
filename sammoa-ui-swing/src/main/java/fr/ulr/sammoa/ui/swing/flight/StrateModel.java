package fr.ulr.sammoa.ui.swing.flight;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.Strates;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Created: 26/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class StrateModel extends AbstractSerializableBean implements Comparable<StrateModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SOURCE = "source";

    public static final String PROPERTY_TRANSECTS = "transects";

    protected Strate source;

    protected List<TransectModel> transects;

    public StrateModel(Strate source) {
        this.source = source;
    }

    public Strate getSource() {
        return source;
    }

    public void setSource(Strate source) {
        Strate oldValue = getSource();
        this.source = source;
        firePropertyChange(PROPERTY_SOURCE, oldValue, source);
    }

    public List<TransectModel> getTransects() {
        if (transects == null) {
            transects = Lists.newArrayList();
        }
        return transects;
    }

    public void setTransects(List<TransectModel> transects) {
        List<TransectModel> oldValue = Lists.newArrayList(getTransects());
        this.transects = transects;
        firePropertyChange(PROPERTY_TRANSECTS, oldValue, transects);
    }

    @Override
    public int compareTo(StrateModel o) {
        int result = Ordering
                .from(Strates.orderBySubRegion())
                .nullsFirst()
                .compare(getSource(), o.getSource());
        return result;
    }

    public static Predicate<StrateModel> withStrate(Strate strate) {
        return new StrateModelWithStratePredicate(strate);
    }

    protected static class StrateModelWithStratePredicate
            implements Predicate<StrateModel> {

        protected Strate strate;

        public StrateModelWithStratePredicate(Strate strate) {
            this.strate = strate;
        }

        @Override
        public boolean apply(StrateModel input) {
            return strate.equals(input.getSource());
        }
    }
}
