package fr.ulr.sammoa.ui.swing;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.UIManager;
import java.awt.Color;

/**
 * Created: 16/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @deprecated since 0.6, should use {@link UIManager} instead.
 */
@Deprecated
public final class SammoaColors {

    private SammoaColors() {
        // class with statics doesn't need constructor
    }

    public static final Color ON_EFFORT_BACKGROUND_COLOR = Color.YELLOW;

    public static final Color OBSERVER_PILOT_BACKGROUND_COLOR = Color.CYAN;

    public static final Color CURRENT_TRANSECT_ROW_COLOR = Color.YELLOW;

    public static final Color POSITION_LEFT_COLOR = Color.RED;

    public static final Color POSITION_RIGHT_COLOR = Color.GREEN;

//    public static final Color NEXT_TRANSECT_ROW_COLOR = new Color(233, 255, 235);

    public static final Color NEXT_TRANSECT_ROW_COLOR = new Color(200, 0, 255);

    public static final Color OBSERVATION_FOR_ROUTE_ROW_COLOR = new Color(171, 214, 255);

    public static final Color ROUTE_FOR_TRANSECT_ROW_COLOR = new Color(171, 214, 255);

//    public static final Color ROUTE_NO_MODIFICATION_ROW_COLOR = new Color(255, 233, 233);

    public static final Color ROUTE_NO_MODIFICATION_ROW_COLOR = Color.RED;

    public static final Color FLIGHT_TRACKING_LINE_COLOR = Color.RED;

    public static final Color TRANSECT_LINE_COLOR = new Color(102, 0, 255);

    public static final Color TRANSECT_SELECT_LINE_COLOR = new Color(255, 126, 0);

    public static final Color TRANSECT_WITHOUT_GRAPHIC_BACKGROUND_COLOR = new Color(255, 233, 233);

    public static final Color DELETED_ROW_COLOR = Color.GRAY;

    public static final Color VALID_ROW_COLOR = new Color(9, 157, 20);

//    public static final Color SELECTED_ROW_COLOR = new Color(171, 214, 255);

}
