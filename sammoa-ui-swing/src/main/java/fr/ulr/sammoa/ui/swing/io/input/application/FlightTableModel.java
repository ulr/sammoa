package fr.ulr.sammoa.ui.swing.io.input.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Table model with flight entries to import.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class FlightTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected final List<FlightImportEntry> importEntries;

    public FlightTableModel(List<FlightImportEntry> importEntries) {
        Preconditions.checkNotNull(importEntries);
        this.importEntries = importEntries;
    }

    public List<FlightImportEntry> getImportEntries() {
        return importEntries;
    }

    @Override
    public String getColumnName(int column) {
        return FlightColumn.valueOf(column).getColumnName();
    }

    @Override
    public int getRowCount() {
        return getImportEntries().size();
    }

    @Override
    public int getColumnCount() {
        return FlightColumn.values().length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        FlightColumn column = FlightColumn.valueOf(columnIndex);
        FlightImportEntry bean = getImportEntries().get(rowIndex);
        return column.isEditable(bean);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        FlightColumn column = FlightColumn.valueOf(columnIndex);
        return column.getType();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FlightImportEntry bean = getImportEntries().get(rowIndex);
        FlightColumn column = FlightColumn.valueOf(columnIndex);
        return column.getValue(rowIndex, bean);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        FlightImportEntry bean = getImportEntries().get(rowIndex);
        FlightColumn column = FlightColumn.valueOf(columnIndex);
        column.setValue(bean, aValue);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    protected enum FlightColumn {

        SURVEY(String.class, "survey", importEntry -> importEntry.getStorage().getName(), null),
        SURVEY_EXIST(Boolean.class, "surveyExist", FlightImportEntry::isSurveyExist, null),
        FLIGHT(String.class, "flight", importEntry -> importEntry.getFlightStorage().getName(), null),
        FLIGHT_EXIST(Boolean.class, "FlightExist", importEntry -> importEntry.getExistingFlight() != null, null),
        TREAT(Boolean.class, "treat", FlightImportEntry::isTreat, (importEntry, value) -> importEntry.setTreat(Boolean.valueOf(value.toString())));

        private final Class<?> type;

        private final String columnName;

        private final Function<FlightImportEntry, Object> valueGetter;
        
        private final BiConsumer<FlightImportEntry, Object> valueSetter;

        FlightColumn(Class<?> type, String columnName,
                     Function<FlightImportEntry, Object> valueGetter,
                     BiConsumer<FlightImportEntry, Object> valueSetter) {
            this.type = type;
            this.columnName = columnName;
            this.valueGetter = valueGetter;
            this.valueSetter = valueSetter;
        }

        public Class<?> getType() {
            return type;
        }

        public Object getValue(int index, FlightImportEntry bean) {
            return valueGetter.apply(bean);
        }

        public void setValue(FlightImportEntry bean, Object aValue) {
            if (valueSetter !=null) {
                valueSetter.accept(bean, aValue);
            }
        }

        public boolean isEditable(FlightImportEntry bean) {
            return valueSetter != null;
        }

        public static FlightColumn valueOf(int ordinal) {
            for (FlightColumn value : values()) {
                if (ordinal == value.ordinal()) {
                    return value;
                }
            }
            throw new EnumConstantNotPresentException(FlightColumn.class,
                    "ordinal=" + ordinal);
        }

        public String getColumnName() {
            return columnName;
        }
    }
}
