<!--
  #%L
  SAMMOA :: UI Swing
  %%
  Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='regionUI'
        layout='{new BorderLayout()}'
        implements='fr.ulr.sammoa.ui.swing.SammoaUI&lt;RegionUIHandler&gt;'>

  <import>
    fr.ulr.sammoa.ui.swing.CloseAction
    fr.ulr.sammoa.ui.swing.SammoaUIContext

    jaxx.runtime.swing.editor.FileEditor
    jaxx.runtime.validator.swing.SwingValidatorUtil
    jaxx.runtime.validator.swing.SwingValidatorMessageTableModel

    static org.nuiton.i18n.I18n.t
  </import>

  <script><![CDATA[

    public RegionUI(SammoaUIContext context) {
        RegionUIHandler handler = new RegionUIHandler(context, this);
        setContextValue(handler);
        handler.beforeInitUI();
    }

    protected void $afterCompleteSetup() {
        getHandler().afterInitUI();
    }
  ]]></script>

  <SwingValidatorMessageTableModel id='errorTableModel'/>

  <RegionUIHandler id='handler'
                     initializer='getContextValue(RegionUIHandler.class)'/>

  <RegionUIModel id='model'
                 initializer='getContextValue(RegionUIModel.class)'/>

  <BeanValidator id='validator'
                 bean='model'
                 errorTableModel='errorTableModel'
                 uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='code' component='regionCodeField'/>
    <field name='name' component='regionNameField'/>
  </BeanValidator>

  <CloseAction id='closeAction' constructorParams='this'/>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell anchor='west'>
        <JLabel id='regionCodeLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1.0'>
        <JTextField id='regionCodeField'
                    onKeyReleased='getModel().setCode(regionCodeField.getText())'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='regionNameLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1.0'>
        <JTextField id='regionNameField'
                    onKeyReleased='getModel().setName(regionNameField.getText())'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='regionSpeciesLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1.0'>
        <FileEditor id='speciesFileEditor'/>
      </cell>
    </row>
    <row>
      <cell fill='both' columns='2'>
        <JPanel id='messagePanel' layout='{new GridLayout()}'>
          <JScrollPane columnHeaderView='{errorTable.getTableHeader()}'>
            <JTable id='errorTable' />
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell fill='both' columns='2'>
        <JPanel layout='{new GridLayout(1,2,0,0)}'>
          <JButton id='closeButton'/>
          <JButton id='saveButton'
                   onActionPerformed='getHandler().saveRegion()'/>
        </JPanel>
      </cell>
    </row>
  </Table>
</JPanel>
