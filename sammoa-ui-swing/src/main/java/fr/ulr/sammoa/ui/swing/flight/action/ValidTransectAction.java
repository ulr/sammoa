package fr.ulr.sammoa.ui.swing.flight.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.FluentIterable;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.Validables;
import fr.ulr.sammoa.ui.swing.flight.FlightUI;
import fr.ulr.sammoa.ui.swing.flight.TransectFlightModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ValidTransectAction extends ValidAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidTransectAction.class);

    private static final long serialVersionUID = 1L;

    protected boolean validatorIsAdjusting;

    public ValidTransectAction(JAXXContext context) {
        super(t("sammoa.action.validTransect"), context);
        putValue(Action.SHORT_DESCRIPTION, t("sammoa.action.validTransect.tip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        TransectFlightModel bean = getModel().getTransectFlightEditBean();

        if (bean != null) {

            TransectFlight transectFlight = bean.getSource();

            int index = getModel().indexOfTransectFlights(bean);

            if (log.isDebugEnabled()) {
                log.debug("Validation for transectFlight " + index + " : " + transectFlight.getTransect().getName());
            }

            if (transectFlight.isDeleted()) {

                if (askConfirmDelete(t("sammoa.validable.transect"))) {
                    try {
                        SammoaUtil.updateBusyState((FlightUI) context, true);
                        getValidationService().validateTransectFlight(getModel().getFlight(), transectFlight);
                        getModel().removeTransectFlight(index);

                    } finally {
                        SammoaUtil.updateBusyState((FlightUI) context, false);
                    }
                }

            } else {

                // Get routes for this transectFlight
                FluentIterable<Route> routes = FluentIterable
                        .from(getModel().getRoutes())
                        .filter(Routes.withTransectFlight(transectFlight));

                boolean hasDeletedRoute = routes.anyMatch(Validables.isDeleted());

                if (!hasDeletedRoute
                    || askConfirmDeleteByCascade(t("sammoa.validable.transect"),
                                                 t("sammoa.validable.route"))) {

                    boolean hasDeletedObservation = false;
                    for (Route route : routes) {
                        hasDeletedObservation |= Observations.isAnyDeletedFromRoute(
                                getModel().getObservations(), route, getModel().getRoutes());
                    }

                    if (!hasDeletedObservation
                        || askConfirmDeleteByCascade(t("sammoa.validable.transect"),
                                                     t("sammoa.validable.observation"))) {

                        try {
                            SammoaUtil.updateBusyState((FlightUI) context, true);

                            TransectFlight transectFlightChanged =
                                    getValidationService().validateTransectFlight(getModel().getFlight(), transectFlight);
                            getModel().updateTransectFlight(transectFlightChanged);

                            validatorIsAdjusting = true;

                            reloadRoutes();

                            reloadObservations();

                            validatorIsAdjusting = false;

                        } finally {
                            SammoaUtil.updateBusyState((FlightUI) context, false);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected boolean checkEnabled() {
        boolean result = isEnabled();
        if (!validatorIsAdjusting) {
            TransectFlightModel bean = getModel().getTransectFlightEditBean();
            result = bean != null
                     && getValidModel().isTransectFlightValid(bean.getSource());
        }
        return result;
    }
}
