/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing;

import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.ui.swing.util.LogFileInitializer;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.topia.persistence.TopiaException;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 16/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class StartApp {

    private static final Log log = LogFactory.getLog(StartApp.class);

    public static void main(String... args) {

        // Create sammoa configuration from sammoa.properties file
        SammoaConfig config = new SammoaConfig("sammoa.properties", args);

        // Create application context
        SammoaContext appContext = new SammoaContext(config);

        try {
            // Create ui context
            final SammoaUIContext context = SammoaUIContext.newUIContext(appContext);

            // Use shutdownHook to close context on System.exit
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

                @Override
                public void run() {
                    if (log.isInfoEnabled()) {
                        log.info("Close context on shutdown");
                    }
                    try {
                        context.close();
                    } catch (IOException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Error on close", e);
                        }
                    }
                }
            }));

            context.open();

            // Initialize log file
            File logFile = config.getLogFile();
            if (logFile != null) {
                new LogFileInitializer(logFile).addFileAppender();
                if (log.isInfoEnabled()) {
                    log.info("Use log file " + logFile.getAbsolutePath());
                }
            }

            // Initialize i18n
            I18n.init(new DefaultI18nInitializer("sammoa-i18n"), Locale.UK);

            // Prepare ui look&feel and load ui properties
            try {
                SwingUtil.initNimbusLoookAndFeel();
            } catch (Exception e) {
                // could not find nimbus look-and-feel
                if (log.isWarnEnabled()) {
                    log.warn("Failed to init nimbus look and feel");
                }
            }

            UIManager.put("BlockingLayerUI.blockingColor", new Color(50, 50, 50));

            MainUI mainUI = new MainUI(context);
            mainUI.setVisible(true);

            URL iconURL = StartApp.class.getClassLoader().getResource("fr/ulr/sammoa/assets/logo_sammoa_icone.png");
            if(iconURL != null) {
                ImageIcon icon = new ImageIcon(iconURL);
                mainUI.setIconImage(icon.getImage());
            }

        } catch (TopiaException ex) {
            if (log.isErrorEnabled()) {
                log.error("Error during db connection", ex);
            }
            SammoaUtil.showErrorMessage(null, t("sammoa.messageDialog.persistence.error", ex.getMessage()));
            System.exit(1);
        }
    }

}
