package fr.ulr.sammoa.ui.swing.flight.effort.action;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import java.awt.event.ActionEvent;

/**
 * Action to edit next editable cell from selected cell.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class MoveToNextEditableCellAction extends AbstractAction {
    private static final long serialVersionUID = 1L;

    private static final Log log =
            LogFactory.getLog(MoveToNextEditableCellAction.class);

    protected final JTable table;

    public MoveToNextEditableCellAction(JTable table) {
        this.table = table;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        int currentRow = table.getSelectedRow();
        int currentColumn = table.getSelectedColumn();

        if (log.isDebugEnabled()) {
            log.debug("Move to next editable cell, " + currentRow + ", " + currentColumn);
        }

        while (currentRow <= table.getRowCount() || currentColumn <= table.getColumnCount()) {

            // go to next cell
            currentColumn++;

            // select next cell
            if (currentColumn >= table.getColumnCount()) {
                currentColumn = 0;
                currentRow++;
            }

            if (table.isCellEditable(currentRow, currentColumn)) {
                doSelectCell(currentRow, currentColumn);
//                    // select cell
//                    setColumnSelectionInterval(currentColumn, currentColumn);
//                    setRowSelectionInterval(currentRow, currentRow);
//                    editCellAt(currentRow, currentColumn);
//                    log.debug("While select cell at {}, " +  currentRow, currentColumn);
                break;
            } else if (log.isDebugEnabled()) {
                log.debug("Cell at " + currentRow + ", " + currentColumn + " not editable");
            }
        }
    }

    protected void doSelectCell(int currentRow, int currentColumn) {
        if (log.isDebugEnabled()) {
            log.debug("While select cell at " + currentRow + ", " + currentColumn);
        }
        table.editCellAt(currentRow, currentColumn);
        table.setColumnSelectionInterval(currentColumn, currentColumn);
        table.setRowSelectionInterval(currentRow, currentRow);
    }
}
