/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observer;
import jaxx.runtime.swing.ListSelectorModel;
import jaxx.runtime.swing.model.GenericListModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

/** @author sletellier <letellier@codelutin.com> */
public class ObserverListSelectorModel extends ListSelectorModel<Observer> {

    public static final String PROPERTY_BEAN = "bean";

    protected FlightUIModel flightUIModel;

    protected Flight bean;

    public ObserverListSelectorModel(FlightUIModel flightUIModel) {
        this.flightUIModel = flightUIModel;
    }

    public Flight getBean() {
        return bean;
    }

    public void setBean(Flight bean) {
        Flight oldValue = getBean();
        this.bean = bean;
        if (bean != null) {
            flightUIModel.addPropertyChangeListener(FlightUIModel.PROPERTY_OBSERVERS,
                                                    new PropertyChangeListener() {

                                                        @Override
                                                        public void propertyChange(PropertyChangeEvent evt) {
                                                            setValues(flightUIModel.getObservers());
                                                            setSelectedValues(Lists.newArrayList(getBean().getObserver()));
                                                        }
                                                    });
            firePropertyChange(PROPERTY_BEAN, oldValue, bean);
            setValues(flightUIModel.getObservers());
            setSelectedValues(bean.getObserver());
        }
    }

    @Override
    public void add() {
        bean.addAllObserver(Sets.newHashSet(fromModel.getSelectedValues()));
        super.add();
    }

    @Override
    protected void moveSelect(GenericListModel<Observer> from, GenericListModel<Observer> to) {
        Collection<Observer> selectedValues = Sets.newHashSet(from.getSelectedValues());
        from.removeElements(selectedValues);
        to.addElements(selectedValues);
    }

    @Override
    public void remove() {

        // TODO sletellier 29062012 : genererate removeAll in topia
        for (Object o : toModel.getSelectedValues()) {
            Observer observer = (Observer) o;
            if (bean.getObserver().contains(observer)) {
                bean.removeObserver(observer);
            }
        }
//        FlightService service = SammoaUIContext.getUIContext().getAppContext().getFlightService();
//        service.removeAllFlightObservers(bean, toModel.getSelectedValues());
        super.remove();
    }
}
