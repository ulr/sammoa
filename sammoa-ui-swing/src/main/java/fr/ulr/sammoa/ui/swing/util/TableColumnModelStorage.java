package fr.ulr.sammoa.ui.swing.util;

/*-
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaConfig;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TableColumnModelStorage implements TableColumnModelListener {

    private static final Log log = LogFactory.getLog(TableColumnModelStorage.class);

    protected final JTable table;

    protected final SammoaConfig config;

    protected final SammoaConfig.Table tableId;

    protected List<String> defaultOrder;

    protected boolean loading;

    public TableColumnModelStorage(JTable table, SammoaConfig config, SammoaConfig.Table tableId) {
        this.table = table;
        this.config = config;
        this.tableId = tableId;
    }

    protected List<String> getColumnsOrder() {
        TableColumnModel model = table.getColumnModel();

        return Collections.list(model.getColumns()).stream()
                .map(TableColumn::getIdentifier)
                .map(String.class::cast)
                .collect(Collectors.toList());
    }

    public void init() {
        defaultOrder = getColumnsOrder();
        loadColumnsOrder();
        table.getColumnModel().addColumnModelListener(this);
    }

    public void loadColumnsOrder() {
        loading = true;
        List<String> columnsOrder = config.getTableColumnsOrder(tableId);

        if (CollectionUtils.isEmpty(columnsOrder)) {
            columnsOrder = defaultOrder;
        }

        for (int index = 0; index < columnsOrder.size(); index++) {
            String columnId = columnsOrder.get(index);

            int modelIndex = table.getColumnModel().getColumnIndex(columnId);
            table.getColumnModel().moveColumn(modelIndex, index);
        }
        loading = false;
    }

    @Override
    public void columnAdded(TableColumnModelEvent e) {

    }

    @Override
    public void columnRemoved(TableColumnModelEvent e) {

    }

    @Override
    public void columnMoved(TableColumnModelEvent e) {
        if (!loading && e.getFromIndex() != e.getToIndex()) {

            List<String> columnsOrder = getColumnsOrder();
            if (defaultOrder.equals(columnsOrder)) {
                columnsOrder = null;
            }
            if (log.isInfoEnabled()) {
                log.info("save order");
            }

            config.setTableColumnsOrder(tableId, columnsOrder);
        }
    }

    @Override
    public void columnMarginChanged(ChangeEvent e) {

    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e) {

    }
}
