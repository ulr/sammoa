package fr.ulr.sammoa.ui.swing.io.output.map;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.io.output.map.ExportMapModel;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.Species2;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StringRef;
import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Model of export map UI.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class ExportMapUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SURVEYS = "surveys";

    public static final String PROPERTY_SELECTED_SURVEY = "selectedSurvey";

    public static final String PROPERTY_BEGIN_DATE = "beginDate";

    public static final String PROPERTY_END_DATE = "endDate";

    public static final String PROPERTY_STRATES = "strates";

    public static final String PROPERTY_SELECTED_STRATES = "selectedStrates";

    public static final String PROPERTY_ROUTE_TYPES = "routeTypes";

    public static final String PROPERTY_SELECTED_ROUTE_TYPES = "selectedRouteTypes";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SELECTED_SPECIES = "selectedSpecies";

    public static final String PROPERTY_SPECIES_TYPES = "speciesTypes";

    public static final String PROPERTY_SELECTED_SPECIES_TYPES = "selectedSpeciesTypes";

    public static final String PROPERTY_EXPORT_DIRECTORY = "exportDirectory";

    public static final String PROPERTY_EXPORT_FILENAME = "exportFilename";

    public static final String PROPERTY_EXPORT_EFFORTS_FILENAME = "exportEffortsFilename";

    public static final String PROPERTY_EXPORT_OBSERVATIONS_FILENAME = "exportObservationsFilename";

    public static final String PROPERTY_EXPORT_GEO_POINTS_FILENAME = "exportGeoPointsFilename";

    protected List<Survey> surveys;

    protected Survey selectedSurvey;

    protected Date beginDate;

    protected Date endDate;

    protected List<Strate> strates;

    protected List<Strate> selectedStrates;

    protected List<RouteType> routeTypes;

    protected List<RouteType> selectedRouteTypes;

    protected List<Species> species;

    protected List<Species> selectedSpecies;

    protected List<StringRef> selectedSpeciesTypes;

    protected List<StringRef> speciesTypes;

    protected File exportDirectory;

    protected String exportFilename;

    protected String exportEffortsFilename;

    protected String exportObservationsFilename;

    protected String exportGeoPointsFilename;

    public File getExportDirectory() {
        return exportDirectory;
    }

    public void setExportDirectory(File exportDirectory) {
        File oldValue = this.exportDirectory;
        this.exportDirectory = exportDirectory;
        firePropertyChange(PROPERTY_EXPORT_DIRECTORY, oldValue, exportDirectory);
    }

    public String getExportFilename() {
        return exportFilename;
    }

    public void setExportFilename(String exportFilename) {
        String oldValue = this.exportFilename;
        this.exportFilename = exportFilename;
        firePropertyChange(PROPERTY_EXPORT_FILENAME, oldValue, exportFilename);
    }

    public String getExportEffortsFilename() {
        return exportEffortsFilename;
    }

    public void setExportEffortsFilename(String exportEffortsFilename) {
        String oldValue = this.exportEffortsFilename;
        this.exportEffortsFilename = exportEffortsFilename;
        firePropertyChange(PROPERTY_EXPORT_EFFORTS_FILENAME, oldValue, exportEffortsFilename);
    }

    public String getExportObservationsFilename() {
        return exportObservationsFilename;
    }

    public void setExportObservationsFilename(String exportObservationsFilename) {
        String oldValue = this.exportObservationsFilename;
        this.exportObservationsFilename = exportObservationsFilename;
        firePropertyChange(PROPERTY_EXPORT_OBSERVATIONS_FILENAME, oldValue, exportObservationsFilename);
    }

    public String getExportGeoPointsFilename() {
        return exportGeoPointsFilename;
    }

    public void setExportGeoPointsFilename(String exportGeoPointsFilename) {
        String oldValue = this.exportGeoPointsFilename;
        this.exportGeoPointsFilename = exportGeoPointsFilename;
        firePropertyChange(PROPERTY_EXPORT_GEO_POINTS_FILENAME, oldValue, exportGeoPointsFilename);
    }

    public List<Survey> getSurveys() {
        if (surveys == null) {
            surveys = Lists.newArrayList();
        }
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        List<Survey> oldValue = this.surveys;
        this.surveys = surveys;
        firePropertyChange(PROPERTY_SURVEYS, oldValue, surveys);
    }

    public Survey getSelectedSurvey() {
        return selectedSurvey;
    }

    public void setSelectedSurvey(Survey selectedSurvey) {
        Survey oldValue = this.selectedSurvey;
        this.selectedSurvey = selectedSurvey;
        firePropertyChange(PROPERTY_SELECTED_SURVEY, oldValue, selectedSurvey);
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        Date oldValue = this.beginDate;
        this.beginDate = beginDate;
        firePropertyChange(PROPERTY_BEGIN_DATE, oldValue, beginDate);
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        Date oldValue = this.endDate;
        this.endDate = endDate;
        firePropertyChange(PROPERTY_END_DATE, oldValue, endDate);
    }

    public List<Strate> getStrates() {
        if (strates == null) {
            strates = Lists.newArrayList();
        }
        return strates;
    }

    public void setStrates(List<Strate> strates) {
        List<Strate> oldValue = this.strates;
        this.strates = strates;
        firePropertyChange(PROPERTY_STRATES, oldValue, strates);
    }

    public List<Strate> getSelectedStrates() {
        if (selectedStrates == null) {
            selectedStrates = Lists.newArrayList();
        }
        return selectedStrates;
    }

    public void setSelectedStrates(List<Strate> selectedStrates) {
        List<Strate> oldValue = this.selectedStrates;
        this.selectedStrates = selectedStrates;
        firePropertyChange(PROPERTY_SELECTED_STRATES, oldValue, selectedStrates);
    }

    public List<RouteType> getRouteTypes() {
        if (routeTypes == null) {
            routeTypes = Lists.newArrayList();
        }
        return routeTypes;
    }

    public void setRouteTypes(List<RouteType> routeTypes) {
        List<RouteType> oldValue = this.routeTypes;
        this.routeTypes = routeTypes;
        firePropertyChange(PROPERTY_ROUTE_TYPES, oldValue, routeTypes);
    }

    public List<RouteType> getSelectedRouteTypes() {
        if (selectedRouteTypes == null) {
            selectedRouteTypes = Lists.newArrayList();
        }
        return selectedRouteTypes;
    }

    public void setSelectedRouteTypes(List<RouteType> selectedRouteTypes) {
        List<RouteType> oldValue = this.selectedRouteTypes;
        this.selectedRouteTypes = selectedRouteTypes;
        firePropertyChange(PROPERTY_SELECTED_ROUTE_TYPES, oldValue, selectedRouteTypes);
    }

    public List<Species> getSpecies() {
        if (species == null) {
            species = Lists.newArrayList();
        }
        return species;
    }

    public void setSpecies(List<Species> species) {
        List<Species> oldValue = this.species;
        this.species = species;
        firePropertyChange(PROPERTY_SPECIES, oldValue, species);
    }

    public List<Species> getSelectedSpecies() {
        if (selectedSpecies == null) {
            selectedSpecies = Lists.newArrayList();
        }
        return selectedSpecies;
    }

    public void setSelectedSpecies(List<Species> selectedSpecies) {
        List<Species> oldValue = this.selectedSpecies;
        this.selectedSpecies = selectedSpecies;
        firePropertyChange(PROPERTY_SELECTED_SPECIES, oldValue, selectedSpecies);
    }

    public List<StringRef> getSpeciesTypes() {
        if (speciesTypes == null) {
            speciesTypes = Lists.newArrayList();
        }
        return speciesTypes;
    }

    public void setSpeciesTypes(List<StringRef> speciesTypes) {
        List<StringRef> oldValue = this.speciesTypes;
        this.speciesTypes = speciesTypes;
        firePropertyChange(PROPERTY_SPECIES_TYPES, oldValue, speciesTypes);
    }

    public List<StringRef> getSelectedSpeciesTypes() {
        if (selectedSpeciesTypes == null) {
            selectedSpeciesTypes = Lists.newArrayList();
        }
        return selectedSpeciesTypes;
    }

    public void setSelectedSpeciesTypes(List<StringRef> selectedSpeciesTypes) {
        List<StringRef> oldValue = this.selectedSpeciesTypes;
        this.selectedSpeciesTypes = selectedSpeciesTypes;
        firePropertyChange(PROPERTY_SELECTED_SPECIES_TYPES, oldValue, selectedSpeciesTypes);
    }

    public ExportMapModel toModel(String filename) {

        List<Species> speciesToUse = Species2.getSelectedSpecies(
                getSpecies(), getSelectedSpecies(), getSelectedSpeciesTypes());

        // Ensure bounds for beginDate and enDate
        Date beginDate = getBeginDate();
        Date surveyBeginDate = getSelectedSurvey().getBeginDate();
        if (beginDate == null || beginDate.before(surveyBeginDate)) {
            beginDate = surveyBeginDate;
        }
        beginDate = Dates.toDateTime(beginDate)
                .secondOfDay()
                .withMinimumValue()
                .toDate();

        Date endDate = getEndDate();
        Date surveyEndDate = getSelectedSurvey().getEndDate();
        if (endDate == null || endDate.after(surveyEndDate)) {
            endDate = surveyEndDate;
        }
        endDate = Dates.toDateTime(endDate)
                .secondOfDay()
                .withMaximumValue()
                .toDate();

        ExportMapModel result = ExportMapModel.newModel(
                getExportDirectory(),
                filename,
                getSelectedSurvey(),
                beginDate,
                endDate,
                getSelectedStrates(),
                getSelectedRouteTypes(),
                speciesToUse
        );
        return result;
    }
}
