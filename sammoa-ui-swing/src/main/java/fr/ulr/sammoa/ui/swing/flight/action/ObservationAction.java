/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.action;

import fr.ulr.sammoa.application.flightController.FlightState;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import jaxx.runtime.JAXXContext;

import javax.swing.Timer;
import java.awt.event.ActionEvent;

/**
 * Created: 03/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public abstract class ObservationAction extends BaseFlightAction {

    private static final long serialVersionUID = 1L;

    protected Position position;

    public ObservationAction(String name, JAXXContext context, Position position) {
        super(name, context);
        this.position = position;
        bindModelProperty(FlightUIModel.PROPERTY_FLIGHT_STATE, false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // on attend 100 ms avant de créé l'observation pour laisser le temps a swing de finir de propager les évenements
        // cf https://forge.codelutin.com/issues/9758
        Timer timer = new Timer(100,
                evt -> getFlightController().observation(position));
        timer.setRepeats(false);
        timer.start();
    }

    @Override
    protected boolean checkEnabled() {
        FlightState state = getModel().getFlightState();
        return state == FlightState.ON_EFFORT
               || state == FlightState.OFF_EFFORT;
    }
}
