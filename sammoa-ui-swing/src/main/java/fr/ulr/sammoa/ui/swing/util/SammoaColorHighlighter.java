package fr.ulr.sammoa.ui.swing.util;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.util.PaintUtils;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Component;

/**
 * Created: 16/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SammoaColorHighlighter extends AbstractHighlighter {

    protected Color color;

    protected boolean foreground;

    public SammoaColorHighlighter(HighlightPredicate predicate, Color color, boolean foreground) {
        super(predicate);
        this.color = color;
        this.foreground = foreground;
    }

    @Override
    protected Component doHighlight(Component component, ComponentAdapter adapter) {
        if (component instanceof JButton) {
            // do nothing

        } else {
            if (foreground) {
                component.setForeground(color);

            } else {
                component.setBackground(color);
                if (adapter.isSelected()) {
                    component.setForeground(PaintUtils.computeForeground(color));
                }
            }
        }
        return component;
    }
}
