/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.ui.swing.flight.effort;

import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.ui.swing.flight.FlightUIModel;
import fr.ulr.sammoa.ui.swing.util.SammoaUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/** @author sletellier <letellier@codelutin.com> */
public class RouteTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected FlightUIModel flightUIModel;

    public RouteTableModel(FlightUIModel flightUIModel) {
        this.flightUIModel = flightUIModel;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        RouteColumn routeColumn = RouteColumn.valueOf(column);
        return routeColumn.getType();
    }

    @Override
    public int getRowCount() {
        return getBean().size();
    }

    public Route getRow(int index) {
        return getBean().get(index);
    }

    public List<Route> getBean() {
        return flightUIModel.getRoutes();
    }

    @Override
    public int getColumnCount() {
        return RouteColumn.values().length;
    }

    @Override
    public String getColumnName(int column) {
        return RouteColumn.valueOf(column).getColumnName();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        Route route = getRow(row);
        RouteColumn routeColumn = RouteColumn.valueOf(column);
        boolean result = routeColumn.isEditable(route, flightUIModel.isValidationMode());
        return result;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Route route = getRow(row);
        RouteColumn routeColumn = RouteColumn.valueOf(column);
        Object result = routeColumn.getValue(route, this);
        return result;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        Route route = getRow(row);
        RouteColumn routeColumn = RouteColumn.valueOf(column);
        routeColumn.setValue(route, aValue);
        fireTableRowsUpdated(row, row);
    }

    public int getBeanIndex(Route bean) {
        int row = getBean().indexOf(bean);
        return row;
    }

    public Route getBean(int row) {
        SwingUtil.ensureRowIndex(this, row);
        Route bean = getBean().get(row);
        return bean;
    }

    public Pair<Integer, Integer> getCell(Route bean, String fieldName) {

        int row = getBeanIndex(bean);
        int col = RouteColumn.getValueFromFieldName(fieldName).ordinal();

        Pair<Integer, Integer> cell = Pair.of(row, col);
        return cell;
    }

    protected Flight getFlight() {
        return flightUIModel.getFlight();
    }

    protected enum RouteColumn {

        EFFORT_NUMBER(false, Integer.class, Route.PROPERTY_EFFORT_NUMBER),
        BEGIN_TIME(true, Date.class, Route.PROPERTY_BEGIN_TIME) {

            @Override
            public boolean isEditable(Route bean, boolean validationMode) {
                return super.isEditable(bean, validationMode) && validationMode;
            }
        },
        ROUTE_TYPE(false, RouteType.class, Route.PROPERTY_ROUTE_TYPE),
        TRANSECT(true, TransectFlight.class, Route.PROPERTY_TRANSECT_FLIGHT) {

            @Override
            public boolean isEditable(Route bean, boolean validationMode) {
                return super.isEditable(bean, validationMode)
                       && bean.getRouteType() == RouteType.LEG
                       && validationMode;
            }
        },
        SEA_STATE(true, int.class, Route.PROPERTY_SEA_STATE),
        SWELL(true, int.class, Route.PROPERTY_SWELL),
        TURBIDITY(true, int.class, Route.PROPERTY_TURBIDITY),
        SKY_GLINT(true, String.class, Route.PROPERTY_SKY_GLINT),
        GLARE_FROM(true, Integer.class, Route.PROPERTY_GLARE_FROM),
        GLARE_TO(true, Integer.class, Route.PROPERTY_GLARE_TO),
        GLARE_SEVERITY(true, int.class, Route.PROPERTY_GLARE_SEVERITY),
        GLARE_UNDER(true, boolean.class, Route.PROPERTY_GLARE_UNDER),
        CLOUD_COVER(true, int.class, Route.PROPERTY_CLOUD_COVER),
        SUBJECTIVE_CONDITIONS(true, String.class, Route.PROPERTY_SUBJECTIVE_CONDITIONS),
        UNEXPECTED_LEFT(true, String.class, Route.PROPERTY_UNEXPECTED_LEFT),
        UNEXEPECTED_RIGHT(true, String.class, Route.PROPERTY_UNEXPECTED_RIGHT),
        COMMENT(true, String.class, Route.PROPERTY_COMMENT),
        DELETED(true, boolean.class, Route.PROPERTY_DELETED);

        private final boolean editable;

        private final String[] beanProperties;

        private final Class<?> type;

        private final String columnName;

        RouteColumn(boolean editable,
                            Class<?> type,
                            String... beanProperties) {
            this.editable = editable;
            this.type = type;
            this.beanProperties = beanProperties;
            this.columnName = beanProperties[0];
        }

        public Class<?> getType() {
            return type;
        }

        public String getColumnName() {
            return columnName;
        }

        public Object getValue(Route bean, RouteTableModel model) {
            Object result = SammoaUtil.getPropertyValue(bean, beanProperties);
            return result;
        }

        public void setValue(Route bean, Object value) {
            if (type.isPrimitive() && value == null) {
                // can not set a null value to a primitive field
            } else {
                SammoaUtil.setPropertyValue(bean, value, beanProperties);
            }
        }

        public boolean isEditable(Route bean, boolean validationMode) {
            boolean result = !bean.isValid() && editable;
            return result;
        }

        public static RouteColumn valueOf(int ordinal) {
            for (RouteColumn value : values()) {
                if (ordinal == value.ordinal()) {
                    return value;
                }
            }
            throw new EnumConstantNotPresentException(RouteColumn.class,
                                                      "ordinal=" + ordinal);
        }

        public static RouteColumn getValueFromFieldName(String fieldName) {
            RouteColumn result = null;
            for (RouteColumn value : values()) {
                if (fieldName.equals(value.columnName)) {
                    result = value;
                    break;
                }
            }
            return result;
        }
    }
}
