package fr.ulr.sammoa.ui.swing.io.output.application;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * What to do after a application export.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public enum ExportApplicationCallbackMode {
    /**
     * Do nothing.
     * <p/>
     * Says after this every thing stay in place.
     */
    NOTHING,
    /**
     * Remove flight data.
     * <p/>
     * Says after this referential are still there (Region, Survey, Strate, Observer, Species).
     */
    REMOVE_FLIGHTS,
    /**
     * Remove campaing data.
     * <p/>
     * Says after this referential are still there (Region, Species).
     */
    REMOVE_SURVEY, /**
     * Remove survey data.
     * <p/>
     * Says after this, there is no more stuff in db.
     */
    REMOVE_ALL
}
