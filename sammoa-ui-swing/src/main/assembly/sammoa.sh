#!/bin/bash

MEMORY="-Xmx1024M"

java $MEMORY -jar ${project.build.finalName}-full.${project.packaging} $*
