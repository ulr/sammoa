package fr.ulr.sammoa.ui.swing;
/*
 * #%L
 * SAMMOA :: UI Swing
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteImpl;
import fr.ulr.sammoa.ui.swing.survey.SurveyUIModel;
import fr.ulr.sammoa.ui.swing.region.RegionUIModel;
import fr.ulr.sammoa.ui.swing.transect.TransectUIModel;
import org.junit.Test;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.SortedSet;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author chemit <chemit@codelutin.com>
 * @since 0.3
 */
public class BeanValidatorDetectorTest extends AbstractValidatorDetectorTest {

    public BeanValidatorDetectorTest() {
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    /** La liste des classes avec validation. */
    protected static final Class<?>[] VALIDATOR_CLASSES_ON_BOARD = {
            Observation.class,
            Route.class
    };
    protected static final Class<?>[] VALIDATOR_CLASSES_VALIDATION = {
            Observation.class,
            Route.class
    };
    protected static final Class<?>[] VALIDATOR_CLASSES = {
            SurveyUIModel.class,
            RegionUIModel.class,
            TransectUIModel.class
    };

    @Override
    protected File getRootDirectory(File basedir) {
        return new File(basedir,
                        "src" + File.separator + "main" + File.separator + "resources");
    }

    @Test
    public void detectAll() {

        {
            SortedSet<NuitonValidator<?>> validators = detectValidators(
                    Pattern.compile("onBoard"), VALIDATOR_CLASSES_ON_BOARD);
            assertValidatorSetWithSameContextName(
                    validators, "onBoard", VALIDATOR_CLASSES_ON_BOARD);
        }

        {
            SortedSet<NuitonValidator<?>> validators = detectValidators(
                    Pattern.compile("validation"), VALIDATOR_CLASSES_VALIDATION);
            assertValidatorSetWithSameContextName(
                    validators, "validation", VALIDATOR_CLASSES_VALIDATION);
        }

        {
            SortedSet<NuitonValidator<?>> validators = detectValidators(
                    VALIDATOR_CLASSES);
            assertEquals(VALIDATOR_CLASSES.length, validators.size());
        }
    }

    @Test
    public void validateEffort() throws Exception {

        SortedSet<NuitonValidator<?>> validators = detectValidators(
                Pattern.compile("onBoard"), Route.class);
        assertEquals(1, validators.size());
        NuitonValidator<Route> validator = (NuitonValidator<Route>) validators.first();

        {
            Route route = new RouteImpl();
            route.setTurbidity(7);
            route.setSubjectiveConditions("EEE");

            NuitonValidatorResult result = validator.validate(route);
            assertFalse(result.isValid());
            assertTrue(result.getFieldsForError().contains(Route.PROPERTY_TURBIDITY));
            assertTrue(result.getFieldsForError().contains(Route.PROPERTY_SUBJECTIVE_CONDITIONS));
        }

        {
            Route route = new RouteImpl();
            route.setTurbidity(9);
            route.setSubjectiveConditions("EE");

            NuitonValidatorResult result = validator.validate(route);
            assertTrue(result.isValid());
        }
    }
}
