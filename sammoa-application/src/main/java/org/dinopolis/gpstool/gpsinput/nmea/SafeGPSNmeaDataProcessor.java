package org.dinopolis.gpstool.gpsinput.nmea;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.dinopolis.gpstool.gpsinput.GPSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created: 10/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class SafeGPSNmeaDataProcessor extends GPSNmeaDataProcessor {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SafeGPSNmeaDataProcessor.class);

    protected volatile boolean open_;

    // Override to avoid using daemon for read_thread
    @Override
    public void open() throws GPSException {

        if (gps_device_ == null) {
            throw new GPSException("no GPSDevice set!");
        }

        try {
            gps_device_.open();
            open_ = true;
            in_stream_ = gps_device_.getInputStream();
            // start this runnable as thread:
            read_thread_ = new Thread(this, "GPSNmeaDataProcessor");

            // >> START fix : remove daemon line from superclass implementation
//          read_thread_.setDaemon(true); // so thread is finished after exit of application
            // << END fix

            read_thread_.start();

            OutputStream outStream = gps_device_.getOutputStream();
            outStream.write(INIT_HOT_START.getBytes());
            outStream.write(13);
            outStream.write(10);

        } catch (IOException e) {
            throw new GPSException(e.getMessage());
        }
    }

    // Override to clean read_thread on close
    @Override
    public void close() throws GPSException {

        if (gps_device_ == null) {
            throw new GPSException("no GPSDevice set!");
        }

        open_ = false;
        if (read_thread_ != null) {
            try {
                read_thread_.join();
            } catch (InterruptedException ex) {
                throw new GPSException("Error on thread join", ex);
            }
            read_thread_ = null;
        }
        gps_device_.close();
    }

    // Override to allow stop loop with open flag
    @Override
    protected void readMessages()
    {
        if (log.isDebugEnabled()) {
            log.debug("start reading from GPSDevice...");
        }

        char[] buffer;
        int count;
        int data;
        NMEA0183Sentence message;
        try
        {

            if (log.isDebugEnabled()) {
                log.debug("inputstream: " + in_stream_);
            }

            if (!readGarbage()) // try to (re)sync with nmea stream
                return;

            int loopcount = 0;
            // >> START fix : add open_ flag to stop loop on close
            while (open_)
            // << END fix
            {
                loopcount++;
                count = 0;
                buffer = new char[MAX_NMEA_MESSAGE_LENGTH];

                // >> START fix : add open_ flag to stop loop on close
                while (open_ && (data = getNextByte()) != (char) 13) // read data until CR
                // << END fix
                {

                    if (count >= MAX_NMEA_MESSAGE_LENGTH - 1)
                    {
                        System.err.println("ERROR: max. message length exceeded! (" + count + "):" + new String(buffer));
                        if (!readGarbage()) // try to (re)sync with nmea stream
                            return;
                        loopcount++;
                        count = 0;
                        buffer = new char[MAX_NMEA_MESSAGE_LENGTH];
                    } else
                    {
                        if (data != (char) 10) // ignore LF
                        {
                            buffer[count] = (char) data; // add data to the buffer
                            count++;
                        }
                    }
                } // end of while (read until end of line)

                if (buffer[0] != '$') // no valid nmea sentence
                {
                    if (!readGarbage()) // try to (re)sync with nmea stream
                        return;
                } else
                { // valid sentence, no garbage
                    try
                    {
                        message = new NMEA0183Sentence(buffer, 0, count);

                        buffer[count] = 13; // add CR from NMEA message
                        buffer[count + 1] = 10; // add LF from NMEA message
                        fireRawDataReceived(buffer, 0, count + 2);
                        if (log.isDebugEnabled()) {
                            log.debug("message: '" + message + "'");
                            log.debug("sentenceId: '" + message.getSentenceId() + "'");
                        }

                        if (!message.isValid() && ignore_invalid_checksum_ && print_ignore_warning_) {
                            if (log.isErrorEnabled()) {
                                log.error("ERORR: invalid checksum in NMEA message: " + message);
                                log.error("checksum of sentence: " + message.getChecksum() + ", calculated checksum: " + message.getCalculatedChecksum());
                            }
                            if (log.isWarnEnabled()) {
                                log.warn("WARNING: As you chose to ingore invalid messages, this message is only printed once!");
                            }
                            print_ignore_warning_ = false;
                        }

                        if (ignore_invalid_checksum_ || message.isValid() || message.getSentenceId().equals("RFTXT")) {
                            try {
                                processNmeaSentence(message);
                            }
                            catch (Exception e) {
                                if (log.isErrorEnabled()) {
                                    log.error("ERROR: Exception thrown on processing of NMEA sentences:", e);
                                    log.error(message.toString());
                                }
                            }
                        } else {
                            if (log.isErrorEnabled()) {
                                log.error("ERORR: invalid checksum in NMEA message: " + message);
                                log.error("checksum of sentence: " + message.getChecksum() + ", calculated checksum: " + message.getCalculatedChecksum());
                            }
                        }
                    }
                    catch (Exception e) {
                        if (log.isErrorEnabled()) {
                            log.error("ERROR: Exception thrown on creation or processing of NMEA sentences:", e);
                            log.error(new String(buffer));
                        }
                    }
                }

                if (delay_time_ > 0)
                {
                    try
                    {
                        Thread.sleep(delay_time_);
                    }
                    catch (InterruptedException ie)
                    {
                    }
                }
            }
        }
        catch (IOException ioe)
        {
            if (open_) // otherwise, this is the reason for the exception!
                ioe.printStackTrace();
        }
    }

    // Copy from superclass (private access)
    protected int getNextByte() throws IOException
    {
        int data = -1;
        // rxtx 2.1.7 on windows has the problem (?) that even if the stream is not ended
        // it returns -1 here (happens in between nmea sentences)
        // so we just wait a little and then retry to read

        // >> START fix : add open_ flag to stop loop on close
        while (open_ && (data = in_stream_.read()) < 0)
        // << END fix
        {

            try
            {
                Thread.sleep(50);
            } catch (InterruptedException ignore)
            {
            }
        }
        return data;
    }

    // Override to add open_ flag in loops
    @Override
    protected boolean readGarbage()
    {
        int data;
        try
        {
            // >> START fix : add open_ flag to stop loops on close
            while (open_)
            {
                // read until CR/LF>
                while (open_ && (data = getNextByte()) != (char) 13) // read data until CR
                {
                }
            // << END fix

                data = getNextByte(); // char after CR
                if (data == (char) 10) // linefeed
                {
                    return true;
                }
                if (log.isDebugEnabled())
                    log.debug("reading garbage...");
            }
        }
        catch (IOException ioe)
        {
            System.err.println("GPS Nmea Reading: IOException on beginning of reading, try once more: " + ioe.getMessage());
            return false;
        }
        return false;
    }

    // Override to fix parse issue with empty String
    @Override
    protected void processGSA(NMEA0183Sentence sentence) {
        if (log.isDebugEnabled()) {
            log.debug("GSA detected: " + sentence);
        }
        List data_fields = sentence.getDataFields();
        Integer[] satellites_ids = new Integer[12];
        String pdop = (String) data_fields.get(14);
        String hdop = (String) data_fields.get(15);
        String vdop = (String) data_fields.get(16);

        for (int i = 0; i < 12; i++) {
            String id = (String) data_fields.get(i + 2);
            if (id != null && id.length() > 0) {
                satellites_ids[i] = Integer.parseInt(id);
            }
        }

        // >> START fix : resolve NumberFormatException on new Float with empty String
        changeGPSData(PDOP, pdop.isEmpty() ? 0 : new Float(pdop));

        changeGPSData(HDOP, hdop.isEmpty() ? 0 : new Float(hdop));

        changeGPSData(VDOP, vdop.isEmpty() ? 0 : new Float(vdop));
        // << END fix

        changeGPSData(IDS_SATELLITES, satellites_ids);
    }
}
