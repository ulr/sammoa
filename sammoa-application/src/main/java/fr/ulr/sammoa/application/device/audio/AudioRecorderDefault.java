/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device.audio;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created: 16/05/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class AudioRecorderDefault implements AudioRecorder {

    private static final Log log = LogFactory.getLog(AudioRecorderDefault.class);

    private static final AudioFormat.Encoding DEFAULT_ENCODING = AudioFormat.Encoding.PCM_SIGNED;

    private static final float DEFAULT_SAMPLE_RATE = 44100.0F;

    private static final int DEFAULT_SAMPLE_SIZE_IN_BITS = 16;

    private static final int DEFAULT_CHANNELS = 2;

    private static final int DEFAULT_FRAME_SIZE = 4;

    private static final float DEFAULT_FRAME_RATE = 44100.0F;

    private static final boolean DEFAULT_BIG_ENDIAN = false;

    private static final AudioFileFormat.Type DEFAULT_OUTPUT_TYPE = AudioFileFormat.Type.WAVE;

    protected AudioConfig config;

    protected AudioFormat audioFormat;

    protected AudioFileFormat.Type outputType;

    protected TargetDataLine dataLine;

    protected DataLineReader lineReader;

    private Recorder recorder1;

    private Recorder recorder2;

    protected Recorder currentRecorder;

//    // For debug purpose
//    private int count;

    protected DeviceState state;

    protected Set<DeviceStateListener> listeners;

    public AudioRecorderDefault(AudioConfig config) {
        this(new AudioFormat(
                DEFAULT_ENCODING,
                config.getSampleRate(),
                config.getSampleSizeInBits(),
                DEFAULT_CHANNELS,
                DEFAULT_FRAME_SIZE,
                DEFAULT_FRAME_RATE,
                DEFAULT_BIG_ENDIAN

        ), DEFAULT_OUTPUT_TYPE);
        this.config = config;
    }

    public AudioRecorderDefault(AudioFormat audioFormat,
                                AudioFileFormat.Type outputType) {

        this.audioFormat = audioFormat;
        this.outputType = outputType;
        this.listeners = Sets.newHashSet();
        this.state = DeviceState.UNAVAILABLE;
    }

    @Override
    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state, DeviceTechnicalException error) {
        DeviceState oldValue = getState();
        this.state = state;

        // Fire on listeners
        DeviceStateEvent event = new DeviceStateEvent(this, oldValue, state);
        event.setError(error);
        for (DeviceStateListener listener : listeners) {
            listener.stateChanged(event);
        }
    }

    @Override
    public AudioFileFormat.Type getOutputType() {
        return outputType;
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return listeners;
    }

    @Override
    public void open() throws DeviceTechnicalException {

        if (log.isInfoEnabled()) {
            log.info("Open the audio line");
        }

        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        try {
            dataLine = (TargetDataLine) AudioSystem.getLine(info);
            dataLine.open(audioFormat);

            // Start using the line for recording
            dataLine.start();

            setState(DeviceState.READY, null);

            if (log.isInfoEnabled()) {
                log.info("Audio line is ready");
            }

        } catch (IllegalArgumentException e) {
            setState(DeviceState.UNAVAILABLE, null);
            throw new DeviceTechnicalException(this, "Can't open audio device", e);

        } catch (IllegalStateException e) {
            setState(DeviceState.UNAVAILABLE, null);
            throw new DeviceTechnicalException(this, "Can't open audio device", e);

        } catch (SecurityException e) {
            setState(DeviceState.UNAVAILABLE, null);
            throw new DeviceTechnicalException(this, "Can't open audio device", e);

        } catch (LineUnavailableException e) {
            setState(DeviceState.UNAVAILABLE, null);
            throw new DeviceTechnicalException(this, "Can't open audio device", e);
        }
    }

    @Override
    public void start() {

        if (getState() == DeviceState.READY) {

            // Stop previous recording
            stop();

            if (log.isInfoEnabled()) {
                log.info("Start reading audio line");
            }

            lineReader = new DataLineReader();
            lineReader.start();
        }
    }

    @Override
    public void record(File outputFile) {

        if (DeviceState.UNAVAILABLE == state) {
            if (log.isWarnEnabled()) {
                log.warn("Can't record " + outputFile.getAbsolutePath() + ", no available dataLine");
            }

            // XXX-fdesbois-2012-08-03 : try to start the line again ?

            return;
        }

        Preconditions.checkArgument(outputFile != null);
        Preconditions.checkState(dataLine != null,
                                 "You must start the AudioRecorder to initialize " +
                                 "the audio line before call record method");

        try {
            // Start recorder1
            if (currentRecorder == null) {
                recorder1 = new Recorder(outputFile, this, audioFormat, lineReader);
                currentRecorder = recorder1;

                // Stop recorder1 if recording and start recorder2
            } else if (currentRecorder == recorder1) {

                if (recorder1.isRecording()) {
                    recorder1.stop(config.getRecordDelayInSeconds());
                }

                if (recorder2 != null && recorder2.isRecording()) {
                    recorder2.stop();
                }
                recorder2 = new Recorder(outputFile, this, audioFormat, lineReader);
                currentRecorder = recorder2;

                // Stop recorder2 if recording and start recorder1
            } else if (currentRecorder == recorder2) {

                if (recorder2.isRecording()) {
                    recorder2.stop(config.getRecordDelayInSeconds());
                }

                if (recorder1.isRecording()) {
                    recorder1.stop();
                }
                recorder1 = new Recorder(outputFile, this, audioFormat, lineReader);
                currentRecorder = recorder1;
            }

        } catch (IOException e) {
            setState(DeviceState.ERROR,
                     new DeviceTechnicalException(this,
                                                  "Error on saving file "
                                                  + outputFile.getAbsolutePath(), e)
            );
        }
    }

    @Override
    public void stopRecord() {
        if (currentRecorder != null) {
            currentRecorder.stop(config.getRecordDelayInSeconds());
        }
    }

    @Override
    public void stop() {

        if (getState() != DeviceState.UNAVAILABLE) {
            if (log.isInfoEnabled()) {
                log.info("Stop audio line reading");
            }
            // Destroy the thread
            if (lineReader != null) {
                lineReader.interrupt();
            }
            lineReader = null;
        }
    }

    @Override
    public void close() throws DeviceTechnicalException {

        stop();

        if (dataLine != null/* && dataLine.isRunning()*/) {

            if (log.isInfoEnabled()) {
                log.info("Close the audio line");
            }

            dataLine.stop();
            dataLine.drain();
            dataLine.close();
            dataLine = null;

            setState(DeviceState.UNAVAILABLE, null);
        }
    }

    protected class DataLineReader extends Thread {

        private List<Recorder> recorders = Collections.synchronizedList(Lists.<Recorder>newArrayList());

        public void addRecorder(Recorder recorder) {
            recorders.add(recorder);
        }

        public void removeRecorder(Recorder recorder) {
            recorders.remove(recorder);
        }

        @Override
        public void run() {

            int frameSize = audioFormat.getFrameSize();
            int bufferLengthInFrames = dataLine.getBufferSize() / 8;
            int bufferSize = bufferLengthInFrames * frameSize;
            byte[] buffer = new byte[bufferSize];

            // Start reading the line
            int numBytesRead;
            while (lineReader != null) {

                if (!recorders.isEmpty()) {

                    setState(DeviceState.RUNNING, null);

                    numBytesRead = dataLine.read(buffer, 0, bufferSize);
                    synchronized (recorders) {
                        for (Recorder recorder : recorders) {
                            recorder.write(buffer, 0, numBytesRead);
                        }
                    }
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("Stop all recorders (" + recorders.size() + ")");
            }

            List<Recorder> list;
            synchronized (recorders) {
                list = Lists.newArrayList(recorders);
            }
            for (Recorder recorder : list) {
                recorder.stop();
            }
        }
    }

    protected static class Recorder {

        protected AudioRecorderDefault audioRecorder;

        protected AudioFormat audioFormat;

        protected ByteArrayOutputStream bufferStream;

        protected String outputFilePath;

        protected OutputStream outputStream;

        protected DataLineReader lineReader;

        protected boolean stop;

        protected boolean stopScheduled;

        protected TimerTask saveTask = new SaveTask();

        protected TimerTask stopRecording = new StopTask();

        protected class SaveTask extends TimerTask {

            @Override
            public void run() {
                save();
            }
        }

        protected class StopTask extends TimerTask {

            @Override
            public void run() {
                doStop();
            }
        }

        public Recorder(File outputFile,
                        AudioRecorderDefault audioRecorder,
                        AudioFormat audioFormat,
                        DataLineReader lineReader) throws IOException {

            this.outputFilePath = outputFile.getAbsolutePath();
            boolean newFile = outputFile.createNewFile();
            if (log.isInfoEnabled()) {
                if (newFile) {
                    log.info("Record a new audio file '" + outputFilePath + "'");
                } else {
                    log.info("Use existing file '" + outputFilePath + "' to continue recording");
                }
            }
            this.outputStream = new BufferedOutputStream(new FileOutputStream(outputFile, true));
            this.bufferStream = new ByteArrayOutputStream();
            this.audioFormat = audioFormat;
            this.audioRecorder = audioRecorder;
            this.lineReader = lineReader;
            this.lineReader.addRecorder(this);

            // Save the audio file every 3 seconds
            new Timer().schedule(saveTask, 0, 3000);
        }

        public boolean isRecording() {
            return !stop;
        }

        public void write(byte[] buffer, int offset, int length) {

            if (stop) {
                if (log.isDebugEnabled()) {
                    log.debug("File is already saved (" + outputFilePath + ")");
                }
                return;
            }
            synchronized (bufferStream) {
                bufferStream.write(buffer, offset, length);
            }
        }

        public void stop(long delay) {

            if (stop) {
                if (log.isDebugEnabled()) {
                    log.debug("File is already saved (" + outputFilePath + ")");
                }
                return;
            }

            if (stopScheduled) {
                if (log.isDebugEnabled()) {
                    log.debug("Stop is already scheduled (" + outputFilePath + ")");
                }
                return;
            }
            long delayMilliseconds = delay * 1000;
            if (log.isDebugEnabled()) {
                log.debug("Call stop recording (" + outputFilePath + ") with delay = " + delayMilliseconds + " ms");
            }
            new Timer().schedule(stopRecording, delayMilliseconds);
            stopScheduled = true;
        }

        public void stop() {

            if (stop) {
                if (log.isDebugEnabled()) {
                    log.debug("File is already saved (" + outputFilePath + ")");
                }
                return;
            }

            stopRecording.cancel();
            doStop();
        }

        protected void doStop() {

            if (log.isDebugEnabled()) {
                log.debug("Stop recording (" + outputFilePath + ")");
            }
            lineReader.removeRecorder(this);
            saveTask.cancel();
            save();
            stop = true;
            try {
                bufferStream.flush();

            } catch (IOException e) {
                throw new SammoaTechnicalException(e);

            } finally {
                try {
                    bufferStream.close();
                } catch (IOException e) {
                    throw new SammoaTechnicalException(e);
                }
            }
        }

        protected void save() {

            // reset the buffer stream
            byte[] bytes;
            synchronized (bufferStream) {
                bytes = bufferStream.toByteArray();
                bufferStream.reset();
            }

            // prepare AudioStream
            AudioInputStream audioStream = new AudioInputStream(
                    new ByteArrayInputStream(bytes),
                    audioFormat,
                    bytes.length / audioFormat.getFrameSize()
            );

            try {
                if (log.isDebugEnabled()) {
                    log.debug("Save in file " + outputFilePath);
                }

                AudioSystem.write(audioStream, audioRecorder.getOutputType(), outputStream);

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't record file " + outputFilePath, e);
                }
                audioRecorder.setState(DeviceState.ERROR,
                                       new DeviceTechnicalException(audioRecorder,
                                                                    "Error on saving file " + outputFilePath, e));

            } finally {
                try {
                    audioStream.close();
                } catch (IOException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't close file " + outputFilePath, e);
                    }
                }
            }
        }
    }

}
