package fr.ulr.sammoa.application.io.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.nuiton.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created: 08/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public final class DbfTimestampConverter {

    protected static final Long HOUR_MULT = 3600000L;

    protected static final Long MINUTE_MULT = 60000L;

    protected static final Long SECOND_MULT = 1000L;


    private static final Log log = LogFactory.getLog(DbfTimestampConverter.class);

    private DbfTimestampConverter() {
        // static class
    }

    public static String toString(Date date) {

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        Calendar refCalendar = getReferenceCalendar();

        long dateValue = DateUtil.getDifferenceInDays(refCalendar.getTime(), date);
        long timeValue = calendar.get(Calendar.HOUR_OF_DAY) * HOUR_MULT
                         + calendar.get(Calendar.MINUTE) * MINUTE_MULT
                         + calendar.get(Calendar.SECOND) * SECOND_MULT;

        String result = dateValue + " " + timeValue;
        return result;
    }

    public static Date toDate(String timestamp) {
        Preconditions.checkArgument(Strings.nullToEmpty(timestamp).trim().contains(" "),
                                    "A space is missing between the date and time values");

        String[] values = timestamp.split(" ");

        Long dateValue = Long.parseLong(values[0]);
        Long timeValue = Long.parseLong(values[1]);

        Calendar refCalendar = getReferenceCalendar();

        Calendar resultCalendar = new GregorianCalendar();
        resultCalendar.setTime(refCalendar.getTime());

        resultCalendar.add(Calendar.DATE, dateValue.intValue());

        Long hours = timeValue / HOUR_MULT;
        Long timeWithoutHours = timeValue - hours * HOUR_MULT;
        Long minutes = timeWithoutHours / MINUTE_MULT;
        Long seconds = (timeWithoutHours - minutes * MINUTE_MULT) / SECOND_MULT;

        resultCalendar.set(Calendar.HOUR_OF_DAY, hours.intValue());
        resultCalendar.set(Calendar.MINUTE, minutes.intValue());
        resultCalendar.set(Calendar.SECOND, seconds.intValue());

        if (log.isDebugEnabled()) {
            log.debug(String.format("DateValue=%d, TimeValue=%d, Hours=%d, Minutes=%d, Seconds=%d, Result=%s",
                                       dateValue,
                                       timeValue,
                                       hours,
                                       minutes,
                                       seconds,
                                       resultCalendar)
            );
        }

        return resultCalendar.getTime();
    }

    private static Calendar getReferenceCalendar() {
        Calendar result = new GregorianCalendar(4713, Calendar.JANUARY, 1);
        result.set(Calendar.ERA, GregorianCalendar.BC);
        return result;
    }

}
