package fr.ulr.sammoa.application.io.input.application.strategy;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.input.application.SammoaImportModelFactory;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaPersistenceHelper;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportToMap;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.CsvImportResult;
import org.nuiton.topia.service.csv.in.ImportStrategy;

/**
 * Base import stragey for sammo application import.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
abstract class AbstractImportStrategy implements ImportStrategy<SammoaEntityEnum> {

    private final SammoaTopiaPersistenceContext tx;

    private final SammoaPersistenceHelper persistenceHelper;

    private final SammoaImportModelFactory modelFactory;

    AbstractImportStrategy(SammoaImportModelFactory modelFactory,
                           SammoaTopiaPersistenceContext tx,
                           SammoaPersistenceHelper persistenceHelper) {
        this.tx = tx;
        this.persistenceHelper = persistenceHelper;
        this.modelFactory = modelFactory;
    }

    @Override
    public SammoaImportModelFactory getModelFactory() {
        return modelFactory;
    }

    @Override
    public <E extends TopiaEntity> void importTable(TableMeta<SammoaEntityEnum> meta, Import<E> importer,
                                                    CsvImportResult<SammoaEntityEnum> csvResult) throws TopiaException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void importAssociation(AssociationMeta<SammoaEntityEnum> meta,
                                  ImportToMap importer,
                                  CsvImportResult<SammoaEntityEnum> csvResult) throws TopiaException {
        throw new UnsupportedOperationException();
    }

    @Override
    public <E extends TopiaEntity> Iterable<E> importTableAndReturnThem(TableMeta<SammoaEntityEnum> meta,
                                                                        Import<E> importer,
                                                                        CsvImportResult<SammoaEntityEnum> csvResult) throws TopiaException {
        throw new UnsupportedOperationException();
    }

    protected SammoaTopiaPersistenceContext getTx() {
        return tx;
    }

    protected <E extends TopiaEntity> TopiaDao<? extends TopiaEntity> getDAO(SammoaEntityEnum type) {
        return tx.getDao(type.getContract());
    }

    protected void flushTransaction() throws TopiaException {
        tx.flush();
    }

}
