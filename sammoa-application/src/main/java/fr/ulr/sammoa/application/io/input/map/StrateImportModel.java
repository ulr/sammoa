package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.SubRegionImpl;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StrateImpl;
import org.nuiton.csv.ext.AbstractImportModel;

/**
 *  <pre>
 *  - colonne 0 : "Nom" : Nom de la strate (strate.name) C,50
 *  - colonne 1 : "type_strat" : Type de la strate (strate.strateType) N,4,0
 *
 *      * C : COAST (Côte)
 *      * N : NERITIC (Néritique)
 *      * P : SLOPE (Pente)
 *      * O : OCEANIC (Océanique)
 *
 *  - colonne 2 : "Secteur" : Nom du secteur (strate.subRegion.name) C,12
 *  - colonne 3 : "ID_Secteur" : Numéro du secteur (strate.subRegion.subRegionNumber) N,4,0
 *  - colonne 4 : "Area" : Surface de la strate (non utilisé pour le moment) N,19,11
 *  - colonne 5 : "Shape_Leng" : Longueur sur le fichier shape (non utilisé pour le moment) N,19,11
 *  - colonne 6 : "Shape_Area" : Surface sur le fichier shape (non utilisé pour le moment) N,19,11
 *  </pre>
 *
 * Created: 25/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class StrateImportModel extends AbstractImportModel<Strate> {



    public StrateImportModel() {
        super(';');
        newMandatoryColumn("strate", Strate.PROPERTY_CODE);
        newMandatoryColumn("subRegion",Strate.PROPERTY_SUB_REGION + "." + SubRegion.PROPERTY_NAME);
        newMandatoryColumn("name", Strate.PROPERTY_NAME);
        newMandatoryColumn("strateType", Strate.PROPERTY_TYPE);
        newIgnoredColumn("area");
        newIgnoredColumn("shapeLeng");
        newIgnoredColumn("shapeArea");
    }

    @Override
    public Strate newEmptyInstance() {
        Strate result = new StrateImpl();
        result.setSubRegion(new SubRegionImpl());
        return result;
    }
}
