package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.Flight;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.function.Function;

import static org.nuiton.i18n.I18n.t;

public class V1105AddTimeZoneOnFlight extends AbstractMigration {

    private static final Log log = LogFactory.getLog(V1105AddTimeZoneOnFlight.class);

    private List<CsvMigrate.Line> firstRoutes;

    public V1105AddTimeZoneOnFlight() {
        super("1.1.0.5", "Add time zone on flight");
    }

    protected String[] timeZoneIds = TimeZone.getAvailableIDs();

    @Override
    public void migrateLocalDirectory() throws IOException {

    }

    protected Function<String, String> transformerDate(TimeZone timeZone) {

        SimpleDateFormat parser = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSS");
        parser.setTimeZone(timeZone);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSSZ");

        return dateStr -> {
            String date2Str = dateStr;
            if (StringUtils.isNotBlank(dateStr)) {

                try {
                    Date date = parser.parse(dateStr);
                    date2Str = formatter.format(date);
                } catch (ParseException e) {
                    throw new SammoaTechnicalException("unable parse date "+ dateStr, e);
                }
            }

            return date2Str;
        };
    }

    protected void migrateSurvey(File surveyDirectory) throws IOException {

        Function<String, String> transformerDate = transformerDate(TimeZone.getDefault());

        loadCvs(surveyDirectory.toPath().resolve("csv/Survey.csv"))
                .updateValue("beginDate", line -> transformerDate.apply(line.get("beginDate")))
                .updateValue("endDate", line -> transformerDate.apply(line.get("endDate")))
                .save();


        File flightsDir = SammoaDirectories.getFlightDirectory(surveyDirectory);
        File[] flightDirectories = flightsDir.listFiles((file, name) ->
                name.startsWith(Flight.class.getCanonicalName())
        );
        if (flightDirectories != null) {
            for (File flightDirectory : flightDirectories) {
                migrateFlight(flightDirectory);
            }
        }
    }

    protected void migrateFlight(File flightDirectory) throws IOException {

        Properties properties = new Properties();
        Path propertiesPath = flightDirectory.toPath().resolve("flight.properties");
        try (BufferedReader reader = Files.newBufferedReader(propertiesPath)) {
            properties.load(reader);
        }

        String survey = properties.getProperty("survey.code");
        String flightName = properties.getProperty("name");
        String timeZoneId = ask(t("sammoa.migration.1105.ask.timeZone.title"),
                t("sammoa.migration.1105.ask.timeZone.question", survey, flightName),
                timeZoneIds,
                TimeZone.getDefault().getID());

        Function<String, String> transformerDate = transformerDate(TimeZone.getTimeZone(timeZoneId));

        File csvDir = SammoaDirectories.getCsvDirectory(flightDirectory);
        loadCvs(csvDir.toPath().resolve("Flight.csv"))
                .addColumn("timeZone", timeZoneId)
                .updateValue("beginDate", line -> transformerDate.apply(line.get("beginDate")))
                .updateValue("endDate", line -> transformerDate.apply(line.get("endDate")))
                .save();

        loadCvs(csvDir.toPath().resolve("Route.csv"))
                .updateValue("beginTime", line -> transformerDate.apply(line.get("beginTime")))
                .save();

        loadCvs(csvDir.toPath().resolve("Observation.csv"))
                .updateValue("observationTime", line -> transformerDate.apply(line.get("observationTime")))
                .save();

        loadCvs(csvDir.toPath().resolve("GeoPoint.csv"))
                .updateValue("recordTime", line -> transformerDate.apply(line.get("recordTime")))
                .save();

    }

    @Override
    public File migrateImportFile() throws IOException {
        migrateSurvey(dataDirectory);
        return dataDirectory;
    }
}
