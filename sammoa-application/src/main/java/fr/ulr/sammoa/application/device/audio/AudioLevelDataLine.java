package fr.ulr.sammoa.application.device.audio;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.Control;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 * @author Sylvain Bavencoff <bavencoff@codelutin.com>
 */
public class AudioLevelDataLine implements TargetDataLine {

    protected static final float ATTENUATION = 70;

    protected TargetDataLine line;

    protected int frameSize;

    protected boolean bigEndian;

    protected int cursor;

    byte[] audioData;

    public AudioLevelDataLine(TargetDataLine line, AudioFormat format)  {

        this.line = line;

        frameSize = format.getFrameSize();
        bigEndian = format.isBigEndian();

        int size = (int) format.getFrameRate() / 20;
        audioData = new byte[size];
        cursor = 0;

    }

    public float getLevel() {
        long lSum = 0;
        float level = 0;

        if (audioData.length > 0) {

            for(byte sample : audioData) {
                lSum = lSum + sample;
            }

            double dAvg = lSum / audioData.length;

            double sumMeanSquare = 0d;

            for (byte sample : audioData) {
                sumMeanSquare = sumMeanSquare + Math.pow(sample - dAvg, 2d);
            }

            double averageMeanSquare = sumMeanSquare / audioData.length;

            level = (float) Math.pow(averageMeanSquare, 0.5d) / ATTENUATION;
        }

        return level;
    }

    public void forceRead() {
        int available = line.available();
        byte[] bytes = new byte[available];
        read(bytes, 0, available);
    }

    @Override
    public int read(byte[] b, int off, int len) {
        int read = line.read(b, off, len);
        for (int index = 0; index < len; index++) {
            int pow = (off + index) % frameSize;
            if (!bigEndian && pow == frameSize -1 || bigEndian && pow == 0) {
                audioData[cursor] = b[off + index];
                cursor = (cursor + 1) % audioData.length;
            }
        }
        return read;
    }

    @Override
    public Line.Info getLineInfo() {
        return line.getLineInfo();
    }

    @Override
    public void open() throws LineUnavailableException {
        line.open();
    }

    @Override
    public void close() {
        line.close();
    }

    @Override
    public boolean isOpen() {
        return line.isOpen();
    }

    @Override
    public Control[] getControls() {
        return line.getControls();
    }

    @Override
    public boolean isControlSupported(Control.Type control) {
        return line.isControlSupported(control);
    }

    @Override
    public Control getControl(Control.Type control) {
        return line.getControl(control);
    }

    @Override
    public void addLineListener(LineListener listener) {
        line.addLineListener(listener);
    }

    @Override
    public void removeLineListener(LineListener listener) {
        line.removeLineListener(listener);
    }

    @Override
    public void open(AudioFormat format, int bufferSize) throws LineUnavailableException {
        line.open(format, bufferSize);
    }

    @Override
    public void open(AudioFormat format) throws LineUnavailableException {
        line.open(format);
    }

    @Override
    public void drain() {
        line.drain();
    }

    @Override
    public void flush() {
        line.flush();
    }

    @Override
    public void start() {
        line.start();
    }

    @Override
    public void stop() {
        line.stop();
    }

    @Override
    public boolean isRunning() {
        return line.isRunning();
    }

    @Override
    public boolean isActive() {
        return line.isActive();
    }

    @Override
    public AudioFormat getFormat() {
        return line.getFormat();
    }

    @Override
    public int getBufferSize() {
        return line.getBufferSize();
    }

    @Override
    public int available() {
        return line.available();
    }

    @Override
    public int getFramePosition() {
        return line.getFramePosition();
    }

    @Override
    public long getLongFramePosition() {
        return line.getLongFramePosition();
    }

    @Override
    public long getMicrosecondPosition() {
        return line.getMicrosecondPosition();
    }
}
