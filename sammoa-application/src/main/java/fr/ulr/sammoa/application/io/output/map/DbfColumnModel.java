package fr.ulr.sammoa.application.io.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Note: there is an existing {@link com.bbn.openmap.dataAccess.shape.DbfTableModelFactory.Column}
 * class.
 * <p/>
 * Created: 07/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DbfColumnModel {

    protected String name;

    protected byte type;

    protected int length;

    protected byte decimalCount;

    protected int index;

    public DbfColumnModel(String name, byte type) {
        this.name = name;
        this.type = type;
        this.index = -1;
    }

    public String getName() {
        return name;
    }

    public byte getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getDecimalCount() {
        return decimalCount;
    }

    public void setDecimalCount(int decimalCount) {
        this.decimalCount = (byte) decimalCount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
