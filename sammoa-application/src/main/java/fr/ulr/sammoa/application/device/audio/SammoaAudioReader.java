package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.persistence.Dates;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe permettant la lecture d'un fichier audio, et de ce deplace a une
 * position souhaite dans ce fichier.
 *
 * Created: 04 septembre 2012
 *
 * @author Benjamin POUSSIN <poussin@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SammoaAudioReader implements AudioReader {

    private static final Log log = LogFactory.getLog(SammoaAudioReader.class);

    protected File fileToRead;

    protected Clip clip;               // Contents of a sampled audio file
    protected AudioPositionTraker audioPositionTraker;

    // Length and position of the sound are measured in milliseconds for
    // sampled sounds
    protected long audioLength;         // Length of the sound.
    protected long audioPosition = 0;   // Current position within the sound

    protected Date startDate;

    protected DeviceState state = DeviceState.UNAVAILABLE;

    protected Set<DeviceStateListener> listeners = new HashSet<DeviceStateListener>();
    protected Set<AudioPositionListener> audioPositionListener = new HashSet<AudioPositionListener>();

    /**
     * Classe servant a surveiller l'avancement de la lecture
     * et a prevenir les listeners
     */
    static protected class AudioPositionTraker {
        static final protected long STEP = 100; // toutes les 100 ms
        
        protected Timer timer;
        protected TimerTask task;
        protected SammoaAudioReader reader;

        public AudioPositionTraker(SammoaAudioReader reader) {
            timer = new Timer("SammoaAudioReader");
            this.reader = reader;
        }

        /**
         * Demarre la surveillance
         */
        public void start() {
            task = new TimerTask() {

                @Override
                public void run() {
                    if (reader.clip.isActive()) {
                        // on appele pas setPosition car, sinon on modifie la position dans le clips :(
                        // alors que c'est lui qu'on surveille et qui fait le changement
                        long old = reader.audioPosition;
                        long position = reader.getClip().getMicrosecondPosition()/1000L;
                        if (old != position) {
                            reader.audioPosition = position;
                            reader.firePositionChanged(position);
                        }
                    }
                }
            };
            timer.schedule(task , 0, STEP);
        }

        /**
         * Arrete la surveillance
         */
        public void stop() {
            if (task != null) {
                task.cancel();
                task = null;
            }
        }

    }
    
    public SammoaAudioReader() {
        audioPositionTraker = new AudioPositionTraker(this);
    }

    public Clip getClip() {
        return clip;
    }

    @Override
    public File getFileToRead() {
        return fileToRead;
    }

    @Override
    public AudioFileFormat.Type getOutputType() {
        return AudioFileFormat.Type.WAVE;
    }

    @Override
    public void load(File file, Date recordingDate) {
        Preconditions.checkNotNull(file);
        Preconditions.checkArgument(file.exists());
        unload();
        fileToRead = file;
        try {
            // Getting a Clip object for a file of sampled audio data is kind
            // of cumbersome.  The following lines do what we need.
            AudioInputStream ain = AudioSystem.getAudioInputStream(file);

            // Get information about the format of the stream
            AudioFormat format = ain.getFormat( );
            DataLine.Info info = new DataLine.Info(Clip.class, format);

            if (!AudioSystem.isLineSupported(info)) {
                // This is the PCM format we want to transcode to.
                // The parameters here are audio format details that you
                // shouldn't need to understand for casual use.
                AudioFormat pcm =
                        new AudioFormat(format.getSampleRate( ), 16,
                        format.getChannels( ), true, false);

                // Get a wrapper stream around the input stream that does the
                // transcoding for us.
                ain = AudioSystem.getAudioInputStream(pcm, ain);

                // Update the format and info variables for the transcoded data
                format = ain.getFormat( );
                info = new DataLine.Info(Clip.class, format);
            }

            try {
                clip = (Clip) AudioSystem.getLine(info);
                clip.addLineListener(new LineListener() {

                    @Override
                    public void update(LineEvent event) {
                        if (log.isDebugEnabled()) {
                            log.debug("Clip " + event.getType());
                        }
                        if (LineEvent.Type.START.equals(event.getType())) {
                            onClipStart();
                        }
                    }
                });
                clip.open(ain);
            }
            finally { // We're done with the input stream.
                ain.close( );
            }
            // Get the clip length in microseconds and convert to milliseconds
            audioLength = (int)(clip.getMicrosecondLength( )/1000);
            startDate = recordingDate;

            setState(DeviceState.READY);
            if (log.isDebugEnabled()) {
                log.debug(String.format("Sound file '%s' loaded", file));
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(String.format("Can't load audio file '%s'", file), eee);
            }
            setState(DeviceState.ERROR);
        }

    }

    @Override
    public void unload() {
        if (clip != null) {
            stop();
            clip.close();
            clip = null;
        }
        startDate = null;
        audioLength = 0;
        audioPosition = 0;
        fileToRead = null;
        setState(DeviceState.UNAVAILABLE);
    }

    @Override
    public long getLength() {
        return audioLength;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setPosition(long position) {
        if (clip != null) {
            long old = audioPosition;
            this.audioPosition = position;
            if (old != position) {
                clip.setMicrosecondPosition(position * 1000L);
                firePositionChanged(position);
            }
        }
    }

    protected void firePositionChanged(long position) {
        for (AudioPositionListener l : audioPositionListener) {
            l.positionChanged(this, position);
        }
    }

    @Override
    public long getPosition() {
        return audioPosition;
    }

    @Override
    public void setPositionDate(Date date) {
        Preconditions.checkNotNull(date);

        if (startDate != null) {
            long duration = Dates
                    .toInterval(startDate, date)
                    .toDuration()
                    .getMillis();

            setPosition(duration);

        } else {
            if (log.isWarnEnabled()) {
                log.warn("StartDate is not defined, you can't set the " +
                            "position date " + date);
            }
        }
    }

    @Override
    public Date getPositionDate() {
        Date result;
        if (startDate == null) {
            result = null;

        } else {
            result = Dates
                    .toDateTime(startDate)
                    .plus(audioPosition)
                    .toDate();
        }
        return result;
    }

    @Override
    public void open() throws DeviceTechnicalException {
        // no state set here, only open device and throw error
    }

    @Override
    public void start() {
        if (clip != null) {
            clip.start();
        }
    }

    /**
     * There is some delay on asynchronous clip.start(). So we must wait for
     * clip start event fired to launch audioPositionTraker.
     *
     * @see LineListener
     */
    protected void onClipStart() {
        audioPositionTraker.start(); // all time start tracker after clip
        if (log.isDebugEnabled()) {
            log.debug("Sound playing");
        }
        if (state == DeviceState.READY) {
            setState(DeviceState.RUNNING);
        }
    }

    @Override
    public void stop() {
        if (log.isDebugEnabled()) {
            log.debug("Stop playing");
        }
        audioPositionTraker.stop();
        if (clip != null) {
            clip.stop( );
        }

        if (state == DeviceState.RUNNING) {
            setState(DeviceState.READY);
        }
    }

    @Override
    public void close() throws DeviceTechnicalException {
        unload();
    }

    @Override
    public DeviceState getState() {
        return state;
    }

    protected void setState(DeviceState state) {
        DeviceState oldValue = getState();
        this.state = state;

        DeviceStateEvent event = new DeviceStateEvent(this, oldValue, state);
        for (DeviceStateListener listener : listeners) {
            listener.stateChanged(event);
        }
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return listeners;
    }

    @Override
    public void addAudioPositionListener(AudioPositionListener listener) {
        audioPositionListener.add(listener);
    }

    @Override
    public void removeAudioPositionListener(AudioPositionListener listener) {
        audioPositionListener.remove(listener);
    }

    @Override
    public Set<AudioPositionListener> getAudioPositionListeners() {
        return audioPositionListener;
    }
}
