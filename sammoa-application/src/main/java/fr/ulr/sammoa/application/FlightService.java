/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.io.SammoaStorages;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.persistence.AutoSaveListener;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.FlightTopiaDao;
import fr.ulr.sammoa.persistence.Flights;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPointTopiaDao;
import fr.ulr.sammoa.persistence.GeoPoints;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationStatus;
import fr.ulr.sammoa.persistence.ObservationTopiaDao;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.ObserverPositionTopiaDao;
import fr.ulr.sammoa.persistence.ObserverPositions;
import fr.ulr.sammoa.persistence.Observers;
import fr.ulr.sammoa.persistence.PlatformType;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteTopiaDao;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectFlightTopiaDao;
import fr.ulr.sammoa.persistence.TransectFlights;
import fr.ulr.sammoa.persistence.TransectTopiaDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Seconds;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

/**
 * Created: 08/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class FlightService extends SammoaServiceSupport {

    private static final Log log =
            LogFactory.getLog(FlightService.class);

    private static final TimeLog timeLog =
            new TimeLog(FlightService.class, 500, 1000);

    @Override
    public void setSammoaContext(SammoaContext context) {
        super.setSammoaContext(context);
    }

    public List<Flight> getFlights(Survey survey) {

        Preconditions.checkNotNull(survey);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            List<Flight> result = getFlights(transaction, survey);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public List<Flight> getFlights(SammoaTopiaPersistenceContext transaction, Survey survey) {

        Preconditions.checkNotNull(survey);

        long start = TimeLog.getTime();

        FlightTopiaDao dao = transaction.getFlightDao();

        List<Flight> result = dao.forSurveyEquals(survey).findAll();

        Collections.sort(result, Flights.orderByDate());

        timeLog.log(start, "getFlights(" + survey.getCode() + ")");
        return result;
}

    public Iterable<Flight> getFlights(Survey survey,
                                       Date beginDate,
                                       Date endDate) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            Iterable<Flight> result = getFlights(transaction,
                                                 survey,
                                                 beginDate,
                                                 endDate);
            return result;
        } finally {
            endTransaction(transaction);
        }

    }

    public Iterable<Flight> getFlights(SammoaTopiaPersistenceContext transaction,
                                       Survey survey,
                                       Date beginDate,
                                       Date endDate) {

        Preconditions.checkNotNull(survey);
        Preconditions.checkNotNull(beginDate);
        Preconditions.checkNotNull(endDate);

        long start = TimeLog.getTime();

        List<Flight> surveyFlights;

        FlightTopiaDao dao = transaction.getFlightDao();

        surveyFlights = dao.forSurveyEquals(survey).findAll();
        Collections.sort(surveyFlights, Flights.orderByDate());

        // Filter on period
        final Interval interval = Dates.toInterval(beginDate, endDate);
        Iterable<Flight> result = Iterables.filter(surveyFlights, new Predicate<Flight>() {

            @Override
            public boolean apply(Flight input) {
                return Dates.inInterval(input.getBeginDate(), interval);
            }
        });

        timeLog.log(start, "getFlights(" + survey.getCode() + ")");

        return result;
    }

    public Flight getFlight(String flightId) {

        Preconditions.checkNotNull(flightId);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            Flight result = getFlight(transaction, flightId);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public Flight getFlight(SammoaTopiaPersistenceContext transaction, String flightId) {

        Preconditions.checkNotNull(flightId);
    
        long start = TimeLog.getTime();
        FlightTopiaDao dao = transaction.getFlightDao();

        Flight result = dao.forTopiaIdEquals(flightId).findUniqueOrNull();
        Preconditions.checkNotNull(
                result, String.format("%s doesn't exist", flightId));

        if (log.isInfoEnabled()) {
            log.info("Loading flight " + result.getFlightNumber() + " [" + result.getTopiaId() + "]");
        }

        result.sizeObserver();
        result.sizeTransectFlight();

        timeLog.log(start, "getFlight(" + flightId + ")");

        return result;
    }

    /**
     * Create a new flight using the {@code flightNumber}. The flight number
     * could be calculated using {@link #getNextFlightNumber()}.
     *
     * @param flightNumber the number of the new flight
     * @param survey The survey of the flight
     * @return a new flight
     * @see #getNextFlightNumber()
     * @since 0.4
     */
    public Flight createFlight(Survey survey, int flightNumber) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            FlightTopiaDao dao = tx.getFlightDao();

            int nextFlightNumber = getNextFlightNumber(tx);
            Preconditions.checkArgument(flightNumber >= nextFlightNumber);

            String systemId = config.getSystemId();

            Flight result = dao.createByNaturalId(systemId,
                                                  flightNumber,
                                                  survey);
            result.setPlatformType(PlatformType.SIMPLE);
            result.setTimeZone(TimeZone.getDefault());

            if (log.isInfoEnabled()) {
                log.info("Create new flight " + flightNumber + " for survey " + survey.getCode());
            }

            tx.commit();

            // Update flightNumber in config
            config.setFlightNumber(flightNumber);
            config.save();

            // create flight storage
            SurveyStorage surveyStorage =
                    getSurveyStorage(survey.getTopiaId());

            SammoaStorages.createFlightStorage(surveyStorage,
                                               result.getTopiaId());

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(tx);
        }
    }

    /**
     * Retrieve the next flight number to use. It is the maximum value available
     * between the database maximum flightNumber and the configuration one.
     *
     * @return the next available flight number
     */
    public int getNextFlightNumber() {


        SammoaTopiaPersistenceContext tx = persistence.beginTransaction();
        try {
            int result = getNextFlightNumber(tx);
            return result;

        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(tx);
        }
    }

    public void deleteTransectFlight(Flight flight,
                                     TransectFlight transectFlight) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            deleteTransectFlight(tx, flight, transectFlight);

            tx.commit();

        } catch (TopiaException ex) {
            throw new TopiaException(ex);
        } finally {
            endTransaction(tx);
        }
    }

    protected void deleteTransectFlight(SammoaTopiaPersistenceContext tx,
                                        Flight flight,
                                        TransectFlight transectFlight) {

        ObserverPositionTopiaDao observerPositionDao = tx.getObserverPositionDao();

        observerPositionDao.deleteAll(transectFlight.getObserverPosition());

        flight.removeTransectFlight(transectFlight);

        tx.getTransectFlightDao().delete(transectFlight);

        tx.getFlightDao().update(flight);
    }

    /**
     * Remove the given flight from db and storage.
     *
     * @param flightId the flight id to remove
     * @since 0.6
     */
    public void deleteFlight(String surveyId, String flightId) {
        Preconditions.checkArgument(StringUtils.isNotBlank(surveyId));
        Preconditions.checkArgument(StringUtils.isNotBlank(flightId));

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            // send delete queries to db
            deleteFlightInDb(tx, flightId);

            // commit db
            tx.commit();

        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }

        // delete flight storage
        SurveyStorage surveyStorage = getSurveyStorage(surveyId);

        surveyStorage.deleteFlightStorage(flightId);
    }

    /**
     * Remove the given flight from db, but do not commit anything
     *
     * @param tx       current transaction to use
     * @param flightId the flight id to remove
     * @since 0.6
     */
    public void deleteFlightInDb(SammoaTopiaPersistenceContext tx, String flightId) {

        if (log.isInfoEnabled()) {
            log.info("Delete flight " +  flightId);
        }
        try {
            FlightTopiaDao dao = tx.getFlightDao();

            Flight flight = dao.forTopiaIdEquals(flightId).findUnique();

            // delete geoPoints
            List<GeoPoint> geoPoints = getFlightGeoPoints(tx, flight);
            GeoPointTopiaDao geoPointDao = tx.getGeoPointDao();
            geoPointDao.deleteAll(geoPoints);

            Set<ObserverPosition> positions = Sets.newHashSet();

            // delete routes
            List<Route> routes = getRoutes(tx, flight);
            // keep observerPositions to delete from routes
            positions.addAll(Routes.toObserverPositions(routes));
            RouteTopiaDao routeDAO =  tx.getRouteDao();
            routeDAO.deleteAll(routes);

            // delete observations
            List<Observation> observations = getObservations(tx, flight);
            ObservationTopiaDao observationDAO =  tx.getObservationDao();
            observationDAO.deleteAll(observations);

            // keep observerPositions to delete from transectFlights
            positions.addAll(TransectFlights.toObserverPositions(flight.getTransectFlight()));

            // delete flight (cascade delete on transectFlights)
            dao.delete(flight);

            // delete observerPositions
            ObserverPositionTopiaDao observerPositionDAO =  tx.getObserverPositionDao();
            observerPositionDAO.deleteAll(positions);

        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        }
    }


    /**
     * Retrieve all observer from {@code flight} which are allowed to be
     * on positions for transects and routes. This means, no pilots and a null
     * observer that can be set.
     *
     * @param flight Flight where observers are retrieved
     * @return the List of allowed Observer
     * @see ObserverPosition
     * @since 0.3
     */
    public List<Observer> getFlightObserverForPositions(Flight flight) {

        // Filter only no pilot observers
        List<Observer> result = Lists.newArrayList(
                Iterables.filter(flight.getObserver(), Observers.isNotPilot())
        );

        // Sort on initials
        Collections.sort(result, Observers.onInitials());

        // Add a null observer
        result.add(0, null);

        return result;
    }

    public GeoPoint getGeoPoint(Flight flight,
                                Date date) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            List<GeoPoint> geoPoints = getFlightGeoPoints(tx, flight);

            GeoPoint result = getGeoPoint(tx, flight, geoPoints, date);
            return result;

        } catch (TopiaException ex) {
            throw new TopiaException(ex);
        } finally {
            endTransaction(tx);
        }
    }

    public GeoPoint getGeoPoint(SammoaTopiaPersistenceContext tx,
                                Flight flight,
                                List<GeoPoint> geoPoints,
                                Date date) throws TopiaException {

        DateTime newTime = Dates.toDateTime(date);

        // Retrieve the appropriate location
        GeoPoint location = GeoPoints.getClosestPoint(geoPoints, date);
        DateTime locationTime = Dates.toDateTime(location.getRecordTime());

        if (log.isDebugEnabled()) {
            log.debug(String.format("Find locationTime %1$tH:%1$tM:%1$tS.%1$tL",
                                       locationTime.toDate())
            );
        }

        GeoPoint result;

        // Create a new location if no one is available for the newTime
        if (!locationTime.isEqual(newTime)) {

            result = new GeoPointImpl(location.getLatitude(), location.getLongitude());
            result.setSpeed(location.getSpeed());
            result.setAltitude(location.getAltitude());

            int captureDelay = location.getCaptureDelay() +
                    Seconds.secondsBetween(locationTime, newTime).getSeconds();

            result.setCaptureDelay(captureDelay);

            result.setFlight(flight);
            result.setRecordTime(newTime.toDate());
            tx.getGeoPointDao().create(result);
            geoPoints.add(result);

            if (log.isDebugEnabled()) {
                log.debug("Create a new GeoPoint : " +  result);
            }

        } else {
            result = location;
        }
        return result;
    }

    public List<GeoPoint> getFlightGeoPoints(Flight flight) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            List<GeoPoint> result = getFlightGeoPoints(transaction, flight);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public List<GeoPoint> getFlightGeoPoints(SammoaTopiaPersistenceContext transaction,
                                             Flight flight) {

        try {
            GeoPointTopiaDao dao =  transaction.getGeoPointDao();
            List<GeoPoint> result =
                    dao.findAllByFlightOrderedByRecordTime(flight);
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        }
    }

//
//    public void removeAllFlightObservers(Flight flight, List<Observer> observers) {
//
//        context.getAutoSaveListener().flush();
//
//        Set<ObserverPosition> observerPositions = Sets.newHashSet();
//
//        // Add all ObserverPosition matches from TransectFlight
//        for (TransectFlight transectFlight : flight.getTransectFlight()) {
//            Iterables.addAll(observerPositions,
//                             Iterables.filter(transectFlight.getObserverPosition(),
//                                              ObserverPositions.withOneOfObservers(observers)));
//        }
//
//        // Add all ObserverPosition matches from Route
//        List<Route> routes = getRoutes(flight);
//        for (Route route : routes) {
//            Iterables.addAll(observerPositions,
//                             Iterables.filter(route.getEffort().getObserverPosition(),
//                                              ObserverPositions.withOneOfObservers(observers)));
//        }
//
//        // Change to null the Observer value in each ObserverPosition
//        for (ObserverPosition observerPosition : observerPositions) {
//            observerPosition.setObserver(null);
//        }
//
//        // Remove the observers from flight
//        // TODO sletellier 29062012 : genererate removeAll in topia
//        for (Observer observer : observers) {
//            flight.removeObserver(observer);
//        }
//
//        context.getAutoSaveListener().flush();
//    }

    /**
     * Add a list of {@code transects} to the {@code flight} beginning at
     * {@code fromIndex}. New {@link TransectFlight} will be created and added
     * to the {@code flight}. Associated {@link Transect} will be persisted if
     * necessary. Note that all {@link TransectFlight} have link with
     * {@link AutoSaveListener} to manage modifications.
     *
     * @param flight    The flight
     * @param fromIndex Index where transects will be added
     * @param transects The list of transect to add
     * @return the resulting TransectFlight added
     */
    public List<TransectFlight> addTransects(Flight flight,
                                             int fromIndex,
                                             Iterable<Transect> transects) {

        List<TransectFlight> result = Lists.newArrayList();

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            FlightTopiaDao flightDAO = transaction.getFlightDao();

            // Reverse the list to always use the same fromIndex using
            // with List.add method
            ImmutableList<Transect> reverseList =
                    ImmutableList.copyOf(transects).reverse();

            for (Transect transect : reverseList) {

//                referential.persistTransectIfNecessary(transaction, transect);

                TransectFlight transectFlight = createTransectFlight(
                        transaction, transect, flight);

                if (log.isDebugEnabled()) {
                    log.debug("Add transect " + transect.getName() + " to the flight " + flight.getFlightNumber());
                }

                result.add(transectFlight);
                flight.addTransectFlight(fromIndex, transectFlight);
            }
            flightDAO.update(flight);

            transaction.commit();

        } finally {
            endTransaction(transaction);
        }
        Collections.reverse(result);
        return result;
    }

    public Long getTransectRealNbTimes(Transect transect) {
        return getTransectRealNbTimes(Lists.newArrayList(transect)).get(transect);
    }

    public Map<Transect, Long> getTransectRealNbTimes(Collection<Transect> transects) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            TransectFlightTopiaDao dao =  transaction.getTransectFlightDao();

            Map<Transect, Long> result;
            result = dao.countAllByTransect(transects);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    /**
     * Retrieve the list of {@link Route} associated to the given {@code flight}.
     *
     * @param flight Flight
     * @return the List of Route matching the {@code flight}
     */
    public List<Route> getRoutes(Flight flight) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            List<Route> result = getRoutes(transaction, flight);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    /**
     * Retrieve the list of {@link Route} associated to the given {@code flight}.
     *
     * @param flight Flight
     * @return the List of Route matching the {@code flight}
     */
    public List<Route> getRoutes(SammoaTopiaPersistenceContext transaction, Flight flight) {


        RouteTopiaDao dao =  transaction.getRouteDao();
        List<Route> result = dao.findAllByFlightOrderedByBeginTime(flight);
        return result;
    }

    /**
     * Retrieve the last  {@link Route} of the given {@code flight}. If the
     * flight is ended or not started, a null route is returned.
     *
     * @param flight Flight
     * @return the last unfinished route for the flight or null otherwise
     */
    public Route getLastUnfinishedRoute(Flight flight) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            Route result;
            if (flight.getEndDate() == null) {
                RouteTopiaDao dao =  transaction.getRouteDao();
                result = dao.findLastByFlight(flight);

            } else {
                result = null;
            }
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    public Route getPreviousRoute(Route route) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            RouteTopiaDao dao =  transaction.getRouteDao();
            Route result = dao.findPreviousRoute(route);
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    public TransectFlight getPreviousTransect(Route previousRoute) {

        TransectFlight result;
        if (previousRoute.getRouteType() == RouteType.LEG) {
            result = previousRoute.getTransectFlight();

        } else {
            SammoaTopiaPersistenceContext transaction = beginTransaction();
            try {

                RouteTopiaDao dao =  transaction.getRouteDao();
                Route previousLEG = dao.findPreviousLEG(previousRoute);
                result = previousLEG == null ? null : previousLEG.getTransectFlight();
            } catch (TopiaException e) {
                throw new TopiaException(e);

            } finally {
                endTransaction(transaction);
            }
        }
        return result;
    }

    protected TransectFlight getPreviousTransect(SammoaTopiaPersistenceContext tx,
                                                 Route previousRoute)
            throws TopiaException {

        TransectFlight result;
        if (previousRoute.getRouteType() == RouteType.LEG) {
            result = previousRoute.getTransectFlight();

        } else {

            RouteTopiaDao dao =  tx.getRouteDao();
            Route previousLEG = dao.findPreviousLEG(previousRoute);
            result = previousLEG == null ? null : previousLEG.getTransectFlight();
        }
        return result;
    }

    public List<Observation> getObservations(Flight flight) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            List<Observation> result = getObservations(transaction, flight);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public List<Observation> getObservations(SammoaTopiaPersistenceContext transaction,
                                             Flight flight) {

        try {
            ObservationTopiaDao dao =  transaction.getObservationDao();
            List<Observation> result =
                    dao.findAllByFlightOrderedByObservationTime(flight);
            return result;

        } catch (TopiaException e) {
            throw new TopiaException(e);

        }
    }

    public void setRouteObserverByPosition(Route route,
                                           Observer observer,
                                           Position position) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            Collection<ObserverPosition> observerPositions =
                    route.getObserverPosition();

            ObserverPositionTopiaDao observerPositionDAO =  transaction.getObserverPositionDao();

            ObserverPosition observerPosition =
                    Iterables.find(observerPositions,
                                   ObserverPositions.withPosition(position));

            // Apply switch on the other observer position with the observer
            if (observer != null) {

                // keep old observer to change with existing
                Observer oldObserver = observerPosition.getObserver();

                ObserverPosition otherObserverPosition =
                        Iterables.find(observerPositions,
                                       ObserverPositions.withObserver(observer),
                                       null);

                if (otherObserverPosition != null) {
                    otherObserverPosition.setObserver(oldObserver);
                    observerPositionDAO.update(otherObserverPosition);

                    if (log.isDebugEnabled()) {
                        log.debug(String.format(
                                "Set observer %s for position %s",
                                observer.getInitials(),
                                otherObserverPosition.getPosition())
                        );
                    }
                }
            }

            observerPosition.setObserver(observer);
            observerPositionDAO.update(observerPosition);

            debugObserverPositions("Update", observerPositions);

            transaction.commit();

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    protected TransectFlight createTransectFlight(SammoaTopiaPersistenceContext transaction,
                                                  Transect transect,
                                                  Flight flight)
            throws TopiaException {

        TransectFlightTopiaDao transectFlightDAO =  transaction.getTransectFlightDao();

        int crossingNumber =
                transectFlightDAO.getMaxCrossingNumberByTransect(transect) + 1;

        TransectFlight result = (TransectFlight)transectFlightDAO.create();
        result.setTransect(transect);
        result.setCrossingNumber(crossingNumber);

        // Update transect for nbTimes
        TransectTopiaDao transectDAO =  transaction.getTransectDao();
        transectDAO.update(transect);
        if (log.isDebugEnabled()) {
            log.debug("Transect " + transect.getName() + " saved with nbTimes = " + crossingNumber);
        }

        Collection<ObserverPosition> observerPositions =
                createObserverPositions(transaction, flight);
        result.setObserverPosition(observerPositions);

        return result;
    }

    public Observation createObservation(SammoaTopiaPersistenceContext transaction,
                                         Flight flight,
                                         Date beginTime,
                                         Position position)
            throws TopiaException {

        Observation result;

        ObservationTopiaDao dao =  transaction.getObservationDao();

        int number = dao.findLastObservationNumber(flight);

        result = dao.createByNaturalId(number, flight);
        result.setObservationTime(beginTime);
        result.setPosition(position);
        result.setObservationStatus(ObservationStatus.NEW);

        return result;
    }

    public Route createTransit(SammoaTopiaPersistenceContext transaction,
                               Flight flight,
                               Date beginTime,
                               Route previousRoute)
            throws TopiaException {

        Route result = createRoute(transaction,
                                   flight,
                                   beginTime,
                                   RouteType.TRANSIT,
                                   previousRoute,
                                   null);
        return result;
    }

    public Route createLeg(SammoaTopiaPersistenceContext transaction,
                           Flight flight,
                           Date beginTime,
                           Route previousRoute,
                           TransectFlight transectFlight)
            throws TopiaException {

        Preconditions.checkArgument(
                transectFlight != null,
                "You can't create a leg without any transect");

        Route result = createRoute(transaction,
                                   flight,
                                   beginTime,
                                   RouteType.LEG,
                                   previousRoute,
                                   transectFlight);

        RouteTopiaDao dao =  transaction.getRouteDao();

        result.setTransectFlight(transectFlight);

        int effortNumber = dao.getLastEffortNumber(flight);
        result.setEffortNumber(effortNumber);

        return result;
    }

    public Route createCircleBack(SammoaTopiaPersistenceContext transaction,
                                  Flight flight,
                                  Date beginTime,
                                  Route previousRoute,
                                  Observation circleBackCause)
            throws TopiaException {

        Preconditions.checkArgument(
                circleBackCause != null,
                "You can't create a route without any observation cause");

        Route result = createRoute(transaction,
                                   flight,
                                   beginTime,
                                   RouteType.CIRCLE_BACK,
                                   previousRoute,
                                   null);

        result.setCircleBackCause(circleBackCause);

        return result;
    }

    protected Route createRoute(SammoaTopiaPersistenceContext transaction,
                                Flight flight,
                                Date beginTime,
                                RouteType type,
                                Route previousRoute,
                                TransectFlight transectFlight)
            throws TopiaException {

        RouteTopiaDao dao =  transaction.getRouteDao();

        // Create the new Route
        Route result = (Route) dao.create();
        result.setBeginTime(beginTime);
        result.setRouteType(type);
        result.setFlight(flight);

        // Copy previous data
        if (previousRoute != null) {

            result.setSeaState(previousRoute.getSeaState());
            result.setSwell(previousRoute.getSwell());
            result.setTurbidity(previousRoute.getTurbidity());
            result.setCloudCover(previousRoute.getCloudCover());
            result.setSkyGlint(previousRoute.getSkyGlint());
            result.setGlareFrom(previousRoute.getGlareFrom());
            result.setGlareTo(previousRoute.getGlareTo());
            result.setGlareUnder(previousRoute.isGlareUnder());
            result.setGlareSeverity(previousRoute.getGlareSeverity());
            result.setSubjectiveConditions(previousRoute.getSubjectiveConditions());

            // Ensure that there is never two routes one after the other with
            // the same topiaCreateDate
            long topiaCreateDate =
                    result.getTopiaCreateDate().getTime();
            long previousTopiaCreateDate =
                    previousRoute.getTopiaCreateDate().getTime();
            if (topiaCreateDate == previousTopiaCreateDate) {
                result.setTopiaCreateDate(new Date(topiaCreateDate + 1));
            }
        }

        // Prepare observer positions
        Collection<ObserverPosition> observerPositions =
                createRouteObserverPositions(transaction, previousRoute, transectFlight, flight);
        result.setObserverPosition(observerPositions);

        return result;
    }

    protected Collection<ObserverPosition> createRouteObserverPositions(SammoaTopiaPersistenceContext transaction,
                                                                        Route previousRoute,
                                                                        TransectFlight transectFlight,
                                                                        Flight flight)
            throws TopiaException {

        Collection<ObserverPosition> result;

        if (previousRoute != null) {

            // Prepare position depends on transect change
            // The transect has change if the used one is different from the previous route (only for LEG)
            boolean transectChanged = transectFlight != null && !transectFlight.equals(previousRoute.getTransectFlight());
            if (transectChanged) {

                if (log.isDebugEnabled()) {
                    log.debug("Use transect observerPositions");
                }

                result = copyObserverPositions(transaction, transectFlight.getObserverPosition());

                // Copy ObserverPosition from previousRoute
            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Use previous route observerPositions");
                }

                result = copyObserverPositions(transaction, previousRoute.getObserverPosition());
            }
        } else {

            if (log.isDebugEnabled()) {
                log.debug("Create default observerPositions from flight");
            }

            result = createObserverPositions(transaction, flight);
        }
        return result;
    }

    protected Collection<ObserverPosition> createObserverPositions(SammoaTopiaPersistenceContext transaction,
                                                                   Flight flight)
            throws TopiaException {

        Collection<ObserverPosition> result = Lists.newArrayList();

        ObserverPositionTopiaDao observerPositionDAO =  transaction.getObserverPositionDao();

        // Prepare positions
        // TODO-fdesbois-2012-06-13 : make positions depends on platformType
        List<Position> positions = ImmutableList.of(
                Position.NAVIGATOR,
                Position.FRONT_LEFT,
                Position.FRONT_RIGHT,
                Position.CO_NAVIGATOR);

        // Filter observers which are not pilot
        Iterator<Observer> observerIt =
                Iterables.filter(flight.getObserver(), Observers.isNotPilot()).iterator();

        // Add positions to the TransectFlight
        for (Position position : positions) {

            ObserverPosition observerPosition =
                    observerPositionDAO.create();

            observerPosition.setPosition(position);

            if (observerIt.hasNext()) {
                Observer observer = observerIt.next();
                observerPosition.setObserver(observer);
            }

            result.add(observerPosition);
        }

        debugObserverPositions("Create", result);

        return result;
    }

    protected Collection<ObserverPosition> copyObserverPositions(SammoaTopiaPersistenceContext transaction,
                                                                 Collection<ObserverPosition> observerPositions)
            throws TopiaException {

        Collection<ObserverPosition> result = Lists.newArrayList();

        debugObserverPositions("Copy", observerPositions);

        ObserverPositionTopiaDao observerPositionDAO =  transaction.getObserverPositionDao();

        for (ObserverPosition source : observerPositions) {

            ObserverPosition target = observerPositionDAO.create();
            target.setPosition(source.getPosition());
            target.setObserver(source.getObserver());
            result.add(target);
        }

        debugObserverPositions("Result of copy", result);

        return result;
    }

    protected void debugObserverPositions(String title,
                                          Iterable<ObserverPosition> observerPositions) {

        if (log.isTraceEnabled()) {
            log.trace("ObserverPosition : " + title);
            for (ObserverPosition elmt : observerPositions) {
                log.trace(
                        "ObserverPosition : "
                                + elmt.getTopiaId() + ", "
                                + (elmt.getObserver() != null ? elmt.getObserver().getInitials() : "N/A") + ", "
                                + elmt.getPosition());
            }
        }
    }

    protected int getNextFlightNumber(SammoaTopiaPersistenceContext tx) throws TopiaException {

        FlightTopiaDao dao = tx.getFlightDao();

        int confNumber = config.getFlightNumber();
        Integer dbNumber = dao.findLastFlightNumber();

        int result = dbNumber == null
                     ? confNumber : Math.max(confNumber, dbNumber);
        result++;
        return result;
    }

    public TransectFlight getNextTransectFlightFrom(TransectFlight transectFlight,
                                                    Flight flight,
                                                    Date beforeTime) {
        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            TransectFlight result = getNextTransectFlightFrom(tx, transectFlight, flight, beforeTime);
            return result;
        } catch (TopiaException ex) {
            throw new TopiaException(ex);
        } finally {
            endTransaction(tx);
        }
    }

    public TransectFlight getNextTransectFlightFrom(SammoaTopiaPersistenceContext tx,
                                                    TransectFlight transectFlight,
                                                    Flight flight,
                                                    Date beforeTime)
            throws TopiaException {

        // Note that this may not reflect the real next transect if exists in routes

        RouteTopiaDao routeDAO =  tx.getRouteDao();

        // Skip previous elements
        int numberToSkip;
        if (transectFlight == null) {
            numberToSkip = 0;
        } else {
            numberToSkip = flight.getTransectFlight().indexOf(transectFlight) + 1;
        }
        Iterable<TransectFlight> transectFlights = Iterables.skip(
                flight.getTransectFlight(), numberToSkip);

        // Return only if not deleted and doesn't have any route
        for (TransectFlight element : transectFlights) {

            if (!element.isDeleted()
                && routeDAO.countByTransectFlightBeforeTime(element, beforeTime) == 0) {

                return element;
            }
        }
        return null;
    }

    public Flight getFlightByNaturalId(Map<String, Object> naturalId) {
        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            FlightTopiaDao dao = tx.getFlightDao();
            Flight result = dao.forProperties(naturalId).findAnyOrNull();
            return result;
        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }
    }
}
