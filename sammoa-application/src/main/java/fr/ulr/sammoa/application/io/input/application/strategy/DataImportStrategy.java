package fr.ulr.sammoa.application.io.input.application.strategy;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.input.application.SammoaImportModelFactory;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaPersistenceHelper;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectFlightTopiaDao;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportToMap;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.CsvImportResult;
import org.nuiton.topia.service.csv.in.TopiaCsvImports;

import java.util.Map;

/**
 * Strategy to import data (create all entites, no update possible).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class DataImportStrategy extends AbstractImportStrategy {

    public DataImportStrategy(SammoaImportModelFactory modelFactory,
                              SammoaTopiaPersistenceContext tx,
                              SammoaPersistenceHelper persistenceHelper) {
        super(modelFactory, tx, persistenceHelper);
    }

    @Override
    public <E extends TopiaEntity> void importTable(TableMeta<SammoaEntityEnum> meta, Import<E> importer,
                                                    CsvImportResult<SammoaEntityEnum> csvResult) throws TopiaException {

        if (meta.getSource() != SammoaEntityEnum.TransectFlight) {

            TopiaDao dao = getDAO(meta.getSource());
            TopiaCsvImports.importAllEntities(dao, meta, importer, csvResult);
        } else {

            TransectFlightTopiaDao dao = getTx().getTransectFlightDao();

            // import transect flights
            Iterable<TransectFlight> transectFlights =
                    TopiaCsvImports.importAllEntitiesAndReturnThem(dao,
                                                                   meta,
                                                                   (Import<TransectFlight>) importer,
                                                                   csvResult);


            // must flush hibernate session before we want to send our own sql requests...
            flushTransaction();

            //must now reattach (in pure sql indexInFlight)
            dao.reAttachIndexInFlight(transectFlights);
        }
    }

    @Override
    public void importAssociation(AssociationMeta<SammoaEntityEnum> meta,
                                  ImportToMap importer,
                                  CsvImportResult<SammoaEntityEnum> csvResult) throws TopiaException {

        SammoaEntityEnum target = meta.getTarget();
        boolean nmAssociationMeta = getModelFactory().isNMAssociationMeta(meta);

        if (getModelFactory().getDbMetas().isReferential(target)) {

            // must switch topia of target to the one in universe
            Map<String, TopiaEntity> targetUniverse =
                    getModelFactory().getUniverse().get(target);

            if (nmAssociationMeta) {
                TopiaCsvImports.importNMAssociation(getTx().getSqlSupport(),
                                                    meta,
                                                    targetUniverse,
                                                    importer,
                                                    csvResult,
                                                    1000);
            } else {
                TopiaCsvImports.importAssociation(getTx().getSqlSupport(),
                                                  meta,
                                                  targetUniverse,
                                                  importer,
                                                  csvResult,
                                                  1000);
            }


        } else {
            if (nmAssociationMeta) {
                TopiaCsvImports.importNMAssociation(getTx().getSqlSupport(),
                                                    meta,
                                                    importer,
                                                    csvResult,
                                                    1000);
            } else {
                TopiaCsvImports.importAssociation(getTx().getSqlSupport(),
                                                  meta,
                                                  importer,
                                                  csvResult,
                                                  1000);
            }
        }
    }

}
