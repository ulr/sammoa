package fr.ulr.sammoa.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StringRef;
import fr.ulr.sammoa.persistence.Transect;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import java.text.SimpleDateFormat;

import static org.nuiton.i18n.I18n.t;

/**
 * Sammoa decorator service.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class DecoratorService extends SammoaServiceSupport {

    /** Delegate decorator provider. */
    protected DecoratorProvider decoratorProvider;

    public <O> Decorator<O> getDecorator(O object) {
        return decoratorProvider.getDecorator(object);
    }

    public <O> Decorator<O> getDecorator(O object, String name) {
        return decoratorProvider.getDecorator(object, name);
    }

    public <O> Decorator<O> getDecoratorByType(Class<O> type) {
        return decoratorProvider.getDecoratorByType(type);
    }

    public <O> Decorator<O> getDecoratorByType(Class<O> type, String name) {
        return decoratorProvider.getDecoratorByType(type, name);
    }

    @Override
    public void setSammoaContext(SammoaContext context) {
        super.setSammoaContext(context);

        decoratorProvider = new DecoratorProvider() {
            @Override
            protected void loadDecorators() {
                registerDecorator(new Decorator<Route>(Route.class) {

                    @Override
                    public String toString(Object bean) {
                        Route route = (Route) bean;
                        String result = route.getRouteType().name();
                        if (route.getEffortNumber() != null) {
                            result += " " + route.getEffortNumber();
                        }
                        return result;
                    }
                });
                registerJXPathDecorator(Observation.class, "Sighting ${observationNumber}$s");
                registerJXPathDecorator(Transect.class, "${name}$s");
                registerJXPathDecorator(Observer.class, "${initials}$s");
                registerMultiJXPathDecorator(Strate.class, "${code}$s##${name}$s", "##", " - ");
                registerMultiJXPathDecorator(RouteType.class, "${name}$s", "##", " - ");
                registerMultiJXPathDecorator(Species.class, "${code}$s##${commonName}$s", "##", " - ");
                registerMultiJXPathDecorator(StringRef.class, "${value}$s", "##", " - ");


                registerJXPathDecorator(SubRegion.class, "${name}$s");
                registerJXPathDecorator(Region.class, "${code}$s");
                registerMultiJXPathDecorator(Survey.class, "${code}$s##${region/code}$s", "##", " - ");

                registerJXPathDecorator(GeoPoint.class, "${recordTime}$tT => Lat ${latitude}$s - Lon ${longitude}$S");

                registerDecorator(new Decorator<Flight>(Flight.class) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        Flight flight = (Flight) bean;

                        String result;
                        if (flight == null) {
                            result = t("sammoa.flight.decorator.newFlight");

                        } else {
                            SimpleDateFormat dateFormat = new SimpleDateFormat(t("sammoa.dateTimePattern"));
                            dateFormat.setTimeZone(flight.getTimeZone());

                            result = t("sammoa.flight.decorator.flight", flight.getFlightNumber());
                            if (flight.getBeginDate() != null) {
                                result += " - " + dateFormat.format(flight.getBeginDate());

                                if (flight.getEndDate() != null) {
                                    result += " - " + dateFormat.format(flight.getEndDate());
                                } else {
                                    result += " - " + t("sammoa.flight.decorator.notEnded");
                                }

                                result += " - [" + flight.getTimeZone().getID() + "]";

                            } else {
                                result += " - " + t("sammoa.flight.decorator.notStarted");
                            }
                        }
                        return result;
                    }
                });

                registerDecorator(new Decorator<Position>(Position.class) {

                    @Override
                    public String toString(Object bean) {
                        Position position = (Position) bean;
                        return t(position.getLabel());
                    }
                });
            }
        };
    }
}
