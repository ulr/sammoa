/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.flightController;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.DeviceManagerProvider;
import fr.ulr.sammoa.application.io.FlightStorage;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.FlightTopiaDao;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationStatus;
import fr.ulr.sammoa.persistence.ObservationTopiaDao;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import javax.swing.SwingUtilities;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created: 11/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
abstract class BaseFlightController extends SammoaServiceSupport implements FlightController {

    private static final Log log = LogFactory.getLog(BaseFlightController.class);

    private static final TimeLog timeLog = new TimeLog(BaseFlightController.class, 500, 1000);

    protected FlightService service;

    protected boolean initialized;

    protected FlightState state;

    protected Flight flight;

    protected Route currentRoute;

    protected TransectFlight nextTransect;

    protected TransectFlight currentTransect;

    protected final Set<FlightControllerListener> listeners;

    private DeviceManagerProvider deviceManagerProvider;

    protected FlightStorage flightStorage;

    protected Timer timer = new Timer();

    protected TimerTask circleBackTimer;
    protected int remainingCircleBackTime = 0;
    protected boolean alreadyOnCircleBack = false;

    protected TimerTask rejoinTimer;
    protected int remainingRejoinTime = 0;

    protected TimerTask deadtimeTimer;
    protected int remainingDeadtime = 0;
    protected boolean alreadyOnDeadtime = false;

    public BaseFlightController() {
        listeners = Sets.newHashSet();
    }

    protected abstract GeoPoint getLocation(SammoaTopiaPersistenceContext tx) throws TopiaException;

    @Override
    public void addFlightControllerListener(FlightControllerListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeFlightControllerListener(FlightControllerListener listener) {
        listeners.remove(listener);
    }

    @Override
    public FlightState getState() {
        return state;
    }

    @Override
    public Route getCurrentRoute() {
        return currentRoute;
    }

    @Override
    public TransectFlight getNextTransect() {
        return nextTransect;
    }

    @Override
    public <T extends DeviceManager> T getDeviceManager(Class<T> deviceManagerClass) {
        return getDeviceManagerProvider().getDeviceManager(deviceManagerClass);
    }

    @Override
    public boolean isWaiting() {
        return state == FlightState.WAITING;
    }

    @Override
    public boolean isEnded() {
        return state == FlightState.ENDED;
    }

    @Override
    public boolean isOnEffort() {
        return state == FlightState.ON_EFFORT;
    }

    @Override
    public boolean isOffEffort() {
        return state == FlightState.OFF_EFFORT;
    }

    @Override
    public boolean isRunning() {
        return isOnEffort() || isOffEffort();
    }

    @Override
    public void init(Flight flight) {

        Preconditions.checkNotNull(flight);

        this.flight = flight;

        if (initialized) {
            if (log.isWarnEnabled()) {
                log.warn("The FlightController is already initialized");
            }
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("Initialize the FlightController for flight " +
                    this.flight.getFlightNumber());
        }

        initialized = true;

        Survey survey = flight.getSurvey();
        SurveyStorage surveyStorage =
                getSurveyStorage(survey.getTopiaId());
        flightStorage = surveyStorage.getFlightStorage(flight.getTopiaId());

        service = context.getService(FlightService.class);
    }

    @Override
    public void start() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isWaiting(), "You can call start() only if flight is waiting (not started, not ended)");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            setFlightBeginDate(tx, flight, currentDate);

            if (log.isInfoEnabled()) {
                log.info("Start Flight " + flight.getFlightNumber() + " at " +  flight.getBeginDate());
            }

            // Start with TRANSIT route
            if (log.isInfoEnabled()) {
                log.info(String.format("Create TRANSIT [START] at %1$tH:%1$tM:%1$tS.%1$tL", currentDate));
            }
            Route previousRoute = currentRoute;
            currentRoute = service.createTransit(tx, flight, currentDate, previousRoute);

            // Auto selection of the next transect starting from the begining
            nextTransect = service.getNextTransectFlightFrom(tx, null, flight, currentRoute.getBeginTime());

            state = FlightState.OFF_EFFORT;

            tx.commit();

            startTime = timeLog.log(startTime, "start()", "Commited");

            // Fire events after commit
            onRouteAdded(previousRoute, currentRoute);
            onNextTransectChanged(nextTransect);
            onStateChanged(state);

            timeLog.log(startTime, "start()", "Fired");

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void setNextTransect(TransectFlight nextTransect) {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                nextTransect == null || !nextTransect.isDeleted(), "You can't use a deleted transect as next value");

        this.nextTransect = nextTransect;

        // Fire transect changed
        onNextTransectChanged(nextTransect);

        timeLog.log(startTime, "setNextTransect()", "Fired");
    }

    @Override
    public void setCurrentRoute(Route currentRoute) {
        setCurrentRoute(currentRoute, true);
    }

    protected void initCurrentRoute(Route currentRoute) {
        setCurrentRoute(currentRoute, false);
    }

    private void setCurrentRoute(Route newCurrentRoute, boolean fireChanges) {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                                 "The controller must be initialized before calling any action");

        if (log.isInfoEnabled()) {
            log.info("Set currentRoute : " +  Routes.toString(newCurrentRoute));
        }

        currentRoute = newCurrentRoute;

        TransectFlight currentTransectOldValue = currentTransect;

        if (currentRoute == null) {

            if (flight.getEndDate() == null) {
                state = FlightState.WAITING;

            } else {
                state = FlightState.ENDED;
            }

        } else {

            // Use previous not deleted route
            if (currentRoute.isDeleted()) {
                currentRoute = service.getPreviousRoute(newCurrentRoute);
            }

            TransectFlight previousTransect = service.getPreviousTransect(currentRoute);

            // Update state, currentTransect and nextTransect depends on routeType
            switch (currentRoute.getRouteType()) {

                case LEG:
                    state = FlightState.ON_EFFORT;
                    currentTransect = previousTransect;
                    nextTransect = service.getNextTransectFlightFrom(previousTransect, flight, currentRoute.getBeginTime());
                    break;

                case CIRCLE_BACK:
                    state = FlightState.OFF_EFFORT;
                    currentTransect = previousTransect;
                    nextTransect = previousTransect;
                    break;

                case TRANSIT:
                    state = FlightState.OFF_EFFORT;
                    currentTransect = null;
                    nextTransect = service.getNextTransectFlightFrom(previousTransect, flight, currentRoute.getBeginTime());
                    break;

                default:
            }
        }

        if (fireChanges) {

            onStateChanged(state);
            onCurrentTransectChanged(currentTransectOldValue, currentTransect);
            onNextTransectChanged(nextTransect);
            onCurrentRouteChanged(currentRoute);
        }

        timeLog.log(startTime, "setCurrentRoute()");
    }

    @Override
    public void begin() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isOffEffort(), "You can call begin() only if flight is running (started, not ended, not on effort)");

        Preconditions.checkState(
                nextTransect != null && !nextTransect.isDeleted(),
                "You can't call begin() if no transect is selected or if it is " +
                "deleted. Call setNextTransect() method first");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            alreadyOnCircleBack = false;

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            // The current transect becomes the next one
            TransectFlight previousTransect = currentTransect;
            currentTransect = nextTransect;

            // Create new LEG route
            if (log.isInfoEnabled()) {
                log.info(String.format("Create LEG [BEGIN] at %1$tH:%1$tM:%1$tS.%1$tL", currentDate));
            }
            Route previousRoute = currentRoute;
            currentRoute = service.createLeg(tx, flight, currentDate, previousRoute, currentTransect);

            // automatic selection of the next transect
            nextTransect = service.getNextTransectFlightFrom(tx, currentTransect, flight, currentRoute.getBeginTime());

            state = FlightState.ON_EFFORT;

            tx.commit();

            startTime = timeLog.log(startTime, "begin()", "Commited");

            // Fire events after commit
            onCurrentTransectChanged(previousTransect, currentTransect);
            onNextTransectChanged(nextTransect);
            onStateChanged(state);
            SwingUtilities.invokeLater(() ->
                onRouteAdded(previousRoute, currentRoute)
            );

            //deadtime si vient d'un circleback
            if (previousRoute.getRouteType() == RouteType.CIRCLE_BACK && config.isCircleBackTimerEnabled()) {
                //Start deadtime timer
                if (deadtimeTimer != null) {
                    deadtimeTimer.cancel();
                }
                alreadyOnDeadtime = true;
                deadtimeTimer = new TimerTask() {
                    int i = config.getDeadtimeAfterCircleBack();
                    @Override
                    public void run() {
                        i--;
                        remainingDeadtime = i;
                        if ( i < 0) {
                            remainingDeadtime = 0;
                            alreadyOnDeadtime = false;
                            deadtimeTimer.cancel();
                        }
                        onTimerChanged("deadtime", remainingDeadtime);
                    }
                };

                timer.scheduleAtFixedRate(deadtimeTimer, 0, 1000);
            }

            timeLog.log(startTime, "begin()", "Fired");

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void rejoin() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isOffEffort(), "You can call rejoin() only if flight is running (started, not ended, not on effort)");

        Preconditions.checkState(
                nextTransect != null && !nextTransect.isDeleted(),
                "You can't call rejoin() if no transect is selected or if it is " +
                        "deleted. Call setNextTransect() method first");

        //Cancel rejoin timer
        remainingRejoinTime = 0;
        rejoinTimer.cancel();
        onTimerChanged("rejoin", 0);

        timeLog.log(startTime, "rejoin()", "Fired");

    }

    @Override
    public void circleBack(Observation observation) {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                currentRoute != null
                        && currentRoute.getRouteType() != RouteType.TRANSIT,
                "You can't call circleBack() during TRANSIT");


        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            alreadyOnCircleBack = true;

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            setObservationStatus(tx, observation, ObservationStatus.CIRCLE_BACK);

            // Create CIRCLE_BACK
            if (log.isInfoEnabled()) {
                log.info(String.format("Create CIRCLE_BACK at %1$tH:%1$tM:%1$tS.%1$tL", currentDate));
            }
            Route previousRoute = currentRoute;
            currentRoute = service.createCircleBack(tx, flight, currentDate, previousRoute, observation);

            nextTransect = currentTransect;

            state = FlightState.OFF_EFFORT;

            tx.commit();

            startTime = timeLog.log(startTime, "circleBack()", "Commited");

            // Fire events after commit
            onRouteAdded(previousRoute, currentRoute);
            onNextTransectChanged(nextTransect);
            onStateChanged(state);

            //Cancel circlebacktimer
            if (circleBackTimer != null) {
                remainingCircleBackTime = 0;
                circleBackTimer.cancel();
            }
            onTimerChanged("circleBack", 0);

            //Start rejoin timer
            if (config.isCircleBackTimerEnabled()) {
                rejoinTimer = new TimerTask() {
                    int i = config.getCountdownRejoinTrackline();

                    @Override
                    public void run() {
                        i--;
                        remainingRejoinTime = i;
                        if (i < 0) {
                            //timer can go negative -> CIRCLE_FAIL
                            observation.setObservationStatus(ObservationStatus.CIRCLE_FAIL);
                        }
                        onTimerChanged("rejoin", remainingRejoinTime);
                    }
                };
                timer.scheduleAtFixedRate(rejoinTimer, 0, 1000);
            }

            timeLog.log(startTime, "circleBack()", "Fired");

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void add() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isOnEffort(), "You can call add() only if flight is on effort (started, not ended, on effort)");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            // Create new LEG route
            if (log.isInfoEnabled()) {
                log.info(String.format("Create LEG [ADD] at %1$tH:%1$tM:%1$tS.%1$tL", currentDate));
            }
            Route previousRoute = currentRoute;
            currentRoute = service.createLeg(tx, flight, currentDate, previousRoute, currentTransect);

            tx.commit();

            startTime = timeLog.log(startTime, "add()", "Commited");

            // Fire events after commit
            onRouteAdded(previousRoute, currentRoute);

            timeLog.log(startTime, "add()", "Fired");

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void next() {

        long startTime = TimeLog.getTime();

        if (isOnEffort()) {
            end();
        }

        begin();

        timeLog.log(startTime, "next()");
    }

    @Override
    public void observation(Position position) {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isRunning(), "You can call observation() only if flight is running (started, not ended)");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            if (log.isInfoEnabled()) {
                log.info(String.format("Create Observation %2$s at %1$tH:%1$tM:%1$tS.%1$tL",
                        currentDate,
                        position)
                );
            }
            Observation observation = service.createObservation(tx, flight, currentDate, position);

            tx.commit();

            startTime = timeLog.log(startTime, "observation()", "Commited");

            onObservationAdded(observation, geoPoint);

            //(re-)Start circleback timer
            if (config.isCircleBackTimerEnabled()) {
                if (!alreadyOnCircleBack && !alreadyOnDeadtime) {
                    if (circleBackTimer != null) {
                        circleBackTimer.cancel();
                    }
                    if (config.isCircleBackEnabled()) {
                        circleBackTimer = new TimerTask() {
                            int i = config.getCountdownCircleBackConfirmation();

                            @Override
                            public void run() {
                                i--;
                                remainingCircleBackTime = i;
                                if (i < 0) {
                                    remainingCircleBackTime = 0;
                                    circleBackTimer.cancel();
                                }
                                onTimerChanged("circleBack", remainingCircleBackTime);
                            }
                        };
                        timer.scheduleAtFixedRate(circleBackTimer, 0, 1000);
                    }
                }
            }

            timeLog.log(startTime, "observation()", "Fired");

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void end() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isOnEffort(), "You can call end() only if flight is on effort (started, not ended, on effort)");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            TransectFlight previousTransect = currentTransect;
            currentTransect = null;

            // Create new TRANSIT route
            if (log.isInfoEnabled()) {
                log.info(String.format("Create TRANSIT [END] at %1$tH:%1$tM:%1$tS.%1$tL", currentDate));
            }
            Route previousRoute = currentRoute;
            currentRoute = service.createTransit(tx, flight, currentDate, previousRoute);
            remainingCircleBackTime = 0;
            alreadyOnCircleBack = false;
            remainingDeadtime = 0;
            alreadyOnDeadtime = false;
            remainingRejoinTime = 0;
            onTimerChanged("circleBack", remainingCircleBackTime);
            onTimerChanged("deadtime", remainingDeadtime);
            onTimerChanged("rejoin", remainingRejoinTime);

            if (circleBackTimer != null) {
                circleBackTimer.cancel();
            }

            if (deadtimeTimer != null) {
                deadtimeTimer.cancel();
            }

            if (rejoinTimer != null) {
                rejoinTimer.cancel();
            }


            state = FlightState.OFF_EFFORT;

            tx.commit();

            // Fire events after commit
            onRouteAdded(previousRoute, currentRoute);
            onCurrentTransectChanged(previousTransect, currentTransect);
            onNextTransectChanged(nextTransect);
            onStateChanged(state);

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(tx);
        }

        timeLog.log(startTime, "end()");
    }

    @Override
    public void stop() {

        long startTime = TimeLog.getTime();

        Preconditions.checkState(initialized,
                "The controller must be initialized before calling any action");

        Preconditions.checkState(
                isOffEffort() && flight.getEndDate() == null, "You can call stop() only if flight is running (started, not ended, not on effort)");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            GeoPoint geoPoint = getLocation(tx);

            Date currentDate = geoPoint.getRecordTime();

            if (log.isInfoEnabled()) {
                log.info(String.format("Stop flight %2$d at %1$tH:%1$tM:%1$tS.%1$tL",
                        currentDate,
                        flight.getFlightNumber())
                );
            }
            setFlightEndDate(tx, flight, currentDate);

            currentRoute = null;

            nextTransect = null;

            state = FlightState.ENDED;

            tx.commit();

            startTime = timeLog.log(startTime, "stop()", "Commited");

            // Fire events after commit
            onCurrentRouteChanged(currentRoute);
            onNextTransectChanged(nextTransect);
            onStateChanged(state);

            timeLog.log(startTime, "stop()", "Fired");

        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(tx);
        }
    }

    @Override
    public void close() {
        getDeviceManagerProvider().close();
        persistence.stopAutoSaveListener();
    }

    protected void setFlightBeginDate(SammoaTopiaPersistenceContext tx,
                                      Flight flight,
                                      Date beginDate)
            throws TopiaException {

        FlightTopiaDao flightDAO =  tx.getFlightDao();
        flight.setBeginDate(beginDate);
        flight.setTimeZone(TimeZone.getDefault());
        flightDAO.update(flight);
    }

    protected void setFlightEndDate(SammoaTopiaPersistenceContext tx,
                                    Flight flight,
                                    Date endDate)
            throws TopiaException {

        FlightTopiaDao flightDAO =  tx.getFlightDao();
        flight.setEndDate(endDate);
        flightDAO.update(flight);
    }

    protected void setObservationStatus(SammoaTopiaPersistenceContext tx,
                                        Observation observation,
                                        ObservationStatus status)
            throws TopiaException {

        ObservationTopiaDao observationDAO =  tx.getObservationDao();
        observation.setObservationStatus(status);
        observationDAO.update(observation);
    }

    protected DeviceManagerProvider getDeviceManagerProvider() {
        if (deviceManagerProvider == null) {
            deviceManagerProvider = context.getService(DeviceManagerProvider.class);
        }
        return deviceManagerProvider;
    }

    protected void onObservationAdded(Observation observation, GeoPoint location) {
        ObservationEvent event = new ObservationEvent(this, observation, location);
        for (FlightControllerListener listener : listeners) {
            listener.onObservationAdded(event);
        }
    }

    protected void onStateChanged(FlightState state) {
        for (FlightControllerListener listener : listeners) {
            listener.onStateChanged(state);
        }
    }

    protected void onRouteAdded(Route previousRoute, Route newRoute) {
        Preconditions.checkNotNull(newRoute);
        RouteEvent event = new RouteEvent(this, newRoute, true, previousRoute);
        for (FlightControllerListener listener : listeners) {
            listener.onCurrentRouteChanged(event);
        }
    }

    protected void onCurrentRouteChanged(Route route) {
        RouteEvent event = new RouteEvent(this, route, false, null);
        for (FlightControllerListener listener : listeners) {
            listener.onCurrentRouteChanged(event);
        }
    }

    protected void onNextTransectChanged(TransectFlight nextTransect) {
        for (FlightControllerListener listener : listeners) {
            listener.onNextTransectChanged(nextTransect);
        }
    }

    protected void onCurrentTransectChanged(TransectFlight oldValue, TransectFlight newValue) {
        // no fire by default
    }

    protected void onTimerChanged(String timer, int value) {
        for (FlightControllerListener listener : listeners) {
            listener.onTimerChanged(timer, value);
        }

    }

}
