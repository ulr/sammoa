package fr.ulr.sammoa.application.io;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.DecoratorService;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Survey;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Storage for a flight.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class FlightStorage extends SammoaStorage<Flight> {

    private static final long serialVersionUID = 1L;

    protected transient List<Pair<Date, File>> audioFilesCache;

    FlightStorage(File dataDirectory, String id) {
        super(dataDirectory, id, "flight.properties");
    }

    public File getAudioDirectory() {
        return SammoaDirectories.getAudioDirectory(getDirectory());
    }

    public File getAudioFile(Date audioStartTime, String ext) {

        String fileName = (audioStartTime.getTime() / 1000) + "." + ext;

        return new File(getAudioDirectory(), fileName);
    }

    protected List<Pair<Date, File>> getAudioFiles(String ext) {
        if (audioFilesCache == null) {
            audioFilesCache = Lists.newLinkedList();
            File[] audioFiles = getAudioDirectory().listFiles((dir, name) -> name.endsWith(ext));
            if (audioFiles != null) {
                for (File audioFile : audioFiles) {
                    String audioFileName = audioFile.getName();
                    String timeStr = audioFileName.substring(0, audioFileName.lastIndexOf("."));
                    long timeStamp = Long.parseLong(timeStr);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(timeStamp * 1000);
                    audioFilesCache.add(Pair.of(calendar.getTime(), audioFile));
                }
            }
            Comparator<Pair<Date, File>> reversed = Comparator.comparing(Pair::getKey);
            audioFilesCache.sort(reversed.reversed());
        }
        return audioFilesCache;
    }

    public Pair<Date, File> getAudioFileAtTime(Date audioTime, String ext) {
        List<Pair<Date, File>> audioFiles = getAudioFiles(ext);
        return audioFiles.stream()
                .filter(p -> !p.getKey().after(audioTime))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Map<String, Object> getNaturalId() {
        Map<String, Object> result = Maps.newHashMap();
        addInMap(Flight.PROPERTY_SYSTEM_ID, result);
        addIntInMap(Flight.PROPERTY_FLIGHT_NUMBER, result);
        addInMap(Flight.PROPERTY_SURVEY + "." + Survey.PROPERTY_CODE, result);
        addInMap(Flight.PROPERTY_SURVEY + "." + Survey.PROPERTY_REGION + "." + Region.PROPERTY_CODE, result);
        return result;
    }

    @Override
    public void fillProperties(Flight flight,
                               DecoratorService decoratorService) {
        Decorator<Flight> decorator =
                decoratorService.getDecoratorByType(Flight.class);

        putInProperties("name", decorator.toString(flight));
        putInProperties(Flight.PROPERTY_SYSTEM_ID, flight.getSystemId());
        putInProperties(Flight.PROPERTY_FLIGHT_NUMBER, flight.getFlightNumber());
        putInProperties(Flight.PROPERTY_SURVEY + "." + Survey.PROPERTY_CODE, flight.getSurvey().getCode());
        putInProperties(Flight.PROPERTY_SURVEY + "." + Survey.PROPERTY_REGION + "." + Region.PROPERTY_CODE, flight.getSurvey().getRegion().getCode());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("directory", getDirectory()).
                append("id", getId()).
                toString();
    }
}
