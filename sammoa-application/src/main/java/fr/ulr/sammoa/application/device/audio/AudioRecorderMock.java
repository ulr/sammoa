package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;

import javax.sound.sampled.AudioFileFormat;
import java.io.File;
import java.util.Set;

/**
 * Mock for {@link AudioRecorder} that record nothing.
 * <p/>
 * Created: 12/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class AudioRecorderMock implements AudioRecorder {

    protected AudioConfig config;

    public AudioRecorderMock(AudioConfig config) {
        this.config = config;
    }

    @Override
    public DeviceState getState() {
        return DeviceState.UNAVAILABLE;
    }

    @Override
    public AudioFileFormat.Type getOutputType() {
        return AudioFileFormat.Type.WAVE;
    }

    @Override
    public void open() throws DeviceTechnicalException {
        // do nothing
    }

    @Override
    public void start() {
        // do nothing
    }

    @Override
    public void record(File outputFile) {
        // do nothing
    }

    @Override
    public void stopRecord() {
        // do nothing
    }

    @Override
    public void stop() {
        // do nothing
    }

    @Override
    public void close() throws DeviceTechnicalException {
        // do nothing
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener listener) {
        // do nothing
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener listener) {
        // do nothing
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return Sets.newHashSet();
    }
}
