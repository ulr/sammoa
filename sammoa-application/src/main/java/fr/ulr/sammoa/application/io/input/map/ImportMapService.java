package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StrateTopiaDao;
import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.SubRegionTopiaDao;
import fr.ulr.sammoa.persistence.SubRegions;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyTopiaDao;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectTopiaDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Map;

/**
 * To import map.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportMapService extends SammoaServiceSupport {

    private static final Log log =
            LogFactory.getLog(ImportMapService.class);

    public int importTransects(String surveyId, Iterable<Transect> transects) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            SurveyTopiaDao surveyDAO =  transaction.getSurveyDao();

            Survey survey = surveyDAO.forTopiaIdEquals(surveyId).findUnique();

            StrateTopiaDao strateDAO =  transaction.getStrateDao();
            TransectTopiaDao transectDAO =  transaction.getTransectDao();

            // Retrieve directly all strates to avoid multiple select queries to find strates
            List<Strate> strates = strateDAO.findAllBySurveyOrderedByCode(survey);
            Map<String, Strate> strateMap =
                    Maps.uniqueIndex(strates, Strate::getCode);

            int result = 0;
            int index = 0;
            for (Transect transect : transects) {

                boolean isNew = importTransect(transectDAO, transect, strateMap, index);

                if (isNew) {
                    result++;
                }

                index++;
            }

            transaction.commit();

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    public int importStrates(String surveyId, Iterable<Strate> strates) {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            SurveyTopiaDao surveyDAO =  transaction.getSurveyDao();

            Survey survey = surveyDAO.forTopiaIdEquals(surveyId).findUnique();

            StrateTopiaDao strateDAO =  transaction.getStrateDao();
            SubRegionTopiaDao subRegionDAO =  transaction.getSubRegionDao();

            // Retrieve directly all subRegions to avoid multiple select queries to find subRegions
            List<SubRegion> subRegions = subRegionDAO.forSurveyEquals(survey).findAll();
            Map<Integer, SubRegion> subRegionMap =
                    Maps.newHashMap(Maps.uniqueIndex(subRegions,
                                                     SubRegions.toSubRegionNumber()));

            int result = 0;
            for (Strate strate : strates) {

                boolean isNew = importStrate(strateDAO, subRegionDAO, strate, subRegionMap, survey);

                if (isNew) {
                    result++;
                }
            }

            transaction.commit();

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    protected boolean importTransect(TransectTopiaDao dao,
                                     Transect transect,
                                     Map<String, Strate> strates,
                                     int rowIndex) throws TopiaException {

        boolean result = false;

        // This is the link with the source shape file that contains graphic elements
        int graphicIndex = rowIndex;

        String strateCode = transect.getStrate().getCode();

        // We check the strate, it must exists
        // We can't do that in TransectImportModel during parsing because
        // we need two params : subRegionNumber and strateType to create the
        // strateCode and check the existence. It must be done after parsing
        Strate existStrate = strates.get(strateCode);
        if (existStrate == null) {
            throw new ImportRuntimeException(String.format(
                    "Unable to read line %1$d : The strate %2$s doesn't exist",
                    rowIndex + 1,
                    strateCode)
            );
        }

        Optional<Transect> existTransect = dao.forNaturalId(transect.getName(), existStrate).tryFindUnique();

        // If the existing strate is null, create it otherwise ignore
        if (!existTransect.isPresent()) {

            Transect newTransect = dao.createByNaturalId(transect.getName(), existStrate);
            newTransect.setLength(transect.getLength());
            newTransect.setStartX(transect.getStartX());
            newTransect.setStartY(transect.getStartY());
            newTransect.setEndX(transect.getEndX());
            newTransect.setEndY(transect.getEndY());
            newTransect.setNbTimes(transect.getNbTimes());
            newTransect.setGraphicIndex(graphicIndex);

            if (log.isDebugEnabled()) {
                log.debug("Create new transect " + newTransect.getName() +
                             "(graphicIndex = " + graphicIndex + ")");
            }

        } else {

            existTransect.get().setGraphicIndex(graphicIndex);

            if (log.isWarnEnabled()) {
                log.warn("The transect " + existTransect.get().getName() + " already " +
                            "exists and will be ignored " +
                            "(graphicIndex = " + graphicIndex + ")");
            }
        }
        return !existTransect.isPresent();
    }

    protected boolean importStrate(StrateTopiaDao strateDAO,
                                   SubRegionTopiaDao subRegionDAO,
                                   Strate strate,
                                   Map<Integer, SubRegion> subRegions,
                                   Survey survey) throws TopiaException {

        boolean result = false;

        String code = strate.getCode();
        SubRegion subRegion = strate.getSubRegion();
        int subRegionNumber = subRegion.getSubRegionNumber();

        boolean existStrate;

        // Create subRegion if necessary
        SubRegion existSubRegion = subRegions.get(subRegionNumber);
        if (existSubRegion == null) {

            existSubRegion = subRegionDAO.createByNaturalId(subRegionNumber, survey);
            existSubRegion.setName(subRegion.getName());
            subRegions.put(subRegionNumber, existSubRegion);

            existStrate = false;

        } else {

            // Find existing strate only if subRegion is not created
            existStrate = strateDAO.forNaturalId(code, existSubRegion).exists();
        }

        // If the existing strate is null, create it otherwise ignore
        if (!existStrate) {

            Strate newStrate = strateDAO.createByNaturalId(code, existSubRegion);
            newStrate.setName(strate.getName());
            newStrate.setType(strate.getType());

            result = true;

            if (log.isDebugEnabled()) {
                log.debug("Create new strate " + newStrate.getCode());
            }

        } else {

            if (log.isWarnEnabled()) {
                log.warn("The strate " + strate.getCode() + existSubRegion.getSubRegionNumber()
                             + " already exists and will be ignored");
            }
        }
        return result;
    }
}
