package fr.ulr.sammoa.application.io.input.csv;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverImpl;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;

/**
 * Created: 09/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class ObserverImportModel extends AbstractImportModel<Observer> {

    public ObserverImportModel(final Survey survey) {
        super(';');
        newMandatoryColumn("INITIALS", Observer.PROPERTY_INITIALS);
        newMandatoryColumn("FIRST_NAME", Observer.PROPERTY_FIRST_NAME);
        newMandatoryColumn("LAST_NAME", Observer.PROPERTY_LAST_NAME);
        newMandatoryColumn("ORGANIZATION", Observer.PROPERTY_ORGANIZATION);
        newMandatoryColumn("EMAIL", Observer.PROPERTY_EMAIL);
        newMandatoryColumn("PILOT", Observer.PROPERTY_PILOT, Common.BOOLEAN);
        newMandatoryColumn("SURVEY", Observer.PROPERTY_SURVEY, new ValueParser<Survey>() {

            @Override
            public Survey parse(String s) throws ParseException {
                Preconditions.checkArgument(
                        survey.getCode().equals(s),
                        String.format("The current survey is %1$s and not %2$s",
                                      survey.getCode(), s));
                return survey;
            }
        });
    }

    @Override
    public Observer newEmptyInstance() {
        return new ObserverImpl();
    }
}
