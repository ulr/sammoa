/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ulr.sammoa.application.device.gps;

import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPoints;
import gnu.io.CommPortIdentifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dinopolis.gpstool.gpsinput.GPSDataProcessor;
import org.dinopolis.gpstool.gpsinput.GPSDevice;
import org.dinopolis.gpstool.gpsinput.GPSException;
import org.dinopolis.gpstool.gpsinput.GPSPosition;
import org.dinopolis.gpstool.gpsinput.nmea.SafeGPSNmeaDataProcessor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Implementation de la gestion du GPS sur GPSylon.
 *
 * @author echatellier
 */
public class GpsHandlerGpsylon extends BaseGpsHandler {

    private static final Log log = LogFactory.getLog(GpsHandlerGpsylon.class);

    protected GPSDataProcessor gpsDataProcessor;

    protected GPSDevice gpsDevice;

    protected GPSPosition lastPosition;

    protected Date lastPositionDate;

    protected float lastAltitude;

    protected float lastSpeed;

    protected int lastNumberSatellites;

    protected boolean deviceInitialized;

    protected boolean opened;

    public GpsHandlerGpsylon(GpsConfig config) {
        super(config);
    }

    @Override
    public void open() throws DeviceTechnicalException {

        if (opened) {
            if (log.isTraceEnabled()) {
                log.trace("GPS already opened");
            }
            return;
        }

        try {

            if (!deviceInitialized) {
                gpsDataProcessor = new SafeGPSNmeaDataProcessor();
                gpsDevice = new GPSNRSerialDevice();
                deviceInitialized = true;
                gpsDataProcessor.setGPSDevice(gpsDevice);
            }

            Hashtable<String, Object> options = new Hashtable<>();
            options.put(GPSNRSerialDevice.PORT_NAME_KEY, getConfig().getDevice());
            options.put(GPSNRSerialDevice.PORT_SPEED_KEY, getConfig().getSpeed());

            if (log.isDebugEnabled()) {
                log.debug("GPS options: " + options);
            }
            gpsDevice.init(options);

            logAvailablePorts("beforeOpen");

            gpsDataProcessor.addGPSDataChangeListener(gpsDataProcessorListener);

            gpsDataProcessor.open();
            opened = true;

            logAvailablePorts("openedSuccessful");

        } catch (Throwable ex) { // necessaire si on arrive pas a charger la lib native

            setState(DeviceState.UNAVAILABLE, null);

            throw new DeviceTechnicalException(this, "Can't open GPS device", ex);
        }
    }

    @Override
    public void close() throws DeviceTechnicalException {

        stop();

        if (opened) {

            if (log.isInfoEnabled()) {
                log.info("Closing GPS device...");
            }
            try {

                gpsDataProcessor.removeGPSDataChangeListener(gpsDataProcessorListener);

                gpsDataProcessor.close();

                if (log.isInfoEnabled()) {
                    log.info("GPS device is closed");
                }

                setState(DeviceState.UNAVAILABLE, null);
                opened = false;

            } catch (GPSException e) {
                setState(DeviceState.ERROR, null);
                throw new DeviceTechnicalException(this, "Can't close GPS device", e);
            }
        }
    }

    @Override
    protected GeoPoint getLastLocation() {

        if (log.isTraceEnabled()) {
            log.trace("Ask lastPosition = " +  lastPosition);
        }

        double latitude = lastPosition != null
                          ? lastPosition.getLatitude()
                          : GeoPoints.EMPTY_COORDINATE;

        double longitude = lastPosition != null
                           ? lastPosition.getLongitude()
                           : GeoPoints.EMPTY_COORDINATE;

        GeoPoint result = new GeoPointImpl(latitude, longitude);
        result.setAltitude(lastAltitude);
        result.setSpeed(lastSpeed);
        result.setRecordTime(lastPositionDate);

        return result;
    }

    @Override
    protected DeviceTechnicalException onError(GeoPoint location, boolean offLine) {

        DeviceTechnicalException result = null;

        logAvailablePorts("checkIsOpened");

        // TODO checkIsOpened
        // Send error when device is connected :
        // RXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyUSB0: File exists
        // No error when device is disconnected
        // --> close handler
        // Maybe with chance on next plug we will keep the same PORT

        if (log.isDebugEnabled()) {
            log.debug("ERROR on GPS, nbSatellites = " + lastNumberSatellites + " : geoPoint = " + location);
        }

        if (!opened) {
            start();

        } else if (offLine) {

            result = new DeviceTechnicalException(
                    this, "GPS signal lost, please check the connection port");

        } else if (GeoPoints.isCoordinatesEmpty(location)) {

            result = new DeviceTechnicalException(
                    this, "GPS is not ready, turn it on or wait for it to find satellites");

        } else if (lastNumberSatellites == 0) {

            result = new DeviceTechnicalException(
                    this, "GPS signal lost, there is no available satellite found");


        }
        return result;
    }

    protected void logAvailablePorts(String method) {

        if (log.isDebugEnabled()) {

            log.debug("GPS configuration port " + config.getDevice() + " [" + method + "]");

            Enumeration portList = CommPortIdentifier.getPortIdentifiers();

            while (portList.hasMoreElements()) {
                CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
                if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    log.debug("## Available SERIAL port : " +  portId.getName());
                }
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    /**
     * Si l'on recoit des evenements, c'est que le gps fonctionne.
     * Il sont envoyé par le traitement des flux NMEA et propagé par le
     * {@code GPSDataChangeListener}.
     * <p/>
     * Properties available:
     * <ul>
     * <li>LOCATION: the value is a GPSPosition object
     * <li>HEADING: the value is a Float
     * <li>SPEED: the value is a Float and is in kilometers per hour
     * <li>NUMBER_SATELLITES: the value is a Integer
     * <li>ALTITUDE: the value is a Float and is in meters
     * <li>SATELLITE_INFO: the value is a SatelliteInfo object.
     * <li>DEPTH: the value is a Float and is in meters.
     * <li>EPE: estimated position error, the value is a GPSPositionError object.
     * <li>IDS_SATELLITES: An array of Integers holding the number of visible satellites.
     * <li>PDOP: a Float indicating the quality of the gps signal.
     * <li>HDOP: a Float indicating the quality of the gps signal in horizontal direction.
     * <li>VDOP: a Float indicating the quality of the gps signal in vertical direction.
     * </ul>
     */
    protected PropertyChangeListener gpsDataProcessorListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            String property = evt.getPropertyName();

            if (log.isDebugEnabled()) {
                log.debug("Gps data received " + property + " " + evt.getNewValue());
            }

            if (state == DeviceState.UNAVAILABLE) {
                if (log.isInfoEnabled()) {
                    log.info("Connected to GPS device");
                }

                setState(DeviceState.READY, null);
            }

            if (GPSDataProcessor.LOCATION.equals(property)) {
                lastPositionDate = getDate();
                lastPosition = (GPSPosition) evt.getNewValue();

            } else if (GPSDataProcessor.ALTITUDE.equals(property)) {
                lastAltitude = (Float) evt.getNewValue();

            } else if (GPSDataProcessor.SPEED.equals(property)) {
                lastSpeed = (Float) evt.getNewValue();

            } else if (GPSDataProcessor.NUMBER_SATELLITES.equals(property)) {
                lastNumberSatellites = (Integer) evt.getNewValue();
            }

            lastEventTime = System.currentTimeMillis();
        }
    };
}
