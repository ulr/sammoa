package fr.ulr.sammoa.application.flightController;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.TransectFlight;

/**
 * Listener for all change of data in the {@link FlightController}
 * <p/>
 * Created: 12/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @see FlightController
 */
public interface FlightControllerListener {

    /**
     * Fired when a the current {@code route} has changed. It could be a
     * new created route.
     *
     * @param event The event containing route change
     */
    void onCurrentRouteChanged(RouteEvent event);

    /**
     * Fired when the next transect has been updated.
     *
     * @param nextTransect The next transect that will be used on
     *                     {@link FlightController#begin()} action
     */
    void onNextTransectChanged(TransectFlight nextTransect);

    /**
     * Fired when controller change the {@code state} of the flight after
     * each action.
     *
     * @param state The new state
     */
    void onStateChanged(FlightState state);

    /**
     * Fired when controller add a new observation
     *
     * @param event The event containing Observation and its associated GeoPoint
     */
    void onObservationAdded(ObservationEvent event);

    /**
     * Fired when a timer value changes
     *
     * @param timer the timer that changed
     * @param value the new value
     */
    void onTimerChanged(String timer, int value);
}
