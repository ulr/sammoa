package fr.ulr.sammoa.application.io.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created: 07/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DbfTableModelBuilder {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DbfTableModelBuilder.class);

    public static final int DEFAULT_STRING_LENGTH = 64;

    public static final int DEFAULT_INTEGER_LENGTH = 8;

    public static final String DATE_FORMAT_PATTERN = "yyyyMMdd";

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    protected List<DbfColumnModel> columns;

    protected Map<String, DbfColumnModel> columnMap;

    protected DbfTableModel model;

    public DbfTableModelBuilder() {
        columns = Lists.newArrayList();
    }

    public DbfTableModelBuilder dbfColumn(DbfColumnModel column) {
        if (column.getIndex() != -1) {
            columns.add(column.getIndex(), column);
        } else {
            column.setIndex(columns.size());
            columns.add(column);
        }
        return this;
    }

    public DbfColumnModel newStringColumn(String name) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_CHARACTER);
        result.setLength(DEFAULT_STRING_LENGTH);
        return result;
    }

    public DbfColumnModel newIntegerColumn(String name) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_NUMERIC);
        result.setLength(DEFAULT_INTEGER_LENGTH);
        return result;
    }

    public DbfColumnModel newDoubleColumn(String name, int length, int decimalCount) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_NUMERIC);
        result.setLength(length);
        result.setDecimalCount(decimalCount);
        return result;
    }

    public DbfColumnModel newTimestampColumn(String name) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_TIMESTAMP);
        return result;
    }

    public DbfColumnModel newDateColumn(String name) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_DATE);
        result.setLength(DATE_FORMAT_PATTERN.length());
        return result;
    }

    public DbfColumnModel newBooleanColumn(String name) {
        DbfColumnModel result = new DbfColumnModel(name, DbfTableModel.TYPE_LOGICAL);
        result.setLength(1);
        return result;
    }

    public DbfTableModelBuilder stringColumn(String name) {
        return dbfColumn(newStringColumn(name));
    }

    public DbfTableModelBuilder booleanColumn(String name) {
        return dbfColumn(newBooleanColumn(name));
    }

    public DbfTableModelBuilder integerColumn(String name) {
        return dbfColumn(newIntegerColumn(name));
    }

    public DbfTableModelBuilder doubleColumn(String name, int length, int decimalCount) {
        return dbfColumn(newDoubleColumn(name, length, decimalCount));
    }

    public DbfTableModelBuilder timestampColumn(String name) {
        return dbfColumn(newTimestampColumn(name));
    }

    public DbfTableModelBuilder dateColumn(String name) {
        return dbfColumn(newDateColumn(name));
    }

    public DbfTableModelBuilder build() {
        model = new DbfTableModel(columns.size());
        model.setWritable(true);

        for (int i = 0; i < columns.size(); i++) {
            DbfColumnModel columnModel = columns.get(i);
            model.setColumnName(i, columnModel.getName());
            model.setType(i, columnModel.getType());
            model.setLength(i, columnModel.getLength());
            model.setDecimalCount(i, (byte) columnModel.getDecimalCount());
        }
        columnMap = Maps.uniqueIndex(columns,
                                     new Function<DbfColumnModel, String>() {

                                         @Override
                                         public String apply(DbfColumnModel input) {
                                             return input.getName();
                                         }
                                     });
        return this;
    }

    public DbfTableModel getModel() {
        return model;
    }

    public void setValue(int rowIndex, String columnName, Object value) {
        Preconditions.checkState(model != null, "Model must be built before setting values");

        DbfColumnModel column = columnMap.get(columnName);

        int columnIndex = column.getIndex();
        value = cleanValue(value, column.getType());
        model.setValueAt(value, rowIndex, columnIndex);
    }

    public int addValues(Map<String, Object> values) {
        Preconditions.checkState(model != null, "Model must be built before adding values");

        model.addBlankRecord();
        int rowIndex = model.getRowCount() - 1;
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            setValue(rowIndex, entry.getKey(), entry.getValue());
        }
        return rowIndex;
    }

    protected Object cleanValue(Object value, byte type) {

        Object result = value;
        if (DbfTableModel.isNumericalType(type)) {

            // FIX for Integer Format error in DbfTableModel#getStringForType
            if (value instanceof Integer) {
                result = Double.valueOf(String.valueOf(value));
            }
        } else if (DbfTableModel.TYPE_TIMESTAMP == type) {

            // FIX for Date : no format is done
            if (value instanceof Date) {
                Date date = (Date) value;

                result = DbfTimestampConverter.toString(date);
            }
        } else if (DbfTableModel.TYPE_DATE == type) {

            // FIX for Date : no format is done
            if (value instanceof Date) {
                Date date = (Date) value;

                result = DATE_FORMAT.format(date);
            }
        } else if (DbfTableModel.TYPE_LOGICAL == type) {
            result = ((Boolean) value) ? "T" : "F";
        } else if (value instanceof Enum) {
            result = ((Enum) value).name();
        } else if (value instanceof Character) {
            result = String.valueOf(value);
        }

        if (result != null &&
            !(result instanceof String) &&
            !(result instanceof Double)) {
            if (log.isWarnEnabled()) {
                log.warn("Incompatible data value " + result + " (type " + type + ")");
            }
        }
        return result;
    }

}
