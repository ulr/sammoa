package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.csv.ValueParser;

import java.text.ParseException;

/**
 * Created: 26/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class DoubleToIntegerValueParser implements ValueParser<Integer> {

    @Override
    public Integer parse(String s) throws ParseException {
        Double value = Double.parseDouble(s);
        return value.intValue();
    }
}
