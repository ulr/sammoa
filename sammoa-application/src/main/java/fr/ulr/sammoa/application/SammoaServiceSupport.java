package fr.ulr.sammoa.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.SammoaStorages;
import fr.ulr.sammoa.persistence.SammoaPersistence;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.apache.commons.io.FileUtils;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;

/**
 * Sammoa service support.
 * <p/>
 * This simple implementation of {@link SammoaService}, offer to keep the
 * {@link #context}, {@link #config} and {@link #persistence} objects.
 * <p/>
 * <strong>Note:</strong> should be used for any service! and never
 * instanciated by hand but via {@link SammoaContext#getService(Class)} method.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SammoaServiceSupport implements SammoaService {

    protected SammoaContext context;

    protected SammoaConfig config;

    protected SammoaPersistence persistence;

    @Override
    public void setSammoaContext(SammoaContext context) {
        this.context = context;
        this.persistence = context.getPersistence();
        this.config = context.getConfig();
    }

    public <S extends SammoaService> S getService(Class<S> serviceType) {
        return context.getService(serviceType);
    }

    public SurveyStorage getSurveyStorage(String surveyId) {
        File dataDirectory = config.getDataDirectory();
        File directory = SammoaDirectories.getSurveyDirectory(dataDirectory);
        SurveyStorage surveyStorage = SammoaStorages.getSurveyStorage(
                directory, surveyId);
        return surveyStorage;
    }

    protected SammoaTopiaPersistenceContext beginTransaction() {
        SammoaTopiaPersistenceContext sammoaTopiaPersistenceContext = persistence.beginTransaction();
        return sammoaTopiaPersistenceContext;
    }

    protected void endTransaction(SammoaTopiaPersistenceContext tx) {
        persistence.endTransaction(tx);
    }

    @Override
    public void close() throws IOException {
        // by default nothing to close
    }

    protected void compressZipFile(File zipFile, File directory) {

        try {
            FileUtils.forceMkdir(zipFile.getParentFile());

            try {
                ZipUtil.compress(zipFile, directory);
            } finally {
                FileUtils.deleteDirectory(directory);

            }
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not compress directory", e);
        }
    }
}
