package fr.ulr.sammoa.application.io.input.csv;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.io.Files;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverTopiaDao;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.RegionTopiaDao;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.SpeciesTopiaDao;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyTopiaDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * To do csv imports.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportCsvService extends SammoaServiceSupport {

    private static final Log log =
            LogFactory.getLog(ImportCsvService.class);

    public int importSpecies(String regionId, File file) throws IOException {

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {
            int result = 0;

            SammoaTopiaPersistenceContext transaction = beginTransaction();
            try {

                RegionTopiaDao regionDAO =  transaction.getRegionDao();
                Region region = regionDAO.forTopiaIdEquals(regionId).findUnique();
                String regionCode = region.getCode();

                SpeciesTopiaDao dao =  transaction.getSpeciesDao();

                SpeciesImportModel model = new SpeciesImportModel(region);

                Import<Species> importCsv = Import.newImport(model, reader);

                for (Species species : importCsv) {

                    boolean isNew = importSpecies(dao, species, regionCode);

                    if (isNew) {
                        result++;
                    }
                }

                transaction.commit();

            } finally {
                endTransaction(transaction);
            }
            return result;
        }
    }

    public int importObservers(String surveyId, File file) throws IOException {

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {
            int result = 0;
            SammoaTopiaPersistenceContext transaction = beginTransaction();
            try {

                SurveyTopiaDao surveyDAO =  transaction.getSurveyDao();
                Survey survey = surveyDAO.forTopiaIdEquals(surveyId).findUnique();
                String surveyCode = survey.getCode();

                ObserverTopiaDao dao =  transaction.getObserverDao();

                ObserverImportModel model = new ObserverImportModel(survey);

                Import<Observer> importCsv = Import.newImport(model, reader);

                for (Observer observer : importCsv) {

                    boolean isNew = importObserver(dao, observer, surveyCode);

                    if (isNew) {
                        result++;
                    }
                }

                transaction.commit();

            } finally {
                endTransaction(transaction);
            }
            return result;
        }
    }

    protected boolean importSpecies(SpeciesTopiaDao dao,
                                    Species species,
                                    String regionCode) throws TopiaException {
        // while importing a species, it does become valid
        species.setLocalCreation(false);

        Optional<Species> speciesExists = dao.forNaturalId(
                species.getCode(),
                species.getRegion()).tryFindUnique();

        if (!speciesExists.isPresent()) {

            dao.create(species);

            if (log.isDebugEnabled()) {
                log.debug(
                        "Create new species " + species.getCode() + " for region " + regionCode);
            }

        } else {

            speciesExists.get().setCommonName(species.getCommonName());
            speciesExists.get().setLatinName(species.getLatinName());
            speciesExists.get().setGroupName(species.getGroupName());
            speciesExists.get().setFamily(species.getFamily());
            speciesExists.get().setType(species.getType());
            dao.update(speciesExists.get());

            if (log.isInfoEnabled()) {
                log.info("The species " + species.getCode() + " already exists and is updated");
            }
        }

        return !speciesExists.isPresent();
    }

    protected boolean importObserver(ObserverTopiaDao dao,
                                     Observer observer,
                                     String surveyCode) throws TopiaException {

        Optional<Observer> observerExists = dao.forNaturalId(
                observer.getInitials(),
                observer.getSurvey()).tryFindUnique();

        if (!observerExists.isPresent()) {

            dao.create(observer);

            if (log.isDebugEnabled()) {
                log.debug("Create new observer " + observer.getInitials() + "  for survey " + surveyCode);
            }

        } else {
            observerExists.get().setEmail(observer.getEmail());
            observerExists.get().setFirstName(observer.getFirstName());
            observerExists.get().setLastName(observer.getLastName());
            observerExists.get().setOrganization(observer.getOrganization());
            observerExists.get().setPilot(observer.isPilot());
            dao.update(observerExists.get());

            if (log.isInfoEnabled()) {
                log.info("The observer " + observer.getInitials() + " already exists and is updated");
            }
        }
        return !observerExists.isPresent();
    }
}
