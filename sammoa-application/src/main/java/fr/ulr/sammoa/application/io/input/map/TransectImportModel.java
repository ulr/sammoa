package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.SubRegionImpl;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StrateImpl;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectImpl;
import org.nuiton.csv.Common;
import org.nuiton.csv.ext.AbstractImportModel;

/**
 * <pre>
 * - colonne 0 : "TR_ID" : Identifiant et nom du transect (transect.name)
 * - colonne 1 : "Xstart" : Coordonnée X de début du transect (transect.startX)
 * - colonne 2 : "Ystart" : Coordonnée Y de début du transect (transect.startY)
 * - colonne 3 : "Xend" : Coordonnée X de fin du transect (transect.endX)
 * - colonne 4 : "Yend" : Coordonnée Y de fin du transect (transect.endY)
 * - colonne 5 : "Length" : longueur du transect (transect.length)
 * - colonne 6 : "Passage" : Numéro de passage (transect.nbTimes)
 * - colonne 7 : "SECTEUR" : Numéro du secteur (transect.strate.subRegion.subRegionNumber)
 * - colonne 8 : "STRATE" : Type de strate (transect.strate.strateType)
 *
 *      * C : COAST (Côte)
 *      * N : NERITIC (Néritique)
 *      * P : SLOPE (Pente)
 *      * O : OCEANIC (Océanique)
 *
 * </pre>
 *
 * Created: 25/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class TransectImportModel extends AbstractImportModel<Transect> {

    public TransectImportModel() {
        super(';');
        newMandatoryColumn("transect", Transect.PROPERTY_NAME);
        newMandatoryColumn("subRegion",
                Transect.PROPERTY_STRATE + "."
                        + Strate.PROPERTY_SUB_REGION + "."
                        + SubRegion.PROPERTY_NAME
        );
        newMandatoryColumn("strate",
                Transect.PROPERTY_STRATE + "."
                        + Strate.PROPERTY_CODE
        );
        newMandatoryColumn("xStart",
                           Transect.PROPERTY_START_X,
                           Common.DOUBLE_PRIMITIVE
        );
        newMandatoryColumn("yStart",
                           Transect.PROPERTY_START_Y,
                           Common.DOUBLE_PRIMITIVE
        );
        newMandatoryColumn("xEnd",
                           Transect.PROPERTY_END_X,
                           Common.DOUBLE_PRIMITIVE
        );
        newMandatoryColumn("yEnd",
                           Transect.PROPERTY_END_Y,
                           Common.DOUBLE_PRIMITIVE
        );
        newMandatoryColumn("length",
                Transect.PROPERTY_LENGTH,
                Common.DOUBLE_PRIMITIVE
        );
        newMandatoryColumn("passage",
                Transect.PROPERTY_NB_TIMES,
                new DoubleToIntegerValueParser()
        );
    }

    @Override
    public Transect newEmptyInstance() {
        Transect result = new TransectImpl();
        Strate strate = new StrateImpl();
        result.setStrate(strate);
        strate.setSubRegion(new SubRegionImpl());
        return result;
    }

}
