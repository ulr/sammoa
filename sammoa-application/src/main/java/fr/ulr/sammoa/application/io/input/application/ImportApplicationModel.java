package fr.ulr.sammoa.application.io.input.application;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.io.SurveyStorage;

import java.util.List;
import java.util.Map;

/**
 * Model of application import.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportApplicationModel {


    /**
     * flight id to import by survey storage.
     */
    protected Map<SurveyStorage, List<String>> flightIdsToImport = Maps.newHashMap();

    /**
     * flight id to import by survey storage.
     */
    protected Map<SurveyStorage, List<String>> flightIdsToRemove = Maps.newHashMap();

    public void addFlightToImport(SurveyStorage storage, String flightId) {
        List<String> flights = flightIdsToImport.computeIfAbsent(storage, s -> Lists.newArrayList());
        flights.add(flightId);

    }

    public void addFlightToRemove(SurveyStorage storage, String flightId) {
        List<String> flights = flightIdsToRemove.computeIfAbsent(storage, s -> Lists.newArrayList());
        flights.add(flightId);
    }

    public Map<SurveyStorage, List<String>> getFlightIdsToImport() {
        return flightIdsToImport;
    }

    public Map<SurveyStorage, List<String>> getFlightIdsToRemove() {
        return flightIdsToRemove;
    }
}
