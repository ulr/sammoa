/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device.gps;

import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.persistence.GeoPoint;

import java.util.Set;

/** Représente l'accès à un périphérique GPS. */
public interface GpsHandler extends DeviceManager {

    /**
     * Retourne la position courante du GPS.
     *
     * @return la position courante selon le GPS ou {@code null} si elle ne peut
     *         pas être obtenue
     */
    GeoPoint getCurrentLocation();

    /** @param gpsLocationListener GpsLocationListener to add */
    void addGpsLocationListener(GpsLocationListener gpsLocationListener);

    /** @param gpsLocationListener GpsLocationListener to remove */
    void removeGpsLocationListener(GpsLocationListener gpsLocationListener);

    /** @return all GpsLocationListener */
    Set<GpsLocationListener> getGpsLocationListeners();

    /** @param config the new configuration to use when reopening connection*/
    void setConfig (GpsConfig config);
}
