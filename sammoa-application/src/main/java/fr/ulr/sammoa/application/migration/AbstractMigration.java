package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public abstract class AbstractMigration {

    protected final Version version;

    protected final String description;

    protected File dataDirectory;

    protected SammoaTopiaPersistenceContext persistenceContext;

    protected MigrationRequester requester;

    public AbstractMigration(String version, String description) {
        this.version = VersionBuilder.create(version).build();
        this.description = description;
    }

    public Version getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public File getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(File dataDirectory) {
        this.dataDirectory = dataDirectory;
    }

    public MigrationRequester getRequester() {
        return requester;
    }

    public void setRequester(MigrationRequester requester) {
        this.requester = requester;
    }

    public CsvMigrate loadCvs(String path) throws IOException {
        return loadCvs(dataDirectory.toPath().resolve(path));
    }

    public CsvMigrate loadCvs(Path path) throws IOException {
        CsvMigrate csvMigrate = new CsvMigrate(path);
        csvMigrate.load();
        return csvMigrate;
    }

    public abstract File migrateImportFile() throws IOException;

    public abstract void migrateLocalDirectory() throws IOException;

    @Override
    public String toString() {
        return version + " : " + description;
    }

    public void setPersistenceContext(SammoaTopiaPersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
    }

    protected boolean isLocalMigration() {
        return persistenceContext != null;
    }

    protected boolean isImportFileMigration() {
        return ! isLocalMigration();
    }

    public SammoaTopiaPersistenceContext getPersistenceContext() {
        return persistenceContext;
    }

    protected <V> V ask(String title, String question, V[] values, V defaultValue) {
       return requester.ask(title, question, values, defaultValue);
    }
}
