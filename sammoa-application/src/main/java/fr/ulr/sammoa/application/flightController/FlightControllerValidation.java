package fr.ulr.sammoa.application.flightController;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created: 21/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class FlightControllerValidation extends BaseFlightController {

    private static final Log log = LogFactory.getLog(FlightControllerValidation.class);

    protected List<GeoPoint> geoPoints;

    protected List<Route> routes;

    @Override
    public <T extends DeviceManager> void openDeviceManager(Class<T> deviceManager) {
        getDeviceManagerProvider().openDeviceManager(deviceManager, false);
    }

    @Override
    public void init(Flight flight) {

        AudioReader audioReader = getAudioReader();
        Preconditions.checkNotNull(audioReader, "You must open the audioReader device before init");

        super.init(flight);

        initCurrentRoute(null);

        geoPoints = service.getFlightGeoPoints(flight);
        routes = service.getRoutes(flight);
    }

    @Override
    protected GeoPoint getLocation(SammoaTopiaPersistenceContext tx) throws TopiaException {

        Preconditions.checkNotNull(currentRoute, "You must set the current route to retrieve location");
        Preconditions.checkState(!geoPoints.isEmpty(), "No geoPoints available");

        if (log.isDebugEnabled()) {
            log.debug(String.format("Get location after startTime %1$tH:%1$tM:%1$tS.%1$tL (audio position = %2$d)",
                                       getAudioReader().getStartDate(),
                                       getAudioReader().getPosition())
            );
        }

        Date positionDate = getAudioReader().getPositionDate();

        DateTime newTime;
        if (positionDate == null) {
            newTime = Dates.toDateTime(currentRoute.getBeginTime());

        } else {
            newTime = Dates.toDateTime(positionDate).withMillisOfSecond(0);
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format("Get location at newTime %1$tH:%1$tM:%1$tS.%1$tL",
                                       newTime.toDate())
            );
        }

        // Retrieve the appropriate location
        GeoPoint result = service.getGeoPoint(tx, flight, geoPoints, newTime.toDate());

//        GeoPoint location = GeoPoints.getClosestPoint(geoPoints, newTime.toDate());
//        DateTime locationTime = Dates.toDateTime(location.getRecordTime());
//
//        if (log.isDebugEnabled()) {
//            log.debug(String.format("Find locationTime %1$tH:%1$tM:%1$tS.%1$tL",
//                                       locationTime.toDate())
//            );
//        }
//
//        // Create a new location if no one is available for the newTime
//        if (!locationTime.isEqual(newTime)) {
//
//            result = new GeoPointImpl(location.getLatitude(), location.getLongitude());
//            result.setSpeed(location.getSpeed());
//            result.setAltitude(location.getAltitude());
//
//            int captureDelay = Dates
//                    .toInterval(locationTime, newTime)
//                    .toDuration()
//                    .toStandardSeconds()
//                    .getSeconds();
//
//            result.setCaptureDelay(captureDelay);
//
//            result.setFlight(flight);
//            result.setRecordTime(newTime.toDate());
//            SammoaDAOHelper.getGeoPointDAO(tx).create(result);
//            geoPoints.add(result);
//
//            if (log.isDebugEnabled()) {
//                log.debug("Create a new GeoPoint : " +  result);
//            }
//
//        } else {
//            result = location;
//        }
        return result;
    }


    @Override
    protected void onRouteAdded(Route previousRoute, Route newRoute) {

        if (log.isDebugEnabled()) {
            log.debug("Add route " + Routes.toString(newRoute) + " to cache");
        }
        routes.add(newRoute);

        // Ensure previous, it could be different from argument because
        // the audio time could overflow on more than one route
        previousRoute = Routes.findPrevious(routes, newRoute);

        super.onRouteAdded(previousRoute, newRoute);
    }

    protected AudioReader getAudioReader() {
        return getDeviceManager(AudioReader.class);
    }

    public void setAudioReaderPositionDate(Date time) {
        AudioReader audioReader = getAudioReader();

        Pair<Date, File> audioFileAtTime = flightStorage.getAudioFileAtTime(time, audioReader.getOutputType().getExtension());

        if (audioFileAtTime == null) {
            audioReader.unload();
        } else {
            if (!audioFileAtTime.getValue().equals(audioReader.getFileToRead())) {
                audioReader.load(audioFileAtTime.getValue(), audioFileAtTime.getKey());
            }

            // Change position only if AudioReader is ready (not running)
            if (DeviceState.READY == audioReader.getState()) {
                audioReader.setPositionDate(time);
            }
        }

    }
}
