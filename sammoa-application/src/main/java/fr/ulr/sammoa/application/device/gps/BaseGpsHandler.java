/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device.gps;

import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPoints;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Base class for {@link GpsHandler} interface. This class contains common behaviour
 * for automatic check on a given {@code period}. For that, this class contains
 * a {@link TimerTask} scheduled from {@link #start()}. The {@link #open()} method
 * will be called on start if state is still {@link DeviceState#UNAVAILABLE}.
 * The method {@link #getCurrentLocation()} must return null if the location doesn't
 * change or the GPS is down.
 * <p/>
 * Created: 02/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @see FakeGpsHandler
 * @see GpsHandlerGpsylon
 * @since 0.2
 */
abstract class BaseGpsHandler implements GpsHandler {

    private static final Log log =
            LogFactory.getLog(BaseGpsHandler.class);

    protected Set<DeviceStateListener> deviceStateListeners;

    protected Set<GpsLocationListener> gpsLocationListeners;

    /** To test if connection to device is effective. */
    protected long lastEventTime;

    protected DeviceState state;

    protected GpsConfig config;

    protected ScheduledExecutorService executorService =
            Executors.newScheduledThreadPool(1);

    protected Future future;

    /**
     * Value to check the number of timer update failures before the DeviceState
     * becomes {@link DeviceState#ERROR}
     */
    protected long maxLastEventDelays;

    public BaseGpsHandler(GpsConfig config) {
        this.config = config;
        this.state = DeviceState.UNAVAILABLE;
        this.deviceStateListeners = Sets.newHashSet();
        this.gpsLocationListeners = Sets.newHashSet();
        this.maxLastEventDelays = config.getTimeout();

        if (log.isDebugEnabled()) {
            log.debug("GPS maxLastEventDelays = " + maxLastEventDelays + " second");
        }
    }

    public GpsConfig getConfig() {
        return config;
    }

    @Override
    public DeviceState getState() {
        return state;
    }

    @Override
    public void start() throws DeviceTechnicalException {

        if (getState() == DeviceState.READY || (getState() == DeviceState.RUNNING && future == null)) {

            if (log.isDebugEnabled()) {
                log.debug("Start scheduling GpsHandler to retrieve data every "
                             + config.getCheckPeriod() + " seconds");
            }
            future = executorService.scheduleAtFixedRate(recordTask, 0, config.getCheckPeriod(), TimeUnit.SECONDS);
            state = DeviceState.RUNNING;

        } else {
            future = executorService.scheduleAtFixedRate(openTask,  0, config.getCheckPeriod(), TimeUnit.SECONDS);
        }
    }

    @Override
    public void stop() {
        if (future != null) {
            future.cancel(true);
            future = null;
            executorService.shutdown();
        }
        if (state == DeviceState.RUNNING) {
            setState(DeviceState.READY, null);

        } else if (state == DeviceState.ERROR) {
            setState(DeviceState.UNAVAILABLE, null);
        }
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener deviceStateListener) {
        deviceStateListeners.add(deviceStateListener);
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener deviceStateListener) {
        deviceStateListeners.remove(deviceStateListener);
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return deviceStateListeners;
    }

    @Override
    public void addGpsLocationListener(GpsLocationListener gpsLocationListener) {
        gpsLocationListeners.add(gpsLocationListener);
    }

    @Override
    public void removeGpsLocationListener(GpsLocationListener gpsLocationListener) {
        gpsLocationListeners.remove(gpsLocationListener);
    }

    @Override
    public Set<GpsLocationListener> getGpsLocationListeners() {
        return gpsLocationListeners;
    }

    @Override
    public GeoPoint getCurrentLocation() {

        Date currentDate = getDate();

        GeoPoint lastLocation = getLastLocation();

        long lastEventDelaysMS = System.currentTimeMillis() - lastEventTime;

        GeoPoint result = new GeoPointImpl(lastLocation.getLatitude(), lastLocation.getLongitude());
        result.setAltitude(lastLocation.getAltitude());
        result.setSpeed(lastLocation.getSpeed());
        result.setRecordTime(currentDate);

        if (lastLocation.getRecordTime() != null) {
            long captureDelay = Dates
                    .toInterval(lastLocation.getRecordTime(), currentDate)
                    .toDuration()
                    .getStandardSeconds();

            if (captureDelay > maxLastEventDelays || lastEventDelaysMS > maxLastEventDelays * 1000) {

                result.setLatitude(GeoPoints.EMPTY_COORDINATE);
                result.setLongitude(GeoPoints.EMPTY_COORDINATE);
                result.setAltitude(0);
                result.setSpeed(0);
                result.setCaptureDelay(0);

            } else {

                result.setCaptureDelay((int) captureDelay);

            }

            if (log.isTraceEnabled()) {
                log.trace("CaptureDelay = " + captureDelay + " for GeoPoint " + result);
            }
        }

        if (log.isWarnEnabled() && GeoPoints.isCoordinatesEmpty(result)) {
            log.warn("Retrieve a location without any coordinates : " +  result);
        }
        return result;
    }

    @Override
    public void setConfig(GpsConfig config){
        this.config = config;
    }

    protected Date getDate() {
        // FIXME-fdesbois-2012-07-02 : ensure time with GPS value and not system timestamp
        Date result = Dates.newDateWithoutMillis();
        return result;
    }

    protected abstract GeoPoint getLastLocation();

    public void setState(DeviceState state, DeviceTechnicalException error) {
        DeviceState oldValue = getState();
        this.state = state;

        // Fire on listeners
        DeviceStateEvent event = new DeviceStateEvent(this, oldValue, state);
        event.setError(error);
        for (DeviceStateListener listener : deviceStateListeners) {
            listener.stateChanged(event);
        }
    }

    protected Runnable openTask = new Runnable() {

        @Override
        public void run() {
            try {

                open();

                future.cancel(true);
                future = null;

                start();

            } catch (DeviceTechnicalException ex) {

                // We use ERROR, because this task is during start
                setState(DeviceState.ERROR, ex);

                if (log.isErrorEnabled()) {
                    log.error("Can't open GPS device. Try again in "
                                 + config.getCheckPeriod() + " seconds", ex);
                }
            }
        }
    };

    protected Runnable recordTask = new Runnable() {
        protected GeoPoint location;

        protected GeoPoint lastLocation;

        @Override
        public void run() {

            location = getCurrentLocation();

            boolean sameLocation = GeoPoints.equal(location, lastLocation);

            long lastEventDelaysMS = System.currentTimeMillis() - lastEventTime;

            if (log.isTraceEnabled()) {
                log.trace("sameLocation = " + sameLocation + " [" + location + " :: " + lastLocation + "], lastEventDelays= " + lastEventDelaysMS + " ms");
            }

            boolean offLine = lastEventDelaysMS > maxLastEventDelays * 1000;

            // Data is unavailable
            if (GeoPoints.isCoordinatesEmpty(location) || offLine) {

                DeviceTechnicalException error = onError(location, offLine);

                setState(DeviceState.ERROR, error);

                location.setLatitude(GeoPoints.EMPTY_COORDINATE);
                location.setLongitude(GeoPoints.EMPTY_COORDINATE);
                location.setAltitude(0);
                location.setSpeed(0);
                location.setCaptureDelay(0);

                // GPS is recording
            } else {

                setState(DeviceState.RUNNING, null);
            }

            // Fire events for location change
            for (GpsLocationListener listener : gpsLocationListeners) {
                listener.locationChanged(new GpsLocationEvent(BaseGpsHandler.this, lastLocation, location));
            }

            lastLocation = location;
        }
    };

    protected abstract DeviceTechnicalException onError(GeoPoint location, boolean offline);
}
