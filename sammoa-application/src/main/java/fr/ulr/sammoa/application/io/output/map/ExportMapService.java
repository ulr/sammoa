package fr.ulr.sammoa.application.io.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.bbn.openmap.dataAccess.shape.EsriGraphicList;
import com.bbn.openmap.dataAccess.shape.EsriPointList;
import com.bbn.openmap.dataAccess.shape.EsriShapeExport;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMPoint;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPoints;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Side;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.nuiton.util.FileUtil;
import org.nuiton.util.TimeLog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Export map service
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class ExportMapService extends SammoaServiceSupport {

    private static final Log log =
            LogFactory.getLog(ExportMapService.class);

    private static final TimeLog timeLog = new TimeLog(ExportMapService.class);

    protected SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

//    protected final DateFormat timeFormat = new SimpleDateFormat("HHmmss");

    public int exportEffortsMap(ExportMapModel dataModel) {
        Preconditions.checkNotNull(dataModel);
        Preconditions.checkNotNull(dataModel.getSurvey());
        Preconditions.checkNotNull(dataModel.getBeginDate());
        Preconditions.checkNotNull(dataModel.getEndDate());

        if (log.isInfoEnabled()) {
            log.info("Start EffortsMap export to " +
                        dataModel.getExportFileWithoutExtension(""));
        }

        long startTime = TimeLog.getTime();

        int nbRow = 0;

        // *  uniquement pour un parcours de type LEG, sinon la cellule sera vide
        // ** uniquement pour un parcours de type CIRCLE_BACK, sinon la cellule sera vide
        DbfTableModelBuilder builder = new DbfTableModelBuilder()
                .stringColumn("region") // (region.code)
                .stringColumn("survey") // (survey.code)
                .stringColumn("subRegion") // (route.transectFlight.transect.strate.subRegion.name)
                .stringColumn("strate") // * (route.transectFlight.transect.strate.code)
                .stringColumn("strateType") // * (route.transectFlight.transect.strate.strateType.code)
                .stringColumn("transect") // * (route.transectFlight.transect.name)
                .integerColumn("passage") // * (route.transectFlight.crossingNumber)
                .integerColumn("flight") // (route.flight.flightNumber)
                .stringColumn("computer") // (route.flight.systemId)
                .stringColumn("routeType") // (route.routeType.name)
                .stringColumn("effortGrp") // * (voir doc)
                .stringColumn("effort") // * (voir doc)
                .stringColumn("status") // * (voir doc)
                .dateColumn("date") // (route.beginTime)
                .stringColumn("hhmmss") // (format(route.beginTime, "HHmmss"))
                .integerColumn("seaState") // (route.seaState)
                .integerColumn("swell") // (route.swell)
                .integerColumn("turbidity") // (route.turbidity)
                .stringColumn("skyGlint") // (route.skyGlint)
                .stringColumn("glareFrom") // (route.glareFrom)
                .stringColumn("glareTo") // (route.glareTo)
                .integerColumn("glareSeverity") // (route.glareSeverity)
                .booleanColumn("glareUnder") // (route.glareUnder)
                .integerColumn("cloudCover") // (route.cloudCover)
                .stringColumn("subjective") // (route.subjectiveConditions)
                .stringColumn("unexpLeft") // (route.unexpectedLeft)
                .stringColumn("unexpRight") // (route.unexpectedRight)
                .stringColumn("left") // (route.observerPosition[FRONT_LEFT].observer.initials)
                .stringColumn("right") // (route.observerPosition[FRONT_RIGHT].observer.initials)
                .stringColumn("center") // (route.observerPosition[NAVIGATOR].observer.initials)
                .stringColumn("cbCause") // ** ()
                .doubleColumn("lat", 19, 11) // (geoPoint.latitude)
                .doubleColumn("lon", 19, 11) // (geoPoint.longitude)
                .doubleColumn("speed", 19, 11) // (geoPoint.speed)
                .doubleColumn("altitude", 19, 11) // (geoPoint.altitude)
                .integerColumn("gpsDelay") // (geoPoint.captureDelay)
                .stringColumn("aircraft") // (route.flight.immatriculation)
                .stringColumn("comment") // (route.comment)
                .stringColumn("remark") // (route.flight.comment)
                .booleanColumn("digital") // (route.flight.digital)
                .build();

        EsriGraphicList graphicList = new EsriPointList();

        FlightService flightService = getService(FlightService.class);

        Survey survey = dataModel.getSurvey();
        Region region = survey.getRegion();

        List<RouteType> routeTypes = dataModel.getRouteTypes();
        List<Strate> strates = dataModel.getStrates();

        SammoaTopiaPersistenceContext tx = beginTransaction();

        try {
            // get flights for survey (and between begin - end date)
            Iterable<Flight> flights = flightService.getFlights(
                    tx,
                    survey,
                    dataModel.getBeginDate(),
                    dataModel.getEndDate());

            // Export for each flight
            for (Flight flight : flights) {

                if (log.isInfoEnabled()) {
                    log.info("Export Efforts from flight " + flight.getFlightNumber()
                                    + " - " + flight.getSystemId()
                                    + " - " + survey.getCode()
                                    + " - " + region.getCode());
                }

                timeFormat.setTimeZone(flight.getTimeZone());

                // common properties for the flight
                Map<String, Object> commonRecord = Maps.newHashMap();
                fillFlightRecordForEffort(survey, region, flight, commonRecord);

                // get all routes of this flight
                List<Route> routes = flightService.getRoutes(tx, flight);

                // get all geo points for this flight
                List<GeoPoint> geoPoints =
                        flightService.getFlightGeoPoints(tx, flight);

                // get best geo points for each route
                Iterable<GeoPoint> routeGeoPoints =
                        GeoPoints.getClosestPoints(geoPoints, Routes.toDates(routes));

                // to iterate on geoPoints
                Iterator<GeoPoint> geoPointIterator = routeGeoPoints.iterator();

                // to keep current effort group identifier
                String effortGroup = null;

                Route previousRoute = null;
                Route previousLeg = null;
                for (Route route : routes) {

                    if (log.isTraceEnabled()) {
                        log.trace("Export Efforts from route " + route.getBeginTime() + " - " + route.getRouteType());
                    }

                    // get geoPoint associated with this route
                    GeoPoint geoPoint = geoPointIterator.next();

                    // is current route a leg ?
                    boolean routeIsLeg = Routes.isRouteLeg(route);

                    if (!routeIsLeg) {

                        if (effortGroup != null) {

                            // previous route was a leg
                            // need to create a end row (to close the group)

                            Map<String, Object> record = Maps.newHashMap();
                            record.putAll(commonRecord);

                            // add route data
                            fillRouteRecord(flight,
                                            previousRoute,
                                    previousLeg, effortGroup,
                                            LegStatus.END,
                                            record);

                            // add date using the current route one
                            fillDateRecord(route.getBeginTime(), record);

                            // add geoPoint data and flush
                            fillGeoPointRecordAndFlush(geoPoint,
                                                       record,
                                                       builder,
                                                       graphicList);
                            nbRow++;
                        }

                        // reset effort group
                        // will be recomputed by first next leg route
                        effortGroup = null;
                    }

                    boolean routeIsValid = Routes.isAccepted(route,
                                                             routeTypes,
                                                             strates);

                    if (routeIsValid) {

                        // selected route = one row on dbf

                        Map<String, Object> record = Maps.newHashMap();
                        record.putAll(commonRecord);

                        LegStatus legStatus = null;

                        if (routeIsLeg) {

                            if (effortGroup == null) {

                                // this is the first route of the leg group
                                legStatus = LegStatus.BEGIN;

                                // compute the unique effortGroup for this group
                                effortGroup = computeEffortGroup(flight, route);
                            } else {

                                // group already began, just add a route to it
                                legStatus = LegStatus.ADD;
                            }

                        }

                        // add route data
                        fillRouteRecord(flight,
                                        route,
                                        previousLeg,
                                        effortGroup,
                                        legStatus,
                                        record);


                        // add geoPoint data and flush
                        fillGeoPointRecordAndFlush(geoPoint,
                                                   record,
                                                   builder,
                                                   graphicList);
                        nbRow++;
                    }

                    // keep current route as previousroute
                    previousRoute = route;
                    if (routeIsLeg) {
                        previousLeg = route;
                    }
                }
            }

            startTime = timeLog.log(
                    startTime,
                    "exportEffortsMap",
                    "after building EsriGraphicList and DbfTableModel");

            flushExport(builder, dataModel, graphicList);

            timeLog.log(startTime, "exportEffortsMap",
                        "after EsriShapeExport execution");
            return nbRow;
        } finally {
            endTransaction(tx);
        }
    }

    public int exportObservationsMap(ExportMapModel dataModel) {
        Preconditions.checkNotNull(dataModel);
        Preconditions.checkNotNull(dataModel.getSurvey());
        Preconditions.checkNotNull(dataModel.getBeginDate());
        Preconditions.checkNotNull(dataModel.getEndDate());

        if (log.isInfoEnabled()) {
            log.info("Start ObservationsMap export to " +
                        dataModel.getExportFileWithoutExtension(""));
        }
        long startTime = TimeLog.getTime();

        int nbRow = 0;

        // *  uniquement pour un parcours de type LEG, sinon la cellule sera vide
        DbfTableModelBuilder builder = new DbfTableModelBuilder()
                .stringColumn("region") // (survey.region.code)
                .stringColumn("survey") // (survey.code)
                .stringColumn("subRegion") // (route.transectFlight.transect.strate.subRegion.name)
                .stringColumn("strateType") // * (route.transectFlight.transect.strate.strateType.code)
                .stringColumn("strate") // * (route.transectFlight.transect.strate.code)
                .stringColumn("transect") // * (route.transectFlight.transect.name)
                .integerColumn("passage") // * (route.transectFlight.crossingNumber)
                .integerColumn("flight") // (route.flight.flightNumber)
                .stringColumn("computer") // (route.flight.systemId)
                .stringColumn("routeType") // (route.routeType.name)
                .stringColumn("effortGrp") // * (voir doc)
                .stringColumn("effort") // * (voir doc)
                .stringColumn("sighting") // (voir doc)
                .dateColumn("date") // (observation.beginTime)
                .stringColumn("hhmmss") // (format(observation.beginTime, "HHmmss"))
                .stringColumn("taxon") // (observation.species.type)
                .stringColumn("group") // (observation.species.groupName)
                .stringColumn("family") // (observation.species.family)
                .stringColumn("species") // (observation.species.code)
                .stringColumn("speciesName") // (observation.species.commonName)
                .stringColumn("speciesLatin") // (observation.species.latinName)
                .integerColumn("podSize") // (observation.podSize)
                .stringColumn("age") // (observation.age)
                .integerColumn("decAngle") // (observation.decAngle)
                .stringColumn("cue") // (observation.cue)
                .stringColumn("behaviour") // (observation.behaviour)
                .integerColumn("swimDir") // (observation.swimDir)
                .stringColumn("dive") // (observation.dive)
                .stringColumn("reaction") // (observation.reaction)
                .stringColumn("calves") // (observation.calves)
                .booleanColumn("photo") // (observation.photo)
                .stringColumn("observer") // (observation.observerPosition.observer.initials)
                .stringColumn("side") // (observation.observerPosition.observer.position.name)
                .stringColumn("status") // (observation.observationStatus.name)
                .stringColumn("cbCause") // ** ()
                .doubleColumn("lat", 19, 11) // (geoPoint.latitude)
                .doubleColumn("lon", 19, 11) // (geoPoint.longitude)
                .doubleColumn("speed", 19, 11) // (geoPoint.speed)
                .doubleColumn("altitude", 19, 11) // (geoPoint.altitude)
                .integerColumn("gpsDelay") // (geoPoint.captureDelay)
                .stringColumn("aircraft") // (flight.immatriculation)
                .stringColumn("comment") // (observation.comment)
                .build();

        EsriGraphicList graphicList = new EsriPointList();

        Survey survey = dataModel.getSurvey();
        Region region = survey.getRegion();
        List<RouteType> routeTypes = dataModel.getRouteTypes();
        List<Strate> strates = dataModel.getStrates();

        SammoaTopiaPersistenceContext tx = beginTransaction();

        try {
            FlightService flightService = getService(FlightService.class);

            // get flights for survey (and between begin - end date)
            Iterable<Flight> flights = flightService.getFlights(
                    tx,
                    survey,
                    dataModel.getBeginDate(),
                    dataModel.getEndDate());

            // Export for each flight

            for (Flight flight : flights) {

                if (log.isInfoEnabled()) {
                    log.info("Export Observations from flight " + flight.getFlightNumber()
                                    + " - " + flight.getSystemId()
                                    + " - " + survey.getCode()
                                    + " - " + region.getCode());
                }

                timeFormat.setTimeZone(flight.getTimeZone());

                // get all observations for this flight
                List<Observation> observations =
                        flightService.getObservations(tx, flight);

                // apply species filter
                Observations.removeOtherSpecies(observations,
                                                dataModel.getSpecies());

                // get all routes of this flight
                List<Route> routes = flightService.getRoutes(tx, flight);

                // get all geo points for this flight
                List<GeoPoint> routeGeoPoints =
                        flightService.getFlightGeoPoints(tx, flight);

                Map<String, Object> flightRecord = Maps.newHashMap();
                fillFlightRecord(survey, region, flight, flightRecord);

                int nextRouteIndex = 0;
                int nbRoutes = routes.size();
                String effortGroup = null;

                Route previousLeg = null;
                for (Route route : routes) {

                    if (log.isDebugEnabled()) {
                        log.debug("Export Observations from route " + route.getBeginTime()
                                        + " - " +  route.getRouteType()
                                        + " (flight end time " + flight.getEndDate() + ")");
                    }
                    nextRouteIndex++;

                    boolean routeIsLeg = Routes.isRouteLeg(route);

                    if (!routeIsLeg) {

                        // new group will be computed at next leg route
                        effortGroup = null;
                    }

                    // compute route time interval
                    DateTime beginTime = Dates.toDateTime(route.getBeginTime());

                    DateTime endTime;

                    if (nextRouteIndex < nbRoutes) {

                        // use next route begin time
                        Route nextRoute = routes.get(nextRouteIndex);
                        endTime = Dates.toDateTime(nextRoute.getBeginTime());
                    } else {

                        // use flight end date
                        endTime = Dates.toDateTime(flight.getEndDate());
                    }

                    if (endTime.equals(beginTime)) {

                        // ignore this route, we will use the next one
                        continue;
                    }

                    Preconditions.checkState(endTime.isAfter(beginTime),
                                             String.format("Route debut de route %s - fin de route %s", beginTime, endTime));

                    Interval routeInterval =
                            Dates.toInterval(beginTime, endTime);

                    // Get observations for this route
                    List<Observation> routeObservations =
                            Observations.retainsObservations(observations,
                                                             routeInterval);

                    if (log.isDebugEnabled()) {
                        log.debug("Export Observations from route " + route.getEffortNumber() + " [" + beginTime + "-" + endTime + "]" +
                                " nb observations " + routeObservations.size());
                    }
                    if (!Routes.isAccepted(route, routeTypes, strates)) {

                        // not selected route
                        continue;
                    }

                    Map<String, Object> routeRecord = Maps.newHashMap();
                    routeRecord.putAll(flightRecord);

                    if (routeIsLeg && effortGroup == null) {

                        // compute effort group id
                        effortGroup = computeEffortGroup(flight, route);
                    }

                    fillRouteTypeRecord(flight, route, previousLeg, effortGroup, routeRecord);

                    // get best geo points for each observation
                    Iterable<GeoPoint> observationGeoPoints =
                            GeoPoints.getClosestPoints(
                                    routeGeoPoints,
                                    Observations.toDates(routeObservations));

                    // to iterate on geoPoints
                    Iterator<GeoPoint> geoPointIterator =
                            observationGeoPoints.iterator();

                    for (Observation observation : routeObservations) {

                        Map<String, Object> record = Maps.newHashMap();
                        record.putAll(routeRecord);

                        String observationId = computeObservationId(flight, observation);
                        record.put("sighting", observationId);

                        fillDateRecord(observation.getObservationTime(), record);

                        Species species = observation.getSpecies();
                        if (species != null) {
                            record.put("taxon", species.getType());
                            record.put("group", species.getGroupName());
                            record.put("family", species.getFamily());
                            record.put("species", species.getCode());
                            record.put("speciesName", species.getCommonName());
                            record.put("speciesLatin", species.getLatinName());
                        }
                        record.put("podSize", observation.getPodSize());
                        record.put("age", observation.getAge());
                        record.put("decAngle", observation.getDecAngle());
                        record.put("cue", observation.getCue());
                        record.put("behaviour", observation.getBehaviour());
                        record.put("swimDir", observation.getSwimDir());
                        record.put("dive", observation.getDive());
                        record.put("reaction", observation.getReaction());
                        record.put("calves", observation.getCalves());
                        record.put("photo", observation.isPhoto());

                        Position position = observation.getPosition();
                        ObserverPosition observerPosition =
                                route.getObserverPositionByPosition(position);
                        if (observerPosition != null) {
                            Observer observer =  observerPosition.getObserver();
                            if (observer != null) {
                                record.put("observer", observer.getInitials());
                            }
                        }

                        Side side = null;
                        if (position != null) {
                            side = position.getSide();
                        }
                        record.put("side", side);
                        record.put("status", observation.getObservationStatus());
                        record.put("comment", observation.getComment());

                        GeoPoint geoPoint = geoPointIterator.next();

                        // add geoPoint data and flush
                        fillGeoPointRecordAndFlush(geoPoint,
                                                   record,
                                                   builder,
                                                   graphicList);
                        nbRow++;

                    }

                    if (routeIsLeg) {
                        previousLeg = route;
                    }
                }
            }

            startTime = timeLog.log(startTime, "exportObservationsMap", "after building EsriGraphicList and DbfTableModel");

            flushExport(builder, dataModel, graphicList);

            timeLog.log(startTime, "exportObservationsMap", "after EsriShapeExport execution");
            return nbRow;
        } finally {
            endTransaction(tx);
        }
    }

    public int exportGeoPointsMap(ExportMapModel dataModel) {
        Preconditions.checkNotNull(dataModel);
        Preconditions.checkNotNull(dataModel.getSurvey());
        Preconditions.checkNotNull(dataModel.getBeginDate());
        Preconditions.checkNotNull(dataModel.getEndDate());

        if (log.isInfoEnabled()) {
            log.info("Start GeoPointsMap export to " +
                        dataModel.getExportFileWithoutExtension(""));
        }

        long startTime = TimeLog.getTime();

        int nbRow = 0;

        DbfTableModelBuilder builder = new DbfTableModelBuilder()
                .stringColumn("region")
                .stringColumn("survey")
                .integerColumn("flight")
                .stringColumn("computer")
                .stringColumn("aircraft")
                .dateColumn("date")
                .stringColumn("hhmmss")
                .doubleColumn("lat", 19, 11)
                .doubleColumn("lon", 19, 11)
                .doubleColumn("speed", 19, 11)
                .doubleColumn("altitude", 19, 11)
                .integerColumn("gpsDelay") // (geoPoint.captureDelay)
                .build();

        Survey survey = dataModel.getSurvey();
        Region region = survey.getRegion();

        FlightService flightService = getService(FlightService.class);

        // get flights for survey (and between begin - end date)
        Iterable<Flight> flights = flightService.getFlights(survey,
                                                            dataModel.getBeginDate(),
                                                            dataModel.getEndDate());

        EsriGraphicList graphicList = new EsriPointList();

        // Export for each flight
        for (Flight flight : flights) {

            if (log.isInfoEnabled()) {
                log.info("Export GeoPoints from flight " + flight.getFlightNumber()
                                + " - " + flight.getSystemId()
                                + " - " + survey.getCode()
                                + " - " + region.getCode());
            }

            timeFormat.setTimeZone(flight.getTimeZone());

            List<GeoPoint> geoPoints =
                    flightService.getFlightGeoPoints(flight);

            Map<String, Object> commonRecord = Maps.newHashMap();
            fillFlightRecord(survey, region, flight, commonRecord);

            // One record by GeoPoint
            for (GeoPoint geoPoint : geoPoints) {

                Map<String, Object> record = Maps.newHashMap();
                record.putAll(commonRecord);

                fillDateRecord(geoPoint.getRecordTime(), record);

                // add geoPoint data and flush
                fillGeoPointRecordAndFlush(geoPoint,
                                           record,
                                           builder,
                                           graphicList);
                nbRow++;
            }
        }

        startTime = timeLog.log(startTime, "exportGeoPointsMap",
                                "after building EsriGraphicList and DbfTableModel");

        flushExport(builder, dataModel, graphicList);

        timeLog.log(startTime, "exportGeoPointsMap",
                    "after EsriShapeExport execution");
        return nbRow;
    }

    protected void flushExport(DbfTableModelBuilder builder,
                               ExportMapModel dataModel,
                               EsriGraphicList graphicList) {

        DbfTableModel tableModel = builder.getModel();
        File file = dataModel.getExportFileWithoutExtension("");
        FileUtil.createDirectoryIfNecessary(file.getParentFile());
        EsriShapeExport shapeExport = new EsriShapeExport(
                graphicList, tableModel, file.getAbsolutePath());
        shapeExport.export();

        //récupère le .proj dans les ressources et le copie dans la sortie (pour que QGis soit content à l'ouverture)
        if (file.getName().endsWith(".shp")) {
            file = new File(FilenameUtils.removeExtension(file.getPath()));
        }
        File projFile = new File(file.getAbsolutePath()+".prj");
        InputStream prjStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("wgs84.prj");
        if(prjStream == null) {
            throw new SammoaTechnicalException("Prj file not found, should never happen");
        }
        try {
            Files.copy(prjStream, projFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException eee) {
            throw new SammoaTechnicalException("Could not save prj file");
        }

    }


    protected void fillDateRecord(Date date, Map<String, Object> record) {
        record.put("date", date);
        record.put("hhmmss", timeFormat.format(date));
    }

    protected void fillGeoPointRecordAndFlush(GeoPoint geoPoint,
                                              Map<String, Object> record,
                                              DbfTableModelBuilder builder,
                                              EsriGraphicList graphicList) {

        record.put("lat", geoPoint.getLatitude());
        record.put("lon", geoPoint.getLongitude());
        record.put("speed", geoPoint.getSpeed());
        record.put("altitude", geoPoint.getAltitude());
        record.put("gpsDelay", geoPoint.getCaptureDelay());

        builder.addValues(record);

        if (log.isTraceEnabled()) {
            log.trace("Record=" +  record);
        }

        OMGraphic graphic = new OMPoint(geoPoint.getLatitude(),
                                        geoPoint.getLongitude());
        graphicList.add(graphic);
    }

    protected void fillFlightRecord(Survey survey,
                                    Region region,
                                    Flight flight,
                                    Map<String, Object> record) {

        record.put("region", region.getCode());
        record.put("survey", survey.getCode());
        record.put("flight", flight.getFlightNumber());
        record.put("aircraft", flight.getImmatriculation());
        record.put("computer", flight.getSystemId());
    }

    protected void fillFlightRecordForEffort(Survey survey,
                                    Region region,
                                    Flight flight,
                                    Map<String, Object> record) {

        record.put("region", region.getCode());
        record.put("survey", survey.getCode());
        record.put("flight", flight.getFlightNumber());
        record.put("aircraft", flight.getImmatriculation());
        record.put("computer", flight.getSystemId());
        record.put("remark", flight.getComment());
        record.put("digital", flight.isDigital());
    }

    protected void fillRouteRecord(Flight flight,
                                   Route route, Route previousLeg,
                                   String effortGroup,
                                   LegStatus legStatus,
                                   Map<String, Object> record) {

        if (log.isTraceEnabled()) {
            log.trace("Create Route record for route " + route.getBeginTime() + " - " + route.getRouteType());
        }

        fillDateRecord(route.getBeginTime(), record);
        fillRouteTypeRecord(flight, route, previousLeg, effortGroup, record);

        if (legStatus != null) {
            record.put("status", legStatus);
        }
        record.put("comment", route.getComment());
        record.put("seaState", route.getSeaState());
        record.put("swell", route.getSwell());
        record.put("turbidity", route.getTurbidity());
        record.put("skyGlint", route.getSkyGlint());
        if (route.getGlareFrom() != null) {
            record.put("glareFrom", String.valueOf(route.getGlareFrom()));
        }
        if (route.getGlareTo() != null) {
            record.put("glareTo", String.valueOf(route.getGlareTo()));
        }
        record.put("glareSeverity", route.getGlareSeverity());
        record.put("glareUnder", route.isGlareUnder());
        record.put("cloudCover", route.getCloudCover());
        record.put("subjective", route.getSubjectiveConditions());
        record.put("unexpLeft", route.getUnexpectedLeft());
        record.put("unexpRight", route.getUnexpectedRight());

        ObserverPosition observerPositionByPosition =
                route.getObserverPositionByPosition(Position.FRONT_LEFT);
        if (observerPositionByPosition != null && observerPositionByPosition.getObserver() != null) {
            record.put("left", observerPositionByPosition.getObserver().getInitials());
        }
        observerPositionByPosition =
                route.getObserverPositionByPosition(Position.FRONT_RIGHT);
        if (observerPositionByPosition != null && observerPositionByPosition.getObserver() != null) {
            record.put("right", observerPositionByPosition.getObserver().getInitials());
        }
        observerPositionByPosition =
                route.getObserverPositionByPosition(Position.NAVIGATOR);
        if (observerPositionByPosition != null && observerPositionByPosition.getObserver() != null) {
            record.put("center", observerPositionByPosition.getObserver().getInitials());
        }
    }

    protected void fillRouteTypeRecord(Flight flight,
                                       Route route, Route previousLeg,
                                       String effortGroup,
                                       Map<String, Object> record) {

        record.put("routeType", route.getRouteType());
        if (Routes.isRouteLeg(route)) {
            fillLegRouteRecord(route, effortGroup, record);
        }

        if (Routes.isRouteCircleBack(route)) {
            fillCircleBackRouteRecord(flight, route, previousLeg, record);
        }
    }

    protected void fillLegRouteRecord(Route route,
                                      String effortGroup,
                                      Map<String, Object> record) {

        TransectFlight transectFlight = route.getTransectFlight();
        Transect transect = transectFlight.getTransect();
        Strate strate = transect.getStrate();

        String effort = computeEffort(route);

        record.put("subRegion", strate.getSubRegion().getName());
        record.put("strate", strate.getCode());
        record.put("strateType", strate.getType());
        record.put("transect", transect.getName());
        record.put("passage", transectFlight.getCrossingNumber());
        record.put("effortGrp", effortGroup);
        record.put("effort", effort);
    }

    protected void fillCircleBackRouteRecord(Flight flight, Route route,
                                             Route previousLeg, Map<String, Object> record) {


        // compute circle back cause
        if (route.getCircleBackCause() != null) {
            String circleBackCause = computeObservationId(flight, route.getCircleBackCause());
            record.put("cbCause", circleBackCause);
        }

        TransectFlight transectFlight = previousLeg.getTransectFlight();
        if (transectFlight != null) {

            Transect transect = transectFlight.getTransect();
            Strate strate = transect.getStrate();

            record.put("subRegion", strate.getSubRegion().getName());
            record.put("strate", strate.getCode());
            record.put("strateType", strate.getType());
        }

    }

    protected String computeEffort(Route route) {
        return "L" + route.getEffortNumber() + "-" +
                        route.getFlight().getFlightNumber() + "-" +
                        route.getFlight().getSystemId();
    }

    protected String computeEffortGroup(Flight flight, Route route) {
        return "G" + route.getEffortNumber() + "-" +
                        flight.getFlightNumber() + "-" +
                        flight.getSystemId();
    }

    protected String computeObservationId(Flight flight, Observation observation) {
        return "S" + observation.getObservationNumber() + "-" +
               flight.getFlightNumber() + "-" +
               flight.getSystemId();
    }
}
