package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

import static org.nuiton.i18n.I18n.n;

/**
 * Created: 18/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class AudioConfig {

    protected ApplicationConfig applicationConfig;

    public AudioConfig(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
        this.applicationConfig.loadDefaultOptions(AudioConfigOption.values());
    }

    /** @return {@link AudioConfigOption#SAMPLE_RATE} value */
    public String getCompression() {
        String result = applicationConfig.getOption(AudioConfigOption.COMPRESSION.key);
        return result;
    }

    /** @return {@link AudioConfigOption#SAMPLE_RATE} value */
    public float getSampleRate() {
        float result = applicationConfig.getOptionAsFloat(AudioConfigOption.SAMPLE_RATE.key);
        return result;
    }

    /** @return {@link AudioConfigOption#SAMPLE_SIZE_IN_BITS} value */
    public int getSampleSizeInBits() {
        int result = applicationConfig.getOptionAsInt(AudioConfigOption.SAMPLE_SIZE_IN_BITS.key);
        return result;
    }

    /** @return {@link AudioConfigOption#RECORD_DELAY_IN_SECONDS} value */
    public int getRecordDelayInSeconds() {
        int result = applicationConfig.getOptionAsInt(AudioConfigOption.RECORD_DELAY_IN_SECONDS.key);
        return result;
    }

    public enum AudioConfigOption implements ConfigOptionDef {

        /** Sampel rate to record audio */
        COMPRESSION(
                "sammoa.audio.compression",
                n("sammoa.config.audio.compression"),
                "ULAW",
                String.class
        ),
        /** Sampel rate to record audio */
        SAMPLE_RATE(
                "sammoa.audio.sampleRate",
                n("sammoa.config.audio.sampleRate"),
                "8000",
                Float.class
        ),
        /** Period time in seconds for each check of the gps to update location */
        SAMPLE_SIZE_IN_BITS(
                "sammoa.audio.sampleSizeInBits",
                 n("sammoa.config.audio.sampleSizeInBits"),
                 "16",
                 Integer.class
        ),
        /** Time in seconds between each record, this will record in multiple files */
        RECORD_DELAY_IN_SECONDS(
                "sammoa.audio.recordDelayInSeconds",
                n("sammoa.config.audio.recordDelayInSeconds"),
                "0",
                Integer.class
        );

        /** Configuration key. */
        private final String key;

        /** I18n key of option description */
        private final String description;

        /** Type of option */
        private final Class<?> type;

        /** Default value of option. */
        private String defaultValue;

        /** Flag to not keep option value on disk */
        private boolean isTransient;

        /** Flag to not allow option value modification */
        private boolean isFinal;

        AudioConfigOption(String key,
                          String description,
                          String defaultValue,
                          Class<?> type,
                          boolean isTransient,
                          boolean isFinal) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.isTransient = isTransient;
            this.isFinal = isFinal;
        }

        AudioConfigOption(String key,
                          String description,
                          String defaultValue,
                          Class<?> type) {
            this(key, description, defaultValue, type, false, false);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return isTransient;
        }

        @Override
        public boolean isFinal() {
            return isFinal;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public void setTransient(boolean newValue) {
            // not used
        }

        @Override
        public void setFinal(boolean newValue) {
            // not used
        }
    }
}
