/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.flightController;

/**
 * Created: 20/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public enum FlightState {
    /**
     * The WAITING state is when the flight is not started and not ended.
     * It appends when the flight is not really in used.
     */
    WAITING,

    /**
     * The OFF_EFFORT state is when the flight is started but not ended,
     * and also if the current route is not an effort.
     * It appends generally when the flight is in TRANSIT or CIRCLE_BACK.
     */
    OFF_EFFORT,

    /**
     * The ON_EFFORT state is when the flight is started, not ended but in
     * current effort. It appends when a LEG is currently in progress.
     */
    ON_EFFORT,

    /**
     * The ENDED state is when the flight is ended.
     * It appends when the current effort is finished and when no more route
     * need to be registered.
     */
    ENDED
}
