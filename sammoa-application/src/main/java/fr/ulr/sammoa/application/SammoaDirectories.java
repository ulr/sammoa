package fr.ulr.sammoa.application;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Helper about directories used in Sammoa.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.7
 */
public class SammoaDirectories {

    protected SammoaDirectories() {
        // no constructor
    }

    public static File getBackupFile(File container, String filename) {
        return new File(getBackupDirectory(container), filename);
    }

    public static File getBackupDirectory(File container) {
        return new File(container, "backup");
    }

    public static File getSurveyDirectory(File container) {
        return new File(container, "survey");
    }

    public static File getDbDirectory(File container) {
        return new File(container, "db");
    }

    public static File getCsvDirectory(File container) {
        return new File(container, "csv");
    }

    public static File getAudioDirectory(File container) {
        return new File(container, "audio");
    }

    public static File getFlightDirectory(File container) {
        return new File(container, "flight");
    }

    public static File getMapDirectory(File container) {
        return new File(container, "map");
    }

}

