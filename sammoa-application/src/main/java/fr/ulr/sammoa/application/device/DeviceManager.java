package fr.ulr.sammoa.application.device;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * Created: 06/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public interface DeviceManager {

    /**
     * Open the device
     *
     * @throws DeviceTechnicalException for errors on open
     */
    void open() throws DeviceTechnicalException;

    /**
     * Start the device session (recording, reading, ...) and try opening it if necessary. The open
     * could be tried several times to permit a proper start.
     */
    void start();

    /**
     * Stop the device session
     */
    void stop();

    /**
     * Close the device and stop it if necessary
     *
     * @throws DeviceTechnicalException for errors on close
     */
    void close() throws DeviceTechnicalException;

    /**
     * @return the DeviceState
     */
    DeviceState getState();

    /**
     * @param listener DeviceStateListener to add
     */
    void addDeviceStateListener(DeviceStateListener listener);

    /**
     * @param listener DeviceStateListener to remove
     */
    void removeDeviceStateListener(DeviceStateListener listener);

    /**
     * @return all the listeners
     */
    Set<DeviceStateListener> getDeviceStateListeners();

}
