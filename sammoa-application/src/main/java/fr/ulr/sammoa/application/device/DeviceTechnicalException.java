/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device;

/** @author fdesbois <fdesbois@codelutin.com> */
public class DeviceTechnicalException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    protected DeviceManager source;

    public DeviceTechnicalException(DeviceManager source, String message) {
        super(message);
        this.source = source;
    }

    public DeviceTechnicalException(DeviceManager source, String message, Throwable cause) {
        super(message, cause);
        this.source = source;
    }

    public String getMessageWithCause() {
        String result = getMessage();
        if (getCause() != null) {
            result += " : " + getCause().getMessage();
        }
        return result;
    }

    public DeviceManager getSource() {
        return source;
    }
}
