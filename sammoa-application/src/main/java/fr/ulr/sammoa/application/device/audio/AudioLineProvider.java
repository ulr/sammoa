package fr.ulr.sammoa.application.device.audio;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;

public class AudioLineProvider {

    private static final Log log = LogFactory.getLog(AudioLineProvider.class);

    protected static AudioLineProvider instance;

    protected static final int NB_CHANNELS = 1;

    protected static final boolean SIGNED = true;

    protected static final boolean BIG_ENDIAN = false;

    public static final int MAX_LINES = 5;

    public static boolean isMultiRecord() {
        boolean multiRecord = true;

        AudioFormat audioFormat = new AudioFormat(8000.0f, 8, 1, true, true);

        DataLine.Info dataLineInfo = new DataLine.Info(
                TargetDataLine.class, audioFormat);

        TargetDataLine line1 = null;
        TargetDataLine line2 = null;

        try {
            line1 = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
            line1.open(audioFormat);
            line1.start();

            line2 = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
            line2.open(audioFormat);
            line2.start();

        } catch (LineUnavailableException e) {

            multiRecord = false;
            if (log.isDebugEnabled()) {
                log.debug("Can't record multiple file at the same time", e);
            }
            if (log.isInfoEnabled()) {
                log.info("Can't record multiple file at the same time, 0 " +
                        "second delay between recording must be used");
            }


        } finally {
            if (line1 != null) {
                line1.stop();
                line1.close();
            }
            if (line2 != null) {
                line2.stop();
                line2.close();
            }
        }

        return multiRecord;
    }

    public static AudioLineProvider getInstance() {
        if (instance == null) {
            int maxLines = isMultiRecord() ? MAX_LINES : 1;
            instance = new AudioLineProvider(maxLines);
        }
        return instance;
    }

    protected Map<AudioLevelDataLine, Integer> prioritiesByAudioLine;

    protected int maxLines;

    protected AudioLineProvider(int maxLines) {
        this.maxLines = maxLines;
        prioritiesByAudioLine = Maps.newHashMapWithExpectedSize(maxLines);
        if (log.isDebugEnabled()) {
            log.debug("new AudioLineProvider for " + maxLines + " lines max");
        }
    }


    public Optional<AudioLevelDataLine> openAudioLine(int priority, float sampleRate, int sampleSizeInBits) throws LineUnavailableException {

        closeAudioLineLower(priority);

        Optional<AudioLevelDataLine> audioLine;

        if (isFull()) {
            audioLine = Optional.empty();
            if (log.isWarnEnabled()) {
                log.warn("Unable to open new audioLine for priority " + priority);
            }
        } else {
            AudioLevelDataLine line = createLine(sampleRate, sampleSizeInBits);
            prioritiesByAudioLine.put(line, priority);
            audioLine = Optional.of(line);
            if (log.isDebugEnabled()) {
                log.debug("Open new audio line for priority " + priority);
            }
        }
        return audioLine;
    }

    protected boolean isFull() {
        return prioritiesByAudioLine.size() == maxLines;
    }

    public boolean isCanAddRecord() {
        return !isFull();
    }

    public int getMaxLines() {
        return maxLines;
    }

    protected void closeAudioLineLower(int priority) {
        if (isFull()) {
            prioritiesByAudioLine.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() < priority)
                    .min(Comparator.comparing(Map.Entry::getValue))
                    .map(Map.Entry::getKey)
                    .ifPresent(this::closeAudioLine);
        }
    }

    protected AudioLevelDataLine createLine(float sampleRate, int sampleSizeInBits) throws LineUnavailableException {

        AudioFormat audioFormat = new AudioFormat(
                sampleRate,
                sampleSizeInBits,
                NB_CHANNELS,
                SIGNED,
                BIG_ENDIAN);

        DataLine.Info dataLineInfo = new DataLine.Info(
                TargetDataLine.class, audioFormat);

        TargetDataLine line = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
        AudioLevelDataLine audioLine = new AudioLevelDataLine(line, audioFormat);

        audioLine.open(audioFormat);

        audioLine.start();

        return audioLine;

    }

    public void closeAudioLine(AudioLevelDataLine audioLine) {
        Integer priority = prioritiesByAudioLine.remove(audioLine);
        if (priority != null) {
            if (log.isDebugEnabled()) {
                log.debug("close audio line for priority" + priority);
            }
            audioLine.stop();
            audioLine.close();
        }
    }

    public float getLevel() {
        if (prioritiesByAudioLine.isEmpty()) {
            try {
                openAudioLine(0, 8000, 16);
            } catch (LineUnavailableException e) {
                throw new SammoaTechnicalException("unable to ope line for level", e);
            }
        }
        AudioLevelDataLine audioLevelLine = prioritiesByAudioLine.entrySet()
                .stream()
                .min(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .orElseThrow(() -> new SammoaTechnicalException("No audio level line"));

        if (prioritiesByAudioLine.get(audioLevelLine) == 0) {
            audioLevelLine.forceRead();
        }
        return audioLevelLine.getLevel();
    }


    public void closeLevelLine() {
        prioritiesByAudioLine.entrySet()
                .stream()
                .filter(e -> e.getValue() == 0)
                .map(Map.Entry::getKey)
                .findFirst()
                .ifPresent(this::closeAudioLine);
    }
}
