package fr.ulr.sammoa.application.io.output.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.DecoratorService;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.io.FlightStorage;
import fr.ulr.sammoa.application.io.SammoaStorages;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.input.application.DateParserFormatter;
import fr.ulr.sammoa.application.io.input.application.TimeZoneParserFormatter;
import fr.ulr.sammoa.application.migration.StorageMigrationService;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.FlightTopiaDao;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.SammoaDbMeta;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyTopiaDao;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ExportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.EntityCsvModel;
import org.nuiton.topia.service.csv.out.EntityAssociationExportModel;
import org.nuiton.topia.service.csv.out.ExportEntityVisitor;
import org.nuiton.topia.service.csv.out.ExportModelFactory;
import org.nuiton.topia.service.csv.out.TopiaCsvExports;
import org.nuiton.util.TimeLog;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * To export application as a {@code sammoa} file.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ExportApplicationService extends SammoaServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ExportApplicationService.class);

    private static final TimeLog TIME_LOG =
            new TimeLog(ExportApplicationService.class);


    private final ExportModelFactory<SammoaEntityEnum> defaultExportModelFactory = new ExportModelFactory<SammoaEntityEnum>() {
        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(TableMeta<SammoaEntityEnum> meta) {
            EntityCsvModel<SammoaEntityEnum, E> model =
                    EntityCsvModel.newModel(config.getCsvSeparator(),
                                            meta,
                                            TopiaEntity.PROPERTY_TOPIA_ID);


            for (ColumnMeta columnMeta : meta) {
                String propertyName = columnMeta.getName();
                Class<?> type = columnMeta.getType();
                if (columnMeta.isFK()) {

                    Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;

                    // export foreign key value as his topiaId
                    model.addForeignKeyForExport(propertyName, entityType);

                } else {
                    if (Date.class.equals(type)) {
                        model.newColumnForImportExport(
                                propertyName,
                                DateParserFormatter.of());
                    } else if (TimeZone.class.equals(type)) {
                            model.newColumnForImportExport(
                                    propertyName,
                                    TimeZoneParserFormatter.of());
                    } else if (! propertyName.equals(TopiaEntityContextable.PROPERTY_TOPIA_DAO_SUPPLIER)){
                        model.addDefaultColumn(propertyName, type);
                    }
                }
            }
            return model;
        }

        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(AssociationMeta<SammoaEntityEnum> associationMeta) {
            ExportModel<E> model = EntityAssociationExportModel.newExportModel(
                    config.getCsvSeparator(),
                    associationMeta
            );
            return model;
        }
    };

    /**
     * Export some data of a survey.
     *
     * @param model model of data to export
     * @since 0.6
     */
    public void exportApplication(ExportApplicationModel model) {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(model.getExportFile());
        Preconditions.checkNotNull(model.getSurveyId());
        Preconditions.checkNotNull(model.getFlightIds());

        if (log.isInfoEnabled()) {
            log.info("Start Application export to " +
                        model.getExportFile());
        }

        long startTime = TimeLog.getTime();

        String surveyId = model.getSurveyId();

        SurveyStorage storage = getSurveyStorage(surveyId);

        File targetDirectory = new File(config.getTmpDirectory(),
                                        "exportSammoa-" + System.nanoTime());

        if (log.isInfoEnabled()) {
            log.info("Temporary storage: " +  targetDirectory);
        }

        Iterable<String> flightIds = model.getFlightIds();

        SurveyStorage targetStorage =
                SammoaStorages.createSurveyStorage(targetDirectory,
                                                     storage.getId());

        SammoaStorages.copySurveyStorage(storage,
                                           targetStorage,
                                           flightIds);

        fillAndSaveStorageProperties(targetStorage);

        startTime = TIME_LOG.log(startTime, "exportApplication",
                                 "after create Storage structure");

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            exportReferential(tx, targetStorage);

            for (String flightId : flightIds) {
                FlightStorage flightStorage =
                        targetStorage.getFlightStorage(flightId);
                exportFlightData(tx,
                                 flightStorage,
                                 flightId);
            }
        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not create export file", e);
        } finally {
            endTransaction(tx);
        }

        compressZipFile(model.getExportFile(),
                        targetStorage.getDirectory());

        TIME_LOG.log(startTime, "exportApplication",
                     "after export db to csv files ");
    }

    private void fillAndSaveStorageProperties(SurveyStorage storage) {


        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            DecoratorService decoratorService =
                    getService(DecoratorService.class);

            SurveyTopiaDao surveyDAO =  tx.getSurveyDao();
            FlightTopiaDao flightDAO =  tx.getFlightDao();

            Survey survey = surveyDAO.forTopiaIdEquals(storage.getId()).findUnique();

            storage.fillProperties(survey, decoratorService);
            getService(StorageMigrationService.class).addVersion(storage.getProperties());
            storage.saveProperties();

            for (String flightId : storage.getFlightIds()) {

                FlightStorage flightStorage = storage.getFlightStorage(flightId);
                Flight flight = flightDAO.forTopiaIdEquals(flightId).findUnique();
                flightStorage.fillProperties(flight, decoratorService);
                flightStorage.saveProperties();
            }
        } catch (TopiaException | IOException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }
    }

    protected void exportReferential(SammoaTopiaPersistenceContext tx, SurveyStorage storage) throws IOException {

        File csvDirectory = storage.getCsvDirectory();

        FileUtils.forceMkdir(csvDirectory);

        SammoaDbMeta dbMetas = persistence.getDbMetas();

        SammoaPrepareDataForExport prepareDataForExport =
                new SammoaPrepareDataForExport(dbMetas, tx);

        List<TableMeta<SammoaEntityEnum>> referencialEntries =
                dbMetas.getReferentialTables();

        for (TableMeta<SammoaEntityEnum> tableMeta : referencialEntries) {

            File entryFile = new File(csvDirectory, tableMeta.getFilename());
            if (log.isInfoEnabled()) {
                log.info("Will export " + tableMeta + " to " + entryFile);
            }

            TopiaCsvExports.exportData(tableMeta,
                    defaultExportModelFactory,
                    prepareDataForExport,
                    entryFile);
        }
    }

    protected void exportFlightData(SammoaTopiaPersistenceContext tx,
                                    FlightStorage storage,
                                    String flightId) throws IOException, TopiaException {

        File csvDirectory = storage.getCsvDirectory();

        FileUtils.forceMkdir(csvDirectory);

        SammoaDbMeta dbMetas = persistence.getDbMetas();

        Map<SammoaEntityEnum, TopiaCsvExports.EntityExportContext<SammoaEntityEnum>> exportContexts =
                TopiaCsvExports.createReplicateEntityVisitorContexts(
                        defaultExportModelFactory,
                        dbMetas.getDataTables(),
                        dbMetas.getDataAssociations(),
                        csvDirectory);



        FlightService flightService = getService(FlightService.class);

        try (ExportEntityVisitor<SammoaEntityEnum> exportVisitor =
                     ExportEntityVisitor.newVisitor(dbMetas.getPersistenceHelper(),
                             exportContexts)) {

            Flight flight = flightService.getFlight(tx, flightId);
            attachIndexInFlight(flight);
            exportVisitor.export(flight);

            List<GeoPoint> geoPoints =
                    flightService.getFlightGeoPoints(tx, flight);
            exportVisitor.export(geoPoints);

            List<Route> routes = flightService.getRoutes(tx, flight);
            exportVisitor.export(routes);

            List<Observation> observations =
                    flightService.getObservations(tx, flight);
            exportVisitor.export(observations);

        }
    }

    protected void attachIndexInFlight(Flight flight) {

        int indexInFlight = 0;
        for (TransectFlight transectFlight : flight.getTransectFlight()){
            transectFlight.setIndexInFlight(indexInFlight);
            indexInFlight++;
        }

    }

}
