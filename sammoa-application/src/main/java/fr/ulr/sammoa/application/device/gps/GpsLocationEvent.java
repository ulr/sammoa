package fr.ulr.sammoa.application.device.gps;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.GeoPoint;

import java.util.EventObject;

/**
 * Created: 06/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class GpsLocationEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    protected GeoPoint oldValue;

    protected GeoPoint newValue;

    public GpsLocationEvent(GpsHandler source,
                            GeoPoint oldValue,
                            GeoPoint newValue) {
        super(source);
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public GpsHandler getSource() {
        return (GpsHandler) source;
    }

    public GeoPoint getOldValue() {
        return oldValue;
    }

    public GeoPoint getNewValue() {
        return newValue;
    }
}
