package fr.ulr.sammoa.application.io.input.csv;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.SpeciesImpl;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;

/**
 * Created: 17/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SpeciesImportModel extends AbstractImportModel<Species> {

    public SpeciesImportModel(final Region region) {
        super(';');
        newMandatoryColumn("CODE", Species.PROPERTY_CODE);
        newMandatoryColumn("NOM_COMM", Species.PROPERTY_COMMON_NAME);
        newMandatoryColumn("NOM_LATIN", Species.PROPERTY_LATIN_NAME);
        newMandatoryColumn("TYPE", Species.PROPERTY_TYPE);
        newMandatoryColumn("FAMILLE", Species.PROPERTY_FAMILY);
        newMandatoryColumn("GROUPE", Species.PROPERTY_GROUP_NAME);
        newMandatoryColumn("REGION", Species.PROPERTY_REGION, new ValueParser<Region>() {

            @Override
            public Region parse(String s) throws ParseException {
                Preconditions.checkArgument(
                        region.getCode().equals(s),
                        String.format("The current region is %1$s and not %2$s",
                                      region.getCode(), s)
                );
                return region;
            }
        });
    }

    @Override
    public Species newEmptyInstance() {
        return new SpeciesImpl();
    }

}
