package fr.ulr.sammoa.application.device.gps;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Preconditions;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

import static org.nuiton.i18n.I18n.n;

/**
 * Created: 18/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class GpsConfig {

    public static final String PROPERTY_GPS_HANDLER_CLASS = "gpsHandlerClass";

    protected ApplicationConfig applicationConfig;

    public GpsConfig(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
        this.applicationConfig.loadDefaultOptions(GpsConfigOption.values());
    }

    /** @return {@link GpsConfigOption#GPS_HANDLER} value */
    public Class<? extends GpsHandler> getGpsHandlerClass() {
        Class<?> result = applicationConfig.getOptionAsClass(GpsConfigOption.GPS_HANDLER.key);
        Preconditions.checkArgument(GpsHandler.class.isAssignableFrom(result),
                                    "The class " + result.getSimpleName() + " is not a GpsHandler implementation");
        return (Class<? extends GpsHandler>) result;
    }

    /**
     * @param newValue new handler class to set in configuration
     * @since 0.5
     */
    public void setGpsHandlerClass(Class<?> newValue) {
        applicationConfig.setOption(
                GpsConfig.GpsConfigOption.GPS_HANDLER.getKey(),
                newValue.getName());
    }

    /**
     * @param speed new speed to set in configuration
     * @since 1.2.0
     */
    public void setSpeed(String speed) {
        applicationConfig.setOption(
                GpsConfigOption.GPS_SPEED.getKey(),
                speed);
    }

    /**
     * @param device new device to set in configuration
     * @since 1.2.0
     */
    public void setDevice(String device) {
        applicationConfig.setOption(
                GpsConfigOption.GPS_DEVICE.getKey(),
                device);
    }

    /** @return {@link GpsConfigOption#GPS_CHECK_PERIOD} value */
    public int getCheckPeriod() {
        int result = applicationConfig.getOptionAsInt(GpsConfigOption.GPS_CHECK_PERIOD.key);
        return result;
    }

    /** @return {@link GpsConfigOption#GPS_TIMEOUT} value */
    public int getTimeout() {
        int result = applicationConfig.getOptionAsInt(GpsConfigOption.GPS_TIMEOUT.key);
        return result;
    }

    /** @return {@link GpsConfigOption#GPS_DEVICE} value */
    public String getDevice() {
        String result = applicationConfig.getOption(GpsConfigOption.GPS_DEVICE.key);
        return result;
    }

    /** @return {@link GpsConfigOption#GPS_SPEED} value */
    public int getSpeed() {
        int result = applicationConfig.getOptionAsInt(GpsConfigOption.GPS_SPEED.key);
        return result;
    }

    public enum GpsConfigOption implements ConfigOptionDef {

        /** Implementation class for GpsHandler */
        GPS_HANDLER("sammoa.gps.handler",
                    n("sammoa.config.gps.handler"),
                    GpsHandlerGpsylon.class.getName(),
                    Class.class
        ),
        /** Period time in seconds for each check of the gps to update location */
        GPS_CHECK_PERIOD("sammoa.gps.checkPeriod",
                         n("sammoa.config.gps.check.period"),
                         "2",
                         Integer.class
        ),
        /** Time in seconds before timeout (ERROR, UNAVAILABLE) */
        GPS_TIMEOUT("sammoa.gps.timeout",
                    n("sammoa.config.gps.timeout"),
                    "10",
                    Integer.class
        ),
        /** GPS Device name ex: /dev/ttyUSB0 or /dev/ttyS1 or COM5 */
        GPS_DEVICE("sammoa.gps.device",
                   n("sammoa.config.gps.device"),
                   "COM1",
                   String.class
        ),
        /** GPS data speed */
        GPS_SPEED("sammoa.gps.speed",
                  n("sammoa.config.gps.speed"),
                  "4800",
                  Integer.class
        );

        /** Configuration key. */
        private final String key;

        /** I18n key of option description */
        private final String description;

        /** Type of option */
        private final Class<?> type;

        /** Default value of option. */
        private String defaultValue;

        /** Flag to not keep option value on disk */
        private boolean isTransient;

        /** Flag to not allow option value modification */
        private boolean isFinal;

        GpsConfigOption(String key,
                        String description,
                        String defaultValue,
                        Class<?> type,
                        boolean isTransient,
                        boolean isFinal) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.isTransient = isTransient;
            this.isFinal = isFinal;
        }

        GpsConfigOption(String key,
                        String description,
                        String defaultValue,
                        Class<?> type) {
            this(key, description, defaultValue, type, false, false);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return isTransient;
        }

        @Override
        public boolean isFinal() {
            return isFinal;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public void setTransient(boolean newValue) {
            // not used
        }

        @Override
        public void setFinal(boolean newValue) {
            // not used
        }
    }
}
