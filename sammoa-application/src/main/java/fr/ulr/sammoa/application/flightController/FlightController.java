package fr.ulr.sammoa.application.flightController;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaService;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationStatus;
import fr.ulr.sammoa.persistence.Position;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.TransectFlight;

/**
 * The FlightController is the main service used to do all actions during a
 * flight. It is synchronized with the audio and GPS devices. It can be
 * initialized with a flight that already started, the actions allowed depends on
 * flight state.
 * <p/>
 * Created: 12/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @see FlightState
 */
public interface FlightController extends SammoaService {

    /** @return the {@link FlightState} */
    FlightState getState();

    <T extends DeviceManager> void openDeviceManager(Class<T> deviceManager);

    <T extends DeviceManager> T getDeviceManager(Class<T> deviceManagerClass);

    /** @return The current {@link Route} */
    Route getCurrentRoute();

    /** @return The next {@link TransectFlight} */
    TransectFlight getNextTransect();

    /**
     * Initialize all data of FlightController depends on the current flight state.
     * If the flight is running ({@link #isRunning()}), the devices will be started and
     * recording if necessary.
     * <p/>
     * You must call init method to ensure the {@link FlightState}.
     *
     * @param flight flight to use in the controller
     */
    void init(Flight flight);

    /**
     * Start operation. This will set the {@code beginDate} of the flight, then
     * a new {@link RouteType#TRANSIT} route is created. The state is now
     * {@link FlightState#OFF_EFFORT}.
     * <p/>
     * This operation is called when the plane take off, for the moment no
     * observation is done and the audio is not recording. The GPS is enabled
     * and will send data to display the plane tracking.
     * <p/>
     * After a start(), you can call :
     * <ul>
     * <li>{@link #begin()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void start();

    /**
     * Select a {@code nextTransect} to use for the next {@link RouteType#LEG}
     * when {@link #begin()} method is called.
     *
     * @param nextTransect the TransectFlight to use as next one
     */
    void setNextTransect(TransectFlight nextTransect);

    void setCurrentRoute(Route currentRoute);

    /**
     * Begin operation. A new {@link RouteType#LEG} is created based on GPS
     * current date and next transect that becomes the current one. The state
     * is now {@link FlightState#ON_EFFORT}.
     * <p/>
     * This operation is called when the plane arrives on a transect and observation
     * need to begin. Conditions could be set on the currentRoute (TRANSIT) and
     * will be copied on the new LEG. This operation is also called when a
     * CIRCLE_BACK is finished to resume effort on the current transect.
     * <p/>
     * After a begin(), you can call :
     * <ul>
     * <li>{@link #add()}</li>
     * <li>{@link #circleBack(Observation)}</li>
     * <li>{@link #next()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #end()}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void begin();

    /**
     * Rejoin operation. A new {@link RouteType#LEG} is created based on GPS
     * current date and next transect that becomes the current one. The state
     * is now {@link FlightState#ON_EFFORT}.
     * <p/>
     * This operation is called when a CIRCLE_BACK is finished to resume effort on the current transect. We are then in "re-sighting time"
     * <p/>
     * After a rejoin(), you can call :
     * <ul>
     * <li>{@link #add()}</li>
     * <li>{@link #circleBack(Observation)}</li>
     * <li>{@link #next()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #end()}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void rejoin();

    /**
     * Circle Back operation. A new {@link RouteType#CIRCLE_BACK} is created
     * based on GPS current date and attached to the {@code observation}. The
     * next transect will be the current one to allow begin() to not change the
     * transect. The state is now {@link FlightState#OFF_EFFORT}.
     * <p/>
     * This operation is called when an observation need to be checked again,
     * the plane will do a circle back. In this case, we are not on effort
     * anymore, but we can continue creating observations with the appropriate
     * status. The {@code observation} is marked as {@link ObservationStatus#CIRCLE_BACK}.
     * After the circle back, we use {@link #begin()} operation to continue effort
     * with a new LEG.
     * <p/>
     * After a circleBack(), you can call :
     * <ul>
     * <li>circleBack(Observation)</li>
     * <li>{@link #begin()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     *
     * @param observation The observation cause of the circle back
     * @see ObservationStatus
     */
    void circleBack(Observation observation);

    /**
     * Add operation. A new {@link RouteType#LEG} is created based on GPS current
     * date and current transect. The next transect and state doesn't change.
     * <p/>
     * This operation is called when the observation conditions has changed,
     * to mark this, a LEG is created with a GPS point and a new audio file.     *
     * <p/>
     * After a add(), you can call :
     * <ul>
     * <li>add()</li>
     * <li>{@link #circleBack(Observation)}</li>
     * <li>{@link #next()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #end()}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void add();

    /**
     * Next operation. It's a shortcut to end the previous effort and begin the
     * next one on the next transect. This will call {@link #end()} if necessary
     * and then {@link #begin()}.
     * <p/>
     * After a next(), you can call :
     * <ul>
     * <li>add()</li>
     * <li>{@link #circleBack(Observation)}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #end()}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void next();

    /**
     * Observation operation. It's the creation of an observation for the
     * given {@code position}. The observer will be retrieved from the
     * current route and the time is synchronized with the GPS or with the
     * system if not responding.
     *
     * @param position Position of the observer that make the observation
     */
    void observation(Position position);

    /**
     * End operation. This will create a new {@link RouteType#TRANSIT} route and
     * put the next transect depends on the plane list. The audio is still
     * recording for a few minutes and the state becomes {@link FlightState#OFF_EFFORT}
     * <p/>
     * This operation is called when an effort/transect is finished.
     * <p/>
     * After a end(), you can call :
     * <ul>
     * <li>{@link #begin()}</li>
     * <li>{@link #observation(Position)}</li>
     * <li>{@link #stop()}</li>
     * </ul>
     */
    void end();

    /**
     * Stop operation. This will stop the flight (by creating the end date) and
     * stop all devices (GPS and Audio). Current route and next transect are
     * reset (set to null). The state becomes {@link FlightState#ENDED}
     * <p/>
     * This operation is called when the plane is landing.
     * <p/>
     * You can't do anything after a stop.
     */
    void stop();

    /**
     * Close operation. This is used to close properly the controller even if
     * the flight is not finished.
     */
    void close();

    /** @return true if {@link FlightState#WAITING} */
    boolean isWaiting();

    /** @return true if {@link FlightState#ENDED} */
    boolean isEnded();

    /** @return true if {@link FlightState#ON_EFFORT} */
    boolean isOnEffort();

    /** @return true if {@link FlightState#OFF_EFFORT} */
    boolean isOffEffort();

    /** @return true if {@link FlightState#OFF_EFFORT} or {@link FlightState#ON_EFFORT} */
    boolean isRunning();

    /** @param listener FlightControllerListener to add */
    void addFlightControllerListener(FlightControllerListener listener);

    /** @param listener FlightControllerListener to remove */
    void removeFlightControllerListener(FlightControllerListener listener);
}
