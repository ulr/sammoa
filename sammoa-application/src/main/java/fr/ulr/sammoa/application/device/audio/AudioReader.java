package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.DeviceManager;

import javax.sound.sampled.AudioFileFormat;
import java.io.File;
import java.util.Date;
import java.util.Set;

/**
 * Created: 21/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public interface AudioReader extends DeviceManager {

    /** @return the {@link AudioFileFormat.Type} used (default is WAV) */
    AudioFileFormat.Type getOutputType();

    /**
     * @param file Audio file to load, then you can call start() and stop()
     * @param recordingDate Recording date of the file
     */
    void load(File file, Date recordingDate);

    /** Unload the current audio file */
    void unload();

    /** @return loaded audio in milliseconds or 0 if no audio is loaded */
    long getLength();

    /**
     * @return the start date of the loaded file or null if not provided or
     *         no file is loaded
     */
    Date getStartDate();

    /** @param position current audio position in milliseconds to set */
    void setPosition(long position);

    /** @return current audio position in milliseconds */
    long getPosition();

    void setPositionDate(Date date);

    Date getPositionDate();

    void addAudioPositionListener(AudioPositionListener listener);

    void removeAudioPositionListener(AudioPositionListener listener);

    File getFileToRead();

    Set<AudioPositionListener> getAudioPositionListeners();
}
