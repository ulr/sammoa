/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.device.audio.AudioConfig;
import fr.ulr.sammoa.application.device.gps.GpsConfig;
import fr.ulr.sammoa.persistence.AutoSaveListener;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;

/**
 * Sammoa application config
 *
 * @author sletellier <letellier@codelutin.com>
 * @author fdesbois <fdesbois@codelutin.com>
 */
public class SammoaConfig implements Supplier<ApplicationConfig> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SammoaConfig.class);

    public static final String PROPERTY_START = "start";

    public static final String PROPERTY_STOP = "stop";

    public static final String PROPERTY_BEGIN = "begin";

    public static final String PROPERTY_REJOIN = "rejoin";

    public static final String PROPERTY_END = "end";

    public static final String PROPERTY_NEXT = "next";

    public static final String PROPERTY_ADD = "add";

    public static final String PROPERTY_LEFT_OBSERVATION = "leftObservation";

    public static final String PROPERTY_CENTER_OBSERVATION = "centerObservation";

    public static final String PROPERTY_RIGHT_OBSERVATION = "rightObservation";

    public static final String PROPERTY_CIRCLE_BACK = "circleBack";

    public static final String PROPERTY_VALID_FLIGHT = "validFlight";

    public static final String PROPERTY_VALID_TRANSECT = "validTransect";

    public static final String PROPERTY_VALID_ROUTE = "validRoute";

    public static final String PROPERTY_VALID_OBSERVATION = "validObservation";

    public static final String PROPERTY_CIRCLE_BACK_ENABLED = "circleBackEnabled";

    public static final String PROPERTY_CIRCLE_BACK_TIMER_ENABLED = "circleBackTimerEnabled";

    public static final String PROPERTY_DIVE_ENABLED = "diveEnabled";

    public static final String PROPERTY_REACTION_ENABLED = "reactionEnabled";

    public static final String PROPERTY_GPS_CONFIG = "gpsConfig";

    protected static final String TABLE_COLUMN_ORDER = "sammoa.ui.table.%1$s.columns.order";

    protected static final String TABLE_COLUMN_ORDER_CUSTOM = "sammoa.ui.table.%1$s.columns.order.custom";

    public enum Table {
        transects, routes, observations, geoPoints;
    }

    /** Delegate application config object containing configuration. */
    protected final ApplicationConfig applicationConfig;

    protected GpsConfig gpsConfig;

    protected AudioConfig audioConfig;

    public SammoaConfig(String file, String... args) {

        applicationConfig = new ApplicationConfig();
        applicationConfig.setConfigFileName(file);

        if (log.isInfoEnabled()) {
            log.info(this + " is initializing...");
        }

        try {
            applicationConfig.loadDefaultOptions(SammoaConfigOption.class.getEnumConstants());
            applicationConfig.parse(args);

        } catch (ArgumentsParserException e) {
            throw new SammoaTechnicalException(
                    "Could not parse configuration", e);
        }

        gpsConfig = new GpsConfig(applicationConfig);
        audioConfig = new AudioConfig(applicationConfig);

        try {
            prepareDirectories();
        } catch (IOException e) {
            throw new SammoaTechnicalException(e);
        }

        if (log.isDebugEnabled()) {
            log.debug("parsed options in config file " + applicationConfig.getOptions());
        }
    }

    protected void prepareDirectories() throws IOException {

        getOrCreateDirectory(SammoaConfigOption.DATA_DIRECTORY);
        File tmpDir = getOrCreateDirectory(SammoaConfigOption.TMP_DIRECTORY);
        // clean it
        FileUtils.deleteDirectory(tmpDir);

        FileUtils.forceMkdir(tmpDir);

        FileUtils.forceMkdir(SammoaDirectories.getBackupDirectory(getDataDirectory()));
        FileUtils.forceMkdir(SammoaDirectories.getSurveyDirectory(getDataDirectory()));
        File dbDirectory = SammoaDirectories.getDbDirectory(getDataDirectory());
        FileUtils.forceMkdir(dbDirectory);

        // inject in defaults (we don't want anybody to change this
        applicationConfig.setDefaultOption("sammoa.db.directory", dbDirectory.getAbsolutePath());
    }

    @Override
    public ApplicationConfig get() {
        return applicationConfig;
    }

    public ApplicationConfig getApplicationConfig() {
        return applicationConfig;
    }

    public GpsConfig getGpsConfig() {
        return gpsConfig;
    }

    public AudioConfig getAudioConfig() {
        return audioConfig;
    }

    public void save() {
        applicationConfig.saveForUser();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /** @return {@link SammoaConfigOption#LOG_FILE} value */
    public File getLogFile() {
        return applicationConfig.getOptionAsFile(SammoaConfigOption.LOG_FILE.key);
    }

    /** @return {@link SammoaConfigOption#UI_CONFIG_FILE} value */
    public File getUIConfigFile() {
        return applicationConfig.getOptionAsFile(SammoaConfigOption.UI_CONFIG_FILE.key);
    }

    /** @return {@link SammoaConfigOption#VERSION} value */
    public Version getVersion() {
        return applicationConfig.getOptionAsVersion(SammoaConfigOption.VERSION.key);
    }

    /** @return {@link SammoaConfigOption#SITE_URL} value */
    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(SammoaConfigOption.SITE_URL.key);
    }

    /** @return {@link SammoaConfigOption#ORGANIZATION_NAME} value */
    public String getOrganizationName() {
        return applicationConfig.getOption(SammoaConfigOption.ORGANIZATION_NAME.key);
    }

    /** @return {@link SammoaConfigOption#INCEPTION_YEAR} value */
    public int getInceptionYear() {
        return applicationConfig.getOptionAsInt(SammoaConfigOption.INCEPTION_YEAR.key);
    }

    /** @return {@link SammoaConfigOption#DATA_DIRECTORY} value */
    public File getDataDirectory() {
        File result = applicationConfig.getOptionAsFile(SammoaConfigOption.DATA_DIRECTORY.key);
        return result;
    }

    public File getTmpDirectory() {
        File result = applicationConfig.getOptionAsFile(SammoaConfigOption.TMP_DIRECTORY.key);
        return result;
    }

    public char getCsvSeparator() {
        char csvSeparator = applicationConfig.getOption(
                Character.class, SammoaConfigOption.CSV_SEPARATOR.key);
        Preconditions.checkNotNull(csvSeparator);
        return csvSeparator;
    }

    /** @return {@link SammoaConfigOption#SYSTEM_ID} value */
    public String getSystemId() {
        String result = applicationConfig.getOption(SammoaConfigOption.SYSTEM_ID.key);
        return result;
    }

    /** @return {@link SammoaConfigOption#FLIGHT_NUMBER} value */
    public Integer getFlightNumber() {
        int result = applicationConfig.getOptionAsInt(SammoaConfigOption.FLIGHT_NUMBER.key);
        return result;
    }

    public void setFlightNumber(int flightNumber) {
        applicationConfig.setOption(SammoaConfigOption.FLIGHT_NUMBER.key, String.valueOf(flightNumber));
    }

    /** @return {@link SammoaConfigOption#BACKGROUND_SHAPE_FILE} value */
    public File getBackgroundShapeFile() {
        File result = applicationConfig.getOptionAsFile(SammoaConfigOption.BACKGROUND_SHAPE_FILE.key);
        return result;
    }

    /** @return {@link SammoaConfigOption#AUTO_COMMIT_DELAY} value */
    public Long getAutoCommitDelay() {
        long result = applicationConfig.getOptionAsLong(SammoaConfigOption.AUTO_COMMIT_DELAY.key);
        return result;
    }

    public KeyStroke getShortCut(String actionName) {
        KeyStroke result = applicationConfig.getOptionAsKeyStroke(
                "sammoa.ui." + actionName);
        return result;
    }

    /**
     * @param topiaId {@link SammoaConfigOption#SURVEY_ID} value
     * @since 0.2
     */
    public void setSurveyId(String topiaId) {
        applicationConfig.setOption(SammoaConfigOption.SURVEY_ID.key, topiaId);
    }

    /**
     * @return {@link SammoaConfigOption#SURVEY_ID} value
     * @since 0.2
     */
    public String getSurveyId() {
        String result = applicationConfig.getOption(SammoaConfigOption.SURVEY_ID.key);
        return result;
    }

    /**
     * @return {@link SammoaConfigOption#CIRCLE_BACK_ENABLE} value
     * @since 1.1
     */
    public boolean isCircleBackEnabled() {
        return applicationConfig.getOptionAsBoolean(SammoaConfigOption.CIRCLE_BACK_ENABLE.key);
    }

    /**
     * @return {@link SammoaConfigOption#CIRCLE_BACK_TIMER_ENABLE} value
     * @since 2.1.4
     */
    public boolean isCircleBackTimerEnabled() {
        return applicationConfig.getOptionAsBoolean(SammoaConfigOption.CIRCLE_BACK_TIMER_ENABLE.key);
    }

    /**
     * @return {@link SammoaConfigOption#DIVE_ENABLE} value
     * @since 1.3
     */
    public boolean isDiveEnabled() {
        return applicationConfig.getOptionAsBoolean(SammoaConfigOption.DIVE_ENABLE.key);
    }

    /**
     * @return {@link SammoaConfigOption#REACTION_ENABLE} value
     * @since 1.3
     */
    public boolean isReactionEnabled() {
        return applicationConfig.getOptionAsBoolean(SammoaConfigOption.REACTION_ENABLE.key);
    }

    /**
     * @param table table
     * @return table column order value
     * @since 1.1
     */
    public List<String> getTableColumnsOrder(Table table) {
        if (applicationConfig.getOptionAsBoolean(String.format(TABLE_COLUMN_ORDER_CUSTOM, table))) {
            String key = String.format(TABLE_COLUMN_ORDER, table);
            String value = applicationConfig.getOption(key);
            if (StringUtils.isNotEmpty(value)) {
                return Lists.newArrayList(value.split(","));
            }
        }
        return null;
    }

    /**
     * @param table table id
     * @param columnsOrder list of column
     * @since 1.1
     */
    public void setTableColumnsOrder(Table table, List<String> columnsOrder) {
        if (CollectionUtils.isNotEmpty(columnsOrder)) {
            String value = columnsOrder.stream().collect(Collectors.joining(","));
            applicationConfig.setOption(String.format(TABLE_COLUMN_ORDER, table), value);
            applicationConfig.setOption(String.format(TABLE_COLUMN_ORDER_CUSTOM, table), "true");
        } else {
            applicationConfig.setOption(String.format(TABLE_COLUMN_ORDER, table), null);
            applicationConfig.setOption(String.format(TABLE_COLUMN_ORDER_CUSTOM, table), "false");
        }
        save();
    }

    public int getCountdownCircleBackConfirmation() {
        return applicationConfig.getOptionAsInt(SammoaConfigOption.COUNTDOWN_CIRCLE_CONFIRMATION.key);
    }

    public int getCountdownRejoinTrackline() {
        return applicationConfig.getOptionAsInt(SammoaConfigOption.COUNTDOWN_REJOIN_TRACKLINE.key);
    }

    public int getDeadtimeAfterCircleBack() {
        return applicationConfig.getOptionAsInt(SammoaConfigOption.DEADTIME_AFTER_CIRCLE_BACK.key);
    }

    /**
     * Creates a directory given the configuration {@code option}.
     *
     * @param option the configuration option key which contains the location of
     *               the directory to create
     * @return the created directory
     */
    protected File getOrCreateDirectory(SammoaConfigOption option) throws IOException {

        File directory = applicationConfig.getOptionAsFile(option.getKey());

        Preconditions.checkNotNull(
                directory,
                "Could not find directory " + directory +
                " (key " + option.getKey() + "in your configuration file named" +
                " sammoa.properties)"
        );

        log.debug(option + " = " + directory);

        FileUtils.forceMkdir(directory);

        return directory;
    }

    /**
     * All Sammoa configuration options.
     *
     * @author sletellier <letellier@codelutin.com>
     * @author fdesbois <fdesbois@codelutin.com>
     */
    public enum SammoaConfigOption implements ConfigOptionDef {

        /** Main directory where to put sammoa data (logs, and others...). */
        DATA_DIRECTORY("sammoa.data.directory",
                       n("sammoa.config.data.directory"),
                       "${user.home}/.sammoa",
                       File.class
        ),
        TMP_DIRECTORY("sammoa.tmp.directory",
                      n("sammoa.config.tmp.directory"),
                      "${java.io.tmpdir}/sammoa",
                      File.class
        ),
        LOG_FILE("sammoa.logFile",
                 n("sammoa.config.log.file"),
                 "${sammoa.data.directory}/log/sammoa.log",
                 File.class
        ),
        UI_CONFIG_FILE("sammoa.ui.config",
                       n("sammoa.config.ui.config"),
                       "${sammoa.data.directory}/sammoaUI.xml",
                       File.class
        ),
        VERSION("sammoa.version",
                n("sammoa.config.application.version"),
                "",
                Version.class,
                false,
                true
        ),
        SITE_URL("sammoa.url",
                 n("sammoa.config.application.site.url"),
                 "http://forge.codelutin.com/projects/sammoa",
                 URL.class,
                 false,
                 true
        ),
        ORGANIZATION_NAME("sammoa.organizationName",
                 n("sammoa.config.license.organizationName"),
                 "",
                 String.class,
                 false,
                 true
        ),
        INCEPTION_YEAR("sammoa.inceptionYear",
                       n("sammoa.config.license.organizationName"),
                       "2012",
                       Integer.class,
                       false,
                       true
        ),
        /** The id/name of the current system/computer */
        SYSTEM_ID("sammoa.systemId",
                  n("sammoa.config.system.id"),
                  "A",
                  String.class
        ),
        /** The starting value to increment flight number */
        FLIGHT_NUMBER("sammoa.flightNumber",
                      n("sammoa.config.flight.number"),
                      "0",
                      Integer.class
        ),
        /** The backgroud shape file to display the world */
        BACKGROUND_SHAPE_FILE("sammoa.backgroundShapeFile",
                              n("sammoa.config.background.shape.file"),
                              "shape/vmap_area_thin.shp",
                              File.class
        ),
        /** The auto commit delay for {@link AutoSaveListener} */
        AUTO_COMMIT_DELAY("sammoa.autoCommitDelay",
                          n("sammoa.config.autoCommitDelay"),
                          "30000",
                          Long.class
        ),
        SURVEY_ID("sammoa.surveyId",
                    n("sammoa.config.survey.id"),
                    null,
                    Integer.class
        ),

        KEY_START("sammoa.ui." + PROPERTY_START,
                  n("sammoa.config.ui.start"),
                  "ctrl pressed B",
                  KeyStroke.class,
                  false,
                  false),

        KEY_STOP("sammoa.ui." + PROPERTY_STOP,
                 n("sammoa.config.ui.stop"),
                 "ctrl pressed E",
                 KeyStroke.class,
                 false,
                 false),

        KEY_BEGIN("sammoa.ui." + PROPERTY_BEGIN,
                  n("sammoa.config.ui.begin"),
                  "pressed F5",
                  KeyStroke.class,
                  false,
                  false),

        KEY_REJOIN("sammoa.ui." + PROPERTY_REJOIN,
                  n("sammoa.config.ui.rejoin"),
                  "pressed F6",
                  KeyStroke.class,
                  false,
                  false),

        KEY_END("sammoa.ui." + PROPERTY_END,
                n("sammoa.config.ui.end"),
                "pressed F9",
                KeyStroke.class,
                false,
                false),

        KEY_ADD("sammoa.ui." + PROPERTY_ADD,
                n("sammoa.config.ui.add"),
                "pressed F7",
                KeyStroke.class,
                false,
                false),

        KEY_LEFT_OBSERVATION("sammoa.ui." + PROPERTY_LEFT_OBSERVATION,
                             n("sammoa.config.ui.leftObservation"),
                             "pressed F1",
                             KeyStroke.class,
                             false,
                             false),

        KEY_CENTER_OBSERVATION("sammoa.ui." + PROPERTY_CENTER_OBSERVATION,
                               n("sammoa.config.ui.centerObservation"),
                               "pressed F3",
                               KeyStroke.class,
                               false,
                               false),

        KEY_RIGHT_OBSERVATION("sammoa.ui." + PROPERTY_RIGHT_OBSERVATION,
                              n("sammoa.config.ui.rightObservation"),
                              "pressed F12",
                              KeyStroke.class,
                              false,
                              false),

        KEY_CIRCLE_BACK("sammoa.ui." + PROPERTY_CIRCLE_BACK,
                        n("sammoa.config.ui.circleBack"),
                        "pressed F11",
                        KeyStroke.class,
                        false,
                        false),

        KEY_VALID_FLIGHT("sammoa.ui." + PROPERTY_VALID_FLIGHT,
                         n("sammoa.config.ui.validFlight"),
                         "alt pressed F",
                         KeyStroke.class,
                         false,
                         false),

        KEY_VALID_TRANSECT("sammoa.ui." + PROPERTY_VALID_TRANSECT,
                              n("sammoa.config.ui.validTransect"),
                              "alt pressed T",
                              KeyStroke.class,
                              false,
                              false),

        KEY_VALID_ROUTE("sammoa.ui." + PROPERTY_VALID_ROUTE,
                        n("sammoa.config.ui.validRoute"),
                        "alt pressed R",
                        KeyStroke.class,
                        false,
                        false),

        KEY_VALID_OBSERVATION("sammoa.ui." + PROPERTY_VALID_OBSERVATION,
                              n("sammoa.config.ui.validObservation"),
                              "alt pressed O",
                              KeyStroke.class,
                              false,
                              false),

        CIRCLE_BACK_ENABLE("sammoa.ui." + PROPERTY_CIRCLE_BACK_ENABLED,
                n("sammoa.config.ui.circleBackEnabled"),
                "true",
                Boolean.class,
                false,
                false),

        CIRCLE_BACK_TIMER_ENABLE("sammoa.ui." + PROPERTY_CIRCLE_BACK_TIMER_ENABLED,
                n("sammoa.config.ui.circleBackTimerEnabled"),
                "false",
                Boolean.class,
                false,
                false),

        DIVE_ENABLE("sammoa.ui." + PROPERTY_DIVE_ENABLED,
                n("sammoa.config.ui.diveEnabled"),
                "true",
                Boolean.class,
                false,
                false),

        REACTION_ENABLE("sammoa.ui." + PROPERTY_REACTION_ENABLED,
                n("sammoa.config.ui.reactionEnabled"),
                "true",
                Boolean.class,
                false,
                false),

        TABLE_TRANSECTS_COLUMNS_ORDER(String.format(TABLE_COLUMN_ORDER, Table.transects),
                n("sammoa.config.ui.table.transects.column.order"),
                "",
                String.class,
                false,
                false),

        TABLE_TRANSECTS_COLUMNS_ORDER_CUSTOM(String.format(TABLE_COLUMN_ORDER_CUSTOM, Table.transects),
                n("sammoa.config.ui.table.transects.column.order.custom"),
                "false",
                Boolean.class,
                false,
                false),

        TABLE_ROUTES_COLUMNS_ORDER(String.format(TABLE_COLUMN_ORDER, Table.routes),
                n("sammoa.config.ui.table.routes.column.order"),
                "",
                String.class,
                false,
                false),

        TABLE_ROUTES_COLUMNS_ORDER_CUSTOM(String.format(TABLE_COLUMN_ORDER_CUSTOM, Table.routes),
                n("sammoa.config.ui.table.routes.column.order.custom"),
                "false",
                Boolean.class,
                false,
                false),

        TABLE_OBSERVATIONS_COLUMNS_ORDER(String.format(TABLE_COLUMN_ORDER, Table.observations),
                n("sammoa.config.ui.table.observations.column.order"),
                "",
                String.class,
                false,
                false),

        TABLE_OBSERVATIONS_COLUMNS_ORDER_CUSTOM(String.format(TABLE_COLUMN_ORDER_CUSTOM, Table.observations),
                n("sammoa.config.ui.table.observations.column.order.custom"),
                "false",
                Boolean.class,
                false,
                false),

        TABLE_GEO_POINTS_COLUMNS_ORDER(String.format(TABLE_COLUMN_ORDER, Table.geoPoints),
                n("sammoa.config.ui.table.geoPoints.column.order"),
                "",
                String.class,
                false,
                false),

        TABLE_GEO_POINTS_COLUMNS_ORDER_CUSTOM(String.format(TABLE_COLUMN_ORDER_CUSTOM, Table.geoPoints),
                n("sammoa.config.ui.table.geoPoints.column.order.custom"),
                "false",
                Boolean.class,
                false,
                false),

        CSV_SEPARATOR("sammoa.csvSeparator",
                      n("sammoa.config.csv.separator"),
                      ";", char.class),

        COUNTDOWN_CIRCLE_CONFIRMATION("sammoa.countdownCircleConfirmation",
                n("sammoa.config.countdownCircleConfirmation"),
                "40",
                Integer.class),

        COUNTDOWN_REJOIN_TRACKLINE("sammoa.countdownRejoinTrackline",
                n("sammoa.config.countdownRejoinTrackline"),
                "120",
                Integer.class),

        DEADTIME_AFTER_CIRCLE_BACK("sammoa.deadtimeAfterCircle",
                n("sammoa.config.deadtimeAfterCircle"),
                "240",
                Integer.class);

        /** Configuration key. */
        private final String key;

        /** I18n key of option description */
        private final String description;

        /** Type of option */
        private final Class<?> type;

        /** Default value of option. */
        private String defaultValue;

        /** Flag to not keep option value on disk */
        private boolean isTransient;

        /** Flag to not allow option value modification */
        private boolean isFinal;

        SammoaConfigOption(String key,
                           String description,
                           String defaultValue,
                           Class<?> type,
                           boolean isTransient,
                           boolean isFinal) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.isTransient = isTransient;
            this.isFinal = isFinal;
        }

        SammoaConfigOption(String key,
                           String description,
                           String defaultValue,
                           Class<?> type) {
            this(key, description, defaultValue, type, false, false);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return isTransient;
        }

        @Override
        public boolean isFinal() {
            return isFinal;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public void setTransient(boolean newValue) {
            // not used
        }

        @Override
        public void setFinal(boolean newValue) {
            // not used
        }
    }
}
