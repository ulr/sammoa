package fr.ulr.sammoa.application.io.output.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Model of sammoa export.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ExportApplicationModel {

    public static ExportApplicationModel newModel(File exportDirectory,
                                                  String surveyId,
                                                  Iterable<String> flightIds) {
        ExportApplicationModel result = new ExportApplicationModel();
        result.exportFile = exportDirectory;
        result.surveyId = surveyId;
        result.flightIds = flightIds;
        return result;
    }

    protected String surveyId;

    protected Iterable<String> flightIds;

    protected File exportFile;

    public String getSurveyId() {
        return surveyId;
    }

    public Iterable<String> getFlightIds() {
        return flightIds;
    }

    public File getExportFile() {
        return exportFile;
    }
}
