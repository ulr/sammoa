package fr.ulr.sammoa.application.io.output.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.DbMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.out.PrepareDataForExport;

import java.util.List;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
class SammoaPrepareDataForExport implements PrepareDataForExport<SammoaEntityEnum> {

    protected final DbMeta<SammoaEntityEnum> dbMetas;

    protected final SammoaTopiaPersistenceContext tx;

    public SammoaPrepareDataForExport(DbMeta<SammoaEntityEnum> dbMetas, SammoaTopiaPersistenceContext tx) {
        this.dbMetas = dbMetas;
        this.tx = tx;
    }

    @Override
    public <E extends TopiaEntity> List<E> prepareData(TableMeta<SammoaEntityEnum> tableMeta) {

        return getEntities(tx, tableMeta, null);
    }

    @Override
    public <E extends TopiaEntity> List<E> prepareData(AssociationMeta<SammoaEntityEnum> associationMeta) {
        TableMeta<SammoaEntityEnum> tableMeta = dbMetas.getTable(associationMeta.getSource());
        return getEntities(tx, tableMeta, "size(e." + associationMeta.getName() + ") > 0");
    }

    protected <E extends TopiaEntity> List<E> getEntities(SammoaTopiaPersistenceContext tx,
                                                          TableMeta<SammoaEntityEnum> tableMeta,
                                                          String extraWhereQuery) {

        SammoaEntityEnum entityEnum = tableMeta.getSource();
        try {
            TopiaDao dao = tx.getDao(entityEnum.getContract());

            // first query to count datas

            String hql = "FROM " + entityEnum.getImplementationFQN();

            if (StringUtils.isNotEmpty(extraWhereQuery)) {
                hql += " WHERE " + extraWhereQuery;
            }

            ImmutableMap <String, Object> parameters = ImmutableMap.of();

            List<E> result = tx.getJpaSupport().findAll(hql, parameters);

            return result;
        } catch (TopiaException eee) {
            throw new SammoaTechnicalException("Could not obtain data", eee);
        }
    }
}
