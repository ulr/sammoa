package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.io.input.application.DateParserFormatter;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.topia.service.csv.TopiaCsvCommons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CsvMigrate {

    public static final String CSV_SEPARATOR = ";";

    protected static final ValueParserFormatter<Date> DATE_PARSER_FORMATTER = DateParserFormatter.of();

    protected final Path csvPath;

    protected List<String> header;

    protected List<List<String>> bodyLines;

    public CsvMigrate(Path csvPath) {
        this.csvPath = csvPath;
    }

    public CsvMigrate load() throws IOException {
        List<String> lines = Files.readAllLines(csvPath);
        header = null;
        bodyLines = Lists.newArrayListWithCapacity(lines.size());
        lines.stream()
                .filter(StringUtils::isNotBlank)
                .map(line -> Lists.newArrayList(line.split(CSV_SEPARATOR)))
                .forEach(values -> {
                    if (header == null) {
                        header = values;
                    } else {
                        bodyLines.add(values);
                    }
        });
        return this;
    }

    protected void save() throws IOException {
        List<String> lines = Stream.concat(Stream.of(header), bodyLines.stream())
                .map(line -> line.stream().collect(Collectors.joining(CSV_SEPARATOR)))
                .collect(Collectors.toList());
        Files.write(csvPath, lines, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
    }

    protected int getColumnIndex(String column) {
        int index = header.indexOf(column);
        if (index < 0) {
            throw new IllegalArgumentException("Unknown column " + column);
        }
        return index;
    }

    public CsvMigrate renameColumn(String oldName, String newName) {
        int index = getColumnIndex(oldName);
        header.set(index, newName);
        return this;
    }

    public CsvMigrate updateValue(String column, Function<Line, String> newValueFunction) {
        streamLine().forEach(line -> {
            String newValue = newValueFunction.apply(line);
            line.set(column, newValue);
        });
        return this;
    }

    public CsvMigrate addColumn(String columnName, String defaultValue) {
        header.add(columnName);
        bodyLines.forEach(line -> line.add(defaultValue));
        return this;
    }


    public Stream<Line> streamLine() {
        return IntStream.range(0, bodyLines.size())
                .boxed()
                .map(Line::new);
    }

    public class Line {
        protected final int lineIndex;

        public Line(int lineIndex) {
            this.lineIndex = lineIndex;
        }

        public int getLineIndex() {
            return lineIndex;
        }

        protected List<String> getLine() {
            return bodyLines.get(lineIndex);
        }

        public String get(String columnName) {
            int colIndex = getColumnIndex(columnName);
            return getLine().get(colIndex);
        }

        public String set(String columnName, String newValue) {
            int colIndex = getColumnIndex(columnName);
            return getLine().set(colIndex, newValue);
        }

        public Date getOldDate(String columnName) {
            String valueStr = get(columnName);
            try {
                return TopiaCsvCommons.DAY_TIME_SECOND_MILI_WITH_TIMESTAMP.parse(valueStr);
            } catch (ParseException e) {
                return null;
            }
        }

        public void setOldDate(String columnName, Date newValue) {
            String format = TopiaCsvCommons.DAY_TIME_SECOND_MILI_WITH_TIMESTAMP.format(newValue);
            set(columnName, format);
        }

        public Date getDate(String columnName) {
            String valueStr = get(columnName);
            try {
                return DATE_PARSER_FORMATTER.parse(valueStr);
            } catch (ParseException e) {
                return null;
            }
        }

        public void setDate(String columnName, Date newValue) {
            String format = DATE_PARSER_FORMATTER.format(newValue);
            set(columnName, format);
        }
    }
}
