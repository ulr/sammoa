package fr.ulr.sammoa.application.flightController;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ulr.sammoa.application.device.DeviceManager;
import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.device.gps.GpsLocationEvent;
import fr.ulr.sammoa.application.device.gps.GpsLocationListener;
import fr.ulr.sammoa.persistence.Dates;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.util.Date;
import java.util.Map;

/**
 * Created: 21/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class FlightControllerOnBoard extends BaseFlightController implements GpsLocationListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FlightControllerOnBoard.class);

    protected Map<Date, GeoPoint> geoPoints = Maps.newHashMap();

    @Override
    public <T extends DeviceManager> void openDeviceManager(Class<T> deviceManager) {
        boolean autoStart = initialized && isRunning();

        T result = getDeviceManagerProvider().openDeviceManager(deviceManager, autoStart);

        if (result instanceof GpsHandler) {
            ((GpsHandler) result).addGpsLocationListener(this);

        } else if (autoStart && result instanceof  AudioRecorder) {
            restartAudio((AudioRecorder) result);
        }
    }

    @Override
    public void init(Flight flight) {

        GpsHandler gpsHandler = getGpsHandler();
        Preconditions.checkNotNull(gpsHandler, "You must open the gpsHandler device before init");

        AudioRecorder audioRecorder = getAudioRecorder();
        Preconditions.checkNotNull(audioRecorder, "You must open the audioRecorder device before init");

        super.init(flight);

        geoPoints = Maps.newHashMap();
        for (GeoPoint point : service.getFlightGeoPoints(flight)) {
            GeoPoint point2 = geoPoints.put(point.getRecordTime(), point);
            if (point2 != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Duplication point " + point + " , "+ point2);
                }
            }
        }

        initCurrentRoute(service.getLastUnfinishedRoute(flight));

        if (isRunning()) {

            gpsHandler.start();

            audioRecorder.start();

            restartAudio(audioRecorder);
        }
    }

    @Override
    public void start() {

        getGpsHandler().start();

        getAudioRecorder().start();

        super.start();
    }

    @Override
    public void stop() {

        super.stop();

        getAudioRecorder().stop();

        getGpsHandler().stop();
    }

    // TODO Sbavencoff 20/10/2014 : locationChanged et getLocation
    // au demarage du vole il est possible d'enregistrer deux fois la même position
    // si le threads d'enregistrement de la position toute les deux seconde, enregistre la première position
    // après l'enregistrement de la position la position de l'evenement de démmarage du vol

    @Override
    public void locationChanged(GpsLocationEvent event) {
        GeoPoint newLocation = event.getNewValue();
        GeoPoint existLocation = geoPoints.get(newLocation.getRecordTime());
        if (existLocation == null) {
            newLocation.setFlight(flight);
            persistence.delayEntityCreation(newLocation);
            geoPoints.put(newLocation.getRecordTime(), newLocation);
        }
    }

    @Override
    protected GeoPoint getLocation(SammoaTopiaPersistenceContext tx) throws TopiaException {
        GeoPoint gpsLocation = getGpsHandler().getCurrentLocation();
        GeoPoint result = geoPoints.get(gpsLocation.getRecordTime());
        if (result == null) {
            gpsLocation.setFlight(flight);
            if (log.isDebugEnabled()) {
                log.debug("Create a GeoPoint " + gpsLocation);
            }
            result = tx.getGeoPointDao().create(gpsLocation);
            geoPoints.put(result.getRecordTime(), result);
        }
        return result;
    }

    @Override
    protected void onCurrentTransectChanged(TransectFlight oldValue,
                                            TransectFlight newValue) {

        // Record audio for the currentTransect

        if (!Objects.equal(oldValue, newValue)) {

            AudioRecorder audioRecorder = getAudioRecorder();

            if (newValue == null) {
                audioRecorder.stopRecord();

            } else {
                Date audioStartTime = Dates.newDateWithoutMillis();
                File audioFile = flightStorage.getAudioFile(
                        audioStartTime, audioRecorder.getOutputType().getExtension());
                audioRecorder.record(audioFile);
            }
        }

        super.onCurrentTransectChanged(oldValue, newValue);
    }

    protected void restartAudio(AudioRecorder audioRecorder) {
        if (state == FlightState.ON_EFFORT
            || currentRoute.getRouteType() == RouteType.CIRCLE_BACK) {

            Date audioStartTime = Dates.newDateWithoutMillis();
            File audioFile = flightStorage.getAudioFile(audioStartTime, audioRecorder.getOutputType().getExtension());
            audioRecorder.record(audioFile);
        }
    }

    protected GpsHandler getGpsHandler() {
        return getDeviceManager(GpsHandler.class);
    }

    protected AudioRecorder getAudioRecorder() {
        return getDeviceManager(AudioRecorder.class);
    }
}
