/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device;

/**
 * Enumeration to represent device (gps, audio) state.
 *
 * @author echatellier
 */
public enum DeviceState {
    /** voyant gris : pas de peripherique */
    UNAVAILABLE,
    /** voyant bleu : peripherique prêt */
    READY,
    /** voyant vert : reception des données ou lecture en cours */
    RUNNING,
    /** voyant rouge clignotant : périphirique prêt mais aucune données */
    ERROR
}
