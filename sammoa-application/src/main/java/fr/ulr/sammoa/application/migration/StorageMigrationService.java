package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class StorageMigrationService extends SammoaServiceSupport {

    private static final Log log = LogFactory.getLog(StorageMigrationService.class);

    public static final Version INITIAL_MODEL_VERSION = VersionBuilder.create("1.0").build();

    public static final String PROPERTY_MODEL_VERSION = "model.version";

    public static final ImmutableList<AbstractMigration> MIGRATIONS = ImmutableList.of(
            new V1101RenameCampaignToSurvey(),
            new V1102RenameSectorToSubRegion(),
            new V1103RenameAudioFile(),
            new V1104ChangeStrateType(),
            new V1105AddTimeZoneOnFlight()
    );

    public void runMigrateLocalDirectory(List<String> versions) {

        List<AbstractMigration> migrations = MIGRATIONS.stream()
                .filter(mig -> versions.contains(mig.getVersion().getVersion()))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(migrations)) {
            if (log.isInfoEnabled()) {
                log.info("migrateImportFile data directory");
            }

            File dataDirectory = config.getDataDirectory();

            for (AbstractMigration migration : migrations) {

                migration.setDataDirectory(dataDirectory);

                SammoaTopiaPersistenceContext sammoaTopiaPersistenceContext = beginTransaction();

                migration.setPersistenceContext(sammoaTopiaPersistenceContext);

                if (log.isInfoEnabled()) {
                    log.info("apply migration " + migration);
                }

                try {
                    migration.migrateLocalDirectory();
                } catch (Exception e) {
                    throw new SammoaTechnicalException("Error on apply migration " + migration, e);
                } finally {
                    endTransaction(sammoaTopiaPersistenceContext);
                }
            }
        }
    }

    public File runSurveyStorageMigration(File surveyDirectory, MigrationRequester requester) {
        Version storageVersion = getStorageVersion(surveyDirectory);
        Version modelVersion = getModelVersion();

        if (!modelVersion.equals(storageVersion)) {

            if (log.isInfoEnabled()) {
                log.info("migrateImportFile survey directory in " + storageVersion + " to " + modelVersion);
            }

            for (AbstractMigration migration : MIGRATIONS) {

                Version version = migration.getVersion();

                if (storageVersion.before(version) && modelVersion.afterOrEquals(version)) {

                    migration.setPersistenceContext(null);
                    migration.setDataDirectory(surveyDirectory);
                    migration.setRequester(requester);

                    if (log.isInfoEnabled()) {
                        log.info("apply migration " + migration);
                    }

                    try {
                        surveyDirectory = migration.migrateImportFile();
                    } catch (Exception e) {
                        throw new SammoaTechnicalException("Error on apply migration " + migration, e);
                    }
                }
            }
        }

        return surveyDirectory;

    }

    protected Version getStorageVersion(File surveyDirectory) {
        Version version = INITIAL_MODEL_VERSION;
        if (surveyDirectory.getName().startsWith(Survey.class.getName())) {

            File[] propertiesFiles = surveyDirectory.listFiles(
                    file -> file.isFile() && file.getName().endsWith(".properties")
            );

            if (propertiesFiles != null && propertiesFiles.length == 1) {

                Properties properties = new Properties();
                try (BufferedReader reader = Files.newBufferedReader(propertiesFiles[0].toPath())) {
                    properties.load(reader);
                    String versionString = properties.getProperty(PROPERTY_MODEL_VERSION);
                    version = VersionBuilder.create(versionString).build();
                } catch (IOException e) {
                    throw new SammoaTechnicalException(
                            "Could not load storage properties file", e);
                }
            }
        }

        return version;
    }

    protected Version getModelVersion() {
        String versionString = persistence.getRootContext().getModelVersion();
        return VersionBuilder.create(versionString).build();
    }

    public void addVersion(Properties properties) {
        String version = getModelVersion().getVersion();
        properties.put(PROPERTY_MODEL_VERSION, version);
    }

}
