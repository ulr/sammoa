package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.persistence.Survey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class V1101RenameCampaignToSurvey extends AbstractMigration {

    private static final Log log = LogFactory.getLog(V1101RenameCampaignToSurvey.class);

    public static final String CAMPAIGN_DIRECTORY_PREFIX = "fr.ulr.sammoa.persistence.Campaign";
    public static final String OLD_CAMPAIGN_DIRECTORY_NAME = "campaign";

    public V1101RenameCampaignToSurvey() {
        super("1.1.0.1", "Rename Campaign to Survey");
    }

    protected static String migrateTopiaId(String campaignTopiaId) {
        return campaignTopiaId.replace(CAMPAIGN_DIRECTORY_PREFIX, Survey.class.getCanonicalName());
    }

    @Override
    public File migrateImportFile() throws IOException {

        Path campaignPath = dataDirectory.toPath();
        String SurveyName = migrateTopiaId(dataDirectory.getName());
        Path surveyPath = campaignPath.getParent().resolve(SurveyName);
        Files.move(campaignPath, surveyPath);

        Path campaignPropertiesCsvPath = surveyPath.resolve("campaign.properties");
        Path surveyPropertiesPath = surveyPath.resolve("survey.properties");
        Files.move(campaignPropertiesCsvPath, surveyPropertiesPath);

        Path campaignCsvPath = surveyPath.resolve("csv/Campaign.csv");
        Path surveyCsvPath = surveyPath.resolve("csv/Survey.csv");
        Files.move(campaignCsvPath, surveyCsvPath);

        loadCvs(surveyCsvPath)
            .updateValue("topiaId", line -> migrateTopiaId(line.get("topiaId")))
            .save();

        migrateCsv(surveyPath.resolve("csv/Observer.csv"));
        migrateCsv(surveyPath.resolve("csv/Sector.csv"));

        Path flightsPaths = surveyPath.resolve("flight");
        File[] flightFiles = flightsPaths.toFile().listFiles();
        if (flightFiles != null) {
            for(File flightFile : flightFiles) {
                if (flightFile.isDirectory())
                    migrateFlight(flightFile.toPath());
            }
        }
        return surveyPath.toFile();
    }

    @Override
    public void migrateLocalDirectory() throws IOException {
        Path currentSurveyPath = SammoaDirectories.getSurveyDirectory(dataDirectory).toPath();
        Path oldCampaignsPath = dataDirectory.toPath().resolve(OLD_CAMPAIGN_DIRECTORY_NAME);

        if (oldCampaignsPath.toFile().isDirectory()) {

            File[] subFiles = oldCampaignsPath.toFile().listFiles();
            if (subFiles != null) {
                for (File subFile : subFiles) {

                    Path campaignPath = subFile.toPath();
                    String SurveyName = migrateTopiaId(subFile.getName());
                    Path surveyPath = currentSurveyPath.resolve(SurveyName);
                    Files.move(campaignPath, surveyPath);
                }
            }

            Files.delete(oldCampaignsPath);
        }
    }

    protected void migrateCsv(Path path) throws IOException {
        loadCvs(path)
            .renameColumn("campaign", "survey")
            .updateValue("survey", line -> migrateTopiaId(line.get("survey")))
            .save();
    }

    protected void migrateFlight(Path flightPath) throws IOException {
        if (log.isInfoEnabled()) {
            log.info("Migrate flight directory : " + flightPath.toString());
        }
        migrateCsv(flightPath.resolve("csv/Flight.csv"));

        Properties properties = new Properties();
        Path propertiesPath = flightPath.resolve("flight.properties");
        try (BufferedReader reader = Files.newBufferedReader(propertiesPath)) {
            properties.load(reader);
        }

        String surveyRegionCode = properties.getProperty("campaign.region.code");
        String surveyCode = properties.getProperty("campaign.code");
        properties.setProperty("survey.region.code", surveyRegionCode);
        properties.setProperty("survey.code", surveyCode);

        try (BufferedWriter writer = Files.newBufferedWriter(propertiesPath)) {
            properties.store(writer, "Migrate by " + description);
        }
    }
}
