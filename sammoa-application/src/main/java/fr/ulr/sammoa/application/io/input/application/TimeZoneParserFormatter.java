package fr.ulr.sammoa.application.io.input.application;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.util.TimeZone;

public class TimeZoneParserFormatter implements ValueParserFormatter<TimeZone> {

    public static TimeZoneParserFormatter of() {
        return new TimeZoneParserFormatter();
    }

    protected TimeZoneParserFormatter() {
    }

    @Override
    public String format(TimeZone value) {
        String str = null;
        if (value != null) {
            str = value.getID();
        }
        return str;
    }

    @Override
    public TimeZone parse(String value) {
        TimeZone timeZone = null;
        if (StringUtils.isNotBlank(value)) {
            timeZone = TimeZone.getTimeZone(value);
        }
        return timeZone;
    }
}
