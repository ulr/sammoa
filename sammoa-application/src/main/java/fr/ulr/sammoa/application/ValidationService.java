package fr.ulr.sammoa.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observation;
import fr.ulr.sammoa.persistence.ObservationTopiaDao;
import fr.ulr.sammoa.persistence.Observations;
import fr.ulr.sammoa.persistence.ObserverPositionTopiaDao;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.RouteTopiaDao;
import fr.ulr.sammoa.persistence.Routes;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectFlightTopiaDao;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;

/**
 * Created: 23/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ValidationService extends SammoaServiceSupport {

    public void validateFlight(Flight flight,
                               List<Route> routes,
                               boolean valid) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            TransectFlightTopiaDao transectFlightDAO =  tx.getTransectFlightDao();

            RouteTopiaDao routeDAO =  tx.getRouteDao();
            List<Route> allRoutes = routeDAO.forFlightEquals(flight).findAll();
            ObservationTopiaDao observationDAO =  tx.getObservationDao();
            List<Observation> observations = observationDAO.forFlightEquals(flight).findAll();

            // Valid all transectFlights
            List<TransectFlight> transectFlights = ImmutableList.copyOf(flight.getTransectFlight());
            for (int i = 0; i < transectFlights.size(); i++) {
                TransectFlight transectFlight =
                        transectFlightDAO.forTopiaIdEquals(transectFlights.get(i).getTopiaId()).findUnique();

                validateTransectFlight(tx, flight, transectFlight, allRoutes, observations, valid);

                flight.getTransectFlight().set(i, transectFlight);
            }

            Iterable<Route> otherRoutes = Routes.filterWithNoTransectFlight(routes);
            for (Route otherRoute : otherRoutes) {
                Route route = routeDAO.forTopiaIdEquals(otherRoute.getTopiaId()).findUnique();
                validateRoute(tx, route, allRoutes, observations, valid);
            }

            tx.commit();

        } finally {
            endTransaction(tx);
        }
    }

    public TransectFlight validateTransectFlight(Flight flight,
                                                 TransectFlight transectFlight) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            TransectFlightTopiaDao transectFlightDAO =  tx.getTransectFlightDao();
            TransectFlight result = transectFlightDAO.forTopiaIdEquals(transectFlight.getTopiaId()).findUnique();

            RouteTopiaDao routeDAO =  tx.getRouteDao();
            List<Route> routes = routeDAO.forFlightEquals(flight).findAll();
            ObservationTopiaDao observationDAO =  tx.getObservationDao();
            List<Observation> observations = observationDAO.forFlightEquals(flight).findAll();

            boolean valid = !transectFlight.isValid();

            validateTransectFlight(tx, flight, result, routes, observations, valid);

            tx.commit();

            transectFlight.setValid(valid);
            return result;
        } finally {
            endTransaction(tx);
        }
    }

    public Route validateRoute(Route route) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            RouteTopiaDao routeDAO =  tx.getRouteDao();
            Route result = routeDAO.forTopiaIdEquals(route.getTopiaId()).findUnique();

            boolean valid = !route.isValid();

            List<Route> routes = routeDAO.forFlightEquals(route.getFlight()).findAll();
            ObservationTopiaDao observationDAO =  tx.getObservationDao();
            List<Observation> observations = observationDAO.forFlightEquals(route.getFlight()).findAll();

            validateRoute(tx, result, routes, observations, valid);

            tx.commit();

            route.setValid(valid);
            return result;

        } finally {
            endTransaction(tx);
        }
    }

    public Observation validateObservation(Observation observation) {

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {

            ObservationTopiaDao dao =  tx.getObservationDao();
            Observation result =
                    dao.forTopiaIdEquals(observation.getTopiaId()).findUnique();

            boolean valid = !observation.isValid();

            validateObservation(tx, result, valid);

            tx.commit();

            observation.setValid(valid);
            return result;

        } finally {
            endTransaction(tx);
        }
    }

    protected void validateTransectFlight(SammoaTopiaPersistenceContext tx,
                                          Flight flight,
                                          TransectFlight transectFlight,
                                          List<Route> routes,
                                          List<Observation> observations,
                                          boolean valid)
            throws TopiaException {

        if (transectFlight.isDeleted()) {

            // Use FlightService for delete
            context.getService(FlightService.class).deleteTransectFlight(tx, flight, transectFlight);

        } else {

            TransectFlightTopiaDao transectFlightDAO =  tx.getTransectFlightDao();

            routes.stream()
                    .filter(Routes.withTransectFlight(transectFlight))
                    .forEach(route -> validateRoute(tx, route, routes, observations, valid));

            transectFlight.setValid(valid);
            transectFlightDAO.update(transectFlight);
        }
    }

    protected void validateRoute(SammoaTopiaPersistenceContext tx,
                                 Route route,
                                 List<Route> routes,
                                 List<Observation> observations,
                                 boolean valid)
        throws TopiaException {

        RouteTopiaDao routeDAO =  tx.getRouteDao();

        if (route.isDeleted()) {

            ObserverPositionTopiaDao observerPositionDAO =  tx.getObserverPositionDao();
            observerPositionDAO.deleteAll(route.getObserverPosition());

            routeDAO.delete(route);

        } else {

            Iterable<Observation> routeObservations =
                    Observations.filterInRoute(observations, route, routes, true);

            for (Observation observation : routeObservations) {
                validateObservation(tx, observation, valid);
            }

            route.setValid(valid);
            routeDAO.update(route);
        }
    }

    protected void validateObservation(SammoaTopiaPersistenceContext tx,
                                       Observation observation,
                                       boolean valid)
        throws TopiaException {

        ObservationTopiaDao dao =  tx.getObservationDao();

        if (observation.isDeleted()) {

            dao.delete(observation);

        } else {

            observation.setValid(valid);
            dao.update(observation);
        }
    }

}
