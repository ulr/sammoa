package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ulr.sammoa.application.io.SurveyStorage;
import org.apache.commons.io.FileUtils;
import org.nuiton.csv.ImportModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public abstract class ShpImporter<E> {

    private static final Log log =
            LogFactory.getLog(ShpImporter.class);

    protected final ImportModel<E> model;

    protected final String copyFileName;

    public ShpImporter(ImportModel<E> model, String copyFileName) {
        this.model = model;
        this.copyFileName = copyFileName;
    }

    protected abstract String onDbfLoaded(Iterable<E> elements,
                                          SurveyStorage storage);

    public String importShape(File file,
                              SurveyStorage storage) throws IOException {

        File dbfFile;
        String fileName = file.getName();
        File sourceDirectory = file.getParentFile();

        String ext = Files.getFileExtension(fileName);
        int extIndex = fileName.lastIndexOf(ext);
        final String fileNameWithoutExt = fileName.substring(0, extIndex);

        if (log.isDebugEnabled()) {
            log.debug("Source directory '" + sourceDirectory.getAbsolutePath() + "' and fileName '" + fileNameWithoutExt + "'");
        }

        if (!ext.equals("dbf")) {

            fileName = fileNameWithoutExt + "dbf";

            if (log.isDebugEnabled()) {
                log.debug("Use dbfFile " + fileName + " for import");
            }

            dbfFile = new File(sourceDirectory, fileName);

            if (!dbfFile.exists()) {
                throw new FileNotFoundException(
                        "Can't found dbf file '" + dbfFile + "' for import");
            }

        } else {
            dbfFile = file;
        }

        DbfImport<E> importer = new DbfImport<E>(model, dbfFile.toURI().toURL());

        String result = onDbfLoaded(importer, storage);

        String[] files = sourceDirectory.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String fullName) {
                String extension = Files.getFileExtension(fullName);
                int extIndex = fullName.lastIndexOf(extension);
                String name = fullName.substring(0, extIndex);
                if (log.isDebugEnabled()) {
                    log.debug("Read file : " + fullName + " [ext = " + extension + "] -> check equals for " + name);
                }
                return fileNameWithoutExt.equals(name);
            }
        });

        for (String name : files) {
            String extension = Files.getFileExtension(name);
            File sourceFile = new File(sourceDirectory, name);
            File targetFile = new File(storage.getMapDirectory(),
                                       copyFileName + "." + extension);
            if (log.isInfoEnabled()) {
                log.info("Copy file '" + sourceFile.getAbsolutePath() + "' to '" + targetFile.getAbsolutePath() + "'");
            }

            FileUtils.copyFile(sourceFile, targetFile);
        }
        return result;
    }


}
