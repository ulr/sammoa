package fr.ulr.sammoa.application.io.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.RouteType;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.Strate;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Model of export map operation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class ExportMapModel {

    public static ExportMapModel newModel(File exportDirectory,
                                          String exportFilename,
                                          Survey survey,
                                          Date beginDate,
                                          Date endDate,
                                          List<Strate> strates,
                                          List<RouteType> routeTypes,
                                          List<Species> species) {
        ExportMapModel result = new ExportMapModel();
        result.exportDirectory = exportDirectory;
        result.exportFilename = exportFilename;
        result.survey = survey;
        result.beginDate = beginDate;
        result.endDate = endDate;
        result.strates = strates;
        result.routeTypes = routeTypes;
        result.species = species;
        return result;
    }

    protected Survey survey;

    protected Date beginDate;

    protected Date endDate;

    protected List<Strate> strates;

    protected List<RouteType> routeTypes;

    protected List<Species> species;

    protected File exportDirectory;

    protected String exportFilename;

    public File getExportDirectory() {
        return exportDirectory;
    }

    public File getExportFileWithoutExtension(String extension) {
        return new File(getExportDirectory(), exportFilename + extension);
    }

    public Survey getSurvey() {
        return survey;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public List<Strate> getStrates() {
        if (strates == null) {
            strates = Lists.newArrayList();
        }
        return strates;
    }

    public List<RouteType> getRouteTypes() {
        if (routeTypes == null) {
            routeTypes = Lists.newArrayList();
        }
        return routeTypes;
    }

    public List<Species> getSpecies() {
        if (species == null) {
            species = Lists.newArrayList();
        }
        return species;
    }

}
