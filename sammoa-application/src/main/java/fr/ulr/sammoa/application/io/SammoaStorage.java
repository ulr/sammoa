package fr.ulr.sammoa.application.io;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ulr.sammoa.application.DecoratorService;
import fr.ulr.sammoa.application.SammoaDirectories;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

/**
 * Storage for sammoa.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see SurveyStorage
 * @see FlightStorage
 * @since 0.6
 */
public abstract class SammoaStorage<E> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Root directory for this storage.
     */
    private final File directory;

    /**
     * Id of the storage.
     */
    private final String id;

    /**
     * Properties of the storage.
     */
    private final Properties properties;

    /**
     * Name of the properties file of the storage.
     */
    private final String propertiesFilename;

    public abstract Map<String, Object> getNaturalId();

    public abstract void fillProperties(E entity,
                                        DecoratorService decoratorService);

    SammoaStorage(File dataDirectory,
                  String id,
                  String propertiesFilename) {
        this.directory = new File(dataDirectory, id);
        this.id = id;
        this.propertiesFilename = propertiesFilename;
        this.properties = new Properties();
    }

    public String getId() {
        return id;
    }

    public File getDirectory() {
        return directory;
    }

    public File getCsvDirectory() {
        return SammoaDirectories.getCsvDirectory(getDirectory());
    }

    public File getPropertiesFile() {
        return new File(directory, propertiesFilename);
    }

    public Properties getProperties() {
        return properties;
    }

    public void loadProperties() throws IOException {
        try (BufferedReader reader = Files.newReader(getPropertiesFile(), Charsets.UTF_8)){
            properties.load(reader);
        }
    }

    public void saveProperties() throws IOException {

        try (BufferedWriter writer = Files.newWriter(getPropertiesFile(), Charsets.UTF_8)){
            properties.store(writer, "Saved by " + getClass().getName());
        }
    }

    public void delete() throws IOException {
        FileUtils.deleteDirectory(getDirectory());
    }

    protected void addInMap(String key, Map<String, Object> result) {
        String property = getProperties().getProperty(key);
        result.put(key, property);
    }

    protected void addIntInMap(String key, Map<String, Object> result) {
        String property = getProperties().getProperty(key);
        result.put(key, Integer.valueOf(property));
    }

    protected void putInProperties(String key, Object value) {
        getProperties().put(key, String.valueOf(value));
    }

    public String getName() {
        return getProperties().getProperty("name");
    }
}
