package fr.ulr.sammoa.application;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;


/**
 * Application config provider (for site generation).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SammoaApplicationConfigProvider implements ApplicationConfigProvider {

    @Override
    public String getName() {
        return "sammoa";
    }

    @Override
    public String getDescription(Locale locale) {
        return l(locale, "sammoa.application.config");
    }

    @Override
    public ConfigOptionDef[] getOptions() {
        return SammoaConfig.SammoaConfigOption.values();
    }

    @Override
    public ConfigActionDef[] getActions() {
        return new ConfigActionDef[0];
    }
}
