package fr.ulr.sammoa.application.device.gps;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import gnu.io.NRSerialPort;
import gnu.io.NativeResourceException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dinopolis.gpstool.gpsinput.GPSDevice;
import org.dinopolis.gpstool.gpsinput.GPSException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

/**
 * Created: 21/09/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class GPSNRSerialDevice implements GPSDevice {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GPSNRSerialDevice.class);

    public final static String PORT_NAME_KEY = "port_name";
    public final static String PORT_SPEED_KEY = "port_speed";

    protected final static String DEFAULT_PORT_NAME_LINUX = "/dev/ttyS1";
    protected final static String DEFAULT_PORT_NAME_WIN = "COM1";
    protected final static int DEFAULT_PORT_SPEED = 4800;

    protected String serialPortName;

    protected NRSerialPort serialPort;

    @Override
    public void init(Hashtable environment) throws GPSException {

        try {

            if (environment.containsKey(PORT_NAME_KEY)) {
                serialPortName = (String) environment.get(PORT_NAME_KEY);

            } else if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
                serialPortName = DEFAULT_PORT_NAME_WIN;

            } else {
                serialPortName = DEFAULT_PORT_NAME_LINUX;
            }

            final int serialPortSpeed;
            if (environment.containsKey(PORT_SPEED_KEY)) {
                serialPortSpeed = ((Integer) environment.get(PORT_SPEED_KEY)).intValue();

            } else {
                serialPortSpeed = DEFAULT_PORT_SPEED;
            }

            serialPort = new NRSerialPort(serialPortName, serialPortSpeed);

        } catch (Exception ex) {
            throw new GPSException("Invalid environment set for serial " +
                                   "connection : " + ex.getMessage(), ex);
        }
    }

    @Override
    public void open() throws GPSException {
        Preconditions.checkState(serialPort != null,
                                 "Please use init() method to prepare serialDevice");

        boolean connected;
        try {
            connected = serialPort.connect();

            if (connected && log.isInfoEnabled()) {
                log.info("Serial port " + serialPortName + " is opened");
            }

        } catch (NativeResourceException ex) {
            throw new GPSException("Native exception on open SerialPort "
                                   + serialPortName + " : " + ex.getMessage(), ex);
        } catch (Exception ex) {
            throw new GPSException("Unexpected exception on open SerialPort "
                                   + serialPortName + " : " + ex.getMessage(), ex);
        }
        if (!connected) {
            throw new GPSException("Unable to connect to the SerialPort "
                                   + serialPortName);
        }
    }

    @Override
    public void close() throws GPSException {
        try {
            if (serialPort != null) {
                serialPort.disconnect();

                if (log.isInfoEnabled()) {
                    log.info("Serial port " + serialPortName + " is closed");
                }
            }

        } catch (NativeResourceException ex) {
            throw new GPSException("Native exception on close SerialPort "
                                   + serialPortName + " : " + ex.getMessage(), ex);

        } catch (Exception ex) {
            throw new GPSException("Unexpected exception on close SerialPort "
                                   + serialPortName + " : " + ex.getMessage(), ex);
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return serialPort != null ? serialPort.getInputStream() : null;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return serialPort != null ? serialPort.getOutputStream() : null;
    }
}
