/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device.audio;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe permettant l'enregistrement d'un fichier audio. Par defaut la
 * methode record n'ecrase jamais un fichier existant mais incremente le nom
 * du fichier.
 * 
 * Pour l'instant aucune compression n'est faite. Si on souhaite une compression
 * il faut trouver une librairie de compression et modifier la ligne 284
 * AudioFormat.Encoding targetEncoding = AudioFormat.Encoding.PCM_SIGNED;
 * pour utilise le format de compression choisi.
 * 
 * Created: 27 aout 2012
 *
 * @author Benjamin POUSSIN <poussin@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SammoaAudioRecorder implements AudioRecorder {

    private static final Log log = LogFactory.getLog(SammoaAudioRecorder.class);

    protected AudioConfig config;

    protected AudioFormat.Encoding encoding;

    protected AudioFileFormat.Type outputType;

    protected float sampleRate;

    protected int sampleSizeInBits;

    protected int priority;

    protected DeviceState state;

    protected Set<DeviceStateListener> listeners;

    protected AudioRecorderThread currentRecorder;

    protected AudioLineProvider audioLineProvider;

    protected AudioLevelDataLine audioLevelDataLine;

    protected int delay;

    public SammoaAudioRecorder(AudioConfig config, int priority) {
        this.config = config;
        init(config.getSampleRate(), config.getSampleSizeInBits(),
                config.getCompression(), config.getRecordDelayInSeconds(),
                priority);
    }

    /**
     * Permet de forcer le sampleRate et sampleSizeInBits
     * @param sampleRate 8000, 11025, 16000, 22050, 44100
     * @param sampleSizeInBits 8 ou 16
     * @param delay le delai avant de reellement coupe un enregistrement (tous
     * les PC ne le supporte pas)
     */
    public SammoaAudioRecorder(
            float sampleRate, int sampleSizeInBits, String compression, int delay, int priority) {
        init(sampleRate, sampleSizeInBits, compression, delay, priority);
    }

    protected void init(float sampleRate, int sampleSizeInBits, String compression, int delay, int priority) {
        this.delay = delay;
        this.priority = priority;
        
        setSample(sampleRate, sampleSizeInBits, compression);

        outputType = AudioFileFormat.Type.WAVE;
        listeners = Sets.newHashSet();
    }

    public void setSample(float sampleRate, int sampleSizeInBits, String compression) {

        this.sampleRate = sampleRate;
        this.sampleSizeInBits = sampleSizeInBits;

        if (StringUtils.isNotBlank(compression)) {
            encoding = new AudioFormat.Encoding(compression);
        } else {
            encoding = AudioFormat.Encoding.PCM_SIGNED;
        }

        outputType = AudioFileFormat.Type.WAVE;
    }



    @Override
    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state, DeviceTechnicalException error) {
        DeviceState oldValue = getState();
        this.state = state;

        // Fire on listeners
        DeviceStateEvent event = new DeviceStateEvent(this, oldValue, state);
        event.setError(error);
        for (DeviceStateListener listener : listeners) {
            listener.stateChanged(event);
        }
    }

    @Override
    public AudioFileFormat.Type getOutputType() {
        return outputType;
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return listeners;
    }

    @Override
    public void open() throws DeviceTechnicalException {
        try {
            this.audioLineProvider = AudioLineProvider.getInstance();
            setState(DeviceState.READY, null);
        } catch (Exception e) {
            setState(DeviceState.UNAVAILABLE, null);
            throw new DeviceTechnicalException(this, "Can't open audio device", e);
        }
    }

    @Override
    public void start() {
        // nothing to do
    }


    @Override
    public void record(File outputFile) {
        try {
            recordStream(outputFile, false);
        } catch (LineUnavailableException | IllegalArgumentException | SecurityException | IllegalStateException ex) {
            fireError("Error recording " + outputFile.getName(), ex);
        }
    }

    /**
     * Cette methode laisse passer les exceptions sans les intercepter
     * @param outputFile
     * @param overwrite
     * @throws LineUnavailableException
     */
    public void record(File outputFile, boolean overwrite) throws LineUnavailableException {
        recordStream(outputFile, overwrite);
    }

    /***
     *
     * @param outputFile
     * @param overwrite si faux alors on recherche un nom de fichier inexistant
     * pour l'utiliser à la place du fichier d'entre. Par exemple si toto.wav
     * est déjà existant, alors toto-1.wav sera utilise ou toto-2.wav, ...
     */
    public void recordStream(File outputFile, boolean overwrite) throws LineUnavailableException {
        Preconditions.checkNotNull(outputFile);
        
        if (!overwrite && outputFile.exists()) {
            // recherche du premier fichier inexistant
            String path = outputFile.getName();
            String filename = FilenameUtils.getBaseName(path);
            String ext = FilenameUtils.getExtension(path);

            int i = 1;
            while (outputFile.exists() && i < Integer.MAX_VALUE) {
                outputFile = new File(path + "-"+(i++) + ext);
            }
        }

        // stop last started if necessary
        stop();
        openLine();
        if (audioLevelDataLine != null) {
            // start new recorder thread
            currentRecorder = new AudioRecorderThread(audioLevelDataLine, encoding, outputType, outputFile, this);
            currentRecorder.start();

            setState(DeviceState.RUNNING, null);
        }
        
    }

    @Override
    public void stopRecord() {
        stop();
    }

    @Override
    public void stop() {
        if (currentRecorder != null) {
            if (log.isDebugEnabled()) {
                log.debug("Ask for stop record... close in " + delay + " seconds");
            }

            if (delay == 0) {
                currentRecorder.stopRecord(audioLineProvider);

            } else {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        currentRecorder.stopRecord(audioLineProvider);
                    }
                }, delay * 1000);
            }
            currentRecorder = null;
            setState(DeviceState.READY, null);
        } else {
            closeLine();
        }
    }

    protected void openLine() {
        if (audioLevelDataLine == null && audioLineProvider != null) {
            try {
                audioLevelDataLine = audioLineProvider.openAudioLine(this.priority, this.sampleRate, this.sampleSizeInBits)
                        .orElseThrow(() -> new SammoaTechnicalException("Could not open audio line"));
                audioLevelDataLine.addLineListener(this::lineListener);
            } catch (LineUnavailableException e) {
                fireError("Error on open audio line ", e);
            }
        }
    }

    protected void lineListener(LineEvent event) {
        if (LineEvent.Type.CLOSE.equals(event.getType())) {
            audioLevelDataLine = null;
            setState(DeviceState.READY, null);
        }
    }

    protected void closeLine() {
        if (audioLevelDataLine != null) {
            audioLevelDataLine.removeLineListener(this::lineListener);
            audioLineProvider.closeAudioLine(audioLevelDataLine);
            audioLevelDataLine = null;
        }
    }

    @Override
    public void close() throws DeviceTechnicalException {
        stop();
    }

    protected void fireError(String message, Throwable cause) {
        if (log.isErrorEnabled()) {
            log.error(message, cause);
        }
        DeviceTechnicalException error = new DeviceTechnicalException(
                this, message, cause);
        setState(DeviceState.ERROR, error);
    }

    protected static class AudioRecorderThread extends Thread {

        protected AudioLevelDataLine targetDataLine;
        protected AudioFileFormat.Type fileType;
        protected File file;
        protected AudioInputStream stream;
        protected SammoaAudioRecorder audioRecorder;

        public AudioRecorderThread(
                AudioLevelDataLine targetDataLine, AudioFormat.Encoding encoding,
                AudioFileFormat.Type fileType, File file,
                SammoaAudioRecorder audioRecorder) {

            this.fileType = fileType;
            this.file = file;
            this.audioRecorder = audioRecorder;
            this.targetDataLine = targetDataLine;

            stream = new AudioInputStream(targetDataLine);
            try {
                stream = AudioSystem.getAudioInputStream(encoding, stream);
            } catch (Exception eee) {
                throw new SammoaTechnicalException(eee);
            }
        }

        /**
         * Arrete l'enregistrement N seconde apres la demande, N etant
         * le parametre delayToClose passer lors de la construction du thread
         */
        public void stopRecord(AudioLineProvider audioLineProvider) {
            targetDataLine.removeLineListener(audioRecorder::lineListener);
            audioLineProvider.closeAudioLine(targetDataLine);
            if (log.isDebugEnabled()) {
                log.debug("Closing record: " + file);
            }
        }

        @Override
        public void run(){
            if (log.isDebugEnabled()) {
                log.debug("Write audio to: " + file);
            }

            try {
                AudioSystem.write(stream, fileType, file);

            } catch (IOException ex) {
                audioRecorder.fireError("Error recording " + file.getName(), ex);
            }
        }
    }
}
