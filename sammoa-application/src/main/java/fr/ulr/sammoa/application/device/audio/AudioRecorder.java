package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.device.DeviceManager;

import javax.sound.sampled.AudioFileFormat;
import java.io.File;

/**
 * Recorder of audio files. Two recorder are used and a delay between recording
 * could be customized.
 * <p/>
 * Created: 12/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public interface AudioRecorder extends DeviceManager {

    /** @return the {@link AudioFileFormat.Type} used (default is WAV) */
    AudioFileFormat.Type getOutputType();

    /**
     * Record the audio line and save the data in given {@code outputFile}. The
     * previous recording will be stopped after some delay depends
     * on {@link AudioConfig#getRecordDelayInSeconds()}. The delay is
     * useful to avoid recording lost or too quick between two files.
     *
     * @param outputFile   File to record
     */
    void record(File outputFile);

    /**
     * Stop the current recording.
     */
    void stopRecord();
}
