package fr.ulr.sammoa.application.io.input.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ImportableColumn;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 25/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class DbfImport<E> extends Import<E> {

    private static final Log log = LogFactory.getLog(DbfImport.class);

    protected DbfTableModel source;

    protected Iterator<List<Object>> records;

    protected List<Object> record;

    public DbfImport(ImportModel<E> model, URL fileUrl) {
        // Use dummy reader for superclass Import that manage by default csv files
        super(model, new Reader() {

            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                return -1;
            }

            @Override
            public void close() throws IOException {
            }
        });

        try {
            // FIXME-fdesbois-2012-07-26 : there is an issue with encoding, don't know how to customize it
            this.source = DbfTableModel.read(fileUrl);

        } catch (Exception e) {
            throw new SammoaTechnicalException(e);
        }
        this.records = source.getRecords();
    }

    @Override
    protected <T> String readValue(ImportableColumn<E, T> field, int lineNumber) {
        try {

            int columnNumber = source.getColumnIndexForName(field.getHeaderName());

            String value = String.valueOf(record.get(columnNumber));

            return value;

        } catch (Exception e) {
            throw new ImportRuntimeException(
                   t("sammoa.dbf.import.error.unableToReadField",
                      field.getHeaderName(), lineNumber), e);
        }
    }

    @Override
    public void close() {
        // nothing to do
    }

    @Override
    protected boolean readRow() throws ImportRuntimeException {
        boolean result;
        if (records.hasNext()) {
            record = records.next();
            if (log.isTraceEnabled()) {
                log.trace("Read record " +  record);
            }
            result = true;

        } else {
            result = false;
        }
        return result;
    }

    @Override
    protected String[] getHeaders() throws ImportRuntimeException {
        List<String> list = Lists.newArrayList();
        for (int index = 0; index < source.getColumnCount(); index++) {
            list.add(source.getColumnName(index));
        }
        return list.toArray(new String[list.size()]);
    }
}
