package fr.ulr.sammoa.application.device;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.device.audio.AudioConfig;
import fr.ulr.sammoa.application.device.audio.AudioPositionListener;
import fr.ulr.sammoa.application.device.audio.AudioReader;
import fr.ulr.sammoa.application.device.audio.AudioRecorder;
import fr.ulr.sammoa.application.device.audio.SammoaAudioReader;
import fr.ulr.sammoa.application.device.audio.SammoaAudioRecorder;
import fr.ulr.sammoa.application.device.gps.GpsConfig;
import fr.ulr.sammoa.application.device.gps.GpsHandler;
import fr.ulr.sammoa.application.device.gps.GpsLocationListener;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * Created: 21/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DeviceManagerProvider extends SammoaServiceSupport {

    protected final Set<DeviceManager> deviceManagers;

    public DeviceManagerProvider() {
        deviceManagers = Sets.newHashSet();
    }

    public <T extends DeviceManager> T openDeviceManager(Class<T> deviceManagerClass,
                                                         boolean autoStart) {

        T result;
        if (GpsHandler.class.isAssignableFrom(deviceManagerClass)) {
            result = (T) openGpsDevice(context.getConfig().getGpsConfig());

        } else if (AudioRecorder.class.isAssignableFrom(deviceManagerClass)) {
            result = (T) openAudioRecorderDevice(context.getConfig().getAudioConfig());

        } else if (AudioReader.class.isAssignableFrom(deviceManagerClass)) {
            result = (T) openAudioReaderDevice(context.getConfig().getAudioConfig());

        } else {
            throw new IllegalArgumentException("The deviceManager class "
                                               + deviceManagerClass.getName()
                                               + " is not supported");
        }

        if (autoStart) {
            result.start();
        } else {
            result.open();
        }
        return result;
    }

    public <T extends DeviceManager> T getDeviceManager(Class<T> deviceManagerClass) {
        Preconditions.checkNotNull(deviceManagerClass);
        for (DeviceManager deviceManager : deviceManagers) {
            if (deviceManagerClass.isAssignableFrom(deviceManager.getClass())) {
                return (T) deviceManager;
            }
        }
        return null;
    }

    public GpsHandler openGpsDevice(final GpsConfig config)
            throws DeviceTechnicalException {

        GpsHandler oldInstance = getDeviceManager(GpsHandler.class);

        Set<GpsLocationListener> locationListeners;
        if (oldInstance != null) {

            // Remove all existing listeners and keep them for the new instance
            locationListeners = Sets.newHashSet(oldInstance.getGpsLocationListeners());
            for (GpsLocationListener listener : locationListeners) {
                oldInstance.removeGpsLocationListener(listener);
            }

        } else {
            locationListeners = Sets.newHashSet();
        }

        GpsHandler result = newDeviceManager(oldInstance, new Supplier<GpsHandler>() {

            @Override
            public GpsHandler get() {

                // Instanciate the GpsHandler
                Class<? extends GpsHandler> gpsHandlerClass = config.getGpsHandlerClass();
                try {

                    Constructor<? extends GpsHandler> constructor =
                            gpsHandlerClass.getConstructor(GpsConfig.class);

                    GpsHandler result = constructor.newInstance(config);
                    return result;

                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new SammoaTechnicalException(e);
                }
            }
        });

        for (GpsLocationListener listener : locationListeners) {
            result.addGpsLocationListener(listener);
        }
        return result;
    }

    public AudioRecorder openAudioRecorderDevice(final AudioConfig config)
            throws DeviceTechnicalException {

        AudioRecorder oldInstance = getDeviceManager(AudioRecorder.class);

        AudioRecorder result = newDeviceManager(oldInstance, new Supplier<AudioRecorder>() {

            @Override
            public AudioRecorder get() {
//                return new AudioRecorderDefault(config);
                return new SammoaAudioRecorder(config, 10);
            }
        });
        return result;
    }

    public AudioReader openAudioReaderDevice(final AudioConfig config)
            throws DeviceTechnicalException {

        AudioReader oldInstance = getDeviceManager(AudioReader.class);

        Set<AudioPositionListener> positionListeners;
        if (oldInstance != null) {

            // Remove all existing listeners and keep them for the new instance
            positionListeners = Sets.newHashSet(oldInstance.getAudioPositionListeners());
            for (AudioPositionListener listener : positionListeners) {
                oldInstance.removeAudioPositionListener(listener);
            }

        } else {
            positionListeners = Sets.newHashSet();
        }

        AudioReader result = newDeviceManager(oldInstance, new Supplier<AudioReader>() {

            @Override
            public AudioReader get() {
                return new SammoaAudioReader();
            }
        });

        for (AudioPositionListener listener : positionListeners) {
            result.addAudioPositionListener(listener);
        }
        return result;
    }

    protected <T extends DeviceManager> T newDeviceManager(T oldInstance, Supplier<T> supplier)
            throws DeviceTechnicalException {

        Set<DeviceStateListener> stateListeners;
        if (oldInstance != null) {

            // Remove all existing listeners and keep them for the new instance
            stateListeners = Sets.newHashSet(oldInstance.getDeviceStateListeners());
            for (DeviceStateListener listener : stateListeners) {
                oldInstance.removeDeviceStateListener(listener);
            }

            oldInstance.close();

            deviceManagers.remove(oldInstance);

        } else {
            stateListeners = Sets.newHashSet();
        }

        T result = supplier.get();

        // Attach all updateListener
        for (DeviceStateListener listener : stateListeners) {
            result.addDeviceStateListener(listener);
        }

        deviceManagers.add(result);

        return result;
    }

    @Override
    public void close() {
        for (DeviceManager deviceManager : deviceManagers) {
            deviceManager.close();
        }
        deviceManagers.clear();
    }
}
