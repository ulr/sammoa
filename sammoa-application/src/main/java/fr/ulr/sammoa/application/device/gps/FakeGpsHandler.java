/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application.device.gps;

import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPoints;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Random;

/**
 * Un Fake pour {@link GpsHandler}, il simule un déplacement toute les secondes
 * à partir du point 0, 0.
 */
public class FakeGpsHandler extends BaseGpsHandler {

    private static final Log log = LogFactory.getLog(FakeGpsHandler.class);

    public static final int MAX_POINTS = 100;

    protected GeoPoint startingPoint;

    protected GeoPoint currentLocation;

    protected Thread thread;

    public FakeGpsHandler(GpsConfig config) {
        this(config, new GeoPointImpl(46.164139, -1.150503));
    }

    public FakeGpsHandler(GpsConfig config, GeoPoint origin) {
        super(config);

        startingPoint = new GeoPointImpl(origin.getLatitude(), origin.getLongitude());
//
//        // add new thread to simulate gps state change
//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            protected Random r = new Random();
//            protected DeviceState currentState = DeviceState.UNAVAILABLE;
//
//            @Override
//            public void run() {
//                DeviceState newState = currentState;
//                if (r.nextInt(32) > 25) {
//                    newState = DeviceState.UNAVAILABLE;
//                } else {
//                    if (started) {
//                        if (r.nextInt(32) > 25) {
//                            newState = DeviceState.ERROR;
//                        } else {
//                            newState = DeviceState.RUNNING;
//                        }
//                    } else {
//                        newState = DeviceState.READY;
//                    }
//                }
//
//                if (!newState.equals(currentState)) {
//                    currentState = newState;
//                    for (GpsLocationListener locationUpdateListener : gpsLocationListeners) {
//                        locationUpdateListener.stateChanged(currentState);
//                    }
//                }
//            }
//        }, 10000, 10000);
    }

    @Override
    public void open() throws DeviceTechnicalException {
        setState(DeviceState.READY, null);
    }

    @Override
    public void start() {
        if (thread != null) {
            thread.stop();
        }

        thread = new Thread(new GpsPointGenerator());
        thread.start();

        super.start();
        setState(DeviceState.RUNNING, null);
    }

    @Override
    protected DeviceTechnicalException onError(GeoPoint location, boolean offline) {
        return null;
    }

    @Override
    public void close() throws DeviceTechnicalException {
        setState(DeviceState.UNAVAILABLE, null);
    }

    @Override
    protected void finalize() throws Throwable {

        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

        super.finalize();
    }

    @Override
    protected GeoPoint getLastLocation() {
        GeoPoint result;
        if (currentLocation == null) {
            result = new GeoPointImpl(GeoPoints.EMPTY_COORDINATE, GeoPoints.EMPTY_COORDINATE);
            result.setRecordTime(getDate());
        } else {
            result = currentLocation;
        }
        return result;
    }

    //
//    @Override
//    public GeoPoint getCurrentLocation() {
//        GeoPoint result;
//        if (currentLocation == null) {
//            result = new GeoPointImpl(GeoPoints.EMPTY_COORDINATE, GeoPoints.EMPTY_COORDINATE);
//            result.setRecordTime(getDate());
//
//        } else if (currentLocation.getTopiaId() != null) {
//            result = new GeoPointImpl(currentLocation.getLatitude(), currentLocation.getLongitude());
//            result.setAltitude(currentLocation.getAltitude());
//            result.setSpeed(currentLocation.getSpeed());
//            result.setRecordTime(getDate());
//
//        } else {
//            result = currentLocation;
//        }
//        return result;
//    }

    protected class GpsPointGenerator implements Runnable {

        @Override
        public void run() {

            double latitude = startingPoint.getLatitude();
            double longitude = startingPoint.getLongitude();

            Random random = new Random(1337);

            int count = MAX_POINTS;

            while (count != 0) {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    if (log.isDebugEnabled()) {
                        log.debug("Interrupt sleeping, thread will be destroy", e);
                    }
                }

                latitude += random.nextDouble() - 0.5;
                longitude += random.nextDouble() - 0.5;

                currentLocation = new GeoPointImpl(latitude, longitude);
                currentLocation.setRecordTime(getDate());
                lastEventTime = System.currentTimeMillis();

                count--;
            }
        }
    }

}
