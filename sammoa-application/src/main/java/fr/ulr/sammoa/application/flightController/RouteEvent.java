package fr.ulr.sammoa.application.flightController;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.Route;

import java.util.EventObject;

/**
 * Created: 06/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class RouteEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    protected Route route;

    protected boolean isNew;

    protected Route previousRoute;

    public RouteEvent(FlightController source,
                      Route route,
                      boolean isNew,
                      Route previousRoute) {
        super(source);
        this.route = route;
        this.isNew = isNew;
        this.previousRoute = previousRoute;
    }

    @Override
    public FlightController getSource() {
        return (FlightController) source;
    }

    public Route getRoute() {
        return route;
    }

    public boolean isNew() {
        return isNew;
    }

    public Route getPreviousRoute() {
        return previousRoute;
    }
}
