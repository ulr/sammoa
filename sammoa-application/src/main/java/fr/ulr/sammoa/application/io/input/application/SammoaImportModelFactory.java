package fr.ulr.sammoa.application.io.input.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ulr.sammoa.persistence.SammoaDbMeta;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.EntityCsvModel;
import org.nuiton.topia.service.csv.in.EntityAssociationImportModel;
import org.nuiton.topia.service.csv.in.ImportModelFactory;

import java.io.Reader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * To produce csv import model.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SammoaImportModelFactory implements ImportModelFactory<SammoaEntityEnum> {

    private final char csvSeparator;

    private final Map<SammoaEntityEnum, Map<String, TopiaEntity>> universe;

    private final SammoaTopiaPersistenceContext tx;

    private final SammoaDbMeta dbMetas;

    SammoaImportModelFactory(char csvSeparator,
                             SammoaTopiaPersistenceContext tx,
                             SammoaDbMeta dbMetas) {
        this.dbMetas = dbMetas;
        this.universe = Maps.newTreeMap();
        this.csvSeparator = csvSeparator;
        this.tx = tx;
    }

    public Map<SammoaEntityEnum, Map<String, TopiaEntity>> getUniverse() {
        return universe;
    }

    public <E extends TopiaEntity> Import<E> newModel(TableMeta<SammoaEntityEnum> meta, Reader reader) {
        ImportModel<E> model = buildForImport(meta);
        Import<E> importer = Import.newImport(model, reader);
        return importer;
    }

    @Override
    public <E extends TopiaEntity> ImportModel<E> buildForImport(TableMeta<SammoaEntityEnum> meta) {

        EntityCsvModel<SammoaEntityEnum, E> model = EntityCsvModel.newModel(
                csvSeparator,
                meta,
                TopiaEntity.PROPERTY_TOPIA_ID
        );

        for (ColumnMeta columnMeta : meta) {
            String propertyName = columnMeta.getName();
            Class<?> type = columnMeta.getType();
            if (!columnMeta.isFK()) {
                if (Date.class.equals(type)) {
                    model.newColumnForImportExport(
                            propertyName,
                            DateParserFormatter.of());
                } else if (TimeZone.class.equals(type)) {
                    model.newColumnForImportExport(
                            propertyName,
                            TimeZoneParserFormatter.of());
                } else if (! propertyName.equals(TopiaEntityContextable.PROPERTY_TOPIA_DAO_SUPPLIER)){
                    model.addDefaultColumn(propertyName, type);
                }
            } else {

                Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;
                SammoaEntityEnum entityEnum = SammoaEntityEnum.valueOf(entityType);

                if (dbMetas.isReferential(entityEnum)) {

                    // referential is always loaded by incoming universe (and not from db)
                    //since we need to do some topiaId translations
                    Map<String, TopiaEntity> entitiesbyId = universe.get(entityEnum);
                    model.addForeignKeyForImport(propertyName, entityType, entitiesbyId);
                } else {

                    // foreign keys on data are always coming from db
                    try {
                        List<TopiaEntity> entities = tx.getDao(entityType).findAll();
                        model.addForeignKeyForImport(propertyName, entityType, entities);
                    } catch (TopiaException e) {
                        throw new TopiaException(e);
                    }
                }
            }
        }
        return model;
    }

    @Override
    public ImportModel<Map<String, Object>> buildForImport(AssociationMeta<SammoaEntityEnum> meta) {
        ImportModel<Map<String, Object>> model =
                EntityAssociationImportModel.newImportModel(
                        csvSeparator,
                        meta
                );
        return model;
    }

    @Override
    public boolean isNMAssociationMeta(AssociationMeta<SammoaEntityEnum> meta) {
        SammoaEntityEnum source = meta.getSource();
        SammoaEntityEnum target = meta.getTarget();
        boolean result = false;
        if (source == SammoaEntityEnum.Flight && target == SammoaEntityEnum.Observer) {
            result = true;
        } else if (source == SammoaEntityEnum.TransectFlight && target == SammoaEntityEnum.ObserverPosition) {
            result = true;
        }
//        else if (source == SammoaEntityEnum.Route && target == SammoaEntityEnum.ObserverPosition) {
//            result = true;
//        }
        return result;
    }

    public void close() {
        universe.clear();
    }

    public SammoaDbMeta getDbMetas() {
        return dbMetas;
    }
}
