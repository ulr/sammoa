package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Route;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.TransectFlight;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class V1103RenameAudioFile extends AbstractMigration {

    private static final Log log = LogFactory.getLog(V1103RenameAudioFile.class);

    private List<CsvMigrate.Line> firstRoutes;

    public V1103RenameAudioFile() {
        super("1.1.0.3", "Rename audio file");
    }

    @Override
    public void migrateLocalDirectory() throws IOException {
        File surveysDirectory = SammoaDirectories.getSurveyDirectory(dataDirectory);
        File[] surveyDirectories = surveysDirectory.listFiles((file, name) ->
                name.startsWith(Survey.class.getCanonicalName())
        );
        if (surveyDirectories != null) {
            for (File surveyDirectory : surveyDirectories) {
                migrateSurvey(surveyDirectory);
            }
        }
    }

    protected void migrateSurvey(File surveyDirectory) throws IOException {
        File flightsDir = SammoaDirectories.getFlightDirectory(surveyDirectory);
        File[] flightDirectories = flightsDir.listFiles((file, name) ->
                name.startsWith(Flight.class.getCanonicalName())
        );
        if (flightDirectories != null) {
            for (File flightDirectory : flightDirectories) {
                migrateFlight(flightDirectory);
            }
        }
    }

    protected void migrateFlight(File flightDirectory) throws IOException {
        if (isImportFileMigration()) {

            File csvDir = SammoaDirectories.getCsvDirectory(flightDirectory);
            CsvMigrate routeCsv = loadCvs(csvDir.toPath().resolve("Route.csv"));

            // filtre des premières route de chacque transect
            firstRoutes = routeCsv.streamLine()
                    .filter(line -> StringUtils.isNotBlank(line.get(Route.PROPERTY_TRANSECT_FLIGHT)))
                    .collect(Collectors.groupingBy(
                            line -> line.get(Route.PROPERTY_TRANSECT_FLIGHT),
                            Collectors.minBy(Comparator.comparing(line -> line.getOldDate(Route.PROPERTY_BEGIN_TIME)))))
                    .values()
                    .stream()
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList());

            routeCsv.save();
        }


        File audiosDir = SammoaDirectories.getAudioDirectory(flightDirectory);
        File[] audioFiles = audiosDir.listFiles((file, name) ->
                name.startsWith(TransectFlight.class.getCanonicalName())
        );
        if (audioFiles != null) {
            for (File audioFile : audioFiles) {
                migrateAudioFile(audioFile);
            }
        }
    }

    protected void migrateAudioFile(File audioFile) throws IOException {
        String name = audioFile.getName();
        String transectFlightId = name.substring(0, name.lastIndexOf("."));
        Date audioStartTime = migrateDataFromTransectFlight(transectFlightId);

        if (audioStartTime != null) {
            String extension = name.substring(name.lastIndexOf("."));
            String newName = audioStartTime.getTime() / 1000 + extension;

            Path newPath = audioFile.getParentFile().toPath().resolve(newName);
            Files.move(audioFile.toPath(), newPath);
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Transect flight not found for audio file " + audioFile.getAbsolutePath());
            }
        }

    }

    protected Date migrateDataFromTransectFlight(String transectFlightId) {
        Date audioStartTime;

        if (isLocalMigration()) {
            audioStartTime = migrateDataFromTransectFlightInDB(transectFlightId);
        } else {
            audioStartTime = migrateDataFromTransectFlightInCSV(transectFlightId);
        }

        return audioStartTime;
    }

    protected Date migrateDataFromTransectFlightInCSV(String transectFlightId) {
        return firstRoutes.stream()
                .filter(line -> transectFlightId.equals(line.get(Route.PROPERTY_TRANSECT_FLIGHT)))
                .findFirst()
                .map(line -> line.getOldDate(Route.PROPERTY_BEGIN_TIME))
                .orElse(null);
    }

    protected Date migrateDataFromTransectFlightInDB(String transectFlightId) {
        return getPersistenceContext().getRouteDao()
                .forProperties(Route.PROPERTY_TRANSECT_FLIGHT + "." + TransectFlight.PROPERTY_TOPIA_ID, transectFlightId)
                .setOrderByArguments(Route.PROPERTY_BEGIN_TIME)
                .tryFindFirst()
                .toJavaUtil()
                .map(Route::getBeginTime)
                .orElse(null);
    }


    @Override
    public File migrateImportFile() throws IOException {
        migrateSurvey(dataDirectory);
        return dataDirectory;
    }
}
