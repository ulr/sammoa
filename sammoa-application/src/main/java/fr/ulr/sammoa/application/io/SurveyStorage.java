package fr.ulr.sammoa.application.io;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.DecoratorService;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.Survey;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Storage for a survey.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SurveyStorage extends SammoaStorage<Survey> implements Iterable<FlightStorage> {

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(SurveyStorage.class);

    protected Map<String, FlightStorage> flightStorages;

    SurveyStorage(File dataDirectory, String id) {
        super(dataDirectory, id, "survey.properties");
    }

    public File getMapDirectory() {
        return SammoaDirectories.getMapDirectory(getDirectory());
    }

    public File getMapFile(String mapFilename) {
        return new File(getMapDirectory(), mapFilename);
    }

    public File getFlightDirectory() {
        return SammoaDirectories.getFlightDirectory(getDirectory());
    }

    public Set<String> getFlightIds() {
        String[] files = getFlightDirectory().list(DirectoryFileFilter.INSTANCE);
        Set<String> result = files == null ?
                             Sets.<String>newHashSet() : Sets.newHashSet(files);
        return result;
    }

    public String getCode() {
        return getProperties().getProperty("code");
    }

    public FlightStorage getFlightStorage(String flightId) {
        Preconditions.checkNotNull(flightId);
        FlightStorage result = getFlightStorages().get(flightId);
        if (result == null) {

            // create this new flight storage
            addFlightInStore(result = getFlightToStorage().apply(flightId));
        }
        return result;
    }

    @Override
    public void fillProperties(Survey survey,
                               DecoratorService decoratorService) {

        Decorator<Survey> decorator =
                decoratorService.getDecoratorByType(Survey.class);

        putInProperties("name", decorator.toString(survey));
        putInProperties(Survey.PROPERTY_CODE, survey.getCode());
        putInProperties(Survey.PROPERTY_REGION + "." +
                        Region.PROPERTY_CODE, survey.getRegion().getCode());
    }

    @Override
    public Map<String, Object> getNaturalId() {
        Map<String, Object> result = Maps.newHashMap();
        addInMap(Survey.PROPERTY_CODE, result);
        addInMap(Survey.PROPERTY_REGION + "." + Region.PROPERTY_CODE, result);
        return result;
    }

    @Override
    public Iterator<FlightStorage> iterator() {
        return getFlightStorages().values().iterator();
    }

    protected Map<String, FlightStorage> getFlightStorages() {
        if (flightStorages == null) {

            flightStorages = Maps.newTreeMap();
            for (FlightStorage s : Iterables.transform(getFlightIds(), getFlightToStorage())) {
                addFlightInStore(s);
            }
        }
        return flightStorages;
    }

    protected transient Function<String, FlightStorage> flightToStorage;

    protected Function<String, FlightStorage> getFlightToStorage() {
        if (flightToStorage == null) {
            flightToStorage = new Function<String, FlightStorage>() {
                @Override
                public FlightStorage apply(String input) {
                    return new FlightStorage(getFlightDirectory(), input);
                }
            };
        }
        return flightToStorage;
    }

    protected void addFlightInStore(FlightStorage flightStorage) {
        if (log.isInfoEnabled()) {
            log.info("Add flightStorage " + flightStorage + " to store " + this);
        }
        flightStorages.put(flightStorage.getId(), flightStorage);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("directory", getDirectory()).
                append("id", getId()).
                append("nbFlightStoragesLoaded", flightStorages == null ? 0 : flightStorages.size()).
                toString();
    }

    public void deleteFlightStorage(String flightId) {
        Preconditions.checkNotNull(flightId);
        Preconditions.checkArgument(getFlightIds().contains(flightId));
        FlightStorage flightStorage = getFlightStorage(flightId);
        try {
            flightStorage.delete();
        } catch (IOException e) {
            throw new SammoaTechnicalException(
                    "Could not delete flight storage", e);
        }
    }

    public void deletePropertiesFiles() {

        FileUtils.deleteQuietly(getPropertiesFile());

        for (FlightStorage flightStorage : this) {
            FileUtils.deleteQuietly(flightStorage.getPropertiesFile());
        }
    }
}
