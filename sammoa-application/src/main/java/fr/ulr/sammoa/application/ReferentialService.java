/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.SammoaStorages;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyTopiaDao;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverPosition;
import fr.ulr.sammoa.persistence.ObserverPositionTopiaDao;
import fr.ulr.sammoa.persistence.ObserverTopiaDao;
import fr.ulr.sammoa.persistence.Observers;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.RegionTopiaDao;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.SubRegionTopiaDao;
import fr.ulr.sammoa.persistence.Species;
import fr.ulr.sammoa.persistence.SpeciesTopiaDao;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StrateTopiaDao;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectTopiaDao;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created: 18/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class ReferentialService extends SammoaServiceSupport {

    //------------------------------------------------------------------------//
    //-- Region methods ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Region> getRegions() {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            RegionTopiaDao dao =  transaction.getRegionDao();

            List<Region> result = dao.findAll();
            if (result.isEmpty()) {

                Region region = dao.createByNaturalId("FR-METRO");
                transaction.commit();

                result = Lists.newArrayList(region);
            }

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(transaction);
        }
    }

    public String saveRegion(Region region) {

        Preconditions.checkNotNull(region);
        Preconditions.checkNotNull(region.getCode());

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            RegionTopiaDao dao =  transaction.getRegionDao();

            String result;
            if (region.getTopiaId() == null) {

                Preconditions.checkArgument(!dao.forNaturalId(region.getCode()).exists());

                Region newRegion = dao.createByNaturalId(region.getCode());
                newRegion.setName(region.getName());

                result = newRegion.getTopiaId();

            } else {

                result = region.getTopiaId();

                Region existRegion = dao.forTopiaIdEquals(result).findUnique();
                existRegion.setName(region.getName());
                dao.update(existRegion);
            }

            transaction.commit();

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(transaction);
        }
    }

    //------------------------------------------------------------------------//
    //-- Species methods -----------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Species> getAllSpecies(Survey survey) {
        Preconditions.checkNotNull(survey);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            SpeciesTopiaDao dao =  transaction.getSpeciesDao();
            List<Species> result = dao.forRegionEquals(survey.getRegion()).findAll();
            return result;

        } finally {
            endTransaction(transaction);
        }
    }

    public Collection<Species> getAllValidSpecies() {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            SpeciesTopiaDao dao =  transaction.getSpeciesDao();
            Collection<Species> result = dao.forLocalCreationEquals(false).findAll();
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public Species getSpecies(String code , Region region) {
        SammoaTopiaPersistenceContext transaction = beginTransaction();

        Species species;

        try {
            SpeciesTopiaDao dao = transaction.getSpeciesDao();
            species = dao.forNaturalId(code, region).findUniqueOrNull();
        } finally {
            endTransaction(transaction);
        }

        return species;

    }

    //------------------------------------------------------------------------//
    //-- Survey methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Survey> getSurveys() {

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            SurveyTopiaDao dao =  transaction.getSurveyDao();
            List<Survey> result =
                    dao.forAll().setOrderByArguments(Survey.PROPERTY_BEGIN_DATE + " DESC").findAll();
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public Survey getSurveyByNaturalId(Map<String, Object> naturalId) {
        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            SurveyTopiaDao dao =  tx.getSurveyDao();
            Survey result = dao.forProperties(naturalId).findAnyOrNull();
            return result;
        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }
    }

    public Survey getSurvey(String topiaId) {
        Preconditions.checkNotNull(topiaId);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            SurveyTopiaDao dao =  transaction.getSurveyDao();
            Survey result = dao.forTopiaIdEquals(topiaId).findUnique();
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public String saveSurvey(Survey survey) {

        Preconditions.checkNotNull(survey);
        Preconditions.checkNotNull(survey.getBeginDate());
        Preconditions.checkNotNull(survey.getEndDate());
        Preconditions.checkArgument(survey.getEndDate().after(survey.getBeginDate()));
        Preconditions.checkNotNull(survey.getCode());
        Preconditions.checkNotNull(survey.getRegion());

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            SurveyTopiaDao dao =  transaction.getSurveyDao();

            String result;

            boolean createSurvey = survey.getTopiaId() == null;

            if (createSurvey) {

                Preconditions.checkArgument(!dao.forNaturalId(survey.getCode(), survey.getRegion()).exists());

                Survey newSurvey = dao.createByNaturalId(
                        survey.getCode(), survey.getRegion());
                newSurvey.setName(survey.getName());
                newSurvey.setBeginDate(survey.getBeginDate());
                newSurvey.setEndDate(survey.getEndDate());
                newSurvey.setLocalCreation(true);

                result = newSurvey.getTopiaId();

            } else {

                result = survey.getTopiaId();

                Survey existSurvey = dao.forTopiaIdEquals(result).findUnique();
                existSurvey.setName(survey.getName());
                existSurvey.setBeginDate(survey.getBeginDate());
                existSurvey.setEndDate(survey.getEndDate());
                dao.update(existSurvey);
            }

            transaction.commit();

            if (createSurvey) {

                File dataDirectory = config.getDataDirectory();

                // creates the survey storage
                SammoaStorages.createSurveyStorage(
                        SammoaDirectories.getSurveyDirectory(dataDirectory),
                        result);
            }
            return result;

        } finally {
            endTransaction(transaction);
        }
    }

    public void deleteSurvey(String surveyId) {
        Preconditions.checkNotNull(surveyId);

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            SurveyTopiaDao dao =  tx.getSurveyDao();

            Survey survey = dao.forTopiaIdEquals(surveyId).findUnique();

            Preconditions.checkNotNull(survey);

            FlightService flightService = getService(FlightService.class);

            // delete all flights from db
            List<Flight> flights = flightService.getFlights(tx, survey);

            for (Flight flight : flights) {
                flightService.deleteFlightInDb(tx, flight.getTopiaId());
            }

            // delete observers and observerPositions
            ObserverTopiaDao observerDAO =  tx.getObserverDao();
            List<Observer> observers = observerDAO.forSurveyEquals(survey).findAll();
            ObserverPositionTopiaDao observerPositionDAO =  tx.getObserverPositionDao();
            Set<ObserverPosition> positions = Sets.newHashSet();
            for (Observer observer : observers) {
                positions.addAll(observerPositionDAO.forObserverEquals(observer).findAll());
            }
            observerDAO.deleteAll(observers);
            observerPositionDAO.deleteAll(positions);

            // delete transects
            List<Transect> transects = getAllTransects(tx, survey);
            TransectTopiaDao transectDAO =  tx.getTransectDao();
            transectDAO.deleteAll(transects);

            // delete strates
            List<Strate> strates = getAllStrates(tx, survey);
            StrateTopiaDao strateDAO =  tx.getStrateDao();
            strateDAO.deleteAll(strates);

            // delete subRegions
            SubRegionTopiaDao subRegionDAO =  tx.getSubRegionDao();
            List<SubRegion> subRegions = subRegionDAO.forSurveyEquals(survey).findAll();
            subRegionDAO.deleteAll(subRegions);

            // delete survey
            dao.delete(survey);

            // commit
            tx.commit();

        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }

        // delete survey storage
        deleteSurveyStorage(surveyId);
    }

    //------------------------------------------------------------------------//
    //-- Transect methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Transect> getAllTransects(Survey survey) {
        Preconditions.checkNotNull(survey);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            List<Transect> result = getAllTransects(transaction, survey);
            return result;
        } finally {
            endTransaction(transaction);
        }
    }

    public List<Transect> getAllTransects(SammoaTopiaPersistenceContext tx, Survey survey) {
        Preconditions.checkNotNull(tx);
        Preconditions.checkNotNull(survey);

        try {
            TransectTopiaDao dao =  tx.getTransectDao();
            List<Transect> result =
                    dao.findAllBySurveyOrderedByName(survey);
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        }
    }

    public Transect getTransect(String transectId) {
        Preconditions.checkNotNull(transectId);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            TransectTopiaDao dao =  transaction.getTransectDao();
            Transect result = (Transect) dao.forTopiaIdEquals(transectId).findUniqueOrNull();
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);

        } finally {
            endTransaction(transaction);
        }
    }

    public String createTransect(Transect transect) {

        Preconditions.checkNotNull(transect);
        Preconditions.checkNotNull(transect.getName());
        Preconditions.checkNotNull(transect.getStrate());

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            TransectTopiaDao dao =  transaction.getTransectDao();

            Preconditions.checkArgument(! dao.forNaturalId(transect.getName(),transect.getStrate()).exists());

            Transect newTransect = dao.createByNaturalId(transect.getName(),
                                                         transect.getStrate());
            newTransect.setLocalCreation(true);

            transaction.commit();

            String result = newTransect.getTopiaId();
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(transaction);
        }
    }

    //------------------------------------------------------------------------//
    //-- Strate methods ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Strate> getAllStrates(Survey survey) {
        Preconditions.checkNotNull(survey);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {
            List<Strate> result = getAllStrates(transaction, survey);
            return result;

        } finally {
            endTransaction(transaction);
        }
    }

    public List<Strate> getAllStrates(SammoaTopiaPersistenceContext tx, Survey survey) {
        Preconditions.checkNotNull(tx);
        Preconditions.checkNotNull(survey);

        try {
            StrateTopiaDao dao =  tx.getStrateDao();
            List<Strate> result = dao.findAllBySurveyOrderedByCode(survey);
            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        }
    }

    //------------------------------------------------------------------------//
    //-- Observer methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Observer> getAllObservers(Survey survey) {
        Preconditions.checkNotNull(survey);

        SammoaTopiaPersistenceContext transaction = beginTransaction();
        try {

            ObserverTopiaDao dao =  transaction.getObserverDao();
            List<Observer> result = dao.forSurveyEquals(survey).findAll();
            Collections.sort(result, Observers.onInitials());

            // realy creating observers
            transaction.commit();

            return result;
        } catch (TopiaException e) {
            throw new TopiaException(e);
        } finally {
            endTransaction(transaction);
        }
    }


    public void deleteAllData() {
        Iterable<String> surveyIds;

        SammoaTopiaPersistenceContext tx = beginTransaction();
        try {
            SurveyTopiaDao dao =  tx.getSurveyDao();
            surveyIds = dao.findAllIds();
        } catch (TopiaException e) {
            throw new SammoaTechnicalException(e);
        } finally {
            endTransaction(tx);
        }

        persistence.destroyDb();

        for (String surveyId : surveyIds) {

            // delete survey storage
            deleteSurveyStorage(surveyId);
        }
    }

    protected void deleteSurveyStorage(String surveyId) {
        SurveyStorage surveyStorage = getSurveyStorage(surveyId);
        try {
            surveyStorage.delete();
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not delete survey storage", e);
        }
    }
}
