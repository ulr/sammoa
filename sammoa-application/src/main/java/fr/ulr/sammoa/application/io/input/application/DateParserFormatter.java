package fr.ulr.sammoa.application.io.input.application;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.Common;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

public class DateParserFormatter extends Common.DateValue {

    public static DateParserFormatter of() {
        return new DateParserFormatter();
    }

    protected DateParserFormatter() {
        super("dd/MM/yyyy HH:mm:ss.SSSSZ");
    }

    @Override
    public Date parse(String value) throws ParseException {

        Date parse = super.parse(value);
        if (parse != null) {
            parse = new Timestamp(parse.getTime());
        }
        return parse;
    }


}
