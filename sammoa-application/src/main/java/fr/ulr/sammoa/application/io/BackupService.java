package fr.ulr.sammoa.application.io;

/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.OrFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to backup (or/and later restore) sammoa.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.7
 */
public class BackupService extends SammoaServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(BackupService.class);

    protected static final OrFileFilter DB_FILTER =
            new OrFileFilter(DirectoryFileFilter.INSTANCE,
                             new SuffixFileFilter(".h2.db"));

    /**
     * Export all the application data to the backup directory.
     *
     * @param backupFilename filename of the backup file
     * @since 0.7
     */
    public void backupApplication(String backupFilename, Consumer<String> setterStatus) {
        Preconditions.checkNotNull(backupFilename);

        // sammoa data directory to use
        File sourceDirectory = config.getDataDirectory();

        // create export directory
        File targetDirectory = new File(new File(config.getTmpDirectory(),
                                                 "backupSammoa-" + System.nanoTime()), "sammoa");

        try {
            setterStatus.accept(t("sammoa.backup.copySurveyDirectory"));
            // copy in it all the survey directory
            File surveyDirectory = SammoaDirectories.getSurveyDirectory(targetDirectory);
            FileUtils.copyDirectory(SammoaDirectories.getSurveyDirectory(sourceDirectory),
                                    surveyDirectory);

            setterStatus.accept(t("sammoa.backup.copyDbDirectory"));
            // copy in it the db
            File dbDirectory = SammoaDirectories.getDbDirectory(targetDirectory);
            FileUtils.copyDirectory(SammoaDirectories.getDbDirectory(sourceDirectory),
                                    dbDirectory, DB_FILTER);

            setterStatus.accept(t("sammoa.backup.copyDbDirectory"));
            // final zip file
            File zipFile = SammoaDirectories.getBackupFile(sourceDirectory,
                                                           backupFilename);

            if (log.isInfoEnabled()) {
                log.info("Compress backup to " +  zipFile);
            }

            setterStatus.accept(t("sammoa.backup.compressFiles"));
            // zip it and clean target directory
            compressZipFile(zipFile, targetDirectory);

            FileUtils.deleteDirectory(targetDirectory.getParentFile());

        } catch (IOException e) {
            throw new SammoaTechnicalException("could not backup data", e);
        }

    }
}
