package fr.ulr.sammoa.application.io;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaTechnicalException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Useful methods around {@link SammoaStorage}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SammoaStorages {

    public static SurveyStorage getSurveyStorage(File directory,
                                                     String surveyId) {
        SurveyStorage result = new SurveyStorage(directory, surveyId);
        return result;
    }

    public static SurveyStorage createSurveyStorage(File directory,
                                                        String surveyId) {
        try {
            SurveyStorage result = new SurveyStorage(directory, surveyId);
            createEmptySurveyStructure(result);
            return result;
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not create survey storage", e);
        }
    }

    public static void copySurveyStorage(SurveyStorage source,
                                           SurveyStorage target,
                                           Iterable<String> flightIds) {

        try {

            FileUtils.copyDirectory(source.getMapDirectory(),
                                    target.getMapDirectory());

            for (String flightId : flightIds) {
                FlightStorage flightStorage = source.getFlightStorage(flightId);

                FlightStorage targetFlightStorage =
                        target.getFlightStorage(flightStorage.getId());

                copyFlightStorage(flightStorage, targetFlightStorage);

            }
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not copy survey storage", e);
        }
    }

    public static void copyFlightStorage(FlightStorage source,
                                         FlightStorage target) {

        try {

            FileUtils.copyDirectory(source.getAudioDirectory(),
                                    target.getAudioDirectory());
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not copy flight storage", e);
        }
    }

    public static FlightStorage createFlightStorage(SurveyStorage source,
                                                    String flightId) {
        try {
            FlightStorage result = source.getFlightStorage(flightId);
            createEmptyFlightStructure(result);
            return result;
        } catch (IOException e) {
            throw new SammoaTechnicalException("Could not create flight storage", e);
        }
    }

    protected static void createEmptySurveyStructure(SurveyStorage storage) throws IOException {
        FileUtils.forceMkdir(storage.getDirectory());
        FileUtils.forceMkdir(storage.getFlightDirectory());
        FileUtils.forceMkdir(storage.getMapDirectory());
    }

    protected static void createEmptyFlightStructure(FlightStorage storage) throws IOException {
        FileUtils.forceMkdir(storage.getDirectory());
        FileUtils.forceMkdir(storage.getAudioDirectory());
    }

    public static void loadProperties(SurveyStorage storage) {

        try {
            storage.loadProperties();

            for (FlightStorage flightStorage : storage) {
                flightStorage.loadProperties();
            }
        } catch (IOException e) {
            throw new SammoaTechnicalException(
                    "Could not load storage properties file", e);
        }
    }
}
