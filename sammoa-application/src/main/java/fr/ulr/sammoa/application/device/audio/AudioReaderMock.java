package fr.ulr.sammoa.application.device.audio;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ulr.sammoa.application.device.DeviceState;
import fr.ulr.sammoa.application.device.DeviceStateEvent;
import fr.ulr.sammoa.application.device.DeviceStateListener;
import fr.ulr.sammoa.application.device.DeviceTechnicalException;

import javax.sound.sampled.AudioFileFormat;
import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created: 21/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class AudioReaderMock implements AudioReader {

    protected AudioConfig config;

    protected File fileToRead;

    protected long position;

    protected DeviceState state = DeviceState.UNAVAILABLE;

    protected Set<DeviceStateListener> listeners = Sets.newHashSet();
    protected Set<AudioPositionListener> audioPositionListener = new HashSet<AudioPositionListener>();

    public AudioReaderMock(AudioConfig config) {
        this.config = config;
    }

    @Override
    public AudioFileFormat.Type getOutputType() {
        return AudioFileFormat.Type.WAVE;
    }

    @Override
    public void load(File file, Date recordingDate) {
        Preconditions.checkNotNull(file);
        Preconditions.checkArgument(file.exists());
        unload();
        fileToRead = file;
        setState(DeviceState.READY);
    }

    @Override
    public void unload() {
        stop();
        fileToRead = null;
        setState(DeviceState.UNAVAILABLE);
    }

    @Override
    public File getFileToRead() {
        return fileToRead;
    }

    @Override
    public long getLength() {
        return 0;
    }

    @Override
    public Date getStartDate() {
        return null;
    }

    @Override
    public void setPosition(long position) {
        this.position = position;
    }

    @Override
    public long getPosition() {
        return position;
    }

    @Override
    public void setPositionDate(Date date) {
    }

    @Override
    public Date getPositionDate() {
        return null;
    }

    @Override
    public void open() throws DeviceTechnicalException {
        // no state set here, only open device and throw error
    }

    @Override
    public void start() {
        if (state == DeviceState.READY) {
            setState(DeviceState.RUNNING);
        }
    }

    @Override
    public void stop() {
        if (state == DeviceState.RUNNING) {
            setState(DeviceState.READY);
        }
    }

    @Override
    public void close() throws DeviceTechnicalException {
        unload();
    }

    @Override
    public DeviceState getState() {
        return state;
    }

    protected void setState(DeviceState state) {
        DeviceState oldValue = getState();
        this.state = state;

        DeviceStateEvent event = new DeviceStateEvent(this, oldValue, state);
        for (DeviceStateListener listener : listeners) {
            listener.stateChanged(event);
        }
    }

    @Override
    public void addDeviceStateListener(DeviceStateListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeDeviceStateListener(DeviceStateListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<DeviceStateListener> getDeviceStateListeners() {
        return listeners;
    }

    @Override
    public void addAudioPositionListener(AudioPositionListener listener) {
        audioPositionListener.add(listener);
    }

    @Override
    public void removeAudioPositionListener(AudioPositionListener listener) {
        audioPositionListener.remove(listener);
    }

    @Override
    public Set<AudioPositionListener> getAudioPositionListeners() {
        return audioPositionListener;
    }
}
