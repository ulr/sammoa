package fr.ulr.sammoa.application.migration;

/*-
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.SubRegion;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class V1102RenameSectorToSubRegion extends AbstractMigration {

    public V1102RenameSectorToSubRegion() {
        super("1.1.0.2", "Rename Sector to SubRegion");
    }

    protected static String migrateTopiaId(String campaignTopiaId) {
        return campaignTopiaId.replace("fr.ulr.sammoa.persistence.Sector", SubRegion.class.getCanonicalName());
    }

    @Override
    public File migrateImportFile() throws IOException {
        Path surveyPath = dataDirectory.toPath();
        Path sectorCsvPath = surveyPath.resolve("csv/Sector.csv");
        Path subRegionCsvPath = surveyPath.resolve("csv/SubRegion.csv");
        Files.move(sectorCsvPath, subRegionCsvPath);

        loadCvs(subRegionCsvPath)
                .renameColumn("sectorNumber", "subRegionNumber")
                .updateValue("topiaId", line -> migrateTopiaId(line.get("topiaId")))
                .save();

        loadCvs(surveyPath.resolve("csv/Strate.csv"))
                .renameColumn("sector", "subRegion")
                .updateValue("subRegion", line -> migrateTopiaId(line.get("subRegion")))
                .save();

        return dataDirectory;
    }

    @Override
    public void migrateLocalDirectory() {
        // nothing
    }
}
