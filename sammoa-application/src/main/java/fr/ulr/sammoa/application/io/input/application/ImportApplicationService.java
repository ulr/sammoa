package fr.ulr.sammoa.application.io.input.application;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.ReferentialService;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.application.SammoaDirectories;
import fr.ulr.sammoa.application.SammoaServiceSupport;
import fr.ulr.sammoa.application.SammoaTechnicalException;
import fr.ulr.sammoa.application.io.FlightStorage;
import fr.ulr.sammoa.application.io.SammoaStorage;
import fr.ulr.sammoa.application.io.SammoaStorages;
import fr.ulr.sammoa.application.io.SurveyStorage;
import fr.ulr.sammoa.application.io.input.application.strategy.DataImportStrategy;
import fr.ulr.sammoa.application.io.input.application.strategy.ReferentialImportStrategy;
import fr.ulr.sammoa.application.migration.MigrationRequester;
import fr.ulr.sammoa.application.migration.StorageMigrationService;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.SammoaDbMeta;
import fr.ulr.sammoa.persistence.SammoaEntityEnum;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.MetaFilenameAware;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.TopiaCsvImports;
import org.nuiton.util.TimeLog;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to import sammoa files.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportApplicationService extends SammoaServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImportApplicationService.class);

    private static final TimeLog timeLog =
            new TimeLog(ImportApplicationService.class);

    private FlightService flightService;

    private ReferentialService referentialService;

    private StorageMigrationService storageMigrationService;

    @Override
    public void setSammoaContext(SammoaContext context) {
        super.setSammoaContext(context);
        flightService = getService(FlightService.class);
        referentialService = getService(ReferentialService.class);
        storageMigrationService = getService(StorageMigrationService.class);
    }

    public SurveyStorage loadSurveyStorage(File file, MigrationRequester requester) {
        File tmpDirectory = context.getConfig().getTmpDirectory();
        File deflateDirectory = new File(tmpDirectory, file.getName() + "_" + System.nanoTime());
        if (log.isInfoEnabled()) {
            log.info("Will deflate sammoa storage " + file + " to " + deflateDirectory);
        }
        try {
            ZipUtil.uncompress(file, deflateDirectory);
        } catch (IOException e) {
            throw new SammoaTechnicalException(
                    "Could not deflate sammoa storage", e);
        }
        File[] files = deflateDirectory.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY);

        if (files == null || files.length == 0) {
            throw new SammoaTechnicalException("Empty sammoa file");
        }
        File surveyDirectory = storageMigrationService.runSurveyStorageMigration(files[0], requester);
        String surveyId = surveyDirectory.getName();
        SurveyStorage storage = SammoaStorages.getSurveyStorage(
                deflateDirectory, surveyId);

        SammoaStorages.loadProperties(storage);

        return storage;
    }

    public void importApplication(ImportApplicationModel model, Consumer<String> setterStatus) {

        if (log.isInfoEnabled()) {
            log.info("Start Application import to " +
                        model.getFlightIdsToImport()
                                .keySet()
                                .stream()
                                .map(SurveyStorage::toString)
                                .collect(Collectors.joining(",")));
        }

        long startTime = TimeLog.getTime();

        importDb(model, setterStatus);

        startTime = timeLog.log(startTime, "importApplication", "after db import");

        try {
            importStorages(model, setterStatus);

        } catch (IOException e) {
            throw new SammoaTechnicalException(
                    "Could not delete storage", e);
        }

        timeLog.log(startTime, "importApplication", "after import storages");
        setterStatus.accept(t("sammoa.import.complet"));
    }

    protected void importDb(ImportApplicationModel model, Consumer<String> setterStatus) {

        SammoaTopiaPersistenceContext sammoaTopiaPersistenceContext = beginTransaction();

        try {

            long startTime = TimeLog.getTime();

            model.getFlightIdsToRemove()
                    .values()
                    .stream()
                    .flatMap(Collection::stream)
                    .forEach(flightId -> {
                        Flight flight = flightService.getFlight(sammoaTopiaPersistenceContext, flightId);
                        setterStatus.accept(t("sammoa.import.deleteFlight", flight.getSurvey().getCode(), flight.getFlightNumber()));
                        flightService.deleteFlightInDb(sammoaTopiaPersistenceContext, flightId);
                    });

            sammoaTopiaPersistenceContext.flush();

            startTime = timeLog.log(startTime, "importDb",
                                    "after flights delete");

            for (Map.Entry<SurveyStorage, List<String>> entry : model.getFlightIdsToImport().entrySet()) {

                importStorage(entry.getKey(), entry.getValue(), sammoaTopiaPersistenceContext, setterStatus);

            }

            sammoaTopiaPersistenceContext.commit();

            timeLog.log(startTime, "importDb", "after data import");

        } catch (TopiaException | IOException e) {
            throw new SammoaTechnicalException("Could not import", e);
        } finally {
            endTransaction(sammoaTopiaPersistenceContext);
        }
    }

    private void importStorage(SurveyStorage storage, List<String> flightIds, SammoaTopiaPersistenceContext sammoaTopiaPersistenceContext, Consumer<String> setterStatus) throws IOException {
        SammoaDbMeta dbMetas = persistence.getDbMetas();

        Map<TableMeta<SammoaEntityEnum>, File> tableMap;
        Map<AssociationMeta<SammoaEntityEnum>, File> associationMap;

        tableMap = getEntries(dbMetas.getReferentialTables(), storage);
        associationMap = getEntries(dbMetas.getReferentialAssociations(), storage);
        Preconditions.checkArgument(
                associationMap.isEmpty(),
                "No association to import from referential");

        SammoaImportModelFactory modelFactory =
                new SammoaImportModelFactory(config.getCsvSeparator(),
                        sammoaTopiaPersistenceContext,
                        dbMetas);

        try {
            importReferentials(sammoaTopiaPersistenceContext, modelFactory, tableMap);

            DataImportStrategy strategy = new DataImportStrategy(
                    modelFactory, sammoaTopiaPersistenceContext, persistence.getPersistenceHelper()
            );
            for (String flightId :flightIds) {

                FlightStorage flightStorage =
                        storage.getFlightStorage(flightId);
                importFlight(sammoaTopiaPersistenceContext,
                        dbMetas,
                        strategy,
                        flightStorage,
                        setterStatus);
            }
        } finally {
            modelFactory.close();
        }
    }


    private void importFlight(SammoaTopiaPersistenceContext tx,
                              SammoaDbMeta dbMetas,
                              DataImportStrategy strategy,
                              FlightStorage storage,
                              Consumer<String> setterStatus) throws IOException, TopiaException {

        Map<TableMeta<SammoaEntityEnum>, File> tableMap =
                getEntries(dbMetas.getDataTables(), storage);
        Map<AssociationMeta<SammoaEntityEnum>, File> associationMap =
                getEntries(dbMetas.getDataAssociations(), storage);

        for (Map.Entry<TableMeta<SammoaEntityEnum>, File> entry : tableMap.entrySet()) {
            TableMeta<SammoaEntityEnum> meta = entry.getKey();
            File file = entry.getValue();

            setterStatus.accept(t("sammoa.import.flight", storage.getName(), entry.getKey().getName()));
            if (file.length() != 0) {
                try (Reader reader = Files.newBufferedReader(file.toPath())) {

                    TopiaCsvImports.importTable(reader, strategy, meta, null);

                }
            }
        }

        // need to flush session (otherwise object are not accessible by a find ?)
        tx.flush();

        for (Map.Entry<AssociationMeta<SammoaEntityEnum>, File> entry : associationMap.entrySet()) {
            AssociationMeta<SammoaEntityEnum> meta = entry.getKey();
            File file = entry.getValue();
            if (file.length() != 0) {
                try (Reader reader = Files.newBufferedReader(file.toPath())) {

                    TopiaCsvImports.importAssociation(reader, strategy, meta, null);

                }
            }
        }
    }

    private void importReferentials(SammoaTopiaPersistenceContext tx,
                                    SammoaImportModelFactory modelFactory,
                                    Map<TableMeta<SammoaEntityEnum>, File> tableMap) throws IOException, TopiaException {

        ReferentialImportStrategy strategy = new ReferentialImportStrategy(
                modelFactory, tx, persistence.getPersistenceHelper()
        );


        for (Map.Entry<TableMeta<SammoaEntityEnum>, File> entry : tableMap.entrySet()) {
            TableMeta<SammoaEntityEnum> meta = entry.getKey();
            File file = entry.getValue();

            try (Reader reader = Files.newBufferedReader(file.toPath())) {

                TopiaCsvImports.importTable(reader, strategy, meta, null);

            }
        }
    }

    protected <M extends MetaFilenameAware<SammoaEntityEnum>> Map<M, File> getEntries(
            List<M> metas, SammoaStorage storage) {

        List<String> missingEntries = Lists.newArrayList();
        Map<M, File> result = TopiaCsvImports.discoverEntries(
                metas,
                storage.getCsvDirectory(),
                missingEntries);
        Preconditions.checkState(
                missingEntries.isEmpty(),
                "There is some missing csv files for import:" + missingEntries);
        return result;
    }

    protected void importStorages(ImportApplicationModel model, Consumer<String> setterStatus) throws IOException {

        //storage to copy
        try {

            for (Map.Entry<SurveyStorage, List<String>> entry : model.getFlightIdsToRemove().entrySet()) {

                for (String flightId : entry.getValue()) {
                    setterStatus.accept(t("sammoa.import.delete.storageFlight", flightId));
                    entry.getKey().deleteFlightStorage(flightId);
                }

                setterStatus.accept(t("sammoa.import.delete.map", entry.getKey().getCode()));
                FileUtils.deleteDirectory(entry.getKey().getMapDirectory());

            }

            for (Map.Entry<SurveyStorage, List<String>> entry : model.getFlightIdsToImport().entrySet()) {
                SurveyStorage storageToCopy = entry.getKey();

                // get the new survey from db  using his naturalId
                Survey newSurvey = referentialService.getSurveyByNaturalId(
                        storageToCopy.getNaturalId());
                Preconditions.checkNotNull(newSurvey);

                setterStatus.accept(t("sammoa.import.copy.storage", newSurvey.getCode()));

                // create the new empty (or not) survey storage
                File dataDirectory = config.getDataDirectory();
                SurveyStorage newStorage = SammoaStorages.createSurveyStorage(
                        SammoaDirectories.getSurveyDirectory(dataDirectory),
                        newSurvey.getTopiaId());

                // remove properties files from storage
                storageToCopy.deletePropertiesFiles();

                // recopy storage (and selected flight ids) to new storage
                SammoaStorages.copySurveyStorage(storageToCopy,
                        newStorage,
                        entry.getValue());
            }
        } finally {
            // at the end remove the storage to copy
            for (SurveyStorage flightStorages : model.getFlightIdsToImport().keySet()) {
                flightStorages.delete();
            }

        }
    }


}
