/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.migration.StorageMigrationService;
import fr.ulr.sammoa.persistence.SammoaPersistence;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created: 08/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SammoaContext implements Closeable {
    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SammoaContext.class);

    protected static SammoaContext instance;

    protected final SammoaConfig config;

    protected final SammoaPersistence persistence;

    protected final LoadingCache<Class<? extends SammoaService>, SammoaService> services;

    public SammoaContext(SammoaConfig config) {
        this(config, new SammoaPersistence());
    }

    public SammoaContext(SammoaConfig config, SammoaPersistence persistence) {
        this.config = config;
        this.persistence = persistence;
        this.services = CacheBuilder.newBuilder().build(new CacheLoader<Class<? extends SammoaService>, SammoaService>() {
            @Override
            public SammoaService load(Class<? extends SammoaService> key) throws Exception {
                Preconditions.checkNotNull(key);
                Constructor<?> constructor = key.getConstructor();
                Preconditions.checkNotNull(constructor);
                SammoaService s = (SammoaService) constructor.newInstance();
                if (log.isInfoEnabled()) {
                    log.info("New service " +  s);
                }
                s.setSammoaContext(SammoaContext.this);
                return s;
            }
        });
    }

    public SammoaConfig getConfig() {
        return config;
    }

    public SammoaPersistence getPersistence() {
        return persistence;
    }

    public <S extends SammoaService> S getService(Class<S> serviceType) {
        try {
            S s = (S) services.get(serviceType);
            return s;
        } catch (ExecutionException e) {
            throw new SammoaTechnicalException(
                    "Could not instanciate service " + serviceType, e);
        }
    }

    public <S extends SammoaService> void closeService(S service) throws IOException {

        services.invalidate(service.getClass());

        if (log.isInfoEnabled()) {
            log.info("Close service " +  service);
        }
        service.close();
    }

    public void open() {
        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        BeanTopiaConfiguration topiaConfiguration =
                topiaConfigurationBuilder.readProperties(config.getApplicationConfig().getFlatOptions());

        List<String> migrations = Lists.newArrayList();

        persistence.open(
                topiaConfiguration,
                config.getAutoCommitDelay(),
                migrations::add
        );

        StorageMigrationService storageMigrationService = getService(StorageMigrationService.class);
        storageMigrationService.runMigrateLocalDirectory(migrations);
    }

    @Override
    public void close() throws IOException {

        if (persistence != null) {
            persistence.close();
        }

        for (SammoaService service : services.asMap().values()) {
            if (service != null) {
                if (log.isInfoEnabled()) {
                    log.info("Close service " +  service);
                }
                service.close();
            }
        }
        services.cleanUp();
    }
}
