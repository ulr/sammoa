package fr.ulr.sammoa.application.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.io.output.map.DbfTimestampConverter;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.util.Date;

/**
 * Created: 08/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DbfTimestampConverterTest {

    @Test
    public void testToString() {

        Date date = DateUtil.createDate(52, 2, 19, 5, 3, 2007);
        String result = DbfTimestampConverter.toString(date);
        Assert.assertEquals("2454165 68572000", result);
    }

    @Test
    public void testToDate() {

        Date expected = DateUtil.createDate(52, 2, 19, 5, 3, 2007);
        Date result = DbfTimestampConverter.toDate("2454165 68572000");
        Assert.assertEquals(expected, result);
    }
}
