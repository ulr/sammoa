package fr.ulr.sammoa.application.output.map;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ulr.sammoa.application.FlightService;
import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaConfigMock;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.application.SammoaDatabase;
import fr.ulr.sammoa.application.io.input.map.DbfImport;
import fr.ulr.sammoa.application.io.input.map.DoubleToIntegerValueParser;
import fr.ulr.sammoa.application.io.output.map.DbfTableModelBuilder;
import fr.ulr.sammoa.application.io.output.map.ExportMapModel;
import fr.ulr.sammoa.application.io.output.map.ExportMapService;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyImpl;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.FlightImpl;
import fr.ulr.sammoa.persistence.GeoPoint;
import fr.ulr.sammoa.persistence.GeoPointImpl;
import fr.ulr.sammoa.persistence.GeoPointTopiaDao;
import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.RegionImpl;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.csv.Common;
import org.nuiton.csv.ImportModel;
import org.nuiton.csv.ImportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueSetter;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created: 08/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ExportMapServiceTest {

    public SammoaConfig config =
            new SammoaConfigMock("sammoa-test.properties");

    @Rule
    public SammoaDatabase database = new SammoaDatabase("/" +
                                                        config.getApplicationConfig().getConfigFileName(), config.getAutoCommitDelay());

    protected SammoaTopiaPersistenceContext transaction;

    @Before
    public void setUp() throws TopiaException {
        transaction = database.beginTransaction();
    }

    @After
    public void tearDown() throws TopiaException {
        database.endTransaction(transaction);
    }

    @Test
    public void testExportGeoPointsMap() throws Exception {

        SammoaContext context = new SammoaContext(config,
                                                  database.getPersistence());

        Survey survey = database.createSurvey("PACOMM", "FRANCE");
        survey.setBeginDate(DateUtil.createDate(1, 1, 2012));
        survey.setEndDate(DateUtil.createDate(31, 12, 2012));

        FlightService flightService = context.getService(FlightService.class);
        Flight flight = flightService.createFlight(survey, flightService.getNextFlightNumber());

        flight.setBeginDate(DateUtil.createDate(18, 0, 12, 18, 7, 2012));

        createGeoPoint(DateUtil.createDate(30, 15, 12, 18, 7, 2012), 1, 2, flight);
        createGeoPoint(DateUtil.createDate(39, 18, 12, 18, 7, 2012), 3, 4, flight);

        transaction.commit();

        ExportMapService exportMapService = context.getService(ExportMapService.class);

        File exportDirectory = database.getTestBasedir();

        ExportMapModel exportMapModel = ExportMapModel.newModel(
                exportDirectory,
                "export-geoPoints.shp",
                survey,
                survey.getBeginDate(),
                survey.getEndDate(),
                null,
                null,
                null
        );

        exportMapService.exportGeoPointsMap(exportMapModel);

        Assert.assertTrue(new File(exportDirectory, "export-geoPoints.shp").exists());
        Assert.assertTrue(new File(exportDirectory, "export-geoPoints.dbf").exists());
        Assert.assertTrue(new File(exportDirectory, "export-geoPoints.shx").exists());
        Assert.assertTrue(new File(exportDirectory, "export-geoPoints.prj").exists());

        URL fileUrl = new File(exportDirectory, "export-geoPoints.dbf").toURI().toURL();
        DbfImport<GeoPoint> dbfImport = new DbfImport<GeoPoint>(new GeoPointImportModel(), fileUrl);

        List<GeoPoint> results = Lists.newArrayList(dbfImport);

        Assert.assertEquals(2, results.size());
        {
            GeoPoint result = results.get(0);
            Flight resultFlight = result.getFlight();
            Survey resultSurvey = resultFlight.getSurvey();
            Region resultRegion = resultSurvey.getRegion();
            Assert.assertEquals("FRANCE", resultRegion.getCode());
            Assert.assertEquals("PACOMM", resultSurvey.getCode());
            Assert.assertEquals(1, resultFlight.getFlightNumber());
            Assert.assertEquals("A", resultFlight.getSystemId());
            Assert.assertEquals(DateUtil.createDate(30, 15, 12, 18, 7, 2012), result.getRecordTime());
            Assert.assertEquals(1., result.getLatitude(),.001);
            Assert.assertEquals(2., result.getLongitude(),.001);
            Assert.assertEquals(0., result.getAltitude(),.001);
            Assert.assertEquals(0., result.getSpeed(),.001);
        }
        {
            GeoPoint result = results.get(1);
            Flight resultFlight = result.getFlight();
            Survey resultSurvey = resultFlight.getSurvey();
            Region resultRegion = resultSurvey.getRegion();
            Assert.assertEquals("FRANCE", resultRegion.getCode());
            Assert.assertEquals("PACOMM", resultSurvey.getCode());
            Assert.assertEquals(1, resultFlight.getFlightNumber());
            Assert.assertEquals("A", resultFlight.getSystemId());
            Assert.assertEquals(DateUtil.createDate(39, 18, 12, 18, 7, 2012), result.getRecordTime());
            Assert.assertEquals(3., result.getLatitude(),.001);
            Assert.assertEquals(4., result.getLongitude(),.001);
            Assert.assertEquals(0., result.getAltitude(),.001);
            Assert.assertEquals(0., result.getSpeed(),.001);
        }
    }

    protected GeoPoint createGeoPoint(Date date,
                                      double latitude,
                                      double longitude,
                                      Flight flight)
            throws TopiaException {

        GeoPointTopiaDao transectDAO =  transaction.getGeoPointDao();
        GeoPoint result = transectDAO.create();
        result.setRecordTime(date);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setFlight(flight);

        return result;
    }

    protected class GeoPointImportModel implements ImportModel<GeoPoint> {

        @Override
        public char getSeparator() {
            return ' ';
        }

        @Override
        public void pushCsvHeaderNames(List<String> headerNames) {
        }

        @Override
        public GeoPoint newEmptyInstance() {
            Region region = new RegionImpl();
            Survey survey = new SurveyImpl();
            survey.setRegion(region);
            Flight flight = new FlightImpl();
            flight.setSurvey(survey);
            GeoPoint result = new GeoPointImpl();
            result.setFlight(flight);
            return result;
        }

        @Override
        public Iterable<ImportableColumn<GeoPoint, Object>> getColumnsForImport() {
            ModelBuilder builder = new ModelBuilder();
            builder.newMandatoryColumn("region", "flight.survey.region.code");
            builder.newMandatoryColumn("aircraft", "flight.immatriculation");
            builder.newMandatoryColumn("survey", "flight.survey.code");
            builder.newMandatoryColumn("flight", "flight.flightNumber", new DoubleToIntegerValueParser());
            builder.newMandatoryColumn("computer", "flight.systemId");
            builder.newMandatoryColumn("date", "recordTime", new Common.DateValue(DbfTableModelBuilder.DATE_FORMAT_PATTERN));
            builder.newMandatoryColumn("hhmmss", new Common.DateValue("HHmmss"), new GeoPointTimeValueSetter());
            builder.newMandatoryColumn("lat", "latitude", Common.DOUBLE_PRIMITIVE);
            builder.newMandatoryColumn("lon", "longitude", Common.DOUBLE_PRIMITIVE);
            builder.newMandatoryColumn("speed", "speed", Common.DOUBLE_PRIMITIVE);
            builder.newMandatoryColumn("altitude", "altitude", Common.DOUBLE_PRIMITIVE);
            builder.newMandatoryColumn("gpsDelay", "captureDelay", new DoubleToIntegerValueParser());
            return builder.getColumnsForImport();
        }
    }

    protected class GeoPointTimeValueSetter implements ValueSetter<GeoPoint, Date> {

        @Override
        public void set(GeoPoint object, Date value) throws Exception {
            Date recordTime = object.getRecordTime();
//            if (recordTime != null) {
            Calendar resultCalendar = Calendar.getInstance();
            resultCalendar.setTime(recordTime);

            Calendar valueCalendar = Calendar.getInstance();
            valueCalendar.setTime(value);
            resultCalendar.set(Calendar.HOUR_OF_DAY, valueCalendar.get(Calendar.HOUR_OF_DAY));
            resultCalendar.set(Calendar.MINUTE, valueCalendar.get(Calendar.MINUTE));
            resultCalendar.set(Calendar.SECOND, valueCalendar.get(Calendar.SECOND));

            object.setRecordTime(resultCalendar.getTime());

//            } else {
//                object.setRecordTime(value);
//            }
        }

    }
}
