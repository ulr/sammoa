/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import fr.ulr.sammoa.persistence.Region;
import fr.ulr.sammoa.persistence.RegionTopiaDao;
import fr.ulr.sammoa.persistence.SammoaPersistence;
import fr.ulr.sammoa.persistence.SammoaTopiaApplicationContext;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.SurveyTopiaDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created: 06/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SammoaDatabase extends TestWatcher {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SammoaDatabase.class);

    protected SammoaPersistence persistence;

    protected long autoCommitDelay;

    /** A time-stamp, allow to make multiple build and keep the tests data. */
    public static final String TIMESTAMP = String.valueOf(System.nanoTime());

    protected File testBasedir;

    protected BeanTopiaConfiguration dbConfiguration;

    protected SammoaTopiaApplicationContext rootCtxt;

    protected Description description;

    protected final String configurationPath;

    protected final String classifier;

    public SammoaDatabase(String configurationPath, long autoCommitDelay) {
        this.configurationPath = configurationPath;
        this.classifier = "";
        this.autoCommitDelay = autoCommitDelay;
    }

    @Override
    protected void starting(Description description) {

        this.description = description;

        // get test directory
        testBasedir = getTestSpecificDirectory(
                description.getTestClass(),
                description.getMethodName(),
                classifier);

        if (log.isDebugEnabled()) {
            log.debug("testBasedir = " + testBasedir);
        }

        // create the root context
        try {

            TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
            dbConfiguration = topiaConfigurationBuilder.forTest(description.getTestClass(), description.getMethodName());
            onDbConfigurationCreate(dbConfiguration);
            rootCtxt = new SammoaTopiaApplicationContext(dbConfiguration);
            onRootCtxtCreate(rootCtxt);

        } catch (Exception e) {
            throw new IllegalStateException(
                    "Could not start db at " + testBasedir, e);
        }
    }

    @Override
    public void finished(Description description) {

        if (rootCtxt != null && !rootCtxt.isClosed()) {
            try {
                rootCtxt.close();
            } catch (TopiaException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close topia root context", e);
                }
            }
        }
        rootCtxt = null;
        dbConfiguration = null;
    }

    protected void onDbConfigurationCreate(TopiaConfiguration configuration) {
        persistence = new SammoaPersistence();
        persistence.open(configuration, autoCommitDelay, null);
    }

    protected void onRootCtxtCreate(SammoaTopiaApplicationContext rootCtxt) {
        persistence.setRootContext(rootCtxt);
    }

    public SammoaTopiaPersistenceContext beginTransaction() throws TopiaException {
        return persistence.beginTransaction();
    }

    public void endTransaction(SammoaTopiaPersistenceContext transaction) {
        persistence.endTransaction(transaction);
    }

    public SammoaPersistence getPersistence() {
        return persistence;
    }

    public Survey createSurvey(String code, String regionCode) throws TopiaException {

        SammoaTopiaPersistenceContext transaction = beginTransaction();

        RegionTopiaDao regionDAO =  transaction.getRegionDao();
        Region region = regionDAO.createByNaturalId(regionCode);

        SurveyTopiaDao surveyDAO =  transaction.getSurveyDao();
        Survey result = surveyDAO.createByNaturalId(code, region);

        transaction.commit();
        endTransaction(transaction);

        return result;
    }

    public File getTestBasedir() {
        return testBasedir;
    }

    public SammoaTopiaApplicationContext getRootCtxt() {
        return rootCtxt;
    }

    public BeanTopiaConfiguration getDbConfiguration() {
        return dbConfiguration;
    }

    public Description getDescription() {
        return description;
    }

    protected Properties newDbConfiguration(String configurationPath) throws IOException {

        Properties result = new Properties();
        InputStream stream =
                getClass().getResourceAsStream(configurationPath);

        try {
            result.load(stream);
        } finally {
            stream.close();
        }

        return result;
    }

    public static File getTestSpecificDirectory(Class<?> testClassName,
                                                String methodName) {
        return getTestSpecificDirectory(testClassName, methodName, "");
    }

    public static File getTestSpecificDirectory(Class<?> testClassName,
                                                String methodName,
                                                String classifier) {
        // Trying to look for the temporary folder to store data for the test
        String tempDirPath = System.getProperty("java.io.tmpdir");
        if (tempDirPath == null) {
            // can this really occur ?
            tempDirPath = "";
            if (log.isWarnEnabled()) {
                log.warn("'\"java.io.tmpdir\" not defined");
            }
        }
        File tempDirFile = new File(tempDirPath);

        // create the directory to store database data
        String dataBasePath = testClassName.getName()
                              + File.separator // a directory with the test class name
                              + methodName; // a sub-directory with the method name

        if (StringUtils.isNotBlank(classifier)) {
            dataBasePath += classifier;
        }
        dataBasePath += '_'
                        + TIMESTAMP; // and a timestamp
        File databaseFile = new File(tempDirFile, dataBasePath);
        return databaseFile;
    }
}
