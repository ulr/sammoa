/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.application;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Flight;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverTopiaDao;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import fr.ulr.sammoa.persistence.SubRegion;
import fr.ulr.sammoa.persistence.SubRegionTopiaDao;
import fr.ulr.sammoa.persistence.Strate;
import fr.ulr.sammoa.persistence.StrateTopiaDao;
import fr.ulr.sammoa.persistence.Transect;
import fr.ulr.sammoa.persistence.TransectFlight;
import fr.ulr.sammoa.persistence.TransectTopiaDao;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Map;

/**
 * Created: 08/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class FlightServiceTest {

    public SammoaConfig config =
            new SammoaConfigMock("sammoa-test.properties");

    @Rule
    public SammoaDatabase database = new SammoaDatabase(
            "/" + config.getApplicationConfig().getConfigFileName(),
            config.getAutoCommitDelay());

    @After
    public void tearDown() {
        config.setFlightNumber(0);
    }

    @Test
    public void testCreateFlight() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");

        context.getConfig().setFlightNumber(17);

        Flight flight = service.createFlight(survey, 18);

        Assert.assertEquals(18, flight.getFlightNumber());
        Assert.assertEquals(18, (int) config.getFlightNumber());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateFlightError() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");

        context.getConfig().setFlightNumber(18);

        service.createFlight(survey, 1);
    }

    @Test
    public void testGetNextFlightNumber() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");

        {
            service.createFlight(survey, 124);
            int result = service.getNextFlightNumber();
            Assert.assertEquals(125, result);
        }

        {
            config.setFlightNumber(18);
            int result = service.getNextFlightNumber();
            Assert.assertEquals(125, result);
        }

        {
            config.setFlightNumber(149);
            int result = service.getNextFlightNumber();
            Assert.assertEquals(150, result);
        }
    }

    @Test
    public void testAddTransects() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");
        Strate strate = createStrate(survey, 1, "A");
        Transect transect1 = createTransect("C1", strate);
        Transect transect2 = createTransect("C2", strate);
        Transect transect3 = createTransect("C3", strate);

        Flight flight = service.createFlight(survey, service.getNextFlightNumber());

        // add 4 observers in flight
        for (int i = 0; i < 4; i++) {
            flight.addObserver(createObserver(String.valueOf(i), survey));
        }

        // Add transect1 and transect2
        Iterable<Transect> transects = Lists.newArrayList(transect1, transect2);

        service.addTransects(flight, 0, transects);

        Assert.assertEquals(2, flight.getTransectFlight().size());
        TransectFlight transectFlight1 = flight.getTransectFlight().get(0);
        Assert.assertEquals(transect1, transectFlight1.getTransect());
        Assert.assertEquals(1, transectFlight1.getCrossingNumber());
        TransectFlight transectFlight2 = flight.getTransectFlight().get(1);
        Assert.assertEquals(transect2, transectFlight2.getTransect());
        Assert.assertEquals(1, transectFlight2.getCrossingNumber());

        // Add an other transectFlight for transect2 and transect3
        transects = Lists.newArrayList(transect2, transect3);

        service.addTransects(flight, 1, transects);

        Assert.assertEquals(4, flight.getTransectFlight().size());
        TransectFlight transectFlight3 = flight.getTransectFlight().get(2);
        Assert.assertEquals(transect3, transectFlight3.getTransect());
        Assert.assertEquals(1, transectFlight3.getCrossingNumber());
        TransectFlight transectFlightAtIndex2 = flight.getTransectFlight().get(1);
        Assert.assertNotSame(transectFlight2, transectFlightAtIndex2);
        Assert.assertEquals(transect2, transectFlightAtIndex2.getTransect());
        Assert.assertEquals(2, transectFlightAtIndex2.getCrossingNumber());
    }

    @Test
    public void testDeleteTransectFlight() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");
        Strate strate = createStrate(survey, 1, "A");
        Transect transect = createTransect("C1", strate);

        Flight flight = service.createFlight(survey, service.getNextFlightNumber());

        List<TransectFlight> transectFlights =
                service.addTransects(flight, 0, ImmutableList.of(transect));

        TransectFlight transectFlight = transectFlights.get(0);
        Assert.assertEquals(1, transectFlight.getCrossingNumber());

        service.deleteTransectFlight(flight, transectFlight);

        transectFlights =
                service.addTransects(flight, 0, ImmutableList.of(transect));
        transectFlight = transectFlights.get(0);
        Assert.assertEquals(1, transectFlight.getCrossingNumber());
    }

    @Test
    public void testCrossingNumber() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        FlightService service = context.getService(FlightService.class);

        Survey survey = database.createSurvey("PACOMM", "FRANCE");

        Strate strate = createStrate(survey, 1, "A");
        Transect transect = createTransect("C1/01", strate);

        Flight flight1 = service.createFlight(survey, service.getNextFlightNumber());
        Flight flight2 = service.createFlight(survey, service.getNextFlightNumber());
        Flight flight3 = service.createFlight(survey, service.getNextFlightNumber());

        int index = 0;
        // 1st crossing on flight 1
        {
            service.addTransects(flight1, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight1.getTransectFlight().get(index);
            Assert.assertEquals(1, transectFlight.getCrossingNumber());
        }
        // 2nd crossing on flight 1
        {
            service.addTransects(flight1, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight1.getTransectFlight().get(index);
            Assert.assertEquals(2, transectFlight.getCrossingNumber());
        }
        // 3rd crossing on flight 2
        {
            service.addTransects(flight2, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight2.getTransectFlight().get(index);
            Assert.assertEquals(3, transectFlight.getCrossingNumber());
        }
        // 6th crossing on flight 3
        // manual set because an other system does the 4th and 5th crossings
        {
            service.addTransects(flight3, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight3.getTransectFlight().get(index);
            Assert.assertEquals(4, transectFlight.getCrossingNumber());
            transectFlight.setCrossingNumber(6);
        }
        // 7th crossing on flight 3
        {
            service.addTransects(flight3, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight3.getTransectFlight().get(index);
            Assert.assertEquals(7, transectFlight.getCrossingNumber());
        }
        // We delete the flight3, the next crossing become the 4th
        {
            service.deleteFlight(survey.getTopiaId(), flight3.getTopiaId());
        }
        // 4th crossing on flight 2
        {
            service.addTransects(flight2, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight2.getTransectFlight().get(index);
            Assert.assertEquals(4, transectFlight.getCrossingNumber());
        }
        // We update the transect nbTimes (usually done by import)
        {
            transect.setNbTimes(5);
        }
        // 6th crossing on flight 2
        {
            service.addTransects(flight2, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight2.getTransectFlight().get(index);
            Assert.assertEquals(6, transectFlight.getCrossingNumber());
        }
        // Mark 2nd crossing as deleted
        {
            TransectFlight transectFlight = flight1.getTransectFlight().get(index);
            Assert.assertEquals(2, transectFlight.getCrossingNumber());
            transectFlight.setDeleted(true);
        }
        // 5th crossing on flight 1 (manual change)
        {
            service.addTransects(flight1, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight1.getTransectFlight().get(index);
            Assert.assertEquals(7, transectFlight.getCrossingNumber());
            transectFlight.setCrossingNumber(5);
        }
        // Change the 6th crossingNumber to 2nd (2nd is marked deleted)
        {
            TransectFlight transectFlight = flight2.getTransectFlight().get(index);
            Assert.assertEquals(6, transectFlight.getCrossingNumber());
            transectFlight.setCrossingNumber(2);
        }
        // Check that 6 transectFlight are attached to the transect
        // but one is deleted
        {
            SammoaTopiaPersistenceContext tx = database.beginTransaction();
            Map<Transect, Long> map = tx.getTransectFlightDao().countAllByTransect(null);
            Assert.assertEquals(new Long(6), map.get(transect));
        }
        // 6th crossing on flight 1 marked as 1st one (strange but not forbidden)
        {
            service.addTransects(flight1, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight1.getTransectFlight().get(index);
            Assert.assertEquals(6, transectFlight.getCrossingNumber());
            transectFlight.setCrossingNumber(1);
        }
        // 7th crossing on flight 2
        // There is 6 transectFlight not deleted, 2 as 1st crossing and no 6th crossing
        // No check is done, but here we assume that there is 6 valid crossing
        // So the next one is the 7th
        {
            service.addTransects(flight2, index, ImmutableList.of(transect));
            TransectFlight transectFlight = flight2.getTransectFlight().get(index);
            Assert.assertEquals(7, transectFlight.getCrossingNumber());
        }
    }

    protected Observer createObserver(String initials, Survey survey) throws TopiaException {

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();

        ObserverTopiaDao observerDAO =  transaction.getObserverDao();
        Observer result = observerDAO.createByNaturalId(initials, survey);

        transaction.commit();
        database.endTransaction(transaction);

        return result;
    }

    protected Transect createTransect(String name, Strate strate) throws TopiaException {

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();

        TransectTopiaDao transectDAO =  transaction.getTransectDao();
        Transect result = transectDAO.createByNaturalId(name, strate);

        transaction.commit();
        database.endTransaction(transaction);

        return result;
    }

    protected Strate createStrate(Survey survey, int subRegionNumber, String code) throws TopiaException {

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();

        SubRegionTopiaDao subRegionDAO =  transaction.getSubRegionDao();
        SubRegion subRegion = subRegionDAO.createByNaturalId(subRegionNumber, survey);


        StrateTopiaDao strateDAO =  transaction.getStrateDao();
        Strate result = strateDAO.createByNaturalId(code, subRegion);

        transaction.commit();
        database.endTransaction(transaction);

        return result;
    }
}
