package fr.ulr.sammoa.application.io.input.csv;
/*
 * #%L
 * SAMMOA :: Application
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.application.SammoaConfig;
import fr.ulr.sammoa.application.SammoaConfigMock;
import fr.ulr.sammoa.application.SammoaContext;
import fr.ulr.sammoa.application.SammoaDatabase;
import fr.ulr.sammoa.persistence.Survey;
import fr.ulr.sammoa.persistence.Observer;
import fr.ulr.sammoa.persistence.ObserverTopiaDao;
import fr.ulr.sammoa.persistence.SammoaTopiaPersistenceContext;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * To test {@link ImportCsvService}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class ImportCsvServiceTest {

    public SammoaConfig config =
            new SammoaConfigMock("sammoa-test.properties");

    @Rule
    public SammoaDatabase database = new SammoaDatabase(
            "/" + config.getApplicationConfig().getConfigFileName(),
            config.getAutoCommitDelay());

    @Test
    public void importObservers() throws Exception {

        SammoaContext context = new SammoaContext(config, database.getPersistence());

        ImportCsvService instance = context.getService(ImportCsvService.class);

        Survey survey = database.createSurvey("PACOMM-2012", "FRANCE");

        URL stream = getClass().getResource("/observers.csv");

        int result = instance.importObservers(survey.getTopiaId(),
                                              new File(stream.toURI()));

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();
        ObserverTopiaDao dao =  transaction.getObserverDao();
        Collection<Observer> observers = dao.findAll();
        database.endTransaction(transaction);

        assertEquals(7, result);
        assertEquals(result, observers.size());
    }
}
