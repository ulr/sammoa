package fr.ulr.sammoa.migration;

/*-
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ulr.sammoa.persistence.SammoaTopiaConfiguration;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.callback.BaseFlywayCallback;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;

import java.sql.Connection;
import java.util.Map;
import java.util.function.Consumer;

public class SammoaFlywayMigrationService extends TopiaFlywayServiceImpl {

    protected Consumer<String> migrationCallBack;

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {
        super.initTopiaService(topiaApplicationContext, serviceConfiguration);
        TopiaConfiguration configuration = topiaApplicationContext.getConfiguration();
        if (SammoaTopiaConfiguration.class.isInstance(configuration)) {
            migrationCallBack = SammoaTopiaConfiguration.class.cast(configuration).getMigrationCallBack();
        }
    }

    @Override
    public void runSchemaMigration() {

        if (flyway.info().applied().length == 0) {
            flywayBaselineVersion = "1.0";
            useModelVersion = false;
        }

        flyway.setCallbacks(new BaseFlywayCallback() {
            @Override
            public void afterEachMigrate(Connection connection, MigrationInfo info) {
                super.afterEachMigrate(connection, info);
                if (migrationCallBack != null) {
                    String version = info.getVersion().getVersion();
                    migrationCallBack.accept(version);
                }
            }
        });

        super.runSchemaMigration();
    }
}
