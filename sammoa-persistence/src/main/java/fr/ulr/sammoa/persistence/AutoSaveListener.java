/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.event.ListenableBean;
import org.nuiton.topia.persistence.event.TopiaEntityEvent;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created: 06/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class AutoSaveListener implements PropertyChangeListener, TopiaEntityListener {

    private static final Log log = LogFactory.getLog(AutoSaveListener.class);

    protected SammoaPersistence persistence;

    protected long commitDelay;

    protected final Set<TopiaEntity> changedEntities;

    protected final Set<TopiaEntity> createdEntities;

    protected Timer timer;

    protected boolean stop;

    protected TimerTask currentTask;

    public AutoSaveListener(SammoaPersistence persistence,
                            long commitDelay) {

        this.persistence = persistence;
        this.commitDelay = commitDelay;
        changedEntities = Collections.synchronizedSet(Sets.<TopiaEntity>newHashSet());
        createdEntities = Collections.synchronizedSet(Sets.<TopiaEntity>newHashSet());
        timer = new Timer();
        start();
    }

    @Override
    public void create(TopiaEntityEvent event) {
        ListenableBean entity = (ListenableBean)event.getEntity();
        entity.addPropertyChangeListener(this);
    }

    @Override
    public void load(TopiaEntityEvent event) {
        ListenableBean entity = (ListenableBean)event.getEntity();
        entity.addPropertyChangeListener(this);
    }

    @Override
    public void update(TopiaEntityEvent event) {
        // do nothing on update
    }

    @Override
    public void delete(TopiaEntityEvent event) {
        // do nothing on delete
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        if (!stop && evt.getSource() instanceof TopiaEntity) {

            TopiaEntity entity = (TopiaEntity) evt.getSource();
            changedEntities.add(entity);

            if (log.isTraceEnabled()) {
                log.trace("Record entity '" + entity + "' for autoCommit [propertyChange=" + evt.getPropertyName() + "]");
            }
        }
    }

    public void addCreatedEntity(TopiaEntity entity) {
        createdEntities.add(entity);
    }

    public synchronized void start() {
        stop = false;
        if (currentTask != null) {
            currentTask.cancel();
        }
        currentTask = new AutoCommit();
        timer.schedule(currentTask, commitDelay);
    }

    public synchronized void commit() {
        if (currentTask != null) {
            currentTask.cancel();
        }
        currentTask = new AutoCommit();
        currentTask.run();
    }

    public synchronized void stop() {
        stop = true;
        commit();
        if (log.isWarnEnabled() && !changedEntities.isEmpty() || !createdEntities.isEmpty()) {
            log.warn("Some entities are removed from autoSave but unsaved, check log for errors");
        }
        changedEntities.clear();
        createdEntities.clear();
    }

    protected class AutoCommit extends TimerTask {

        @Override
        public void run() {

            if (isCommitNeeded()) {

                SammoaTopiaPersistenceContext sammoaTopiaPersistenceContext = persistence.openContext();
                try {

                    Set<TopiaEntity> errorSavedEntities =
                            saveEntities(sammoaTopiaPersistenceContext, createdEntities, CREATE_ACTION);

                    changedEntities.addAll(
                            saveEntities(sammoaTopiaPersistenceContext, changedEntities, UPDATE_ACTION));

                    changedEntities.addAll(errorSavedEntities);

                } finally {
                    persistence.closeContext(sammoaTopiaPersistenceContext);
                }
            }

            if (!stop) {
                currentTask = new AutoCommit();
                timer.schedule(currentTask, commitDelay);
            }
        }
    }

    protected boolean isCommitNeeded() {
        return !changedEntities.isEmpty() || !createdEntities.isEmpty();
    }

    /**
     * Save the {@code entities} using the {@code action}. The input set is
     * manipulated as a synchronized collection. A copy is made before save.
     * Commit is done after all entities save. If an error occurs, the entity
     * are keeped in source {@code entities}, otherwise they are removed.
     *
     * @param sammoaContext SammoaTopiaPersistenceContext used to commit
     * @param entities    synchronized Set of entities to save
     * @param action      SaveAction to use
     * @return a Set of entities in error during save
     */
    protected Set<TopiaEntity> saveEntities(SammoaTopiaPersistenceContext sammoaContext,
                                            Set<TopiaEntity> entities,
                                            SaveAction action) {

        Set<TopiaEntity> result;
        if (entities.isEmpty()) {
            result = Collections.emptySet();

        } else {

            Set<TopiaEntity> entitiesCopy;

            synchronized (entities) {
                entitiesCopy = ImmutableSet.copyOf(entities);
                entities.clear();
            }

            try {
                for (TopiaEntity entity : entitiesCopy) {

                    Class<? extends TopiaEntity> contractClass = SammoaEntityEnum.getContractClass(entity.getClass());

                    TopiaDao<? extends TopiaEntity> dao = sammoaContext.getDao(contractClass);

                    action.save((TopiaDao<TopiaEntity>) dao, entity);
                }

                sammoaContext.commit();

                result = Collections.emptySet();

            } catch (TopiaException e) {
                if (log.isErrorEnabled()) {
                    log.error(String.format("Error during %s", action), e);
                }

                result = entitiesCopy;
            }

            if (log.isDebugEnabled() && !entitiesCopy.isEmpty()) {
                log.debug(String.format("%s %d entities", action, entitiesCopy.size()));
            }
        }
        return result;
    }

    protected interface SaveAction {

        <E extends TopiaEntity> void save(TopiaDao<E> dao, E entity) throws TopiaException;
    }

    protected static SaveAction CREATE_ACTION = new SaveAction() {

        @Override
        public String toString() {
            return "Create";
        }

        @Override
        public <E extends TopiaEntity> void save(TopiaDao<E> dao, E entity) throws TopiaException {
            dao.create(entity);
        }
    };

    protected static SaveAction UPDATE_ACTION = new SaveAction() {

        @Override
        public String toString() {
            return "Update";
        }

        @Override
        public <E extends TopiaEntity> void save(TopiaDao<E> dao, E entity) throws TopiaException {
            dao.update(entity);
        }
    };
}
