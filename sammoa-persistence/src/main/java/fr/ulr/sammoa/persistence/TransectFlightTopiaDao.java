package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class TransectFlightTopiaDao extends AbstractTransectFlightTopiaDao<TransectFlight> {

    public Map<Transect, Long> countAllByTransect(Collection<Transect> transects) {

        String hql = "SELECT " + TransectFlight.PROPERTY_TRANSECT + ", count(*) " +
                     newFromClause();

        ImmutableMap<String, Object> parameters = ImmutableMap.of();

        if (CollectionUtils.isNotEmpty(transects)) {
            hql += " WHERE " + TransectFlight.PROPERTY_TRANSECT + " IN (:transects) ";
            parameters = ImmutableMap.of("transects", (Object) transects);
        }

        hql +=  " GROUP BY " + TransectFlight.PROPERTY_TRANSECT;

        List<Object[]> queryResults = findAll(hql, parameters);

        Map<Transect, Long> result = Maps.newHashMap();
        for (Object[] row : queryResults) {
            Transect transect = (Transect) row[0];
            Long nbTransectFlights = (Long) row[1];
            result.put(transect, nbTransectFlights);
        }
        return result;
    }

    public int getMaxCrossingNumberByTransect(Transect transect) {

        String hql = "SELECT max(" + TransectFlight.PROPERTY_CROSSING_NUMBER + "), count(*)" +
                newFromClause() + " " +
                "WHERE " + TransectFlight.PROPERTY_TRANSECT + " = :transect " +
                "AND " + TransectFlight.PROPERTY_DELETED + " = :deleted ";

        ImmutableMap<String, Object> parameters = ImmutableMap.of("transect", (Object) transect, "deleted", false);

        Object[] queryResults = findAny(hql, parameters);

        int maxCrossingNumber = queryResults[0] != null ? (Integer) queryResults[0] : 0;
        int nbTransectFlights = ((Long) queryResults[1]).intValue();
        int result = Math.max(Math.max(maxCrossingNumber, nbTransectFlights), transect.getNbTimes());

        return result;
    }


    public void reAttachIndexInFlight(Iterable<TransectFlight> transectFlights) throws TopiaException {

        StringBuilder builder = new StringBuilder();

        String ql = "UPDATE TransectFlight SET flight_idx = %s " +
                    "WHERE topiaid = '%s';";

        for (TransectFlight transectFlight : transectFlights) {
            builder.append(String.format(ql, transectFlight.getIndexInFlight(),
                                         transectFlight.getTopiaId()));
        }

        topiaSqlSupport.executeSql(builder.toString());
    }

}
