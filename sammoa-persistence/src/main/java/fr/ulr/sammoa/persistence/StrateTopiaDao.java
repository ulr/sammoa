package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

public class StrateTopiaDao extends AbstractStrateTopiaDao<Strate> {

    public List<Strate> findAllBySurveyOrderedByCode(Survey survey) {

        List<Strate> strates = forEquals(Strate.PROPERTY_SUB_REGION + "." + SubRegion.PROPERTY_SURVEY, survey)
                          .setOrderByArguments(Strate.PROPERTY_SUB_REGION + "." + SubRegion.PROPERTY_SUB_REGION_NUMBER, Strate.PROPERTY_CODE)
                          .findAll();

        return strates;
    }
} //StrateDAOImpl<E extends Strate>
