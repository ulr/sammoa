/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import org.nuiton.topia.persistence.TopiaException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class ObservationTopiaDao extends AbstractObservationTopiaDao<Observation> {

    private static final Log log = LogFactory.getLog(ObservationTopiaDao.class);

    /**
     * Override update method to have special treatment for Species.
     * We can't save the species before the Observation is saved, and
     * the Observation could be automatically saved at any time.
     * So we check if the species exists and if the code is correct to push
     * this species in the observation. If the species doesn't exist in the
     * database, it could be created (localCreation) at the same time (sort of
     * aggregate relation that allow cascade on update).
     *
     * @param e Observation to update
     * @return the updated obervsation
     * @throws TopiaException for any errors
     */
    @Override
    public Observation update(Observation e) throws TopiaException {

        Species species = e.getSpecies();

        // Only if species is not persisted and contains a code
        if (species != null
            && !Strings.isNullOrEmpty(species.getCode())
            && species.getTopiaId() == null) {

            SpeciesTopiaDao speciesDAO = topiaDaoSupplier.getDao(Species.class, SpeciesTopiaDao.class);

            Optional<Species> existSpecies = speciesDAO.forNaturalId(species.getCode(), species.getRegion()).tryFindUnique();
            if (existSpecies.isPresent()) {

                if (log.isDebugEnabled()) {
                    log.debug("Use existing species '" + existSpecies.get().getCode() + "'");
                }

                e.setSpecies(existSpecies.get());

            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Create a new species '" + species.getCode() + "'");
                }

                species.setLocalCreation(true);
                speciesDAO.create(species);

            }
        }

        return super.update(e);
    }

    public List<Observation> findAllByFlightOrderedByObservationTime(Flight flight) {

        List<Observation> observations = forFlightEquals(flight).setOrderByArguments(Observation.PROPERTY_OBSERVATION_TIME, Observation.PROPERTY_TOPIA_CREATE_DATE).findAll();
        return observations;
    }

    public int findLastObservationNumber(Flight flight) {

        String hql = "SELECT max(" + Observation.PROPERTY_OBSERVATION_NUMBER + ") " +
                    newFromClause() + " " +
                    "WHERE " + Observation.PROPERTY_FLIGHT + " = :flight";

        Integer queryResult = findAnyOrNull(hql, ImmutableMap.of("flight", (Object)flight));

        int result = queryResult == null ? 1 : queryResult + 1;

        return result;
    }

} //ObservationDAOImpl<Observation extends Observation>
