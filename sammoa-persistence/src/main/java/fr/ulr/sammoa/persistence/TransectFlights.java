package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class TransectFlights {

    private TransectFlights() {
        // static class do not have instanciation
    }

    public static Set<ObserverPosition> toObserverPositions(Iterable<TransectFlight> transectFlights) {
        Set<ObserverPosition> result = Sets.newHashSet();
        for (TransectFlight transectFlight : transectFlights) {
            if (transectFlight != null) {
                result.addAll(transectFlight.getObserverPosition());
            }
        }
        return result;
    }

    public static boolean isValid(TransectFlight transectFlight,
                                  Iterable<Route> routes,
                                  Function<Route, Boolean> isRouteValid) {

        boolean result = transectFlight.isValid() || transectFlight.isDeleted();

        if (!result) {

            // Check validity of routes
            result = FluentIterable
                    .from(routes)
                    .filter(Routes.withTransectFlight(transectFlight))
                    .transform(isRouteValid)
                    .allMatch(Predicates.equalTo(true));
        }
        return result;
    }

    public static Predicate<TransectFlight> isDeleted() {
        return IS_DELETED_PREDICATE;
    }

    public static Function<TransectFlight, Transect> toTransect() {
        return TO_TRANSECT_FUNCTION;
    }

    private static Function<TransectFlight, Transect> TO_TRANSECT_FUNCTION =
            new Function<TransectFlight, Transect>() {

                @Override
                public Transect apply(TransectFlight input) {
                    return input.getTransect();
                }
            };

    private static Predicate<TransectFlight> IS_DELETED_PREDICATE =
            new Predicate<TransectFlight>() {

                @Override
                public boolean apply(TransectFlight input) {
                    return input.isDeleted();
                }
            };
}
