/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;

/**
 * Created: 13/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class FlightImpl extends FlightAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public List<TransectFlight> getTransectFlight() {
        if (transectFlight == null) {
            transectFlight = Lists.newArrayList();
        }
        return transectFlight;
    }

    @Override
    public TransectFlight getTransectFlightByTransect(Transect transect) {
        TransectFlight result = null;
        for (TransectFlight element : getTransectFlight()) {
            if (transect.equals(element.getTransect())) {
                result = element;
                break;
            }
        }
        return result;
    }

    @Override
    public Collection<Observer> getObserver() {
        if (observer == null) {
            observer = Lists.newArrayList();
        }
        return observer;
    }

    @Override
    public void removeObserver(Observer observer) {
        super.removeObserver(observer);

        for (TransectFlight transectFlight : getTransectFlight()) {
            ObserverPosition observerPosition = transectFlight.getObserverPositionByObserver(observer);
            if (observerPosition != null) {
                observerPosition.setObserver(null);
            }
        }
    }
}
