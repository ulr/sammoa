package fr.ulr.sammoa.persistence;

/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.n;

/**
 * Created: 29/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public enum Position {

    NAVIGATOR(n("sammoa.position.navigator"), Side.CENTER),

    FRONT_LEFT(n("sammoa.position.frontLeft"), Side.LEFT),

    FRONT_RIGHT(n("sammoa.position.frontRight"), Side.RIGHT),

    BACK_LEFT(n("sammoa.position.backLeft"), Side.LEFT),

    BACK_RIGHT(n("sammoa.position.backRight"), Side.RIGHT),

    CO_NAVIGATOR(n("sammoa.position.coNavigator"), Side.CENTER);

    private String label;

    private Side side;

    Position(String label, Side side) {
        this.label = label;
        this.side = side;
    }

    public String getLabel() {
        return label;
    }

    public Side getSide() {
        return side;
    }
}
