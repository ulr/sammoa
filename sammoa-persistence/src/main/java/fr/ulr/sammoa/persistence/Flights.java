package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Ordering;
import org.nuiton.validator.bean.list.BeanListValidator;

import java.util.Comparator;

/**
 * Created: 19/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class Flights {

    private Flights() {
        // static class do not have instanciation
    }

    public static boolean withValidFlag(Flight flight,
                                        BeanListValidator<Route> routeValidator) {

        // Check validity of all transectFlights
        boolean result = FluentIterable
                .from(flight.getTransectFlight())
                .allMatch(Validables.isValid(null));

        // Check validity of all other routes with no transectFlight
        if (result) {

            result = FluentIterable
                    .from(routeValidator.getBeans())
                    .filter(Routes.withNoTransectFlight())
                    .allMatch(Validables.isValid(routeValidator));
        }
        return result;
    }

    public static boolean isValid(Flight flight,
                                  Iterable<Route> routes,
                                  Function<TransectFlight, Boolean> isValidTransectFlight,
                                  Function<Route, Boolean> isValidRoute) {

        // Check validity of all transectFlights
        boolean result = FluentIterable
                .from(flight.getTransectFlight())
                .transform(isValidTransectFlight)
                .allMatch(Predicates.equalTo(true));

        // Check validity of all other routes with no transectFlight
        if (result) {

            result = FluentIterable
                    .from(routes)
                    .filter(Routes.withNoTransectFlight())
                    .transform(isValidRoute)
                    .allMatch(Predicates.equalTo(true));
        }
        return result;
    }

    public static Comparator<Flight> orderByDate() {
        return ORDER_BY_DATE_COMPARATOR;
    }

    public static Function<Flight, String> toSystemId() {
        return TO_SYSTEM_ID_FUNCTION;
    }

    public static Function<Flight, String> toFlightNumber() {
        return TO_FLIGHT_NUMBER_FUNCTION;
    }

    public static Predicate<Flight> withSystemId(String systemId) {
        return new FlightWithSystemIdPredicate(systemId);
    }

    protected static Function<Flight, String> TO_SYSTEM_ID_FUNCTION =
            new Function<Flight, String>() {

                @Override
                public String apply(Flight input) {
                    return input.getSystemId();
                }
            };

    protected static Function<Flight, String> TO_FLIGHT_NUMBER_FUNCTION =
            new Function<Flight, String>() {

                @Override
                public String apply(Flight input) {
                    return input != null ? String.valueOf(input.getFlightNumber()) : "undefined";
                }
            };

    protected static class FlightWithSystemIdPredicate implements Predicate<Flight> {

        protected String systemId;

        public FlightWithSystemIdPredicate(String systemId) {
            this.systemId = systemId;
        }

        @Override
        public boolean apply(Flight input) {
            return input == null || systemId.equals(input.getSystemId());
        }
    }

    protected static Comparator<Flight> ORDER_BY_DATE_COMPARATOR =
            new Comparator<Flight>() {

                @Override
                public int compare(Flight flight1, Flight flight2) {
                    int result = ComparisonChain
                            .start()
                                    // on beginDate, first null
                            .compare(
                                    flight1.getBeginDate(),
                                    flight2.getBeginDate(),
                                    Ordering.natural().reverse().nullsFirst()
                            )
                                    // on endDate null
                            .compareTrueFirst(
                                    flight1.getEndDate() == null,
                                    flight2.getEndDate() == null
                            )
                                    // on flightNumber
                            .compare(
                                    flight1.getFlightNumber(),
                                    flight2.getFlightNumber(),
                                    Ordering.natural().reverse()
                            )
                            .result();
                    return result;
                }
            };
}
