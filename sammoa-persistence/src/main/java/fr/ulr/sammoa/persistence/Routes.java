package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.nuiton.validator.bean.list.BeanListValidator;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created: 16/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class Routes {

    private Routes() {
        // static class do not have instanciation
    }

    public static Iterable<Route> filterWithNoTransectFlight(Iterable<Route> routes) {
        return Iterables.filter(routes, WITH_NO_TRANSECT_FLIGHT_PREDICATE);
    }

    public static boolean isAnyMatchTransectFlight(Iterable<Route> routes, TransectFlight transectFlight) {
        return Iterables.any(routes, withTransectFlight(transectFlight));
    }

    public static boolean isRouteLeg(Route route) {
        return route != null && RouteType.LEG == route.getRouteType();
    }

    public static boolean isRouteCircleBack(Route route) {
        return route != null && RouteType.CIRCLE_BACK == route.getRouteType();
    }

    public static Iterable<Date> toDates(Iterable<Route> routes) {
        return Iterables.transform(routes, TO_DATE_FUNCTION);
    }

    public static Function<Route, Date> toDate() {
        return TO_DATE_FUNCTION;
    }

    public static Set<ObserverPosition> toObserverPositions(Iterable<Route> routes) {
        Set<ObserverPosition> result = Sets.newHashSet();
        for (Route route : routes) {
            result.addAll(route.getObserverPosition());
        }
        return result;
    }

    public static boolean isAccepted(Route route,
                                     List<RouteType> routeTypes,
                                     List<Strate> strates) {
        boolean result = false;

        if (CollectionUtils.isEmpty(routeTypes) ||
            routeTypes.contains(route.getRouteType())) {

            TransectFlight transectFlight = route.getTransectFlight();

            if (CollectionUtils.isEmpty(strates) ||
                transectFlight == null ||
                strates.contains(transectFlight.getTransect().getStrate())) {
                result = true;
            }
        }

        return result;
    }

    public static boolean isValid(Route route,
                                  BeanListValidator<Route> routeValidator,
                                  Iterable<Observation> observations,
                                  Function<Observation, Boolean> isValidObservation) {

        boolean result;
        if (route.isDeleted()) {
            result = true;

        } else {

            Set<Route> routes = routeValidator.getBeans();

            result = Validables.isValid(route, routeValidator)
                     && !Routes.equal(route, Routes.findPrevious(routes, route));

            if (result) {

                // Check validity of observations
                result = FluentIterable
                        .from(observations)
                        .filter(Observations.inRoute(route, routes, true))
                        .transform(isValidObservation)
                        .allMatch(Predicates.equalTo(true));
            }
        }
        return result;
    }

    public static boolean equal(Route o1, Route o2) {

        if (o1 == null && o2 != null) {
            return false;
        }
        if (o2 == null && o1 != null) {
            return false;
        }
        if (o1 == null) {
            return true;
        }

        boolean result = Objects.equal(o1.getRouteType(), o2.getRouteType());

        // We assume that time equality for a same routeType is an equivalence
        if (result && Objects.equal(o1.getBeginTime(), o2.getBeginTime())) {
            result = true;

        } else {

            result &= Objects.equal(o1.getTransectFlight(), o2.getTransectFlight());
            result &= Objects.equal(o1.getCircleBackCause(), o2.getCircleBackCause());

            result &= Objects.equal(o1.isDeleted(), o2.isDeleted());

            result &= Objects.equal(o1.getSeaState(), o2.getSeaState());
            result &= Objects.equal(o1.getSwell(), o2.getSwell());
            result &= Objects.equal(o1.getTurbidity(), o2.getTurbidity());
            result &= Objects.equal(o1.getCloudCover(), o2.getCloudCover());
            result &= Objects.equal(o1.getSkyGlint(), o2.getSkyGlint());
            result &= Objects.equal(o1.getGlareFrom(), o2.getGlareFrom());
            result &= Objects.equal(o1.getGlareTo(), o2.getGlareTo());
            result &= Objects.equal(o1.isGlareUnder(), o2.isGlareUnder());
            result &= Objects.equal(o1.getGlareSeverity(), o2.getGlareSeverity());
            result &= Objects.equal(o1.getSubjectiveConditions(), o2.getSubjectiveConditions());

            result &= o1.getObserverPosition().size() == o2.getObserverPosition().size();

            if (result) {

                for (ObserverPosition position1 : o1.getObserverPosition()) {
                    ObserverPosition position2 =
                            o2.getObserverPositionByPosition(position1.getPosition());

                    result &= Objects.equal(position1.getObserver(), position2.getObserver());
                }
            }
        }
        return result;
    }

    public static String toString(Route route) {
        String result;
        if (route != null) {
            result = new ToStringBuilder(route)
                    .append(Route.PROPERTY_BEGIN_TIME, route.getBeginTime())
                    .append(Route.PROPERTY_ROUTE_TYPE, route.getRouteType())
                    .append(Route.PROPERTY_EFFORT_NUMBER, route.getEffortNumber())
                    .append(Route.PROPERTY_DELETED, route.isDeleted())
                    .append(Route.PROPERTY_VALID, route.isValid())
                    .build();

        } else {
            result = "null";
        }
        return result;
    }

    public static Iterable<Route> filterNotDeleted(Iterable<Route> routes) {
        return Iterables.filter(routes, Predicates.not(Validables.isDeleted()));
    }

    public static Route findPrevious(Iterable<Route> routes, Route route) {
        return getPrevious(routes, route, orderByDate());
    }

    public static Route findNext(Iterable<Route> routes, Route route) {
        return getNext(routes, route, orderByDate());
    }

    public static Route findWithObservation(Iterable<Route> routes,
                                            Observation observation,
                                            boolean ignoreDeleted) {

        return Iterables.tryFind(routes, withObservation(routes, observation, ignoreDeleted)).orNull();
    }

    public static boolean withTransectFlight(Route route, TransectFlight transectFlight) {
        return transectFlight.equals(route.getTransectFlight());
    }

    public static Predicate<Route> withTransectFlight(TransectFlight transectFlight) {
        return new WithTransectFlightPredicate(transectFlight);
    }

    public static Predicate<Route> withNoTransectFlight() {
        return WITH_NO_TRANSECT_FLIGHT_PREDICATE;
    }
//
//    public static Predicate<Route> isValid(BeanListValidator<Route> validator,
//                                           BeanListValidator<Observation> obsValidator) {
//        return new IsValidPredicate(validator, obsValidator);
//    }

    public static Comparator<Route> orderByDate() {
        return BY_DATE_COMPARATOR;
    }

    public static Predicate<Route> withObservation(Iterable<Route> routes,
                                                   Observation observation,
                                                   boolean ignoreDeleted) {
        return new WithObservationPredicate(observation, routes, ignoreDeleted);
    }

    private static Comparator<Route> BY_DATE_COMPARATOR = new Comparator<Route>() {

        @Override
        public int compare(Route o1, Route o2) {
            int result = ComparisonChain
                    .start()
                    .compare(o1.getBeginTime(), o2.getBeginTime())
                    .compare(o1.getTopiaCreateDate(), o2.getTopiaCreateDate())
                    .result();
            return result;
        }
    };

    private static Function<Route, Date> TO_DATE_FUNCTION = new Function<Route, Date>() {

        @Override
        public Date apply(Route input) {
            return input.getBeginTime();
        }
    };

    private static Predicate<Route> WITH_NO_TRANSECT_FLIGHT_PREDICATE = new Predicate<Route>() {

        @Override
        public boolean apply(Route input) {
            return input.getTransectFlight() == null;
        }
    };

    private static class WithObservationPredicate implements Predicate<Route> {

        protected Observation observation;

        protected Iterable<Route> routes;

        protected boolean ignoreDeleted;

        public WithObservationPredicate(Observation observation,
                                        Iterable<Route> routes,
                                        boolean ignoreDeleted) {
            Preconditions.checkNotNull(observation);
            Preconditions.checkNotNull(routes);
            this.observation = observation;
            this.routes = routes;
            this.ignoreDeleted = ignoreDeleted;
        }

        @Override
        public boolean apply(Route input) {
            return Observations.inRoute(observation, input, routes, ignoreDeleted);
        }
    }

    private static class WithTransectFlightPredicate implements Predicate<Route> {

        protected TransectFlight transectFlight;

        public WithTransectFlightPredicate(TransectFlight transectFlight) {
            Preconditions.checkNotNull(transectFlight);
            this.transectFlight = transectFlight;
        }

        @Override
        public boolean apply(Route input) {
            return withTransectFlight(input, transectFlight);
        }
    }
//
//    private static class IsValidPredicate implements Predicate<Route> {
//
//        protected BeanListValidator<Route> validator;
//
//        protected BeanListValidator<Observation> obsValidator;
//
//        private IsValidPredicate(BeanListValidator<Route> validator,
//                                 BeanListValidator<Observation> obsValidator) {
//            Preconditions.checkNotNull(validator);
//            Preconditions.checkNotNull(obsValidator);
//            this.validator = validator;
//            this.obsValidator = obsValidator;
//        }
//
//        @Override
//        public boolean apply(Route input) {
//            return isValid(input, validator, obsValidator);
//        }
//    }

    private static <T> T getNext(Iterable<T> elements,
                                 final T element,
                                 final Comparator<T> comparator) {
        Iterable<T> nextElements =
                Iterables.filter(elements, new Predicate<T>() {

                    @Override
                    public boolean apply(T input) {
                        return comparator.compare(input, element) > 0;
                    }
                });

        T result;
        if (Iterables.isEmpty(nextElements)) {
            result = null;

        } else {
            result = Ordering.from(comparator).min(nextElements);
        }
//
//        int index = Iterables.indexOf(elements, Predicates.equalTo(element));
//
//        T result;
//        if (index < Iterables.size(elements)  - 1) {
//            List<T> ordered = Ordering.from(comparator).sortedCopy(elements);
//            result = ordered.get(index + 1);
//
//        } else {
//            result = null;
//        }
        return result;
    }

    private static <T> T getPrevious(Iterable<T> elements,
                                     final T element,
                                     final Comparator<T> comparator) {
        Iterable<T> previousElements =
                Iterables.filter(elements, new Predicate<T>() {

                    @Override
                    public boolean apply(T input) {
                        return comparator.compare(input, element) < 0;
                    }
                });

        T result;
        if (Iterables.isEmpty(previousElements)) {
            result = null;

        } else {
            result = Ordering.from(comparator).max(previousElements);
        }
//
//        int index = Iterables.indexOf(elements, Predicates.equalTo(element));
//
//        T result;
//        if (index > 0) {
//            List<T> ordered = Ordering.from(comparator).sortedCopy(elements);
//            result = ordered.get(index - 1);
//
//        } else {
//            result = null;
//        }
        return result;
    }

}
