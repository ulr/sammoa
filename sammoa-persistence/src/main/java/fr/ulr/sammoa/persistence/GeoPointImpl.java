/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created: 01/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class GeoPointImpl extends GeoPointAbstract {

    private static final long serialVersionUID = 1L;

    public GeoPointImpl() {
        // default constructor
    }

    public GeoPointImpl(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(PROPERTY_LATITUDE, latitude)
                .append(PROPERTY_LONGITUDE, longitude)
                .append(PROPERTY_ALTITUDE, altitude)
                .append(PROPERTY_SPEED, speed)
                .append(PROPERTY_RECORD_TIME, recordTime)
                .append(PROPERTY_CAPTURE_DELAY, captureDelay)
                .build();
    }
}
