/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Date;
import java.util.List;

/**
 * Created: 15/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class RouteTopiaDao extends AbstractRouteTopiaDao<Route> {

    public List<Route> findAllByFlightOrderedByBeginTime(Flight flight) {

        List<Route> routes = forFlightEquals(flight).setOrderByArguments(Route.PROPERTY_BEGIN_TIME, Route.PROPERTY_TOPIA_CREATE_DATE).findAll();

        return routes;
    }

    public Route findLastByFlight(Flight flight) {

        Optional<Route> routeOptional = forFlightEquals(flight)
                .addEquals(Route.PROPERTY_DELETED, false)
                .setOrderByArguments(Route.PROPERTY_BEGIN_TIME + " DESC", Route.PROPERTY_TOPIA_CREATE_DATE + " DESC")
                .tryFindFirst();

        Route route = null;

        if (routeOptional.isPresent()) {

            route = routeOptional.get();

        }

        return  route;
    }

    public Route findPreviousLEG(Route route) throws TopiaException {

        route = forTopiaIdEquals(route.getTopiaId()).findUnique();

        String hql = newFromClause() + " " +
                     "WHERE " + Route.PROPERTY_FLIGHT + " = :flight " +
                     "AND " + Route.PROPERTY_ROUTE_TYPE + " = :type " +
                     "AND " + Route.PROPERTY_BEGIN_TIME + " <= :date " +
                     "AND " + Route.PROPERTY_DELETED + " = :deleted " +
                     "ORDER BY " + Route.PROPERTY_BEGIN_TIME + " DESC, " + Route.PROPERTY_TOPIA_CREATE_DATE + " DESC";

        ImmutableMap<String, Object> parameters = ImmutableMap.of(
                "flight", (Object) route.getFlight(),
                "type", RouteType.LEG,
                "date", route.getBeginTime(),
                "deleted", false);

        Route result = findFirstOrNull(hql, parameters );

        return result;
    }

    public Route findPreviousRoute(Route route) throws TopiaException {

        route = forTopiaIdEquals(route.getTopiaId()).findUnique();

        String hql = newFromClause() + " " +
                "WHERE " + Route.PROPERTY_FLIGHT + " = :flight " +
                "AND " + Route.PROPERTY_BEGIN_TIME + " <= :date " +
                "AND " + Route.PROPERTY_DELETED + " = :deleted " +
                "ORDER BY " + Route.PROPERTY_BEGIN_TIME + " DESC, " + Route.PROPERTY_TOPIA_CREATE_DATE + " DESC";

        ImmutableMap<String, Object> parameters = ImmutableMap.of(
                "flight", (Object) route.getFlight(),
                "date", route.getBeginTime(),
                "deleted", false);

        Route result = findFirstOrNull(hql, parameters );

        return result;

    }

    public int getLastEffortNumber(Flight flight) {

        String hql = "SELECT max(" + Route.PROPERTY_EFFORT_NUMBER + ") " +
                     newFromClause() + " " +
                     "WHERE " + Route.PROPERTY_EFFORT_NUMBER + " IS NOT NULL " +
                     "AND " + Route.PROPERTY_FLIGHT + " = :flight ";

        ImmutableMap<String, Object> parameters = ImmutableMap.of(
                "flight", (Object) flight);

        Integer queryResult = findAnyOrNull(hql, parameters );

        int result = queryResult == null ? 1 : queryResult + 1;

        return result;

    }

    public long countByTransectFlightBeforeTime(TransectFlight transectFlight,
                                                Date time) {

        String hql = "SELECT count(*) " +
                     newFromClause() + " " +
                     "WHERE " + Route.PROPERTY_TRANSECT_FLIGHT + " = :transectFlight " +
                     "AND " + Route.PROPERTY_BEGIN_TIME + " <= :date ";

        ImmutableMap<String, Object> parameters = ImmutableMap.of(
                "transectFlight", (Object) transectFlight,
                "date", time);

        long count = count(hql, parameters);

        return count;

    }

}
