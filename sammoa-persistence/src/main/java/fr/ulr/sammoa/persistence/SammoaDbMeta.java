package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.DbMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import java.util.Arrays;
import java.util.List;

/**
 * All db metas for the sammoa persistence unit.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SammoaDbMeta extends DbMeta<SammoaEntityEnum> {

    private static final List<SammoaEntityEnum> REFERENTIAL_TYPES = ImmutableList.copyOf(Arrays.asList(
            SammoaEntityEnum.Region,
            SammoaEntityEnum.Species,
            SammoaEntityEnum.Survey,
            SammoaEntityEnum.Observer,
            SammoaEntityEnum.SubRegion,
            SammoaEntityEnum.Strate,
            SammoaEntityEnum.Transect
    ));

    private static final List<SammoaEntityEnum> DATA_TYPES = ImmutableList.copyOf(Arrays.asList(
            SammoaEntityEnum.Flight,
            SammoaEntityEnum.GeoPoint,
            SammoaEntityEnum.ObserverPosition,
            SammoaEntityEnum.TransectFlight,
            SammoaEntityEnum.Observation,
            SammoaEntityEnum.Route
    ));

    private final List<TableMeta<SammoaEntityEnum>> referentialTables;

    private final List<AssociationMeta<SammoaEntityEnum>> referentialAssociations;

    private final List<TableMeta<SammoaEntityEnum>> dataTables;

    private final List<AssociationMeta<SammoaEntityEnum>> dataAssociations;

    public static SammoaDbMeta newDbMeta(TopiaEntityEnumProvider<SammoaEntityEnum> persistenceHelper) {
        return new SammoaDbMeta(persistenceHelper);
    }

    SammoaDbMeta(TopiaEntityEnumProvider<SammoaEntityEnum> persistenceHelper) {
        super(persistenceHelper, SammoaEntityEnum.values());

        {
            // referential tables

            List<TableMeta<SammoaEntityEnum>> result = Lists.newArrayList();
            addTables(result, REFERENTIAL_TYPES);
            referentialTables = ImmutableList.copyOf(result);
        }

        {
            // referential associations

            List<AssociationMeta<SammoaEntityEnum>> result = Lists.newArrayList();
            addAssociations(result, REFERENTIAL_TYPES);
            referentialAssociations = ImmutableList.copyOf(result);
        }

        {
            // data tables

            List<TableMeta<SammoaEntityEnum>> result = Lists.newArrayList();
            addTables(result, DATA_TYPES);
            dataTables = ImmutableList.copyOf(result);
        }

        {
            // data associations

            List<AssociationMeta<SammoaEntityEnum>> result = Lists.newArrayList();
            addAssociations(result, DATA_TYPES);
            dataAssociations = ImmutableList.copyOf(result);
        }
    }

    public boolean isReferential(SammoaEntityEnum type) {
        return REFERENTIAL_TYPES.contains(type);
    }

    public List<TableMeta<SammoaEntityEnum>> getReferentialTables() {
        return referentialTables;
    }

    public List<AssociationMeta<SammoaEntityEnum>> getReferentialAssociations() {
        return referentialAssociations;
    }

    public List<TableMeta<SammoaEntityEnum>> getDataTables() {
        return dataTables;
    }

    public List<AssociationMeta<SammoaEntityEnum>> getDataAssociations() {
        return dataAssociations;
    }

}
