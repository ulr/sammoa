package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import java.util.Comparator;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class Observers {

    private Observers() {
        // static class do not have instanciation
    }

    public static Predicate<Observer> isNotPilot() {
        return Predicates.not(isPilot());
    }

    public static Predicate<Observer> isPilot() {
        return IS_PILOT_PREDICATE;
    }

    public static Comparator<Observer> onInitials() {
        return ON_INITIALS_COMPARATOR;
    }

    protected static Predicate<Observer> IS_PILOT_PREDICATE =
            new Predicate<Observer>() {

                @Override
                public boolean apply(Observer input) {
                    return input.isPilot();
                }
            };

    protected static Comparator<Observer> ON_INITIALS_COMPARATOR =
            new Comparator<Observer>() {

                @Override
                public int compare(Observer o1, Observer o2) {
                    return o1.getInitials().compareTo(o2.getInitials());
                }
            };
}
