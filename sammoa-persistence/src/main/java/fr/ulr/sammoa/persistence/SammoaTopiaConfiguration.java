package fr.ulr.sammoa.persistence;

/*-
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaService;

import java.sql.Driver;
import java.util.Map;
import java.util.function.Consumer;

public class SammoaTopiaConfiguration implements TopiaConfiguration {

    protected final TopiaConfiguration delegate;

    protected final Consumer<String> migrationCallBack;

    public SammoaTopiaConfiguration(TopiaConfiguration delegate, Consumer<String> migrationCallBack) {
        this.delegate = delegate;
        this.migrationCallBack = migrationCallBack;
    }

    public Consumer<String> getMigrationCallBack() {
        return migrationCallBack;
    }

    @Override
    public boolean isInitSchema() {
        return delegate.isInitSchema();
    }

    @Override
    public boolean isValidateSchema() {
        return delegate.isValidateSchema();
    }

    @Override
    public TopiaIdFactory getTopiaIdFactory() {
        return delegate.getTopiaIdFactory();
    }

    @Override
    public String getSchemaName() {
        return delegate.getSchemaName();
    }

    @Override
    public Map<String, String> getHibernateExtraConfiguration() {
        return delegate.getHibernateExtraConfiguration();
    }

    @Override
    public Map<String, Class<? extends TopiaService>> getDeclaredServices() {
        return delegate.getDeclaredServices();
    }

    @Override
    public Map<String, Map<String, String>> getDeclaredServicesConfiguration() {
        return delegate.getDeclaredServicesConfiguration();
    }

    @Override
    public boolean isUseHikariForJdbcConnectionPooling() {
        return delegate.isUseHikariForJdbcConnectionPooling();
    }

    @Override
    public String getJdbcConnectionUrl() {
        return delegate.getJdbcConnectionUrl();
    }

    @Override
    public String getJdbcConnectionUser() {
        return delegate.getJdbcConnectionUser();
    }

    @Override
    public String getJdbcConnectionPassword() {
        return delegate.getJdbcConnectionPassword();
    }

    @Override
    public Class<? extends Driver> getJdbcDriverClass() {
        return delegate.getJdbcDriverClass();
    }
}
