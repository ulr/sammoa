package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.Interval;
import org.nuiton.validator.bean.list.BeanListValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created: 16/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class Observations {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Observations.class);

    private Observations() {
        // static class do not have instanciation   
    }

    public static Iterable<Date> toDates(Iterable<Observation> observations) {
        return Iterables.transform(observations, toDate());
    }

    public static String toLabel(Observation observation) {
        Species species = observation.getSpecies();
        return species != null ? species.getCode() : "";
    }

    public static GeoPoint toGeoPoint(Observation observation,
                                      Iterable<GeoPoint> geoPoints) {
        Date date = observation.getObservationTime();
        return Iterables.find(geoPoints, GeoPoints.withDate(date), GeoPoints.newEmptyGeoPoint(date));
    }

    public static Iterable<Observation> filterInRoute(Iterable<Observation> observations,
                                                      Route route,
                                                      Iterable<Route> routes,
                                                      boolean ignoreDeleted) {
        return Iterables.filter(observations, inRoute(route, routes, ignoreDeleted));
    }

    public static boolean isAnyDeletedFromRoute(Iterable<Observation> observations,
                                                Route route,
                                                Iterable<Route> routes) {
        boolean result = FluentIterable
                .from(observations)
                .filter(Observations.inRoute(route, routes, false))
                .anyMatch(Validables.isDeleted());
        return result;
    }

    public static boolean isValid(Observation observation,
                                  BeanListValidator<Observation> validator) {
        return Validables.isValid(observation, validator);
    }

    public static boolean inRoute(Observation observation,
                                  Route route,
                                  Iterable<Route> routes,
                                  boolean ignoreDeleted) {

        if (ignoreDeleted) {
            routes = Routes.filterNotDeleted(routes);
        }

        Route previousRoute = route;
        if (ignoreDeleted && route.isDeleted()) {
            previousRoute = Routes.findPrevious(routes, route);
        }
        if (previousRoute == null) {
            previousRoute = Routes.findNext(routes, route);
        }

        Preconditions.checkNotNull(previousRoute);

        Route nextRoute = Routes.findNext(routes, previousRoute);

        if (log.isTraceEnabled()) {
            log.trace("inRoute obs " + observation.getObservationNumber()
                    + " [" + observation.getObservationTime()
                    + "]: previousRoute:[" + Routes.toString(previousRoute)
                    + "], nextRoute:[" + Routes.toString(nextRoute) + "]");
        }

        Date begin = previousRoute.getBeginTime();
        Date end = nextRoute != null ? nextRoute.getBeginTime() : null;

        return Dates.inInterval(observation.getObservationTime(), begin, end);
    }

    public static Predicate<Observation> inRoute(Route route,
                                                 Iterable<Route> routes,
                                                 boolean ignoreDeleted) {
        return new InRoutePredicate(route, routes, ignoreDeleted);
    }

    public static void removeOtherSpecies(List<Observation> observations,
                                          List<Species> species) {
        if (CollectionUtils.isNotEmpty(species)) {

            // filter by species
            Iterator<Observation> itr = observations.iterator();
            while (itr.hasNext()) {
                Observation observation = itr.next();
                if (!species.contains(observation.getSpecies())) {
                    itr.remove();
                }
            }
        }
    }

    public static List<Observation> retainsObservations(List<Observation> observations,
                                                        Interval interval) {

//        List<Observation> result = Lists.newArrayList();
//        Iterator<Observation> itr = observations.iterator();
//        if (periodDate.getThruDate() == null) {
//            Date fromDate = periodDate.getFromDate();
//            // no up bound
//            while (itr.hasNext()) {
//                Observation observation = itr.next();
//                Date observationTime = observation.getObservationTime();
//                if (fromDate.before(observationTime) || fromDate.equals(observationTime)) {
//                    itr.remove();
//                    result.add(observation);
//                }
//            }
//        } else {
//            while (itr.hasNext()) {
//                Observation observation = itr.next();
//                Date observationTime = observation.getObservationTime();
//                if (periodDate.between(observationTime) && !periodDate.getThruDate().equals(observationTime)) {
//                    itr.remove();
//                    result.add(observation);
//                }
//            }
//        }

        List<Observation> result = FluentIterable
                .from(observations)
                .filter(inDateInterval(interval))
                .toList();

        Iterables.removeAll(observations, result);

        return result;
    }

    public static Function<Observation, GeoPoint> toGeoPoint(final Iterable<GeoPoint> geoPoints) {
        return new ToGeoPointFunction(geoPoints);
    }

    public static Function<Observation, Date> toDate() {
        return TO_DATE_FUNCTION;
    }

    public static Function<Observation, String> toLabel() {
        return TO_LABEL_FUNCTION;
    }

    public static Predicate<Observation> inDateInterval(Interval interval) {
        return new InDateIntervalPredicate(interval);
    }

    public static Predicate<Observation> isValid(BeanListValidator<Observation> validator) {
        return new IsValidPredicate(validator);
    }

    private static Function<Observation, Date> TO_DATE_FUNCTION = new Function<Observation, Date>() {

        @Override
        public Date apply(Observation input) {
            return input.getObservationTime();
        }
    };

    private static Function<Observation, String> TO_LABEL_FUNCTION = new Function<Observation, String>() {

        @Override
        public String apply(Observation input) {
            return toLabel(input);
        }
    };

    private static class ToGeoPointFunction implements Function<Observation, GeoPoint> {

        protected Iterable<GeoPoint> geoPoints;

        public ToGeoPointFunction(Iterable<GeoPoint> geoPoints) {
            this.geoPoints = geoPoints;
        }

        @Override
        public GeoPoint apply(Observation input) {
            return toGeoPoint(input, geoPoints);
        }
    }

    private static class InRoutePredicate implements Predicate<Observation> {

        protected Date begin;

        protected Date end;

        public InRoutePredicate(Route route,
                                Iterable<Route> routes,
                                boolean ignoreDeleted) {
            Preconditions.checkNotNull(route);
            Preconditions.checkNotNull(routes);

            if (ignoreDeleted) {
                routes = Routes.filterNotDeleted(routes);
            }

            Route previousRoute = route;
            if (ignoreDeleted && route.isDeleted()) {
                previousRoute = Routes.findPrevious(routes, route);
            }
            if (previousRoute == null) {
                previousRoute = Routes.findNext(routes, route);
            }

            Preconditions.checkNotNull(previousRoute);

            Route nextRoute = Routes.findNext(routes, previousRoute);

            begin = previousRoute.getBeginTime();
            end = nextRoute != null ? nextRoute.getBeginTime() : null;

        }

        @Override
        public boolean apply(Observation input) {
            return Dates.inInterval(input.getObservationTime(), begin, end);
        }
    }

    private static class InDateIntervalPredicate implements Predicate<Observation> {

        protected Interval interval;

        public InDateIntervalPredicate(Interval interval) {
            Preconditions.checkNotNull(interval);
            this.interval = interval;
        }

        @Override
        public boolean apply(Observation input) {
            return Dates.inInterval(input.getObservationTime(), interval);
        }
    }

    private static class IsValidPredicate implements Predicate<Observation> {

        protected BeanListValidator<Observation> validator;

        public IsValidPredicate(BeanListValidator<Observation> validator) {
            Preconditions.checkNotNull(validator);
            this.validator = validator;
        }

        @Override
        public boolean apply(Observation input) {
            return isValid(input, validator);
        }
    }

}
