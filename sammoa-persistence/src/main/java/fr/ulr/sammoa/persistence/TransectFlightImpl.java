/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.Collection;

/**
 * Created: 13/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 */
public class TransectFlightImpl extends TransectFlightAbstract {

    private static final long serialVersionUID = 1L;

    private transient int indexInFlight;

    @Override
    public int getIndexInFlight() {
        return indexInFlight;
    }

    @Override
    public void setIndexInFlight(int indexInFlight) {
        this.indexInFlight = indexInFlight;
    }

    @Override
    public Collection<ObserverPosition> getObserverPosition() {
        if (observerPosition == null) {
            observerPosition = Lists.newArrayList();
        }
        return observerPosition;
    }

    @Override
    public ObserverPosition getObserverPositionByPosition(Position position) {
        ObserverPosition result =
                Iterables.find(getObserverPosition(), ObserverPositions.withPosition(position), null);
        return result;
    }

    @Override
    public ObserverPosition getObserverPositionByObserver(Observer observer) {
        ObserverPosition result =
                Iterables.find(getObserverPosition(), ObserverPositions.withObserver(observer), null);
        return result;
    }

    @Override
    public void setObserverByPosition(Position position, Observer observer) {
        ObserverPosition observerPositionToUpdate =
                getObserverPositionByPosition(position);

        // keep old observer to change with existing
        Observer oldObserver = observerPositionToUpdate.getObserver();

        ObserverPosition observerPosition = getObserverPositionByObserver(observer);
        if (observerPosition != null) {
            observerPosition.setObserver(oldObserver);
        }
        observerPositionToUpdate.setObserver(observer);
    }

}
