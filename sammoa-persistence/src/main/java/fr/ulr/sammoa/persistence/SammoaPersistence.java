/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import com.google.common.base.Preconditions;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.Closeable;
import java.util.function.Consumer;

/**
 * Persistence context for SAMMOA. This class must be used for all transaction
 * management. An {@link AutoSaveListener} will be used to commit all entities
 * changed every {@code autoCommitDelay} defined in constructor
 * {@link #open(TopiaConfiguration, long, Consumer<String>)}.
 * <p/>
 * You will create a transaction with {@link #beginTransaction()} and on finally
 * block call {@link #endTransaction(SammoaTopiaPersistenceContext)}.
 * <p/>
 * Created: 08/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class SammoaPersistence implements Closeable {

    protected static SammoaPersistence instance;

    protected SammoaTopiaApplicationContext rootContext;

    protected SammoaTopiaConfiguration dbConfiguration;

    protected AutoSaveListener autoSaveListener;

    protected SammoaPersistenceHelper persistenceHelper;

    protected SammoaDbMeta dbMetas;

    /**
     * Open the Persistence with {@code dbConfiguration} for
     * {@link SammoaTopiaApplicationContext#SammoaTopiaApplicationContext(java.util.Map)} and {@code autoCommitDelay}
     * for {@link AutoSaveListener}. You can't open an already opened context.
     * You have to call {@link #close()} before open it.
     *
     * @param dbConfiguration Configuration for Topia
     * @param autoCommitDelay Delay in milliseconds for auto commit period
     */
    public void open(TopiaConfiguration dbConfiguration, long autoCommitDelay, Consumer<String> migrationCallBack) {
        Preconditions.checkState(rootContext == null);

        this.dbConfiguration = new SammoaTopiaConfiguration(dbConfiguration, migrationCallBack);

        autoSaveListener = new AutoSaveListener(this, autoCommitDelay);

        // Retrieve the rootContext and try opening a transaction to check
        // the connection
        SammoaTopiaPersistenceContext tx = null;
        try {
            rootContext = new SammoaTopiaApplicationContext(this.dbConfiguration);
            tx = rootContext.newPersistenceContext();

        } finally {
            if (tx != null) {
                closeContext(tx);
            }
        }
    }

    /**
     * Begin a new transaction (SammoaTopiaPersistenceContext). The {@link AutoSaveListener}
     * will be automatically bind on all entities change. During the transaction
     * time, no auto commit will be done, the next {@link #endTransaction(SammoaTopiaPersistenceContext)}
     * call will release the AutoSaveListener to continue looking for change.
     *
     * @return a new SammoaTopiaPersistenceContext
     * @see #endTransaction(SammoaTopiaPersistenceContext)
     * @see SammoaTopiaApplicationContext#newPersistenceContext()
     */
    public SammoaTopiaPersistenceContext beginTransaction() {
        autoSaveListener.stop();
        SammoaTopiaPersistenceContext result = openContext();
        result.getFiresSupport().addTopiaEntityListener(autoSaveListener);
        return result;
    }

    /**
     * Delay the {@code entity} creation using the {@link AutoSaveListener}
     *
     * @param entity TopiaEntity to create automatically on next auto commit
     */
    public void delayEntityCreation(TopiaEntity entity) {
        autoSaveListener.addCreatedEntity(entity);
    }

    /** Stop the auto save listener */
    public void stopAutoSaveListener() {
        autoSaveListener.stop();
    }

    /**
     * End the {@code transaction}. The {@link AutoSaveListener} will be released
     * to continue looking for entities change.
     *
     * @param transaction SammoaTopiaPersistenceContext to end
     * @see #beginTransaction()
     * @see SammoaTopiaPersistenceContext#close()
     */
    public void endTransaction(SammoaTopiaPersistenceContext transaction) {
        closeContext(transaction);
        autoSaveListener.start();
    }

    /** @return the main SammoaTopiaPersistenceContext where all other transactions are opened */
    public SammoaTopiaApplicationContext getRootContext() {
        Preconditions.checkState(rootContext != null,
                                 "You have to open the persistence to use rootContext");
        return rootContext;
    }

    /** @param rootContext the main SammoaTopiaPersistenceContext to use */
    public void setRootContext(SammoaTopiaApplicationContext rootContext) {
        this.rootContext = rootContext;
    }

    /**
     * Destroy the current db (only works for h2 db).
     *
     * @since 0.6
     */
    public void destroyDb() {
        autoSaveListener.stop();
        if (rootContext != null) {
            // destroy current db (only orks with h2)
            rootContext.dropSchema();

            // clean root context
            setRootContext(null);
        }
    }

    /**
     * Close the persistence context. This will close the main {@link SammoaTopiaPersistenceContext}
     * and the {@link AutoSaveListener}.
     *
     * @see AutoSaveListener#stop()
     */
    @Override
    public void close() {
        autoSaveListener.stop();
        if (rootContext != null) {
            rootContext.close();
            rootContext = null;
        }
    }

    public SammoaPersistenceHelper getPersistenceHelper() {
        if (persistenceHelper == null) {
            persistenceHelper = new SammoaPersistenceHelper();
        }
        return persistenceHelper;
    }

    public SammoaDbMeta getDbMetas() {
        if (dbMetas == null) {
            dbMetas = SammoaDbMeta.newDbMeta(getPersistenceHelper());
        }
        return dbMetas;
    }

    /**
     * Open a new {@link SammoaTopiaPersistenceContext}. Errors are thrown using {@link TopiaException}
     *
     * @return a new SammoaTopiaPersistenceContext
     * @see SammoaTopiaApplicationContext#newPersistenceContext()
     */
    protected SammoaTopiaPersistenceContext openContext() {
            return getRootContext().newPersistenceContext();
    }

    /**
     * Close the given {@link SammoaTopiaPersistenceContext}. Errors are thrown using {@link TopiaException}
     *
     * @param context the SammoaTopiaPersistenceContext
     * @see SammoaTopiaPersistenceContext#close()
     */
    protected void closeContext(SammoaTopiaPersistenceContext context) {
        if (!context.isClosed()) {
            context.close();
        }
    }

    public void flushTransaction(SammoaTopiaPersistenceContext tx) throws TopiaException {
        tx.getHibernateSupport().getHibernateSession().flush();
    }
}
