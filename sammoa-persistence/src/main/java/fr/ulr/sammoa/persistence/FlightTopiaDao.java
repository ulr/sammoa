/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import java.util.HashMap;

/**
 * Created: 08/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 */
public class FlightTopiaDao extends AbstractFlightTopiaDao<Flight> {

    public Integer findLastFlightNumber() {

        String ql = "SELECT max(" + Flight.PROPERTY_FLIGHT_NUMBER + ") FROM " + Flight.class.getCanonicalName() ;

        Integer result = findUniqueOrNull(ql, new HashMap<String, Object>());

//            int result = queryResult == null ? defaultValue : queryResult + 1;
//
//            if (result < defaultValue) {
//                result = defaultValue;
//            }
        return result;
    }

}
