package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import java.util.Comparator;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class Transects {

    private Transects() {
        // static class do not have instanciation
    }

    public static Function<Transect, String> toName() {
        return TO_NAME_FUNCTION;
    }

    public static Comparator<Transect> onName() {
        return ON_NAME_COMPARATOR;
    }

    public static Predicate<Transect> withStrate(Strate strate) {
        return new TransectWithStratePredicate(strate);
    }

    protected static Function<Transect, String> TO_NAME_FUNCTION =
            new Function<Transect, String>() {

                @Override
                public String apply(Transect input) {
                    return input.getName();
                }
            };

    protected static Comparator<Transect> ON_NAME_COMPARATOR =
            new Comparator<Transect>() {

                @Override
                public int compare(Transect o1, Transect o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            };

    protected static class TransectWithStratePredicate
            implements Predicate<Transect> {

        protected Strate strate;

        public TransectWithStratePredicate(Strate strate) {
            this.strate = strate;
        }

        @Override
        public boolean apply(Transect input) {
            return strate == null || strate.equals(input.getStrate());
        }
    }
}
