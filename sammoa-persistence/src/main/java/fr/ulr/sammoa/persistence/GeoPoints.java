package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import java.util.Date;
import java.util.List;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class GeoPoints {

    public static final double EMPTY_COORDINATE = 0;

    private GeoPoints() {
        // static class do not have instanciation
    }

    public static GeoPoint newEmptyGeoPoint(Date recordTime) {
        GeoPointImpl result = new GeoPointImpl(EMPTY_COORDINATE, EMPTY_COORDINATE);
        result.setRecordTime(recordTime);
        return result;
    }

    public static boolean equal(GeoPoint geoPoint1, GeoPoint geoPoint2) {

        boolean result = geoPoint1 == null && geoPoint2 == null;

        if (geoPoint1 != null && geoPoint2 != null) {

            result = Objects.equal(geoPoint1.getLatitude(), geoPoint2.getLatitude());
            result &= Objects.equal(geoPoint1.getLongitude(), geoPoint2.getLongitude());
            result &= Objects.equal(geoPoint1.getAltitude(), geoPoint2.getAltitude());
            result &= Objects.equal(geoPoint1.getSpeed(), geoPoint2.getSpeed());
        }
        return result;
    }

    public static boolean isCoordinatesEmpty(GeoPoint geoPoint) {
        return geoPoint.getLatitude() == GeoPoints.EMPTY_COORDINATE
               && geoPoint.getLongitude() == GeoPoints.EMPTY_COORDINATE;
    }

    public static boolean isOutOfDate(GeoPoint geoPoint, int maxDelay) {
        return geoPoint.getCaptureDelay() > maxDelay ;
    }

    public static boolean isEmptyOrOutOfDate(GeoPoint geoPoint, int maxDelay) {
        return isCoordinatesEmpty(geoPoint) || isOutOfDate(geoPoint, maxDelay);
    }


    public static Function<GeoPoint, Date> toDate() {
        return TO_DATE_FUNCTION;
    }

    public static List<GeoPoint> getClosestPoints(List<GeoPoint> geoPoints,
                                                  Iterable<Date> dates) {

        return FluentIterable.from(dates)
                .transform(toClosestGeoPoint(geoPoints))
                .toList();
    }

    public static GeoPoint getClosestPoint(List<GeoPoint> geoPoints,
                                           Date date) {

        return toClosestGeoPoint(geoPoints).apply(date);
    }

    public static Function<Date, GeoPoint> toClosestGeoPoint(List<GeoPoint> list) {
        return new ToClosestPointFunction(list);
    }

    public static Predicate<GeoPoint> withDate(Date date) {
        return new WithDatePredicate(date);
    }

    public static Predicate<GeoPoint> afterDate(Date date) {
        return new AfterDatePredicate(date);
    }

    private static Function<GeoPoint, Date> TO_DATE_FUNCTION = new Function<GeoPoint, Date>() {

        @Override
        public Date apply(GeoPoint input) {
            return input.getRecordTime();
        }
    };

    private static class WithDatePredicate implements Predicate<GeoPoint> {

        protected Date date;

        private WithDatePredicate(Date date) {
            this.date = date;
        }

        @Override
        public boolean apply(GeoPoint input) {
            return date != null && date.equals(input.getRecordTime());
        }
    }

    private static class AfterDatePredicate implements Predicate<GeoPoint> {

        protected Date date;

        private AfterDatePredicate(Date date) {
            this.date = date;
        }

        @Override
        public boolean apply(GeoPoint input) {
            return date != null && (date.equals(input.getRecordTime()) || input.getRecordTime().after(date));
        }
    }

    private static class ToClosestPointFunction implements Function<Date, GeoPoint> {

        protected Date date;

        protected List<GeoPoint> list;

        protected List<Date> dates;

        private ToClosestPointFunction(List<GeoPoint> list) {
            this.list = list;
            this.dates = FluentIterable
                    .from(list)
                    .transform(toDate())
                    .toSortedList(Ordering.natural());
        }

        @Override
        public GeoPoint apply(Date input) {

            // Get the date before
            Date beforeDate = Dates.getBeforeDate(input, dates);

            // Find the matching GeoPoint or create an empty one with the date
            GeoPoint result = Iterables.find(
                    list, withDate(beforeDate), newEmptyGeoPoint(input));

            return result;
        }
    }

}
