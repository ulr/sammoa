package fr.ulr.sammoa.persistence;

/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.Date;
import java.util.Iterator;

/**
 * Created: 31/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public final class Dates {

    private Dates() {
        // static class do not have instanciation
    }

    public static Interval toInterval(DateTime begin, DateTime end) {
        return new Interval(begin, end);
    }

    public static Interval toInterval(Date begin, Date end) {
        return toInterval(toDateTime(begin), toDateTime(end));
    }

    public static DateTime toDateTime(Date date) {
        return new DateTime(date);
    }

    public static Date getBeforeDate(Date date, Iterable<Date> source) {

        Date result = null;
        if (!Iterables.isEmpty(source)) {

            Iterator<Date> dates = Ordering.natural().sortedCopy(source).iterator();

            Date current = dates.next();

            while (current != null && !current.after(date)) {
                // Current is before or equal to date
                result = current;
                // Go to next date
                current = dates.hasNext() ? dates.next() : null;
            }
        }
        return result;
    }

    public static Date newDateWithoutMillis() {
        return DateTime.now().withMillisOfSecond(0).toDate();
    }

    public static boolean inInterval(Date date, Date begin, Date end) {
        return inInterval(date, toInterval(begin, end));
    }

    public static boolean inInterval(Date date, Interval interval) {
        return interval.contains(toDateTime(date));
    }

    public static Function<Date, DateTime> toDateTime() {
        return new Function<Date, DateTime>() {

            @Override
            public DateTime apply(Date input) {
                return toDateTime(input);
            }
        };
    }

//    public static Predicate<Date> inInterval(Interval interval) {
//        return new InIntervalPredicate(interval);
//    }
//
//    private static class InIntervalPredicate implements Predicate<Date> {
//
//        protected Interval interval;
//
//        public InIntervalPredicate(Interval interval) {
//            Preconditions.checkNotNull(interval);
//            this.interval = interval;
//        }
//
//        @Override
//        public boolean apply(Date input) {
//            return inInterval(input, interval);
//        }
//    }

}
