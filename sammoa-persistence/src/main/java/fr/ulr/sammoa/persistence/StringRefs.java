package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Usefulmethod aroun {@link StringRef}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class StringRefs {

    public static List<StringRef> toRefs(List<String> strings) {

        List<StringRef> result =
                Lists.transform(strings, TO_STRING_REF_FUNCTION);
        return result;
    }

    public static List<String> toStrings(List<StringRef> strings) {

        List<String> result =
                Lists.transform(strings, TO_STRING_FUNCTION);
        return result;
    }

    protected static Function<String, StringRef> TO_STRING_REF_FUNCTION =
            new Function<String, StringRef>() {

                @Override
                public StringRef apply(String input) {
                    return new StringRef(input);
                }
            };

    protected static Function<StringRef, String> TO_STRING_FUNCTION =
            new Function<StringRef, String>() {

                @Override
                public String apply(StringRef input) {
                    return input.getValue();
                }
            };
}
