package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import java.util.Collection;

/**
 * Created: 11/07/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public final class ObserverPositions {

    private ObserverPositions() {
        // static class do not have instanciation
    }

    public static Predicate<ObserverPosition> withOneOfObservers(Collection<Observer> observers) {
        return new ObserverPositionWithObserverPredicate(observers);
    }

    public static Predicate<ObserverPosition> withObserver(Observer observer) {
        return new ObserverPositionWithObserverPredicate(Lists.newArrayList(observer));
    }

    public static Predicate<ObserverPosition> withPosition(Position position) {
        return new ObserverPositionWithPositionPredicate(position);
    }

    protected static class ObserverPositionWithObserverPredicate
            implements Predicate<ObserverPosition> {

        protected Collection<Observer> observers;

        public ObserverPositionWithObserverPredicate(Collection<Observer> observers) {
            this.observers = observers;
        }

        @Override
        public boolean apply(ObserverPosition input) {
            return observers.contains(input.getObserver());
        }
    }

    protected static class ObserverPositionWithPositionPredicate
            implements Predicate<ObserverPosition> {

        protected Position position;

        public ObserverPositionWithPositionPredicate(Position position) {
            this.position = position;
        }

        @Override
        public boolean apply(ObserverPosition input) {
            return position.equals(input.getPosition());
        }
    }
}
