package fr.ulr.sammoa.persistence;

/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.nuiton.validator.bean.list.BeanListValidator;

/**
 * Created: 31/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public final class Validables {

    private Validables() {
        // static class do not have instanciation
    }

    public static <E extends Validable> boolean isAnyDeleted(Iterable<E> iterable) {
        return Iterables.any(iterable, isDeleted());
    }

    public static <E extends Validable> Predicate<E> isDeleted() {
        return new IsDeletedPredicate<E>();
    }

    public static <E extends Validable> boolean isValid(E bean, BeanListValidator<E> validator) {
        return bean.isValid()
               || bean.isDeleted()
               || (validator != null && validator.getContext(bean).isValid());
    }

    public static <E extends Validable> Predicate<E> isValid(BeanListValidator<E> validator) {
        return new IsValidPredicate<E>(validator);
    }

    private static class IsDeletedPredicate<E extends Validable> implements Predicate<E> {

        @Override
        public boolean apply(E input) {
            return input.isDeleted();
        }
    }

    private static class IsValidPredicate<E extends Validable> implements Predicate<E> {

        protected BeanListValidator<E> validator;

        private IsValidPredicate(BeanListValidator<E> validator) {
            this.validator = validator;
        }

        @Override
        public boolean apply(E input) {
            return isValid(input, validator);
        }
    }

}
