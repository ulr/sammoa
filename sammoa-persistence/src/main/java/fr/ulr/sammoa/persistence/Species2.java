package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Useful methods around {@link Species}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5
 */
public class Species2 {

    private Species2() {
        // no instance on util class
    }

    public static List<String> toSpeciesTypes(Collection<Species> species) {
        Set<String> types = Sets.newHashSet();
        for (Species specy : species) {
            types.add(specy.getType());
        }
        return Lists.newArrayList(types);
    }

    /**
     * Given a universe of species ({@code allSpecies}, and selected
     * species ({@code selectedSpecies}) and selected species
     * types ({@code speciesTypeSelected}), get all selected species.
     * <p/>
     * <strong>Note:</strong> if no species types are selected, then selects
     * all of them (means at the end selected all species).
     * <p/>
     * <strong>Note:</strong> if all species are selected, then returns an
     * empty list (usage of empty selected filter).
     *
     * @param allSpecies          universe of definied species
     * @param selectedSpecies     selected species
     * @param speciesTypeSelected selected species types (if empty then all are selected).
     * @return the list of selected species (empty if all species are selected).
     */
    public static List<Species> getSelectedSpecies(List<Species> allSpecies,
                                                   List<Species> selectedSpecies,
                                                   List<StringRef> speciesTypeSelected) {
        Set<Species> result = Sets.newHashSet();

        // add all selected species
        result.addAll(selectedSpecies);

        if (CollectionUtils.isEmpty(speciesTypeSelected)) {

            // no sepecies type selected : means select all types --> all species

        } else {

            // there is some selected species types

            Multimap<String, Species> speciesByType =
                    Multimaps.index(allSpecies, TO_SPECIES_TYPE);

            for (StringRef typeRef : speciesTypeSelected) {
                String type = typeRef.getValue();
                Collection<Species> speciesForType = speciesByType.get(type);
                result.addAll(speciesForType);
            }
        }

        if (result.size() == allSpecies.size()) {

            // all species selected, so use none
            result.clear();
        }
        return Lists.newArrayList(result);
    }

    protected static Function<Species, String> TO_SPECIES_TYPE = new Function<Species, String>() {
        @Override
        public String apply(Species input) {
            return input.getType();
        }
    };
}
