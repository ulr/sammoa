---
-- #%L
-- SAMMOA :: Persistence
-- %%
-- Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Rename Campaign table to Survey

-- 1 add column type
alter table STRATE add TYPE VARCHAR(255);

-- 2 init type
update STRATE set TYPE = 'C' where STRATETYPE = 0;
update STRATE set TYPE = 'N' where STRATETYPE = 1;
update STRATE set TYPE = 'P' where STRATETYPE = 2;
update STRATE set TYPE = 'O' where STRATETYPE = 3;

-- 3 creat index
alter table STRATE add constraint UK8R8ILTI4GSP02B7YBEHQOT65Q unique(SUBREGION, CODE);

-- 4 drop index
set @constraintTypeSubregion =
    select CONSTRAINT_NAME from INFORMATION_SCHEMA.CONSTRAINTS
    where TABLE_NAME = 'STRATE'
    and COLUMN_LIST = 'STRATETYPE,SUBREGION';

CREATE ALIAS execute FOR "fr.ulr.sammoa.migration.DatabaseUtils.execute";

call execute('alter table STRATE drop constraint ' || @constraintTypeSubregion);

-- 3 drop column
alter table STRATE drop column STRATETYPE;
