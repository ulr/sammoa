---
-- #%L
-- SAMMOA :: Persistence
-- %%
-- Copyright (C) 2012 - 2018 UMS 3462, Code Lutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Rename Campaign table to Survey

set referential_integrity false;

-- 1 rename tables
alter table CAMPAIGN rename to SURVEY;

-- 2 change topiaID
update SURVEY set TOPIAID = replace(TOPIAID, 'fr.ulr.sammoa.persistence.Campaign_', 'fr.ulr.sammoa.persistence.Survey_');

-- 3 usage in Observer table
alter table OBSERVER alter column CAMPAIGN rename to SURVEY;
update OBSERVER set SURVEY = replace(SURVEY, 'fr.ulr.sammoa.persistence.Campaign_', 'fr.ulr.sammoa.persistence.Survey_');

-- 4 usage in Sector table
alter table SECTOR alter column CAMPAIGN rename to SURVEY;
update SECTOR set SURVEY = replace(SURVEY, 'fr.ulr.sammoa.persistence.Campaign_', 'fr.ulr.sammoa.persistence.Survey_');

-- 5 usage in Flight table
alter table FLIGHT alter column CAMPAIGN rename to SURVEY;
update FLIGHT set SURVEY = replace(SURVEY, 'fr.ulr.sammoa.persistence.Campaign_', 'fr.ulr.sammoa.persistence.Survey_');

set referential_integrity true;
