package fr.ulr.sammoa.persistence;

/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created: 10/09/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class DatesTest {

    @Test
    public void testGetBeforeDate() throws Exception {

        List<Date> dates = ImmutableList.of(
                new Date(2000),
                new Date(4000),
                new Date(3000)
        );

        // Begin case
        {
            Date date = new Date(1000);
            Date result = Dates.getBeforeDate(date, dates);
            Assert.assertNull(result);
        }

        // Normal case
        {
            Date date = new Date(2100);
            Date result = Dates.getBeforeDate(date, dates);
            Assert.assertEquals(2000, result.getTime());
        }

        // Equal case
        {
            Date date = new Date(3000);
            Date result = Dates.getBeforeDate(date, dates);
            Assert.assertEquals(3000, result.getTime());
        }

        // End case
        {
            Date date = new Date(4100);
            Date result = Dates.getBeforeDate(date, dates);
            Assert.assertEquals(4000, result.getTime());
        }
    }
}
