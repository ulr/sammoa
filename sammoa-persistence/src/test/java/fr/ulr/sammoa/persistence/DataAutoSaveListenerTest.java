/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ulr.sammoa.persistence;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Date;

/**
 * Created: 06/06/12
 *
 * @author fdesbois <desbois@codelutin.com>
 */
public class DataAutoSaveListenerTest {

    @Rule
    public SammoaDatabase database = new SammoaDatabase("/sammoa-test.properties", 300);

    @Test
    public void testAutoUpdate() throws Exception {

        AutoSaveListener instance = database.getPersistence().autoSaveListener;

        Observation observation = createObservation(1, new Date());

        Assert.assertTrue(instance.changedEntities.isEmpty());

        observation.setCalves("4");

        Assert.assertFalse(instance.changedEntities.isEmpty());

        Thread.sleep(500);

        observation = getObservation(observation.getTopiaId());
        Assert.assertEquals("4", observation.getCalves());

        instance.stop();
    }

    protected Observation createObservation(int number, Date time) throws TopiaException {

        Survey survey = database.createSurvey("PACOMM", "FRANCE");

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();

        FlightTopiaDao flightDAO =  transaction.getFlightDao();
        Flight flight = flightDAO.createByNaturalId("A", number, survey);

        ObservationTopiaDao observationDAO =  transaction.getObservationDao();
        Observation result = observationDAO.createByNaturalId(number, flight);
        result.setObservationTime(time);

        transaction.commit();
        database.endTransaction(transaction);

        return result;
    }

    protected Observation getObservation(String topiaId) throws TopiaException {

        SammoaTopiaPersistenceContext transaction = database.beginTransaction();

        ObservationTopiaDao observationDAO =  transaction.getObservationDao();
        Observation result = observationDAO.forTopiaIdEquals(topiaId).findUnique();

        transaction.commit();
        database.endTransaction(transaction);

        return result;
    }

}
