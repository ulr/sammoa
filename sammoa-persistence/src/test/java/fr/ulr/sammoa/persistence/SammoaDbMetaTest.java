package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import java.util.Iterator;
import java.util.List;

/**
 * Tests the{@link SammoaDbMeta}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.6
 */
public class SammoaDbMetaTest {

    protected SammoaDbMeta dbMetas;

    @Before
    public void setUp() throws Exception {
        dbMetas = SammoaDbMeta.newDbMeta(new SammoaPersistenceHelper());
    }

    @Test
    public void testTransectFlightMeta() {
        TableMeta<SammoaEntityEnum> table =
                dbMetas.getTable(SammoaEntityEnum.TransectFlight);

        ColumnMeta column = table.getColumns("indexInFlight");
        Assert.assertNotNull(column);
    }

    @Test
    public void getReferencialTables() throws Exception {

        List<TableMeta<SammoaEntityEnum>> entries =
                dbMetas.getReferentialTables();

        Assert.assertNotNull(entries);
        Assert.assertEquals(7, entries.size());
        Iterator<TableMeta<SammoaEntityEnum>> itr = entries.iterator();
        assertTableMeta(itr.next(), SammoaEntityEnum.Region);
        assertTableMeta(itr.next(), SammoaEntityEnum.Species);
        assertTableMeta(itr.next(), SammoaEntityEnum.Survey);
        assertTableMeta(itr.next(), SammoaEntityEnum.Observer);
        assertTableMeta(itr.next(), SammoaEntityEnum.SubRegion);
        assertTableMeta(itr.next(), SammoaEntityEnum.Strate);
        assertTableMeta(itr.next(), SammoaEntityEnum.Transect);
    }

    @Test
    public void getReferencialAssociations() throws Exception {

        List<AssociationMeta<SammoaEntityEnum>> entries =
                dbMetas.getReferentialAssociations();

        Assert.assertNotNull(entries);
        Assert.assertTrue(entries.isEmpty());
    }

    @Test
    public void getDataTables() throws Exception {

        List<TableMeta<SammoaEntityEnum>> entries =
                dbMetas.getDataTables();
        Assert.assertNotNull(entries);
        Assert.assertEquals(6, entries.size());
        Iterator<TableMeta<SammoaEntityEnum>> itr = entries.iterator();
        assertTableMeta(itr.next(), SammoaEntityEnum.Flight);
        assertTableMeta(itr.next(), SammoaEntityEnum.GeoPoint);
        assertTableMeta(itr.next(), SammoaEntityEnum.ObserverPosition);
        assertTableMeta(itr.next(), SammoaEntityEnum.TransectFlight);
        assertTableMeta(itr.next(), SammoaEntityEnum.Observation);
        assertTableMeta(itr.next(), SammoaEntityEnum.Route);
    }

    @Test
    public void getDataAssociations() throws Exception {

        List<AssociationMeta<SammoaEntityEnum>> entries =
                dbMetas.getDataAssociations();
        Assert.assertNotNull(entries);
        Assert.assertEquals(4, entries.size());
        Iterator<AssociationMeta<SammoaEntityEnum>> itr = entries.iterator();
        assertAssociationMeta(itr.next(), SammoaEntityEnum.Flight, SammoaEntityEnum.Observer);
        assertAssociationMeta(itr.next(), SammoaEntityEnum.Flight, SammoaEntityEnum.TransectFlight);
        assertAssociationMeta(itr.next(), SammoaEntityEnum.TransectFlight, SammoaEntityEnum.ObserverPosition);
        assertAssociationMeta(itr.next(), SammoaEntityEnum.Route, SammoaEntityEnum.ObserverPosition);
    }

    protected void assertAssociationMeta(AssociationMeta<SammoaEntityEnum> meta,
                                         SammoaEntityEnum source,
                                         SammoaEntityEnum target) {
        Assert.assertNotNull(meta);
        Assert.assertEquals(source, meta.getSource());
        Assert.assertEquals(target, ((AssociationMeta) meta).getTarget());
    }

    protected void assertTableMeta(TableMeta<SammoaEntityEnum> meta,
                                   SammoaEntityEnum source) {
        Assert.assertNotNull(meta);
        Assert.assertEquals(source, meta.getSource());
    }
}
