package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.List;

/**
 * Created: 22/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class ObservationsTest {

    @Test
    public void testInRoute() throws Exception {

        List<Route> routes = Lists.newArrayList();
        Route route1 = new RouteImpl();
        route1.setEffortNumber(1);
        route1.setBeginTime(DateUtil.createDate(12, 54, 12, 12, 1, 2012));
        routes.add(route1);

        Route route2 = new RouteImpl();
        route2.setEffortNumber(2);
        route2.setBeginTime(DateUtil.createDate(12, 14, 13, 12, 1, 2012));
        route2.setDeleted(true);
        routes.add(route2);

        Route route3 = new RouteImpl();
        route3.setEffortNumber(3);
        route3.setBeginTime(DateUtil.createDate(12, 54, 13, 12, 1, 2012));
        routes.add(route3);

        Route route4 = new RouteImpl();
        route4.setEffortNumber(4);
        route4.setBeginTime(DateUtil.createDate(12, 55, 13, 12, 1, 2012));
        route4.setDeleted(true);
        routes.add(route4);

        Route route5 = new RouteImpl();
        route5.setEffortNumber(5);
        route5.setBeginTime(DateUtil.createDate(18, 55, 13, 12, 1, 2012));
        routes.add(route5);

        Route route6 = new RouteImpl();
        route6.setEffortNumber(6);
        route6.setBeginTime(DateUtil.createDate(33, 55, 13, 12, 1, 2012));
        route6.setDeleted(true);
        routes.add(route6);

        Route route7 = new RouteImpl();
        route7.setRouteType(RouteType.TRANSIT);
        route7.setTopiaCreateDate(new Date(1000));
        route7.setBeginTime(DateUtil.createDate(12, 54, 14, 12, 1, 2012));
        routes.add(route7);

        Route route8 = new RouteImpl();
        route8.setEffortNumber(7);
        route8.setTopiaCreateDate(new Date(2000));
        route8.setBeginTime(DateUtil.createDate(12, 54, 14, 12, 1, 2012));
        routes.add(route8);

        Route route9 = new RouteImpl();
        route9.setEffortNumber(8);
        route9.setBeginTime(DateUtil.createDate(12, 51, 16, 12, 1, 2012));
        route9.setDeleted(true);
        routes.add(route9);

        // In route1
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(1);
            observation.setObservationTime(DateUtil.createDate(30, 54, 12, 12, 1, 2012));

            Assert.assertTrue(Observations.inRoute(observation, route1, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route2, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route6, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route9, routes, false));
        }

        // In route2
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(2);
            observation.setObservationTime(DateUtil.createDate(30, 14, 13, 12, 1, 2012));

            Assert.assertFalse(Observations.inRoute(observation, route1, routes, false));
            Assert.assertTrue(Observations.inRoute(observation, route2, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route6, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route9, routes, false));
        }

        // In route1 because ignore deleted
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(3);
            observation.setObservationTime(DateUtil.createDate(30, 14, 13, 12, 1, 2012));

            Assert.assertTrue(Observations.inRoute(observation, route1, routes, true));
            Assert.assertTrue(Observations.inRoute(observation, route2, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route6, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route9, routes, true));
        }

        // In route9
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(4);
            observation.setObservationTime(DateUtil.createDate(12, 52, 16, 12, 1, 2012));

            Assert.assertFalse(Observations.inRoute(observation, route1, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route2, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route6, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, false));
            Assert.assertTrue(Observations.inRoute(observation, route9, routes, false));
        }

        // In route8 because ignore deleted
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(5);
            observation.setObservationTime(DateUtil.createDate(12, 52, 16, 12, 1, 2012));

            Assert.assertFalse(Observations.inRoute(observation, route1, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route2, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route6, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, true));
            Assert.assertTrue(Observations.inRoute(observation, route8, routes, true));
            Assert.assertTrue(Observations.inRoute(observation, route9, routes, true));
        }

        // In route6
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(6);
            observation.setObservationTime(DateUtil.createDate(33, 55, 13, 12, 1, 2012));

            Assert.assertFalse(Observations.inRoute(observation, route1, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route2, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route5, routes, false));
            Assert.assertTrue(Observations.inRoute(observation, route6, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, false));
            Assert.assertFalse(Observations.inRoute(observation, route9, routes, false));
        }

        // In route5 because ignore deleted
        {
            Observation observation = new ObservationImpl();
            observation.setObservationNumber(7);
            observation.setObservationTime(DateUtil.createDate(33, 55, 13, 12, 1, 2012));

            Assert.assertFalse(Observations.inRoute(observation, route1, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route2, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route3, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route4, routes, true));
            Assert.assertTrue(Observations.inRoute(observation, route5, routes, true));
            Assert.assertTrue(Observations.inRoute(observation, route6, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route7, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route8, routes, true));
            Assert.assertFalse(Observations.inRoute(observation, route9, routes, true));
        }

    }

    @Test
    public void testInRouteFirstDeleted() throws Exception {

        List<Route> routes = Lists.newArrayList();
        Route route1 = new RouteImpl();
        route1.setEffortNumber(1);
        route1.setDeleted(true);
        route1.setBeginTime(DateUtil.createDate(12, 54, 12, 12, 1, 2012));
        routes.add(route1);

        Route route2 = new RouteImpl();
        route2.setEffortNumber(2);
        route2.setDeleted(true);
        route2.setBeginTime(DateUtil.createDate(12, 14, 13, 12, 1, 2012));
        routes.add(route2);

        Route route3 = new RouteImpl();
        route3.setEffortNumber(3);
        route3.setBeginTime(DateUtil.createDate(12, 54, 13, 12, 1, 2012));
        routes.add(route3);

        Route route4 = new RouteImpl();
        route4.setEffortNumber(4);
        route4.setBeginTime(DateUtil.createDate(12, 55, 13, 12, 1, 2012));
        routes.add(route4);

        Observation observation = new ObservationImpl();
        observation.setObservationNumber(1);
        observation.setObservationTime(DateUtil.createDate(33, 54, 13, 12, 1, 2012));

        Assert.assertTrue(Observations.inRoute(observation, route1, routes, true));
        Assert.assertTrue(Observations.inRoute(observation, route2, routes, true));
        Assert.assertTrue(Observations.inRoute(observation, route3, routes, true));
        Assert.assertFalse(Observations.inRoute(observation, route4, routes, true));
    }
}
