package fr.ulr.sammoa.persistence;
/*
 * #%L
 * SAMMOA :: Persistence
 * %%
 * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created: 06/08/12
 *
 * @author fdesbois <florian.desbois@codelutin.com>
 */
public class GeoPointsTest {

    @Test
    public void testGetClosestPoints() throws Exception {

        List<GeoPoint> geoPoints = Lists.newArrayList();

        for (int i = 0; i < 100000; i++) {

            GeoPoint geoPoint = new GeoPointImpl();
            geoPoint.setRecordTime(new Date(i * 1000));
            geoPoints.add(geoPoint);
        }

        List<Date> dates = Lists.newArrayList(new Date(4178), new Date(5824), new Date(85140));

        List<GeoPoint> result = GeoPoints.getClosestPoints(geoPoints, dates);

        Assert.assertEquals(3, result.size());
        Assert.assertEquals(4000, result.get(0).getRecordTime().getTime());
        Assert.assertEquals(5000, result.get(1).getRecordTime().getTime());
        Assert.assertEquals(85000, result.get(2).getRecordTime().getTime());
    }

    @Test
    public void testGetClosestPoints2() throws Exception {

        List<GeoPoint> geoPoints = Lists.newArrayList();
        {
            GeoPoint geoPoint = new GeoPointImpl();
            geoPoint.setRecordTime(new Date(8000));
            geoPoints.add(geoPoint);
        }
        {
            GeoPoint geoPoint = new GeoPointImpl();
            geoPoint.setRecordTime(new Date(15000));
            geoPoints.add(geoPoint);
        }

        List<Date> dates = Lists.newArrayList(new Date(4178), new Date(8000), new Date(85140));

        List<GeoPoint> result = GeoPoints.getClosestPoints(geoPoints, dates);

        Assert.assertEquals(3, result.size());
        Assert.assertEquals(4178, result.get(0).getRecordTime().getTime());
        Assert.assertEquals(GeoPoints.EMPTY_COORDINATE, result.get(0).getLatitude(),0.01);
        Assert.assertEquals(GeoPoints.EMPTY_COORDINATE, result.get(0).getLongitude(),0.01);
        Assert.assertEquals(8000, result.get(1).getRecordTime().getTime());
        Assert.assertEquals(15000, result.get(2).getRecordTime().getTime());
    }
}
