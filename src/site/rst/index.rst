.. -
.. * #%L
.. * SAMMOA
.. * %%
.. * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

======
Sammoa
======

Présentation
------------

Bienvenue sur le site de l'application SAMMOA.

SAMMOA est un système permettant l’acquisition des données d’observations de
faune marine lors de campagnes d’observations aériennes pour l'UMS 3462 Pélagis,
anciennement baptisé CRMM et situé à l'université de La Rochelle.

Lancement de l'application
--------------------------

Pour télécharger l'application aller `ici
<http://forge.codelutin.com/projects/sammoa/files>`_ .

Après téléchargement, décompresser l'archive dans un dossier. Pour lancer l'application
il vous faut Java JRE 1.8.

Sous windows
~~~~~~~~~~~~

Lancement de l'application via sammoa.bat (ou sammoa64.bat pour la version 64bits)

1. Si ça ne marche pas, exécuter **cmd** dans le terminal, taper **java -version**.
   Si la version est inférieure à **1.8**, il faut installer une nouvelle version
   de java disponible sur `le site d'oracle
   <https://www.java.com/fr/>`_.
   Après l'installation vérifier que la version est bien maintenant celle voulu.
2. Si ce n'est toujours pas le cas, vous pouvez modifier le fichier **sammoa.bat**
   pour ajouter le chemin complet du **java.exe** de la nouvelle installation
   "c:\chemin complet de java\bin\java" (mettre le chemin entre " pour les
   espaces ou autres caractères spéciaux).

Sous linux
~~~~~~~~~~

Lancement de l'application via sammoa.sh (ou sammoa64.sh pour la version 64bits)

Premiers pas
------------

Version 0.7
~~~~~~~~~~~

1. Une fois l'application démarré, il faut créer une nouvelle campagne via le bouton "New" à côté de la
   liste déroulante des campagnes sur la page d'accueil.

2. La région "FR-METRO" pour "France Métropolitaine" existe par défaut. Vous pouvez l'éditer via le bouton "Edit"
   ou en créer une nouvelle avec le bouton "New".

3. C'est lors de l'édition d'une région que l'on peut `importer les espèces
   <import-export.html#especes-csv>`_ .

4. De retour sur l'édition d'une campagne on peut choisir les fichiers pour :

   - `Importer les strates
     <import-export.html#fichier-de-strate-dbf>`_

   - `Importer les transects
     <import-export.html#fichier-de-transect-dbf>`_

   - `Importer les observateurs
     <import-export.html#observateurs-csv>`_

5. Une fois la campagne prête, vous pouvez créer un nouveau vol depuis la page d'accueil avec le bouton "On Board".
   L'application propose un numéro de vol, mais vous pouvez le modifier si vous le souhaitez.
   L'écran principal s'affiche, vous pouvez désormais préparer le plan de vol avec les transects et les observateurs.

6. Une fois le plan de vol préparé, vous pouvez lancer le vol via le bouton START
   et commencer ainsi la saisie de l'effort et des observations.

Version 0.3
~~~~~~~~~~~

1. Une fois l'application démarré, elle vous demande de créer une campagne.

2. Une fois la campagne créée, il faut configurer les fichiers de référentiels strates et
   transects depuis le menu "File > Configuration". Attention ces fichiers doivent respecter un
   certain formalisme défini dans la rubrique `Import
   <import-export.html#fichier-de-transect-dbf>`_ . Une fois les fichiers modifiés,
   l'application est redémarrée.

3. SAMMOA nécessite un import des observateurs depuis le menu "Data > Import observers (csv)".
   Le format et le contenu du fichier est détaillé dans la section `Import/Export
   <import-export.html#Observateurs_CSV>`_

4. Vous pouvez désormais préparer le plan de vol avec les transects et les observateurs.

5. Une fois le plan de vol préparé, vous pouvez lancer le vol via le bouton START
   et commencer ainsi la saisie de l'effort et des observations.
