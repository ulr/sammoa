.. -
.. * #%L
.. * SAMMOA
.. * %%
.. * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============================
Utilisation du GPS Garmin eTrex
===============================

:Author: echatellier <eric.chatellier@codelutin.com>

Detection par le systeme
------------------------

* Connecter le GPS au port Serie
* Allumer le GPS
* Attendre que le GPS affiche "Pret à naviguer"

À ce moment sous windows, il est possible que la souris se mettent à bouger
toute seule. Il faut dans ce cas:
* configurer le GPS sur l'interface Garmin
* désinstaller le peripherique Souris détecté par Windows

Télécharger le logiciel "eTrex Legend C Software":
* le lance
* Auto detecter le GPS, cela doit afficher "Connected to serial port COM1".

Utilisation via Javagps
-----------------------
* Ajouter le jar comm.jar
* Le fichier java.com.properties
* le fichier natif win32com.dll

Pas continuer.

Utilisation via GPSylon
-----------------------

Passer le GPS en mode "NMEA"

Trace des event gpslyson
------------------------

Exemple d'event recu::

  gps data change location GPSPosition[lat: 47.21053, long:-1.4865016666666666]
  gps data change number_satellites 10
  gps data change horizontal_dop 1.3
  gps data change altitude 10.2
  gps data change position_dop 2.1
  gps data change vertical_dop 1.6
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 28], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 21], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 26.0, azimuth: 50.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 29], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 26], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 30], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 16], SatelliteInfo[PRN: 27, elev: 65.0, azimuth: 78.0, SNR: 13]]
  gps data change epe PositionError[epe=24.7, eph=11.2, epv=24.7]
  gps data change location GPSPosition[lat: 47.210523333333335, long:-1.4865016666666666]
  gps data change speed 0.0
  gps data change altitude 10.3
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 22], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 26.0, azimuth: 50.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 33], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 19], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 16], SatelliteInfo[PRN: 27, elev: 65.0, azimuth: 78.0, SNR: 13]]
  gps data change location GPSPosition[lat: 47.21051166666667, long:-1.4864983333333335]
  gps data change altitude 10.5
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 23], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 26.0, azimuth: 50.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 34], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 16], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 16], SatelliteInfo[PRN: 27, elev: 65.0, azimuth: 78.0, SNR: 13]]
  gps data change epe PositionError[epe=24.7, eph=11.1, epv=24.7]
  gps data change location GPSPosition[lat: 47.210503333333335, long:-1.486495]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 23], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 34], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 16], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 16], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change epe PositionError[epe=24.8, eph=11.1, epv=24.8]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 26], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 24], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 34], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 17], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 14], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 24], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 34], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 17], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 14], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 24], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 34], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 33], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 17], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 15], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 24], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 18], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 35], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 32], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 17], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 15], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 227.0, SNR: 25], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 23], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 19], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 22], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 25], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 27], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 17], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change location GPSPosition[lat: 47.210505, long:-1.4864966666666666]
  gps data change horizontal_dop 1.6
  gps data change position_dop 2.3
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change location GPSPosition[lat: 47.21051166666667, long:-1.4865083333333333]
  gps data change speed 3.5185184
  gps data change altitude 10.3
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 228.0, SNR: 26], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 20], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 23], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 8], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 15], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 25], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 21], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change epe PositionError[epe=24.9, eph=11.1, epv=24.9]
  gps data change location GPSPosition[lat: 47.21051833333333, long:-1.486525]
  gps data change speed 3.333333
  gps data change altitude 10.0
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 78.0, azimuth: 38.0, SNR: 0], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 228.0, SNR: 26], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 19], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 24], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 11], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 13], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 26], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 21], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]
  gps data change location GPSPosition[lat: 47.21054, long:-1.4865333333333333]
  gps data change speed 0.0
  gps data change horizontal_dop 1.2
  gps data change altitude 9.0
  gps data change position_dop 1.7
  gps data change vertical_dop 1.3
  gps data change ids_satellites [9, 12, 14, 15, 17, 18, 22, 25, 26, 27, null, null]
  gps data change satellite_info [SatelliteInfo[PRN: 9, elev: 77.0, azimuth: 40.0, SNR: 20], SatelliteInfo[PRN: 12, elev: 62.0, azimuth: 228.0, SNR: 26], SatelliteInfo[PRN: 14, elev: 18.0, azimuth: 316.0, SNR: 16], SatelliteInfo[PRN: 15, elev: 43.0, azimuth: 167.0, SNR: 24], SatelliteInfo[PRN: 17, elev: 25.0, azimuth: 49.0, SNR: 0], SatelliteInfo[PRN: 18, elev: 21.0, azimuth: 247.0, SNR: 27], SatelliteInfo[PRN: 22, elev: 21.0, azimuth: 291.0, SNR: 21], SatelliteInfo[PRN: 25, elev: 20.0, azimuth: 233.0, SNR: 24], SatelliteInfo[PRN: 26, elev: 6.0, azimuth: 145.0, SNR: 17], SatelliteInfo[PRN: 27, elev: 66.0, azimuth: 77.0, SNR: 0]]

Rxtx
----
Rxtx est la lib de communication utilisée par GPSylon:
http://www.cloudhopper.com/opensource/rxtx/

Apparement, sans le fichier "javax.comm.properties", cela ne fonctionne pas:
les librairies natives ne sont pas trouvées.

RXTX est remplacé par nrjavaserial : http://code.google.com/p/nrjavaserial/
Il n'est plus nécessaire d'embarquer les librairies natives, elles sont
incluses dans le jar de nrjavaserial.

Java web start
--------------
Pour java web start, il faut que les librairies natives sont packagées dans
un jar et ajoutées au manifest jnlp:
<nativelib href="lib/rxtx-win-xxx.jar" />

please see: How can I use Lock Files with rxtx? in INSTALL
----------------------------------------------------------
http://rxtx.qbang.org/wiki/index.php/Trouble_shooting#How_can_I_use_Lock_Files_with_rxtx.3F

Sous linux, ajouter un user au groupe "uucp":
usermod -aG uucp myuser


Comment gérer les connexions/déconnexions et les erreurs GPS
--------------------------------------------------------------

TODO non implémenté

- si aucun satellite > erreur à remonter (LED rouge) : "Pas de satellite"

- si je continue à recevoir toujours la même donnée > vérifier si le port est accessible  ::

    Enumeration portList = CommPortIdentifier.getPortIdentifiers();

    while (portList.hasMoreElements()) {
        CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
        if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
            log.trace("Accessible SERIAL port with device : " + portId.getName());
        }
    }

- si le port n'est plus accessible > erreur à remonter (LED rouge) : "pas de GPS"

- si un autre ou plusieurs port potentiels sont disponibles > popup avec le choix du port
