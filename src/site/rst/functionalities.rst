.. -
.. * #%L
.. * SAMMOA
.. * %%
.. * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============
Fonctionnalités
===============

Voici la description des fonctionalités de la version 1.0 de SAMMOA. La
description du `modèle de données
<model.html>`_ est également une aide précieuse pour connaître les différents
éléments métiers du système SAMMOA.

Schéma
======

SAMMOA est un sytème de saisi embarquée à bord d'avion dans le cadre d'
observations scientifiques sur la faune marine et les oiseaux. Le système
doit permettre :

- la manipulation du référentiel (en bleu ciel),
- la préparation et la configuration du vol (en jaune),
- la saisie et la correction de l'effort d'observation (en vert),
- la représentation sur une carte (en bleu foncé),
- l'échange de données et la configuration (en blanc)

Le système doit permettre d'afficher toutes les données scientifique d'un vol
pour effectuer en deux étapes, la saisie de l'effort d'observation et la
validation de cet effort.

.. image:: functionalities.png

Acteurs
=======

Dans le système SAMMOA, un acteur n'est pas forcément un être humain mais est
en action avec lui.

Utilisateur
-----------

L'utilisateur est soit un chef de mission, un observateur ou quelqu'un qui
s'occupe de traiter les données d'observation.

GPS
---

Le GPS permet de capturer les coordonnées géographiques pendant un vol.

Système audio
-------------

Le système audio permet d'enregistrer les conversations des observateurs pendant
le vol ou de les lires à terre pendant la phase de validation des données.

Fonctionnalités
===============
       
Configurer l'application
------------------------

L'utilisateur peut configurer les différents paramètres de l'application grâce
à l'écran *Configuration* du menu *File*.

Cet écran contient :

- La configuration générale : dossier de travail de l'application, identifiant
  du système, numéro du dernier vol enregistré, fond de carte
- La configuration du système audio. Cette configuration est accompagné d'une
  petite interface permettant d'effectuer des essais audio (écran *Audio test*
  du menu *File*)
- La configuration du GPS : port, débit, délai de capture, délai d'inactivité
- La configuration des raccourcis claviers concernant les `actions sur le vol
  <#actions-pendant-le-vol>`_ (début du vol, ajout d'un LEG, validation, ...).

Voir la rubrique `ApplicationConfig
<application-config-report.html>`_ pour la liste de tous les paramètres de
configuration. A noter que certains ne sont pas nécessairement disponible dans
l'interface.

Gérer une région
----------------

L'utilisateur doit pouvoir créer une nouvelle région et modifier une existante.

Une région peut être créée ou modifiée depuis l'écran de gestion d'une campagne.

Le dictionnaire de données défini les propriétés d'une `région
<model.html#Region>`_.

C'est depuis la gestion d'une région que l'on peut effectuer l'import du
référentiel des espèces qui lui est lié. (Voir `import des espèces
<import-export.html#especes-csv>`_)

Gérer une campagne
------------------

L'utilisateur doit pouvoir créer une nouvelle campagne aérienne et modifier une
existante.

Une campagne peut être créée ou modifiée depuis l'écran de sélection d'un vol
(page d'accueil ou *Home*).

Le dictionnaire de données défini les propriétés d'une `campagne
<model.html#Campaign>`_

C'est depuis la gestion d'une campagne que l'on peut effectuer :

- l'import du référentiel des strates : voir `import des strates
  <import-export.html#fichier-de-strate-dbf>`_)

- l'import du référentiel des transects : voir `import des transects
  <import-export.html#fichier-de-transect-dbf>`_)

- l'import du référentiel des observateurs : voir `import des observateurs
  <import-export.html#observateurs-csv>`_)

Gérer un vol
------------

L'utilisateur doit pouvoir créer un nouveau vol ou ouvrir un existant soit
en mode *embarqué*, soit en mode *validation*.

Le dictionnaire de données défini les propriétés d'un `vol
<model.html#Flight>`_

L'utilisateur peut définir manuellement le numéro de vol ou celui-ci sera
incrémenté automatiquement en fonction du système. Le numéro de vol est
conservé dans la configuration, même si la base de données est supprimée ce
numéro sera encore disponible sur le système dans le fichier de configuration
de l'application.

Les observateurs sont associés directement au vol pour indiquer quelles sont
les personnes (y compris les pilotes) qui ont embarquées dans l'avion.

Les dates de début et de fin du vol sont saisies grâce à des `actions dédiées
<#actions-pendant-le-vol>`_ et ne pourront être modifiées que lors de la phase
de validation. Cette phase ne pourra s'enclencher qu'une fois le vol terminé.
Un message utilisateur de confirmation est également affiché lors de la fin du
vol car aucune action ne pourra être disponible si le vol est terminé.

Préparer le plan de vol
-----------------------

L'utilisateur doit pouvoir préparer le plan de vol à réaliser. Pour cela, il
doit pouvoir sélectionner les transects à effectuer dans un ordre donné. Pour
aider la sélection des transects, ces derniers peuvent être filtrer en fonction
d'une strate sélectionnée.

Pour chacun des transects, il peut modifier les positions prévus des observateurs
et configurer le numéro de passage (si celui automatique ne convient pas). le
nombre de référence de transects associés à un vol existant dans la base de
données doit également être visible.

Le plan de vol peut être modifié à tout moment (en vol ou à terre). Un transect
peut être supprimé du plan définitevement si le vol n'a pas encore démarré,
par contre, une fois le vol démarré il ne pourra que être marqué comme étant
supprimé. La phase de validation permettra de supprimer définitevement les
transects du plan de vol marqués comme supprimés. Ces derniers ne peuvent pas
faire l'objet de nouveaux parcours *LEG* pendant le vol.

Voir le dictionnaire de données concernant un `transect dans le plan de vol
<model.html#TransectFlight>`_.

Créer un transect
-----------------

L'utilisateur doit pouvoir créer un nouveau transect qui n'existe pas dans
le fichier SHP importé depuis la campagne. Cette création s'effectuera depuis
le plan de vol et s'ajoutera au référentiel avec un indicateur visuel pour
indiquer qu'aucune donnée géographique n'est disponible pour ce transect.

Le dictionnaire de données défini les propriétés d'un `transect
<model.html#Transect>`_.

Pour aider la saisie du nom du transect, ce dernier est calculé en fonction
de la strate sélectionnée et du dernier numéro de transect disponible pour cette
strate. Le nom pourra également être modifié manuellement lors de la création.

Créer les parcours pendant le vol
---------------------------------

L'utilisateur doit pouvoir segmenter le vol en fonction du plan de vol, de l'
effort d'observation et des conditions d'observation. Pour cela, des actions
sont mises à disposition en fonction de l'état du vol (en attente **WAITING**,
en dehors de l'effort **OFF_EFFORT**, pendant l'effort **ON_EFFORT**,
terminé **ENDED**).

Toutes ces actions sont effectués lors de la saisie embarquée d'un vol. Chaque
action enregistre un point GPS à l'heure et la seconde près où l'action a été
effectuée. Le point GPS peut avoir un décallage par rapport à la capture réelle.

Voir le dictionnaire de données concernant les `données GPS
<model.html#GeoPoint>`_.

Actions pendant le vol
~~~~~~~~~~~~~~~~~~~~~~

+--------------------+----------------------------------+--------------+----------------------------+
| Nom                | Description                      | Raccourci    | Activation                 |
|                    |                                  | par défaut   |                            |
+====================+==================================+==============+============================+
| START              | Démarrage du vol (décollage).    | ctrl + B     | Si le vol n'a pas démarré  |
|                    | Cette action crée le premier     |              | **FlightState.WAITING**    |
|                    | parcours de type **TRANSIT**.    |              |                            |
|                    | L'état du vol passe              |              |                            |
|                    | **OFF_EFFORT**. Le GPS           |              |                            |
|                    | enregistre.                      |              |                            |
+--------------------+----------------------------------+--------------+----------------------------+
| STOP               | Arrêt du vol (atterissage).      | ctrl + E     | Si le vol a démarré et     |
|                    | L'état du vol passe **ENDED**.   |              | aucun effort est en cours  |
|                    | Le GPS s'arrête.                 |              | **FlightState.OFF_EFFORT** |
+--------------------+----------------------------------+--------------+----------------------------+
| BEGIN              | Démarrage de l'effort d'         | F5           | Si le prochain transect    |
|                    | observation sur le prochain      |              | est sélectionné et le vol  |
|                    | transect dans le plan de vol.    |              | n'est pas en effort        |
|                    | Cette action crée un parcours    |              | **FlightState.OFF_EFFORT** |
|                    | de type **LEG** et l'état du vol |              |                            |
|                    | passe **ON_EFFORT**. Le système  |              |                            |
|                    | audio enregistre.                |              |                            |
+--------------------+----------------------------------+--------------+----------------------------+
| ADD                | Changement des conditions d'     | F7           | Si le vol a démarré et     |
|                    | observation. Cette action crée   |              | l'effort est en cours      |
|                    | un parcours de type **LEG**      |              | **FlightState.ON_EFFORT**  |
+--------------------+----------------------------------+--------------+----------------------------+
| END                | Arrêt de l'effort en cours.      | F9           | Si le vol a démarré et     |
|                    | Cette action crée un parcours    |              | l'effort est en cours      |
|                    | de type **TRANSIT** et l'état du |              | **FlightState.ON_EFFORT**  |
|                    | vol passe **OFF_EFFORT**.        |              |                            |
|                    | Le système audio s'arrête.       |              |                            |
+--------------------+----------------------------------+--------------+----------------------------+
| NEXT               | Passage au prochain transect sur | F10          | Si le vol a démarré et     |
|                    | le plan de vol. Effectue un      |              | l'effort est en cours      |
|                    | *END* puis un *BEGIN*            |              | **FlightState.ON_EFFORT**  |
+--------------------+----------------------------------+--------------+----------------------------+
| CIRCLE_BACK        | Demi-tour de l'avion pour        | F11          | Si le vol a démarré et     |
|                    | préciser une observation.        |              | l'effort est en cours      |
|                    | Cette action crée un parcours de |              | **FlightState.ON_EFFORT**  |
|                    | type **CIRCLE_BACK** sur l'      |              |                            |
|                    | observation sélectionnée. L'état |              |                            |
|                    | du vol passe **OFF_EFFORT** mais |              |                            |
|                    | le système audio ne s'arrête pas |              |                            |
|                    | Note: un *BEGIN* après un        |              |                            |
|                    | demi-tour garde le transect      |              |                            |
|                    | en cours avant le demi-tour.     |              |                            |
+--------------------+----------------------------------+--------------+----------------------------+

Changer les conditions d'observation
------------------------------------

L'utilisateur doit pouvoir changer les conditions d'observation sur chacun
des parcours. Pour aider la saisie, les conditions sont copiés à chaque
création de parcours depuis le parcours précédent. Une aide visuelle doit
permettre d'identifier qu'aucun changement de conditions d'observation n'a été
effectué d'un LEG à l'autre sur le même transect.

Le dictionnaire de données défini les propriétés des `conditions d'observation
<model.html#Route>`_.

La saisie des conditions est effectué sous la forme d'un tableau, chaque
ligne représentant un parcours et ses conditions. Les flèches du clavier doivent
permettre la navigation d'une propriété et d'une ligne à l'autre. L'édition se
fait dès la sélection de la cellule sans nécessiter l'utilisation d'une autre
touche. Les données en erreur sont visibles à l'aide d'indicateurs visuels :
jaune pour un *WARNING* et rouge pour une *ERROR*. Le niveau *WARNING* est
positionné lors de la saisie embarquée et le niveau *ERROR* est positionné
lors de la validation.

Créer des observations
----------------------

L'utilisateur doit pouvoir créer de nouvelles observations en fonction de
la position de l'observateur qui a effectué cette observation. Les positions
sont définis en fonction du type de plate-forme de l'avion. Pour le moment
seules les positions d'un avion avec une plate-forme simple sont supportées. A
chaque nouvelle observation, le GPS enregistre le point géographique correspondant
à l'heure et à la seconde près de la saisie de cette observation.

Le dictionnaire de données défini les propriétés des `observations
<model.html#Observation>`_.

La saisie des observations est effectué  sous la forme d'un tableau avec le
même principe et les même contraintes que les conditions d'observation.

L'observateur placé à la position de l'observation doit également être visible
dans le tableau. Cette position peut être modifiée à tout moment et garde la
correspondance de l'observateur depuis le parcours sur lequel a été effectué
l'observation.

Un indicateur visuel doit permettre de distinguer les observations liés au
parcours sélectionné.

Actions des observations
~~~~~~~~~~~~~~~~~~~~~~~~

Toutes les actions sont disponibles dès que le vol a démarré.

+--------------------+----------------------------------+--------------+
| Nom                | Description                      | Raccourci    |
|                    |                                  | par défaut   |
+====================+==================================+==============+
| LEFT               | Nouvelle observation à gauche    | F1           |
+--------------------+----------------------------------+--------------+
| CENTER             | Nouvelle observation au centre   | F3           |
|                    | faite par le navigateur          |              |
+--------------------+----------------------------------+--------------+
| RIGHT              | Nouvelle observation à droite    | F12          |
+--------------------+----------------------------------+--------------+

Voir les données sur une carte
------------------------------

L'utilisateur doit pouvoir voir un ensemble de données sur une carte. Il doit
pouvoir également se déplacer et zoomer sur la carte. La carte doit faire
apparaître l'échelle et les coordonnées géographiques par rapport au pointeur
de la souris.

Les données visibles sont :

- les strates : fournis lors de l'import du SHP des strates lié à la campagne
- les transects : fournis lors de l'import du SHP des transects liés à la campagne
- le parcours de l'avion : chaque capture du GPS est visible sur la carte. La
  liaison de ces points GPS forment le tracé de l'avion.
- les observations : chaque nouvelle observation est visible sur la carte. Le
  nom de l'espèce correspondant à l'observation doit être affiché à côté du
  point de l'observation.

Pour aider la préparation du vol, certaines interactions sont visibles sur la carte :

- lors de la sélection d'une strate, seul les transects de cette strate apparaissent
- lors de la sélection d'un transect, le transect apparaît d'une couleur différente
- lors de la sélection d'un transect depuis le plan de vol, le transect apparaît en gras
- lors de la saisie embarquée, seul les transects du plan de vol ou ceux sélectionnés
  dans le référentiel sont visibles

De plus, l'utilisateur doit pouvoir centrer automatiquement la carte en fonction
de la position courante de l'avion et donc du GPS.

L'ajout d'un fond de carte est également possible depuis l'écran de configuration.

Valider un vol
--------------

Une fois le vol terminé, il est désormais possible pour l'utilisateur de le
corriger et de le valider.

L'utilisateur doit pouvoir valider différents éléments métiers :

- validation d'un transect du plan de vol (TransectFlight)
- validation d'un parcours (Route)
- validation d'une observation (Observation)

Pour valider un élément, il doit être sans erreur ou marqué comme supprimé. Les
sous-éléments doivent respecter les mêmes conditions. Par exemple un parcours
valide ses observations qui doivent être sans erreur ou marqués comme supprimés.
La validation d'un élément supprimé va le supprimer définitivement après la
confirmation de l'utilisateur.

L'écran de validation doit permettre de se replacer dans le contexte du vol
à tout moment, ainsi la sélection d'une ligne (transect, parcours ou observation)
remettra le vol dans l'état où il se trouvait au temps correspondant au parcours.
Un parcours doit toujours être sélectionné pour permettre la validation :
la sélection d'un transect va sélectionner le premier parcours du transect et la
sélection d'une observation va sélectionner le parcours sur lequel elle a été
effectuée.

Le système audio est disponible en lecture dès la sélection d'un
transect. La barre de temps de l'audio se positionnera automatiquement en
fonction de l'élément sélectionné (parcours ou observation). Si le système
audio est en cours de lecture, aucun positionnement automatique lors d'une
sélection ne sera effectué. L'heure et les secondes doivent être visibles
lors de la lecture audio.

Les `actions de la saisie embarquée
<#actions-pendant-le-vol>`_ restent disponibles pendant la validation, et
s'activent en fonction de l'état du vol sur le parcours sélectionné. Le temps
de création des nouveaux parcours ou observations est récupéré depuis la lecture
audio. Il doit également être modifiable manuellement à la seconde près.
Le point GPS du nouveau parcours ou observation est quant à lui récupéré dans la
liste de tous les points capturés pendant le  vol. Si aucun point existe pour le
nouveau temps (depuis l'audio ou saisi manuellement), un nouveau point est créé
avec une précision sur le délai de capture.

L'utilisateur doit pouvoir également trier et filtrer les tableaux des conditions
d'observations (parcours) et des observations.

Actions pendant la validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+--------------------+----------------------------------+--------------+
| Nom                | Description                      | Raccourci    |
|                    |                                  | par défaut   |
+====================+==================================+==============+
| VALID_FIGHT        | Validation de tout le vol, des   | alt + F      |
|                    | transects, des parcours et des   |              |
|                    | observations                     |              |
+--------------------+----------------------------------+--------------+
| VALID_TRANSECT     | Validation d'un transect du plan | alt + T      |
|                    | de vol, des parcours associés    |              |
|                    | et des observations              |              |
+--------------------+----------------------------------+--------------+
| VALID_ROUTE        | Validation d'un parcours et      | alt + R      |
|                    | des observations associées       |              |
+--------------------+----------------------------------+--------------+
| VALID_OBSERVATION  | Validation d'une observation     | alt + O      |
+--------------------+----------------------------------+--------------+

Exporter SHP
------------

Voir la documentation sur `l'export SHP
<import-export.html#export-shp-donnees-reelles>`_

Echanger les données entre système
----------------------------------

Voir la documentation sur `l'import/export
<import-export.html>`_
