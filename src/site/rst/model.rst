.. -
.. * #%L
.. * SAMMOA
.. * %%
.. * Copyright (C) 2012 - 2015 UMS 3462, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=================
Modèle de données
=================  

Le modèle représente tout ce qui sera saisi en vol et par la suite extrait pour
la validation et les exports.

Description
=========== 

Il comporte trois sous-ensembles :

- Le réferentiel (en bleu ciel)

- Le vol (en jaune)

- Les conditions et paramètres d'observations (en vert)

.. image:: model.png

Avant la création des vols, les référentiels doivent être renseignés. Ici on
part d'une région (Region_) auquelle sont liées les espèces (Species_) propre à
cette région. Ensuite on y associe une campagne (Campaign_), puis les observateurs
(Observer_) liés à cette campagne, et les secteurs (Sector_) géographiques liés à
cette campagne. Pour finir, on associe les strates (Strate_) géographiques liés au
secteur et les transects (Transect_) liés à chacune de ces strates.

Au début de chaque vol (Flight_), les transects prévus (TransectFlight_) sont
associés avec pour chacun d'eux, les positions des observateurs (ObserverPosition_).
Des transects peuvent être ajoutés afin d'effectuer de nouvelles observations
non prévues au départ.

Le parcours (Route_) reste l'élément central et permettra de segmenter le vol
(Flight_). Chaque parcours contient les conditions d'observation associées ou
non au transect prévu (TransectFlight_) en fonction du type de parcours
(RouteType_).

Les positions des observateurs (ObserverPosition_) peuvent également changées en
cours de vol et seront donc également associées au parcours.

Une observation (Observation_) n'est liée qu'à la position de l'observateur
(Position_) et garde également en référence le vol (Flight_) auquel elle est
associée. Pour retrouver les observations d'un parcours, on utilisera le temps
(*observationTime*), c'est à dire, qu'un parcours contiendra toutes les
observations jusqu'au temps du prochain parcours
(*route.beginTime <= observation.observationTime < nextRoute.beginTime*). Cette
modélisation a été faites pour simplifier les modifications de temps lors
de la validation. Ainsi, la suppression d'une route ou son changement de temps
ne provoquera aucune manipulation des observations vu qu'aucun lien physique
fort n'existe dans le modèle.

Le temps est aussi la donnée clé pour la liaison avec le GPS et les enregistrements
audio. Ainsi le temps au moment de l'observation (*observationTime*) est
synchronisé avec un point du GPS (GeoPoint_). Pour permettre de garder le
tracé géographique de l'avion, une liste de points GPS sera enregistré pour
chacun des parcours (Route_). Le lien n'apparaît pas car, une nouvelle fois la
liaison se fera sur le temps. D'un parcours à l'autre, il n'y aura aucune coupure,
le temps de début d'un parcours (*beginTime*) déterminera la fin du précédent, on
connaîtra ainsi l'ensemble des points GPS associés à chaque parcours.
L'enregistrement audio de chacun des transects sera quant à lui synchronisé sur le
temps de début (*beginTime*) du premier parcours (LEG) du transect. Il sera toujours
possible grâce au temps de l'observation de caler la lecture audio pour une
observation donnée.

Dictionnaire de données
=======================
       
Campaign
--------

*<<entity>>*

Campagne d'observations sur une région donnée. La campagne est formée d'une
période, de secteurs avec ses strates et transects et des observateurs.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| code                 | Code de la campagne              | String             | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| region               | Région sur laquelle est          | Region_            | naturalId      |
|                      | effectuée la campagne            |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| name                 | Nom de la campagne               | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| beginDate            | Date de début de la campagne     | Date               | required       |
|                      |                                  | (day)              |                |
+----------------------+----------------------------------+--------------------+----------------+
| endDate              | Date de fin de la campagne       | Date               | required       |
|                      |                                  | (day)              |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Flight
------

*<<entity>>*

Vol associé à une campagne. Il contient son plan de vol et plusieurs
informations sur le vol, comme les observateurs embarqués, son numéro et la
référence de l'avion.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| flightNumber         | Numéro du vol par rapport au     | String             | naturalId      |
|                      | système sur lequel il a été      |                    |                |
|                      | effectué. Ce numéro est          |                    |                |
|                      | incrémenté sur chaque système    |                    |                |
|                      | indépendamment. Il peut être     |                    |                |
|                      | saisie manuellement mais         |                    |                |
|                      | aucun retour sur un numéro       |                    |                |
|                      | antérieur n'est possible pour    |                    |                |
|                      | éviter les conflits sur d'autres |                    |                |
|                      | bases permettant de centraliser  |                    |                |
|                      | les données.                     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| systemId             | Identifiant du système sur       | String             | naturalId      |
|                      | lequel le vol est effectué       |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| campaign             | Campagne sur laquelle est        | Campaign_          | naturalId      |
|                      | effectuée le vol                 |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| immatriculation      | Immatriculation de l'avion       | String             |                |
|                      | utilisé pour le vol              |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| beginDate            | Date de début du vol. Un vol     | Date               |                |
|                      | sans date de début n'a pas       | (seconds)          |                |
|                      | encore démarré                   |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| endDate              | Date de fin du vol. Un vol sans  | Date               |                |
|                      | date de fin est en cours de      | (seconds)          |                |
|                      | saisie et ne peut pas être       |                    |                |
|                      | validé                           |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| comment              | Commentaires sur le vol          | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| platformType         | Type de plate-forme en fonction  | PlatformType_      | required       |
|                      | du type d'avion                  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| transectFlight       | Plan de vol. Liste des transects | List of            | **composite**  |
|                      | prévus et effectués sur ce vol.  | TransectFlight_    | *<<indexed>>*  |
|                      | Ils seront effectués si ils sont |                    |                |
|                      | liés à une Route_                |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| observer             | Ensemble des observateurs montés | Collection of      |                |
|                      | à bord de l'avion                | Observer_          |                |
+----------------------+----------------------------------+--------------------+----------------+

GeoPoint
--------

*<<entity>>*

Il s'agit d'un point géographique avec ses coordonnées latitude, longitude et
le temps auquel il a été enregistré.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| recordTime           | Temps auquel a été enregistré    | Date               |                |
|                      | ce point géographique            | (seconds)          |                |
+----------------------+----------------------------------+--------------------+----------------+
| longitude            | Longitude géographique           | double             |                |
+----------------------+----------------------------------+--------------------+----------------+
| latitude             | Latitude géographique            | double             |                |
+----------------------+----------------------------------+--------------------+----------------+
| altitude             | Altitude (feet) lors de la       | double             |                |
|                      | capture                          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| speed                | Vitesse (km/h) lors de la        | double             |                |
|                      | capture                          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| captureDelay         | Temps passé en secondes depuis   | int                |                |
|                      | la capture réelle. Par exemple   |                    |                |
|                      | depuis un GPS, il s'agit du      |                    |                |
|                      | temps entre la capture GPS et    |                    |                |
|                      | l'enregistrement du point        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| flight               | Vol sur lequel est enregistré    | Flight_            | required       |
|                      | le point géographique            |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Observation
-----------

*<<entity>>*

Données d'une observation effectuée pour une position d'observateur à un temps
donné.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| observationNumber    | Numéro d'observation incrémenté  | int                |                |
|                      | par rapport au vol               |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| observationTime      | Temps auquel a été faites        | Date               | required       |
|                      | l'observation                    | (seconds)          |                |
+----------------------+----------------------------------+--------------------+----------------+
| podSize              | Nombre d'individus de l'espèce   | int                | min = 1        |
|                      | observée                         |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| age                  | Tranche d'âge des individus      | String             | values = ["",  |
|                      | observées                        |                    | "J","I","A",   |
|                      |                                  |                    | "M"]           |
+----------------------+----------------------------------+--------------------+----------------+
| cue                  | Code qualifiant ce qui a         | String             | values = ["",  |
|                      | réellement permit de détecter    |                    | "U","A","2",   |
|                      | les animaux                      |                    | "3","4","5",   |
|                      |                                  |                    | "6","7","8",   |
|                      |                                  |                    | "9"]           |
+----------------------+----------------------------------+--------------------+----------------+
| behaviour            | Code se rapportant au            | String             | values = ["",  |
|                      | comportement qui a dominé durant |                    | "SW","MI",     |
|                      | l'observation                    |                    | "BR","LO",     |
|                      |                                  |                    | "FE","FA",     |
|                      |                                  |                    | "SB","OT",     |
|                      |                                  |                    | "DI"]          |
+----------------------+----------------------------------+--------------------+----------------+
| swimDir              | Direction de nage suivie par les | int                | min = 0        |
|                      | animaux. Il s'agit de l'angle de |                    | max = 360      |
|                      | gisement, 360 étant la direction |                    |                |
|                      | suivie par l'avion               |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| calves               | Nombre de jeunes estimé en       | int                | min = 0        |
|                      | fonction de la taille des        |                    |                |
|                      | adultes et du comportement       |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| photo                | Indique si des photos ont été    | boolean            |                |
|                      | prises ou non                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| comment              | Commentaires sur l'observation   | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| valid                | Indique si l'observation a été   | boolean            |                |
|                      | validée ou non                   |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| deleted              | Indique si l'observation a été   | boolean            |                |
|                      | marquée comme supprimée ou non   |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| flight               | Vol pendant lequel a été faites  | Flight_            | required       |
|                      | cette observation                |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| species              | Espèce observée                  | Species_           | required       |
+----------------------+----------------------------------+--------------------+----------------+
| observationStatus    | Status de l'observation utilisé  | ObservationStatus_ | required       |
|                      | lors d'un CIRCLE_BACK, sinon     |                    |                |
|                      | la valeur est NEW par défaut     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| position             | Position de l'observateur depuis | Position_          | required       |
|                      | laquelle a été faites            |                    |                |
|                      | l'observation                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

ObservationStatus
-----------------

*<<enumeration>>*

Status d'une observation, principalement utilisé pour différencier lors de
CIRCLE_BACK les recapture d'observations.

+----------------------+----------------------------------+
| Valeur               | Description                      |
+======================+==================================+
| NEW                  | Nouvelle observation             |
+----------------------+----------------------------------+
| CIRCLE_BACK          | Observation faisant l'objet d'un |
|                      | CIRCLE_BACK                      |
+----------------------+----------------------------------+
| NO_RECAPTURE         | Observation faisant l'objet d'un |
|                      | CIRCLE_BACK mais sans aucune     |
|                      | recapture                        |
+----------------------+----------------------------------+
| RECAPTURE            | Nouvelle observation qui est une |
|                      | recapture de celle ayant causée  |
|                      | le CIRCLE_BACK                   |
+----------------------+----------------------------------+

Observer
--------

*<<entity>>*

Observateur participant à une campagne et positionné sur le plan de vol à une
position dans l'avion. Voir `importer les observateurs
<import-export.html#bservateurs-csv>`_ .

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| initials             | Initiales de l'observateur. La   | String             | required       |
|                      | valeur est unique par campagne.  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| firstName            | Prénom de l'observateur          | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| lastName             | Nom de famille de l'observateur  | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| organization         | Nom de la société de l'          | String             |                |
|                      | observateur                      |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| email                | Adresse email de l'observateur   | String             |                |
|                      |                                  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| pilot                | Indique si cette observateur est | boolean            |                |
|                      | en fait un pilote d'avion. Un    |                    |                |
|                      | pilote ne peut pas être          |                    |                |
|                      | positionné comme un observateur  |                    |                |
|                      | (gauche, droite, centre...)      |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| campaign             | Campagne à laquelle est          | Campaign_          | required       |
|                      | rattachée l'observateur          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

ObserverPosition
----------------

*<<entity>>*

Position pour un observateur. Cette position est d'abord définit dans le plan
de vol prévisionnel puis modifiée si nécessaire sur le parcours lors de
changements de conditions d'observations. D'un point de vue technique on
manipulera deux entités différentes, une pour le plan de vol et une pour
chaque parcours.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| observer             | Observateur                      | Observer_          | required       |
+----------------------+----------------------------------+--------------------+----------------+
| position             | Position dans l'avion            | Position_          | required       |
+----------------------+----------------------------------+--------------------+----------------+

PlatformType
------------

*<<enumeration>>*

Type de plate-forme pour l'avion qui influence les positions (Position_)
possibles des observateurs dans l'avion. (Pour le moment seule la plate-forme
SIMPLE est gérée dans l'application).

+----------------------+----------------------------------+
| Valeur               | Description                      |
+======================+==================================+
| SIMPLE               | Plate-forme simple (4            |
|                      | observateurs)                    |
+----------------------+----------------------------------+
| DOUBLE               | Plate-forme double (6            |
|                      | observateurs)                    |
+----------------------+----------------------------------+

Position
--------

*<<enumeration>>*

Position possible pour un observateur.

+----------------------+----------------------------------+
| Valeur               | Description                      |
+======================+==================================+
| NAVIGATOR            | Navigateur (position centrale)   |
|                      | label = 'C'                      |
+----------------------+----------------------------------+
| FRONT_LEFT           | Avant gauche (position gauche    |
|                      | sur une plate-forme simple)      |
|                      | label = 'L'                      |
+----------------------+----------------------------------+
| FRONT_RIGHT          | Avant droit (position droite     |
|                      | sur une plate-forme simple)      |
|                      | label = 'R'                      |
+----------------------+----------------------------------+
| BACK_LEFT            | Arrière gauche (pour             |
|                      | une plate-forme double)          |
+----------------------+----------------------------------+
| BACK_RIGHT           | Arrière droit (pour              |
|                      | une plate-forme double)          |
+----------------------+----------------------------------+
| CO_NAVIGATOR         | Assistant navigateur             |
+----------------------+----------------------------------+

Region
------

*<<entity>>*

Région sur laquelle est effectuée une campagne.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| code                 | Code de la région                | String             | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| name                 | Nom de la région                 | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| latMin               | Latitude minimum de la zone      | double             |                |
|                      | géographique de la région        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| latMax               | Latitude maximum de la zone      | double             |                |
|                      | géographique de la région        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| longMin              | Longitude minimum de la zone     | double             |                |
|                      | géographique de la région        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| longMax              | Longitude maximum de la zone     | double             |                |
|                      | géographique de la région        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Route
-----

*<<entity>>*

Segment du vol représentant un parcours. Il contient les conditions
d'observations (y compris les positions d'observateurs) et permet de
différencier les types de parcours (TRANSIT, LEG, CIRCLE_BACK). Il est
enregistré à un temps donné pour permettre de retrouver ses observations et ses
coordonnées géographiques. A chaque changement de conditions d'observations, un
nouveau parcours est créé.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| beginTime            | Temps auquel a été commencé      | Date               | required       |
|                      | le parcours                      | (seconds)          |                |
+----------------------+----------------------------------+--------------------+----------------+
| effortNumber         | Numéro d'effort (pour un LEG     | Integer            |                |
|                      | uniquement) incrémenté par       |                    |                |
|                      | rapport au vol                   |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| distance             | ??                               | double             |                |
+----------------------+----------------------------------+--------------------+----------------+
| seaState             | Code pour l'état de la mer       | int                | min = 0        |
|                      | selon l'échelle de Beaufort      |                    | max = 5        |
+----------------------+----------------------------------+--------------------+----------------+
| swell                | Houle (0 pas de houle < 2 houle  | int                | min = 0        |
|                      | gênante)                         |                    | max = 2        |
+----------------------+----------------------------------+--------------------+----------------+
| turbidity            | Turbidité/transparence de l'eau  | int                | values = [0,1, |
|                      | (0 eau claire < 2 eau turbide,   |                    | 2,9]           |
|                      | 9 turbidité inconnue)            |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| cloudCover           | Couverture nuaugeuse défini par  | int                | min = 0        |
|                      | le système des octats (0 pour    |                    | max = 8        |
|                      | dégagé < 8 pour complètement     |                    |                |
|                      | couvert)                         |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| skyGlint             | Réflexion des nuages             | int                | min = 0        |
+----------------------+----------------------------------+--------------------+----------------+
| glareFrom            | Début de la zone d'éblouissement | Integer            | min = 0        |
|                      | en degré (le nez de l'avion 0°)  |                    | max = 360      |
+----------------------+----------------------------------+--------------------+----------------+
| glareTo              | Fin de la zone d'éblouissement   | Integer            | min = 0        |
|                      | en degré (le nez de l'avion 0°)  |                    | max = 360      |
+----------------------+----------------------------------+--------------------+----------------+
| glareUnder           | Eblouissement sous l'avion       | boolean            |                |
+----------------------+----------------------------------+--------------------+----------------+
| glareSeverity        | Intensité d'éblouissement (0     | int                | min = 0        |
|                      | aucun éblouissement < 3          |                    | max = 3        |
|                      | éblouissement fort)              |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| subjectiveConditions | Détectabilité. Estimation des    | String             | regex =        |
|                      | observateurs en fonction des     |                    | "$[EGMP]{2}^"  |
|                      | autres paramètres si les         |                    |                |
|                      | conditions sont bonnes ou non.   |                    |                |
|                      | Il s'agit de deux lettres, une   |                    |                |
|                      | pour le côté gauche et une pour  |                    |                |
|                      | le côté droit.                   |                    |                |
|                      | (E excellentes, G bonnes, M      |                    |                |
|                      | moyennes et P pauvres).          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| unexpectedRight      | Commentaire sur les observations | String             |                |
|                      | à droite. Généralement utilisé   |                    |                |
|                      | pour compter le nombre d'exocet  |                    |                |
|                      | ou de méduse.                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| unexpectedLeft       | Commentaire sur les observations | String             |                |
|                      | à gauche. Généralement utilisé   |                    |                |
|                      | pour compter le nombre d'exocet  |                    |                |
|                      | ou de méduse.                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| comment              | Commentaires sur le parcours     | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| valid                | Indique si le parcours a été     | boolean            |                |
|                      | validé ou non                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| deleted              | Indique si le parcours a été     | boolean            |                |
|                      | marque comme supprimé ou non     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| flight               | Vol auquel est rattaché ce       | Flight_            | required       |
|                      | parcours                         |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| routeType            | Type de parcours                 | RouteType_         | required       |
+----------------------+----------------------------------+--------------------+----------------+
| transectFlight       | Transect référence sur lequel    | TransectFlight_    |                |
|                      | est effectué l'effort d'         |                    |                |
|                      | observation pour un parcours de  |                    |                |
|                      | type LEG                         |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| circleBackCause      | Observation source pour un       | Observation_       |                |
|                      | parcours de type CIRCLE_BACK     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| observerPosition     | Ensemble des positions des       | Collection of      |                |
|                      | observateurs sur ce parcours.    | ObserverPosition_  |                |
|                      | Le premier LEG sur un transect   |                    |                |
|                      | débutera avec le prévisionnel    |                    |                |
|                      | des positions d'observateur      |                    |                |
|                      | depuis un TransectFlight_        |                    |                |
|                      | Il peut y avoir une position     |                    |                |
|                      | sans observateur. Le nombre de   |                    |                |
|                      | position est conditionné par le  |                    |                |
|                      | type de plate-forme de l'avion.  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Autres règles de validation :

- Il ne peut y avoir deux parcours à la suite avec le même type pour la même heure
- Il ne peut y avoir deux parcours à la suite avec le même type et les même conditions d'observations
  (seaState, swell, turbidity, cloudCover, skyGlint, glareFrom, glareTo, glareUnder,
  glareSeverity, subjectiveConditions, observerPosition)

RouteType
---------

*<<enumeration>>*

Type de parcours possible.

+----------------------+----------------------------------+
| Valeur               | Description                      |
+======================+==================================+
| TRANSIT              | Parcours avant et après l'       |
|                      | arrivée sur un transect          |
+----------------------+----------------------------------+
| LEG                  | A chaque changement de           |
|                      | conditions d'observations, un    |
|                      | nouveau LEG est créé pour        |
|                      | marquer l'effort d'observation.  |
|                      | Le LEG est le seul parcours      |
|                      | qui est lié au transect          |
+----------------------+----------------------------------+
| CIRCLE_BACK          | Parcours ayant pour objectif de  |
|                      | faire demi-tour sur une          |
|                      | observation afin de vérifier les |
|                      | données ou de mieux observer l'  |
|                      | animal. L'observation est        |
|                      | enregistrée (*circleBackCause*)  |
|                      | et de nouvelles observations     |
|                      | peuvent être enregistrées        |
|                      | pendant ce CIRCLE_BACK (CB). Le  |
|                      | statut de l'observation initiale |
|                      | peut changer en fonction des     |
|                      | observations effectuées pendant  |
|                      | le CIRCLE_BACK                   |
|                      | (ObservationStatus_). Sinon le   |
|                      | statut sera par défaut "NEW"     |
|                      | pour chacune des observations.   |
+----------------------+----------------------------------+

Sector
------

*<<entity>>*

Secteur géographique sur lequel l'effort est effectué. Voir `importer les strates
<import-export.html#fichier-de-strate-dbf>`_ .

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| sectorNumber         | Numéro de secteur                | int                | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| campaign             | Campagne rattachée à ce secteur  | Campaign_          | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| name                 | Nom du secteur                   | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Species
-------

*<<entity>>*

Espèce observée. Voir `importer les espèces
<import-export.html#especes-csv>`_ .

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| code                 | Code de l'espèce                 | String             | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| region               | Région rattachée à cette espèce  | Region_            | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| commonName           | Nom commun de l'espèce           | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| latinName            | Nom scientifique de l'espèce     | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| groupName            | Nom du group auquel appartient   | String             |                |
|                      | cette espèce                     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| family               | Famille de l'espèce              | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| type                 | Type d'espèce                    | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

Strate
------

*<<entity>>*

Strate géographique sur laquelle l'effort est effectué. Voir `importer les strates
<import-export.html#fichier-de-strate-dbf>`_ .

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| sector               | Secteur rattachée à cette strate | Sector_            | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| strateType           | Type de strate                   | StrateType_        | naturalId      |
+----------------------+----------------------------------+--------------------+----------------+
| code                 | Code de la strate.               | String             |                |
|                      | Utilisation du numéro de secteur |                    |                |
|                      | et du code du type de strate.    |                    |                |
|                      | Ex: sectorNumber = 1,            |                    |                |
|                      | strateTypeCode = 'C' =>          |                    |                |
|                      | code = "C1"                      |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| name                 | Nom usuel de la strate           | String             |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

StrateType
----------

*<<enumeration>>*

Type de strate possible (côtière, pente, néritique, océanique).

+----------------------+----------------------------------+
| Valeur               | Description                      |
+======================+==================================+
| COAST                | Côtière. code = 'C'              |
+----------------------+----------------------------------+
| NERITIC              | Néritique. code = 'N'            |
+----------------------+----------------------------------+
| SLOPE                | Pente. code = 'P'                |
+----------------------+----------------------------------+
| OCEANIC              | Océanique. code = 'O'            |
+----------------------+----------------------------------+

Transect
--------

*<<entity>>*

Segment géographique sur lequel doit être effectué l'effort d'observation.
Voir `importer les transects
<import-export.html#fichier-de-transect-dbf>`_ .

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| name                 | Nom du transect.                 | String             | naturalId      |
|                      | Utilisation du code de la strate |                    |                |
|                      | et d'un numéro incrémenté sur    |                    |                |
|                      | deux chiffres.   |               |                    |                |
|                      | strate. Ex: strateCode = "C1"    |                    |                |
|                      | => name = "C1/01" ou "C1/24"     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| strate               | Strate sur laquelle se situe ce  | Strate_            | naturalId      |
|                      | transect                         |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| length               | Longueur du transect en mètres   | Double             |                |
+----------------------+----------------------------------+--------------------+----------------+
| startX               | Coordonnée géographique X du     | Double             |                |
|                      | début du transect                |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| startY               | Coordonnée géographique Y du     | Double             |                |
|                      | début du transect                |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| endX                 | Coordonnée géographique X de     | Double             |                |
|                      | fin du transect                  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| endY                 | Coordonnée géographique Y de     | Double             |                |
|                      | fin de transect                  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| localCreation        | Indique si la création de        | boolean            |                |
|                      | l'entité a été faites depuis     |                    |                |
|                      | l'interface de l'application     |                    |                |
|                      | (différent d'un import)          |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| nbTimes              | Nombre de fois sur lequel a été  | int                |                |
|                      | effectué le transect. Ce nombre  |                    |                |
|                      | est importé et n'est utilisé     |                    |                |
|                      | comme base pour le prochain      |                    |                |
|                      | *crossingNumber* des             |                    |                |
|                      | TransectFlight_ que si il est    |                    |                |
|                      | supérieur à la plus grande       |                    |                |
|                      | valeur existante de              |                    |                |
|                      | *crossingNumber* pour ce         |                    |                |
|                      | transect.                        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| graphicIndex         | Index de l'objet graphique       | Integer            |                |
|                      | depuis le fichier SHP importé.   |                    |                |
|                      | Il permet de faire correspondre  |                    |                |
|                      | les méta-données avec sa         |                    |                |
|                      | représentation graphique. Il     |                    |                |
|                      | s'agit en fait de l'index de la  |                    |                |
|                      | ligne de transect dans le        |                    |                |
|                      | fichier DBF importé.             |                    |                |
|                      | Cette propriété peut être vide   |                    |                |
|                      | si aucune correspondance         |                    |                |
|                      | graphique n'est disponible dans  |                    |                |
|                      | le cas d'un nouveau transect     |                    |                |
|                      | créé depuis l'application.       |                    |                |
+----------------------+----------------------------------+--------------------+----------------+

TransectFlight
--------------

*<<entity>>* 

Segment géographique prévu (et potentiellement réalisé) pour un vol donné. 
Chaque parcours de type LEG représentant un effort d'observation est 
obligatoirement associé à un transect du plan de vol. L'ensemble de ces 
segments représente un plan de vol avec une numérotation représentant l'ordre.

+----------------------+----------------------------------+--------------------+----------------+
| Nom                  | Description                      | Type               | Contraintes    |
+======================+==================================+====================+================+
| transect             | Numéro d'observation incrémenté  | Transect_          | required       |
|                      | par rapport au vol               |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| crossingNumber       | Numéro de passage sur le         | int                |                |
|                      | transect. Incrémentée depuis     |                    |                |
|                      | le nombre de fois sur le         |                    |                |
|                      | transect (*nbTimes*) où modifié  |                    |                |
|                      | manuellement. Dans ce cas, le    |                    |                |
|                      | nombre de fois du transect est   |                    |                |
|                      | mis à jour pour le prochain      |                    |                |
|                      | incrément                        |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| valid                | Indique si le transect a été     | boolean            |                |
|                      | validé ou non                    |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| deleted              | Indique si le transect a été     | boolean            |                |
|                      | marqué comme supprimé ou non     |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
| observerPosition     | Ensemble des positions des       | Collection of      |                |
|                      | observateurs prévues pour ce     | ObserverPosition_  |                |
|                      | transect.                        |                    |                |
|                      | Il peut y avoir une position     |                    |                |
|                      | sans observateur. Le nombre de   |                    |                |
|                      | position est conditionné par le  |                    |                |
|                      | type de plate-forme de l'avion.  |                    |                |
+----------------------+----------------------------------+--------------------+----------------+
